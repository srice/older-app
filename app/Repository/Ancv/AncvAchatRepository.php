<?php
namespace App\Repository\Ancv;

use App\Model\Ancv\AncvAchat;

class AncvAchatRepository
{
    /**
     * @var AncvAchat
     */
    private $ancvAchat;

    /**
     * AncvAchatRepository constructor.
     * @param AncvAchat $ancvAchat
     */

    public function __construct(AncvAchat $ancvAchat)
    {
        $this->ancvAchat = $ancvAchat;
    }

    public function list()
    {
        return $this->ancvAchat->newQuery()
            ->get();
    }

    public function create(int $getSolutionArray, $total)
    {
        return $this->ancvAchat->newQuery()
            ->create([
                "solution_id"   => $getSolutionArray,
                "total"         => $total
            ]);
    }

}

        