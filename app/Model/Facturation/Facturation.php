<?php

namespace App\Model\Facturation;

use Illuminate\Database\Eloquent\Model;

class Facturation extends Model
{
    protected $guarded = [];

    public function tier(){
        return $this->belongsTo(FacturationTier::class, 'tiers_id');
    }

    public function lignes(){
        return $this->hasMany(FacturationLigne::class, 'factures_id');
    }

    public function reglements(){
        return $this->hasMany(FacturationReglement::class, 'factures_id');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", 'dateFacture'];
    }
}
