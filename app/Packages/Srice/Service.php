<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 29/06/2017
 * Time: 21:28
 */

namespace App\Packages\Srice;


class Service extends Api
{
    public function services(){
        $services = $this->db->table('services')->get();
        return $services;
    }

    public function service($services_id){
        $service = $this->db->table('services')->find($services_id);
        return $service;
    }

}