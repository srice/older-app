<?php

namespace App\Model\ComptaAsc\Remise;

use App\Model\GestionAsc\Billetterie\ReglementBilletSalarie;
use Illuminate\Database\Eloquent\Model;

class RemiseBanqueEspece extends Model
{
    protected $guarded = [];

    public function remises(){
        return $this->belongsTo(RemiseBanque::class);
    }

    public function reglementSalaries(){
        return $this->belongsTo(ReglementBilletSalarie::class, 'numTransaction', 'numReglementBilletSalarie');
    }


}
