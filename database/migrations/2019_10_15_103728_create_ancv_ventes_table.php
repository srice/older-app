<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAncvVentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ancv_ventes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salarie_id');
            $table->integer('solution_id');
            $table->string('total_ancv');
            $table->string('part_ce');
            $table->string('part_salarie');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ancv_ventes');
    }
}
