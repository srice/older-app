<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigAscRembSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config_asc_rembs')->insert([
            "percent"           => 0,
            "percentSalarie"    => 0,
            "percentAd"         => 0
        ]);
    }
}
