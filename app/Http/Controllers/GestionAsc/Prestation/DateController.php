<?php

namespace App\Http\Controllers\GestionAsc\Prestation;

use App\Model\GestionAsc\Prestation\PrestationDate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class DateController extends PrestationController
{
    public static function countDate($prestations_id){
        $dates = PrestationDate::where('prestations_id', $prestations_id)->count();
        return $dates;
    }

    public static function isOutdate($dateEnd){
        if($dateEnd >= Carbon::now()){
            return true;
        }else{
            return false;
        }
    }

    public function storeDate(Request $request, $prestations_id){

        $start = $request->dateStart.' '.$request->heureStart;
        $strt = strtotime($start);
        $dateStart = Carbon::createFromTimestamp($strt);
        $dateEnd = Carbon::createFromTimestamp($strt);

        $date = PrestationDate::create([
            "prestations_id"    => $prestations_id,
            "dateStart"         => $dateStart,
            "dateEnd"           => $dateEnd
        ]);

        if($date){
            Toastr::success("Séance ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout de la séance !");
            return redirect()->back();
        }
    }

    public function deleteDate($prestations_id, $seance_id){
        $del = PrestationDate::destroy($seance_id);

        if($del){
            Toastr::success("Séance supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de la séance !");
            return redirect()->back();
        }
    }
}
