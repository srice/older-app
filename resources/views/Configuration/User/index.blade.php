@extends('template')
@section('title')
    Configuration - Gestion des utilisateurs
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/footable/footable.core.css">
@stop

@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <button class="btn btn-sm btn-primary" onclick="window.location='{{ route('config.user.create') }}'"><i class="fa fa-plus-circle"></i> Nouvelle utilisateur</button>
            </div>
            <h3 class="panel-title"><i class="fa fa-users"></i> Liste des Utilisateurs</h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="listeUser">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Identité</th>
                        <th>Adresse Mail</th>
                        <th>Poste</th>
                        <th class="text-xs-center">Etat</th>
                        <th class="text-xs-center"><i class="fa fa-cog"></i></th>
                    </tr>
                    </thead>
                    <div class="form-inline p-b-15">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 text-xs-right">
                                <div class="form-group">
                                    <input id="filteringSearch" type="text" placeholder="Rechercher" class="form-control" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->poste }}</td>
                        <td class="text-xs-center">{!! \App\Http\Controllers\Configuration\User\UserOtherController::etatUser($user->etat) !!}</td>
                        <td class="text-xs-center">
                            {{ Form::model($user, ["route" => ["config.user.delete", $user->id], "method" => "DELETE"]) }}
                            <a href="{{ route('config.user.show', $user->id) }}" class="btn btn-xs btn-icon btn-default" data-toggle="tooltip" data-original-title="Voir le profil" data-placement="bottom"><i class="fa fa-eye"></i> </a>
                            <a href="{{ route('config.user.edit', $user->id) }}" class="btn btn-xs btn-icon btn-primary" data-toggle="tooltip" data-original-title="Editer le profil" data-placement="bottom"><i class="fa fa-edit"></i> </a>
                            @if($user->etat == 0)
                                <a href="{{ route('config.user.unlock', $user->id) }}" class="btn btn-xs btn-icon btn-success" data-toggle="tooltip" data-original-title="Débloquer le profil" data-placement="bottom"><i class="fa fa-unlock"></i> </a>
                            @else
                                <a href="{{ route('config.user.lock', $user->id) }}" class="btn btn-xs btn-icon bg-purple-500 white" data-toggle="tooltip" data-original-title="Bloquer le profil" data-placement="bottom"><i class="fa fa-lock"></i> </a>
                            @endif
                            <button class="btn btn-xs btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" data-title="Supprimer le profil"><i class="fa fa-trash"></i></button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="7">
                            <div class="text-xs-right">
                                <ul class="pagination"></ul>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/footable/footable.all.min.js"></script>
    <script src="/assets/custom/js/configuration/user/index.js"></script>
@stop