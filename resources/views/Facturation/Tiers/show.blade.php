@extends('template')
@section("title")
    Fiche Tier: <strong>{{ $tier->nomTier }}</strong>
@stop
@section("header_styles")
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
@stop
@section("content")
<div class="row">
    <div class="col-md-3">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title"><i class="fa fa-info-circle text-info"></i> Information</div>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="font-weight: bold; text-align: right;">Identité</td>
                            <td>{{ $tier->nomTier }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; text-align: right;">Coordonnée</td>
                            <td>
                                {{ $tier->adresseTier }}<br>
                                {{ $tier->codePostalTier }} {{ $tier->villeTier }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="panel">
            <div class="nav-tabs-horizontal" data-plugin="tabs">
                <div class="panel-heading">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" data-toggle="tab" href="#facture" aria-controls="facture" role="tab"><i class="fa fa-euro"></i> Facture</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" data-toggle="tab" href="#soldes" aria-controls="soldes" role="tab"><i class="fa fa-balance-scale"></i> Soldes</a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body tab-content p-t-20">
                    <div class="tab-pane active" id="facture" role="tabpanel">
                        <table id="listeFacture" class="table">
                            <thead>
                                <tr>
                                    <th>Date de la facture</th>
                                    <th>Numéro de la facture</th>
                                    <th>Type de Facturation</th>
                                    <th>Tiers</th>
                                    <th>Total de la facture</th>
                                    <th>Etat de la facture</th>
                                    <th><i class="fa fa-bars"></i> </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($tier->factures as $facture)
                                <tr>
                                    <td>{{ \App\Http\Controllers\OtherController::getDateFormated($facture->dateFacture, 'd/m/Y') }}</td>
                                    <td>{{ $facture->numInvoice }}</td>
                                    <td>{{ \App\Http\Controllers\Facturation\Facture\FactureOtherController::typeFacturation($facture->typeCompta) }}</td>
                                    <td>{{ $facture->tier->nomTier }}</td>
                                    <td>{{ \App\Http\Controllers\OtherController::euro($facture->totalFacture) }}</td>
                                    <td>
                                        <strong>Etat Générale:</strong> {!!  \App\Http\Controllers\Facturation\Facture\FactureOtherController::etatGenerale($facture->etatFacture) !!}<br>
                                        <strong>Sous Etat:</strong> {!! \App\Http\Controllers\Facturation\Facture\FactureOtherController::subEtat($facture->sousEtatFacture) !!}
                                    </td>
                                    <td>
                                        <a href="{{ route('facturation.facture.show', $facture->id) }}" class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" title="Voir la facture"><i class="fa fa-eye"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="soldes" role="tabpanel">
                        <div class="panel">
                            <div class="panel-heading bg-blue-300">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h3 class="panel-title" style="color: white"><i class="fa fa-balance-scale"></i> Historique du Tier</h3>
                                    </div>
                                    <div class="col-md-3">
                                        <h4 class="panel-title" style="color: white">
                                            Solde Actuel: {!! \App\Http\Controllers\Facturation\Tiers\TiersSoldeController::lastSolde($tier->id) !!}
                                        </h4>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="panel-title">
                                            <a href="{{ route('facturation.tiers.soldePrint', $tier->id) }}" class="btn btn-sm btn-default btn-icon" style="color: #000;" data-toggle="tooltip" title="Imprimer l'historique"><i class="fa fa-print"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body p-t-20">
                                <table id="listeSolde" class="table">
                                    <thead>
                                        <tr>
                                            <th>Numéro de Document</th>
                                            <th>Type de document</th>
                                            <th>Libellé Opération</th>
                                            <th>Montant</th>
                                            <th>Solde</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tier->soldes as $solde)
                                        <tr>
                                            <td>{{ $solde->numEcriture }}</td>
                                            <td>{{ \App\Http\Controllers\Facturation\Tiers\TiersSoldeController::typeDocument($solde->typeEcriture) }}</td>
                                            <td>{{ $solde->designationEcriture }}</td>
                                            <td>
                                                @if($solde->typeEcriture == 0)
                                                    {{ \App\Http\Controllers\OtherController::euro($solde->debitEcriture) }}
                                                @else
                                                    {{ \App\Http\Controllers\OtherController::euro($solde->creditEcriture) }}
                                                @endif
                                            </td>
                                            <td>
                                                {!! \App\Http\Controllers\Facturation\Tiers\TiersSoldeController::formSolde($solde->soldeEcriture) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section("footer_scripts")
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="/assets/custom/js/facturation/tiers/show.js"></script>
@stop    