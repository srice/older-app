<?php

namespace App\Http\Controllers\Beneficiaire;

use App\Model\Beneficiaire\Ad\AyantDroit;
use App\Model\Beneficiaire\Salarie\Salarie;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class AdController extends Controller
{

    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'beneficiaire';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function create($salaries_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];
        $salarie = Salarie::find($salaries_id);

        return view('Beneficiaire.Ad.create', compact('salarie', 'config'));
    }

    public function store(Request $request, $salaries_id){
        $this->validate($request, [
            "nom"       => "required",
            "prenom"    => "required"
        ]);
        if(!empty($request->dateNaissance)){
            $st = strtotime($request->dateNaissance);
            $dateNaissance = Carbon::createFromTimestamp($st);
        }else{
            $dateNaissance = null;
        }

        $str1 = str_limit($request->nom, 3, '');
        $str2 = str_limit($request->prenom, 3, '');
        $numCompte = "411".$str1.$str2;

        $ad = AyantDroit::create([
            "salaries_id"   => $request->salaries_id,
            "nom"      => $request->nom,
            "prenom"   => $request->prenom,
            "dateNaissance" => $dateNaissance,
            "numCompte"     => $numCompte
        ]);

        $cpt = ComptaPlanCompte::create([
            "numCompte"     => $numCompte,
            "numSector"     => 41,
            "nameCompte"    => $request->nom.' '.$request->prenom
        ]);

        $compte = AscCompte::create(["numCompte" => $numCompte]);

        if($ad && $cpt && $compte){
            Toastr::success("Un ayant droit à été ajouté !");
            return redirect()->route('salaries.show', $salaries_id);
        }else{
            Toastr::error("Erreur lors de la création de l\'ayant droit");
            return redirect()->back();
        }
    }

    public function edit($salaries_id, $ad_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $salarie = Salarie::find($salaries_id);
        $ad = AyantDroit::find($ad_id);

        return view('Beneficiaire.Ad.edit', compact('config', 'salarie', 'ad'));
    }

    public function update(Request $request, $salaries_id, $ad_id){
        $this->validate($request, [
            "nom"       => "required",
            "prenom"    => "required"
        ]);
        $ad = AyantDroit::find($ad_id);
        $cpt = ComptaPlanCompte::where('numCompte', $ad->numCompte)->first();
        $compte = AscCompte::where('numCompte', $ad->numCompte)->first();

        $st = strtotime($request->dateNaissance);
        $dateNaissance = Carbon::createFromTimestamp($st);

        $str1 = str_limit($request->nom, 3, '');
        $str2 = str_limit($request->prenom, 3, '');
        $numCompte = "411".$str1.$str2;


        $ad1 = $ad->update([
            "salaries_id"   => $request->salaries_id,
            "nom"      => $request->nom,
            "prenom"   => $request->prenom,
            "dateNaissance" => $dateNaissance
        ]);

        $cpt1 = $cpt->update([
            "numCompte"     => $numCompte,
            "numSector"     => 41,
            "nameCompte"    => $request->nom.' '.$request->prenom
        ]);

        $compte1 = $compte->update(["numCompte" => $numCompte]);

        if($ad1 && $cpt1){
            Toastr::success("Un ayant droit à été edité !");
            return redirect()->route('salaries.show', $salaries_id);
        }else{
            Toastr::error("Erreur lors de l\'édition de l\'ayant droit");
            return redirect()->back();
        }
    }

    public function delete($salaries_id, $ad_id){
        $ad = AyantDroit::find($ad_id);
        $cpt = ComptaPlanCompte::where('numCompte', $ad->numCompte)->first();
        $compte = AscCompte::where('numCompte', $ad->numCompte)->first();

        $ad1 = $ad->delete();
        $cpt1 = $cpt->delete();
        $compte1 = $compte->delete();

        if($ad1 && $cpt1 && $compte1){
            Toastr::success("Un ayant droit à été supprimé !");
            return redirect()->route('salaries.show', $salaries_id);
        }else{
            Toastr::error("Erreur lors de la suppression de l\'ayant droit");
            return redirect()->back();
        }
    }
}
