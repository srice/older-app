<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 04/07/2017
 * Time: 17:18
 */

namespace App\Packages\Srice;


use App\Http\Controllers\Assistance\Ticket\TicketOtherController;
use Carbon\Carbon;

class Ticket extends Api
{

    protected $contacts_id;

    public function __construct(array $auth = [])
    {
        parent::__construct($auth);
        $comite = new Comite($auth);
        $contact = $comite->firstContact();
        $this->contacts_id = $contact->id;
    }

    public function tickets(){
        $tickets = $this->db->table('tickets')->where('contacts_id', $this->contacts_id)->get();
        return $tickets;
    }

    public function ticket($tickets_id){
        $ticket = $this->db->table('tickets')->where('id', $tickets_id)->first();
        return $ticket;
    }

    public function chats($tickets_id){
        $chats = $this->db->table('ticket_chats')->where('tickets_id', $tickets_id)->get();
        return $chats;
    }

    public function chat($id){
        $chat = $this->db->table('ticket_chats')->where('id', $id)->first();
        return $chat;
    }

    public function lastChat($tickets_id){
        $chat = $this->db->table('ticket_chats')->where('tickets_id', $tickets_id)->get()->last();
        return $chat;
    }

    public function services(){
        $services = $this->db->table('services')->get();
        return $services;
    }

    public function service($services_id){
        $service = $this->db->table('services')->where('id', $services_id)->first();
        return $service;
    }

    public function departements(){
        $deps = $this->db->table('ticket_departements')->get();
        return $deps;
    }

    public function departement($departements_id){
        $dep = $this->db->table('ticket_departements')->where('id', $departements_id)->first();
        return $dep;
    }


    public function postTicket(array $post){
        $tickett = $this->db->table('tickets')->get()->last();
        if($tickett == null){
            $numTicket = 1;
        }else{
            $numTicket = $tickett->id+1;
        }
        $comite = new Comite();
        $com = $comite->firstContact();

        $postage = $this->db->table('tickets')->insert([
            "numTicket"         => TicketOtherController::genNumTicket($numTicket),
            "contacts_id"       => $com->id,
            "sujetTicket"       => $post['sujetTicket'],
            "departements_id"   => $post['departements_id'],
            "services_id"       => $post["services_id"],
            "priorities_id"     => $post["priorities_id"],
            "etatTicket"        => 2,
            "created_at"        => Carbon::now(),
            "updated_at"        => Carbon::now()
        ]);

        $ticket = $this->db->table('tickets')->get()->last();
        $chat = $this->db->table('ticket_chats')->insert([
            "tickets_id"    => $ticket->id,
            "autheur"       => $com->nom." ".$com->prenom,
            "group"         => 1,
            "message"       => $post['message'],
            "created_at"        => Carbon::now(),
            "updated_at"        => Carbon::now()
        ]);

        return $chat;
    }

    public function postReply($tickets_id, $message){
        $comite = new Comite();
        $contact = $comite->contact($this->contacts_id);

        $post = $this->db->table('ticket_chats')->insert([
            "tickets_id"    => $tickets_id,
            "autheur"       => $contact->nom.' '.$contact->prenom,
            "group"         => 1,
            "message"       => $message,
            "created_at"    => Carbon::now(),
            "updated_at"    => Carbon::now()
        ]);

        $up = $this->db->table('tickets')->where('id', $tickets_id)->update([
            "etatTicket"    => 2
        ]);

        return $post;
    }

}