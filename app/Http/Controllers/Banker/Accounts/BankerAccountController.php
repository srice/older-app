<?php

namespace App\Http\Controllers\Banker\Accounts;

use App\Packages\Bankin\Account;
use App\Packages\Bankin\Bridge;
use App\Packages\Bankin\Transaction;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankerAccountController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'banker';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index(Account $account){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $listAccounts = $account->listAccounts(session('access_token'));

        //dd($listAccounts->resources);

        return view('Banker.account.index', [
            "config"    => $config,
            "accounts"  => $listAccounts->resources
        ]);

    }

    public function show($account_id, Account $account, Transaction $transaction)
    {
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $info = $account->account($account_id);
        $transactions = $transaction->listTransactionsByAccount($account_id, null, null, session('access_token'));

        //dd($info, $transactions);

        return view('Banker.account.show', [
            "config"        => $config,
            "account"       => $info,
            "transactions"  => $transactions->resources
        ]);


    }

    public function connect(Bridge $bridge)
    {
        $redirect = $bridge->connectItem(session('access_token'));
        header('Location: '.$redirect);
    }
}
