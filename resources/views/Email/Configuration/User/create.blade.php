<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <title>@yield('title')</title>
    <style type="text/css">


        /* Resets: see reset.css for details */
        .ReadMsgBody { width: 100%; background-color: #ffffff;}
        .ExternalClass {width: 100%; background-color: #ffffff;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        html{width: 100%; }
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
        body {margin:0; padding:0;}
        table {border-spacing:0;}
        img{display:block !important;}

        table td {border-collapse:collapse;}
        .yshortcuts a {border-bottom: none !important;}


        /*

        main color = #34b793

        background color = #ececec


        */


        img{height:auto !important;}


        @media only screen and (max-width: 640px){
            body{
                width:auto!important;
            }

            table[class="container"]{
                width: 100%!important;
                padding-left: 20px!important;
                padding-right: 20px!important;
            }

            img[class="image-100-percent"]{
                width:100% !important;
                height:auto !important;
                max-width:100% !important;
            }

            img[class="small-image-100-percent"]{
                width:100% !important;
                height:auto !important;
            }

            table[class="full-width"]{
                width:100% !important;
            }

            table[class="full-width-text"]{
                width:100% !important;
                background-color:#b91925;
                padding-left:20px !important;
                padding-right:20px !important;
            }

            table[class="full-width-text2"]{
                width:100% !important;
                background-color:#f3f3f3;
                padding-left:20px !important;
                padding-right:20px !important;
            }

            table[class="col-2-3img"]{
                width:50% !important;
                margin-right: 20px !important;
            }
            table[class="col-2-3img-last"]{
                width:50% !important;
            }

            table[class="col-2"]{
                width:47% !important;
                margin-right:20px !important;
            }

            table[class="col-2-last"]{
                width:47% !important;
            }

            table[class="col-3"]{
                width:29% !important;
                margin-right:20px !important;
            }

            table[class="col-3-last"]{
                width:29% !important;
            }

            table[class="row-2"]{
                width:50% !important;
            }

            td[class="text-center"]{
                text-align: center !important;
            }

            /* start clear and remove*/
            table[class="remove"]{
                display:none !important;
            }

            td[class="remove"]{
                display:none !important;
            }
            /* end clear and remove*/

            table[class="fix-box"]{
                padding-left:20px !important;
                padding-right:20px !important;
            }
            td[class="fix-box"]{
                padding-left:20px !important;
                padding-right:20px !important;
            }

            td[class="font-resize"]{
                font-size: 18px !important;
                line-height: 22px !important;
            }


        }



        @media only screen and (max-width: 479px){
            body{
                font-size:10px !important;
            }


            table[class="container"]{
                width: 100%!important;
                padding-left: 10px!important;
                padding-right:10px!important;
            }

            table[class="container2"]{
                width: 100%!important;
                float:none !important;

            }

            img[class="image-100-percent"]{
                width:100% !important;
                height:auto !important;
                max-width:100% !important;
                min-width:124px !important;
            }
            img[class="small-image-100-percent"]{
                width:100% !important;
                height:auto !important;
                max-width:100% !important;
                min-width:124px !important;
            }

            table[class="full-width"]{
                width:100% !important;
            }

            table[class="full-width-text"]{
                width:100% !important;
                background-color:#b91925;
                padding-left:20px !important;
                padding-right:20px !important;
            }

            table[class="full-width-text2"]{
                width:100% !important;
                background-color:#f3f3f3;
                padding-left:20px !important;
                padding-right:20px !important;
            }



            table[class="col-2"]{
                width:100% !important;
                margin-right:0px !important;
            }

            table[class="col-2-last"]{
                width:100% !important;

            }

            table[class="col-3"]{
                width:100% !important;
                margin-right:0px !important;
            }

            table[class="col-3-last"]{
                width:100% !important;

            }

            table[class="row-2"]{
                width:100% !important;
            }


            table[id="col-underline"]{
                float: none !important;
                width: 100% !important;
                border-bottom: 1px solid #eee;
            }

            td[id="col-underline"]{
                float: none !important;
                width: 100% !important;
                border-bottom: 1px solid #eee;
            }

            td[class="col-underline"]{
                float: none !important;
                width: 100% !important;
                border-bottom: 1px solid #eee;
            }



            /*start text center*/
            td[class="text-center"]{
                text-align: center !important;

            }

            div[class="text-center"]{
                text-align: center !important;
            }
            /*end text center*/



            /* start  clear and remove */

            table[id="clear-padding"]{
                padding:0 !important;
            }
            td[id="clear-padding"]{
                padding:0 !important;
            }
            td[class="clear-padding"]{
                padding:0 !important;
            }
            table[class="remove-479"]{
                display:none !important;
            }
            td[class="remove-479"]{
                display:none !important;
            }
            table[class="clear-align"]{
                float:none !important;
            }
            /* end  clear and remove */

            table[class="width-small"]{
                width:100% !important;
            }

            table[class="fix-box"]{
                padding-left:0px !important;
                padding-right:0px !important;
            }
            td[class="fix-box"]{
                padding-left:0px !important;
                padding-right:0px !important;
            }
            td[class="font-resize"]{
                font-size: 14px !important;
            }

            td[class="increase-Height"]{
                height:10px !important;
            }
            td[class="increase-Height-20"]{
                height:20px !important;
            }

        }
        @media only screen and (max-width: 320px){
            table[class="width-small"]{
                width:125px !important;
            }
            img[class="image-100-percent"]{
                width:100% !important;
                height:auto !important;
                max-width:100% !important;
                min-width:124px !important;
            }

        }
    </style>
</head>
<body  style="font-size:12px;">
<table id="mainStructure" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#ececec;">
    <!--START VIEW ONLINE AND ICON SOCAIL -->
    <tr>
        <td align="center" valign="top" style="background-color: #34b793; ">
            <!-- start container 600 -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" bgcolor="#34b793" style="background-color: #34b793; ">
                <tbody>
                <tr>
                    <td valign="top">
                        <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#34b793" style="background-color: #34b793; ">
                            <!-- start space -->
                            <tbody>
                            <tr>
                                <td valign="top" height="10">
                                </td>
                            </tr>
                            <!-- end space -->
                            <tr>
                                <td valign="top">
                                    <!-- start container -->
                                    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td valign="top">
                                                <!-- start view online -->
                                                <table align="left" border="0" cellspacing="0" cellpadding="0" class="container2">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td style="font-size: 12px; line-height: 27px; font-family: Arial,Tahoma, Helvetica, sans-serif; color:#ffffff; font-weight:normal; text-align:center;">
                                                                        <span style="text-decoration: none; color: #ffffff;"><a href="#" style="text-decoration: none; color: rgb(255, 255, 255);">Voir en ligne</a></span>
                                                                    </td>
                                                                    <td style="padding-left:5px;" align="left" valign="middle">
                                                                        <img src="/assets/custom/images/email/arrow1.png" width="10" alt="arrow1" style="max-width:10px; diaplay:block;" border="0" hspace="0" vspace="0"></img>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" class="increase-Height">
                                                        </td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                                <!-- end view online -->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- end container  -->
                                </td>
                            </tr>
                            <!-- start space -->
                            <tr>
                                <td valign="top" height="10">
                                </td>
                            </tr>
                            <!-- end space -->
                            <!-- start space -->
                            <tr>
                                <td valign="top" class="increase-Height">
                                </td>
                            </tr>
                            <!-- end space -->

                            </tbody>
                        </table>
                        <!-- end container 600-->
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <!--END VIEW ONLINE AND ICON SOCAIL-->
    <!--START BORDER OF LAYOUT -->
    <tr>
        <td class="full-width" width="100%" align="center" valign="top" style="background-color: #34b793; ">
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="height:20px; background-color: #ffffff; opacity:0.5;">
                <tbody>
                <tr>
                    <td valign="top" height="20"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <!--END BORDER OF LAYOUT -->
    <!--START TOP NAVIGATION ​LAYOUT-->
    <tr>
        <td valign="top">
            <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">


                <!-- START CONTAINER NAVIGATION -->
                <tbody>
                <tr>
                    <td align="center" valign="top" class="fix-box">
                        <!-- start top navigation container -->
                        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" bgcolor="#ffffff" style="background-color:#ffffff;">
                            <tbody>
                            <tr>
                                <td valign="top">
                                    <!-- start top navigaton -->
                                    <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width">
                                        <!-- start space -->
                                        <tbody>
                                        <tr>
                                            <td valign="top" height="20">
                                            </td>
                                        </tr>
                                        <!-- end space -->
                                        <tr>
                                            <td valign="middle">
                                                <table align="left" border="0" cellspacing="0" cellpadding="0" class="container2">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <!-- LOGO DU COMITE -->
                                                            <a href="#" style="text-decoration: none;"><img src="//gestion.{{ env('APP_DOMAIN') }}/assets/custom/img/logo/{{ $idComite }}.jpg" width="137" style="max-width:137px;" alt="Logo" border="0" hspace="0" vspace="0"></img></a>
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" class="increase-Height-20">
                                                        </td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                                <!--start content nav -->
                                                <table border="0" align="right" cellpadding="0" cellspacing="0" class="container2">
                                                    <!--start call us -->
                                                    <tbody>
                                                    <tr>
                                                        <td valign="middle" align="center">
                                                            <table align="right" border="0" cellpadding="0" cellspacing="0" class="clear-align" style="height:100%;">
                                                                <tbody>
                                                                <tr>
                                                                    <td style="font-size: 13px;  line-height: 18px; color: #a3a2a2;  font-weight:normal; text-align: center; font-family:Arail,Tahoma, Helvetica, Arial, sans-serif;">

                                                                        <span style="text-decoration: none; color: #a3a2a2;">
                                                                            <a href="#" style="text-decoration: none; color: rgb(163, 162, 162);">NOUVELLE UTILISATEUR</a>
                                                                        </span>  &nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!--end call us -->

                                                    </tbody>
                                                </table>
                                                <!--end content nav -->
                                            </td>
                                        </tr>

                                        <!-- start space -->
                                        <tr>
                                            <td valign="top" height="20">
                                            </td>
                                        </tr>
                                        <!-- end space -->

                                        </tbody>
                                    </table>
                                    <!-- end top navigaton -->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- end top navigation container -->
                    </td>
                </tr>
                <!-- END CONTAINER NAVIGATION -->
                </tbody>
            </table>
        </td>
    </tr>
    <!--END TOP NAVIGATION ​LAYOUT-->
    <!--START IMAGE HEADER LAYOUT-->
    <tr>
        <td valign="top" class="fix-box">
            <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width">
                <tbody>
                <tr>
                    <td class="full-width" width="100%" align="center" valign="top">
                        <!-- start header container -->
                        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="background-color:#ffffff; ">
                            <tbody>
                            <tr>
                                <td valign="top" align="center">
                                    <a href="#" style="text-decoration: none;">
                                        <img class="image-100-percent" src="{{ env('APP_URL') }}/assets/custom/images/email/bienvenue.jpg" width="600" alt="image_header_600x300" style="max-width:600px display:block; " border="0" hspace="0" vspace="0"></img>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- end header container -->
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <!--END IMAGE HEADER LAYOUT-->
    <!-- START LAYOUT 1 -->
    <tr>
        <td align="center" valign="top" class="fix-box">
            <!-- start layout-1 container width 600px -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" bgcolor="#ffffff" style="background-color: #ffffff; border-bottom:2px solid #c7c7c7; ">
                <tbody>
                <tr>
                    <td valign="top">
                        <!-- start layout-1 container width 560px -->
                        <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">
                            <!--start space height -->
                            <tbody>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!--end space height -->
                            <!-- start text content -->
                            <tr>
                                <td valign="top">
                                    <table border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tbody>
                                        <tr>
                                            <td style="font-size: 18px; line-height: 22px; font-family: Arial,Tahoma, Helvetica, sans-serif; color:#555555; font-weight:bold; text-align:center;">
                                                <span style="color: #555555; font-weight:bold;">
                                                    <a href="#" style="text-decoration: none; color: rgb(85, 85, 85); font-weight: bold;">
                                                        Bienvenue <span style="color: #34b793; font-weight:bold;">{{ $user->name }}</span>
                                                    </a>
                                                </span>
                                            </td>
                                        </tr>
                                        <!--start space height -->
                                        <tr>
                                            <td height="15"></td>
                                        </tr>
                                        <!--end space height -->
                                        <tr>
                                            <td style="font-size: 13px; line-height: 22px; font-family:Arial,Tahoma, Helvetica, sans-serif; color:#a3a2a2; font-weight:normal; text-align:center; ">
                                                Vous avez maintenant accès au logiciel SRICE.<br>
                                                Le logiciel de gestion et de comptabilité de votre comité d'entreprise.<br>
                                                <p>Voici vos identifiant d'accès:</p>
                                                <ul style="list-style: none">
                                                    <li><strong>Adresse Mail:</strong> {{ $user->email }}</li>
                                                    <li><strong>Mot de Passe:</strong> {{ $password }}</li>
                                                </ul>
                                                <p>Nous vous conseillons de modifier ce mot de passe par l'intermédiaire de votre profil utilisateur.</p>
                                                <p>Toutes l'équipe de SRICE vous souhaitent une bonne utilisation.</p>
                                            </td>
                                        </tr>

                                        <!--start space height -->
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <!--end space height -->

                                        <tr>
                                            <td valign="top" width="auto" align="center">
                                                <!-- start button -->
                                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="auto" align="center" valign="middle" height="32" style=" background-color:#34b793;  border-radius:24px; background-clip: padding-box;font-size:13px; font-family: Arial,Tahoma, Helvetica, sans-serif; text-align:center;  color:#ffffff; font-weight: normal; padding-left:18px; padding-right:18px; ">
                                                                 <span style="color: #ffffff; font-weight: normal;">
                                                                   <a href="{{ env('APP_URL') }}" style="text-decoration: none; color: rgb(255, 255, 255); font-weight: normal;">
                                                                    Accèder au logiciel
                                                                   </a>
                                                                 </span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- end button -->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- end text content -->

                            <!--start space height -->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!--end space height -->
                            </tbody>
                        </table>
                        <!-- end layout-1 container width 560px -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end layout-1 container width 600px -->
        </td>
    </tr>
    <!-- END LAYOUT 1 -->
    <!--START FOOTER LAYOUT-->
    <tr>
        <td valign="top">
            <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
                <!-- START CONTAINER  -->
                <tbody>
                <tr>
                    <td align="center" valign="top" class="fix-box">
                        <!-- start footer container -->
                        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container">
                            <tbody>
                            <tr>
                                <td valign="top">
                                    <!-- start footer -->
                                    <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width">
                                        <!-- start space -->
                                        <tbody>
                                        <tr>
                                            <td valign="top" height="20">
                                            </td>
                                        </tr>
                                        <!-- end space -->
                                        <tr>
                                            <td valign="middle">
                                                <table align="left" border="0" cellspacing="0" cellpadding="0" class="container2">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <a href="#" style="text-decoration: none;"><img src="//gestion.{{ env('APP_DOMAIN') }}/assets/custom/img/logo/{{ $idComite }}.jpg" width="137" style="max-width:137px;" alt="Logo" border="0" hspace="0" vspace="0"></img></a>
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" class="increase-Height-20">
                                                        </td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- start space -->
                                        <tr>
                                            <td valign="top" height="20">
                                            </td>
                                        </tr>
                                        <!-- end space -->

                                        </tbody>
                                    </table>
                                    <!-- end footer -->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- end footer container -->
                    </td>
                </tr>
                <!-- END CONTAINER  -->
                </tbody>
            </table>
        </td>
    </tr>
    <!--END FOOTER ​LAYOUT-->
    <tr>
        <td align="center" valign="top" class="fix-box" style="background-color:#34b793;">
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color:#34b793;">
                <tbody>
                <tr>
                    <td valign="top">
                        <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color:#34b793;">
                            <!--start space height -->
                            <tbody>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <!--end space height -->
                            <tr>
                                <!-- start COPY RIGHT content -->
                                <td valign="top" style="font-size: 13px; line-height: 22px; font-family: Arial,Tahoma, Helvetica, sans-serif; color:#ffffff; font-weight:normal; text-align:center; ">
                                    Logiciel SRICE , all rights reserved 2017 ©
                                </td>
                                <!-- end COPY RIGHT content -->
                            </tr>
                            <!--start space height -->
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <!--end space height -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <!--  END FOOTER COPY RIGHT -->
</table>
</body>
</html>