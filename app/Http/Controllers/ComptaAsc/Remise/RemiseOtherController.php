<?php

namespace App\Http\Controllers\ComptaAsc\Remise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RemiseOtherController extends RemiseController
{
    public static function getNameTypeRemise($val){
        switch ($val){
            case 0: return 'Chèque';
            case 1: return 'Espèce';
            default: return 'Inconnue';
        }
    }

    public static function etatRemiseLabel($val){
        switch ($val){
            case 0: return "<span class='tag tag-default'><i class='fa fa-pencil'></i> Brouillon</span>";
            case 1: return "<span class='tag tag-success'><i class='fa fa-check-circle'></i> Valider</span>";
            default: return false;
        }
    }
}
