<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigBeneficiairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_beneficiaires', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('soldeActive')->default(0);
            $table->string('soldeInit')->nullable();
            $table->integer('activateur')->default(0);
            $table->integer('adAgeLimitActive')->default(0);
            $table->string('adAgeLimit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_beneficiaires');
    }
}
