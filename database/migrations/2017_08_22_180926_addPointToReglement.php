<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPointToReglement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reglement_billet_salaries', function (Blueprint $table) {
            $table->integer('point')->default(0);
        });

        Schema::table('reglement_billet_ads', function (Blueprint $table){
            $table->integer('point')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reglement_billet_salaries', function (Blueprint $table) {
            $table->removeColumn('point');
        });

        Schema::table('reglement_billet_ads', function (Blueprint $table) {
            $table->removeColumn('point');
        });
    }
}
