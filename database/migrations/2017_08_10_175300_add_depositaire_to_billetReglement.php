<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepositaireToBilletReglement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reglement_billet_salaries', function (Blueprint $table) {
            $table->string('depositaireReglement');
        });

        Schema::table('reglement_billet_ads', function (Blueprint $table){
            $table->string('depositaireReglement');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reglement_billet_salaries', function (Blueprint $table) {
            $table->removeColumn('depositaireReglement');
        });

        Schema::table('reglement_billet_ads', function (Blueprint $table) {
            $table->removeColumn('depositaireReglement');
        });
    }
}
