<?php

namespace App\Http\Controllers\ComptaAsc\Remise;

use App\Model\ComptaAsc\Remise\RemiseBanque;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class RemiseController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptaasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $remises = RemiseBanque::all();
        $typeRemises = [
            ["id" => 0, "designation" => "Chèque"],
            ["id" => 1, "designation" => "Espèce"],
        ];

        return view('ComptaAsc.Remise.index', compact('config', 'remises', 'typeRemises'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "numRemise"     => "required",
            "typeRemise"    => "required",
            "dateRemise"    => "required"
        ]);

        $date = $request->dateRemise;
        $strt = strtotime($date);
        $dateRemise = Carbon::createFromTimestamp($strt);

        $remise = RemiseBanque::create([
            "numRemise"     => $request->numRemise,
            "dateRemise"    => $dateRemise,
            "typeRemise"    => $request->typeRemise,
            "totalRemise"   => 0,
            "etatRemise"    => 0
        ]);

        if($remise){
            Toastr::success("La remise N°".$request->numRemise." à été créé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la création de la remise en banque !");
            return redirect()->back();
        }
    }

    public function delete($numRemise){
        $remise = RemiseBanque::where('numRemise', $numRemise)->first();


    }
}
