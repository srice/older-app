<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 15/08/2017
 * Time: 23:07
 */

namespace App\Packages\Bankin;


class Transaction extends Appel
{
    public function listTransactions($since, $until, $bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "since"         => $since,
            "until"         => $until,
            "limit"         => 50
        ];

        $call = $this->get('transactions?', $params, true, $bearer);

        return $call;
    }

    public function listUpdatedTransactions($since, $until, $bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "since"         => $since,
            "until"         => $until,
            "limit"         => 50
        ];

        $call = $this->get('transactions/updated?', $params, true, $bearer);

        return $call;
    }

    public function transaction($transactions_id, $bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
        ];

        $call = $this->get('transactions/'.$transactions_id.'?', $params, true, $bearer);

        return $call;
    }

    public function listTransactionsByAccount($account_id, $since, $until, $bearer)
    {
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "since"         => $since,
            "until"         => $until,
            "limit"         => 50
        ];

        $call = $this->get('accounts/'.$account_id.'/transactions?', $params, true, $bearer);

        return $call;
    }
}