<?php

namespace App\Http\Controllers\ComptaAsc\Journal\Vente;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanSector;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalVente;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class VenteController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptaasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public static function genNumVente(){
        return "JVG".date('Ym').rand(0, 999);
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $ventes = AscJournalVente::all()->load('compte');
        $sectors = ComptaPlanSector::where('numClasse', 7)->get();

        return view('ComptaAsc.Journal.Vente.index', compact('config', 'ventes', 'sectors'));
    }

    public function store(Request $request){

        $this->validate($request, [
            "dateVente"     => "required",
            "numCompte"     => "required",
            "libelleVente"  => "required|min:5",
            "montantVente"  => "required"
        ]);

        // Mise en forme de la date
        $date = $request->dateVente;
        $strt = strtotime($date);
        $dateVente = Carbon::createFromTimestamp($strt);

        //Numéro de facture
        if(empty($request->numVente)){
            $numVente = self::genNumVente();
        }else{
            $numVente = $request->numVente;
        }

        // Choix du débit ou du crédit
        if($request->montantVente >= 0){
            $debitVente = 0;
            $creditVente = $request->montantVente;
        }else{
            $debitVente = $request->montantVente;
            $creditVente = 0;
        }

        $vente = AscJournalVente::create([
            "numVente"          => $numVente,
            "dateVente"         => $dateVente,
            "numCompte"         => $request->numCompte,
            "libelleVente"      => $request->libelleVente,
            "debitVente"        => $debitVente,
            "creditVente"       => $creditVente
        ]);

        // Mise à jour des comptes
        $produit = AscCompte::where('numCompte', $request->numCompte)->first();
        $client = AscCompte::where('numCompte', '410DIV')->first();

        if($request->montantVente >= 0){
            $creditVente = $request->montantVente;
            $newCredit = $produit->credit + $creditVente;
            $produit->update(["credit" => $newCredit]);

            $debitClient = $request->montantVente;
            $newDebit = $client->debit + $debitClient;
            $client->update(["debit" => $newDebit]);
        }else{
            $debitVente = $request->montantVente;
            $newDebit = $produit->debit + $debitVente;
            $produit->update(["debit" => $newDebit]);

            $creditClient = $request->montantVente;
            $newCredit = $client->credit + $creditClient;
            $client->update(["credit" => $newCredit]);
        }

        if($produit && $client && $vente){
            Toastr::success("La facture de vente à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout de la facture de vente !");
            return redirect()->back();
        }
    }

    public function delete($numVente){
        $vente = AscJournalVente::where('numVente', $numVente)->first();
        $produit = AscCompte::where('numCompte', $vente->numCompte)->first();
        $client = AscCompte::where('numCompte', '410DIV')->first();

        if($vente->creditVente > 0){
            $creditProduit = $produit->credit;
            $newCredit = $creditProduit - $vente->creditVente;
            $produit->update(["credit" => $newCredit]);

            $debitClient = $client->debit;
            $newDebit = $debitClient - $vente->creditVente;
            $client->update(["debit" => $newDebit]);
        }else{
            $debitProduit = $produit->debit;
            $newDebit = $debitProduit - $vente->debitVente;
            $produit->update(["debit" => $newDebit]);

            $creditClient = $client->credit;
            $newCredit = $creditClient - $vente->debitVente;
            $client->update(["credit" => $newCredit]);
        }

        $vente->delete();

        if($vente && $produit && $client){
            Toastr::success("La factire de vente à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de la facture de vente !");
            return redirect()->back();
        }
    }
}
