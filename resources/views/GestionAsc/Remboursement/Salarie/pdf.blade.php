<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $name }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="/assets/custom/css/pdf.css">
    <style type="text/css">
        body{
            font-family: Arial, sans-serif;
            font-size: 15px;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
<table style="width: 100%;">
    <tr>
        <td style="width: 33%" class="comite">
            @if($infoCom->logo == 1)
                @if(env('APP_ENV') == 'dev')
                    <img src="{{ url('https://gestion.srice.dev/assets/custom/img/logo/'.$infoCom->id.'.jpg') }}" width="100" alt="...">
                @elseif(env('APP_ENV') == 'testing')
                    <img src="{{ url('https://gestion.srice.ovh/assets/custom/img/logo/'.$infoCom->id.'.jpg') }}" width="100" alt="...">
                @else
                    <img src="{{ url('https://gestion.srice.eu/assets/custom/img/logo/'.$infoCom->id.'.jpg') }}" width="100" alt="...">
                @endif
                <br>
            @endif
            {{ $infoCom->nameComite }}<br>
            {{ $infoCom->adresse }}<br>
            {{ $infoCom->codePostal }} {{ $infoCom->ville }}<br>
            <span style="font-weight: 900;">Tel:</span> {{ $infoCom->telephone }}
        </td>
        <td style="width: 33%;">&nbsp;</td>
        <td style="width: 33%; opacity: 0.5; font-weight: 400; font-size: 24px;">
            Facture de remboursement N°{{ $remb->numRembSalarie }}
        </td>
    </tr>
</table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<table style="width: 100%;">
    <tr>
        <td style="width: 50%;">
            &nbsp;
        </td>
        <td style="width: 50%; border: solid 1px #006600; padding: 15px; border-radius: 5px;">
            <span style="font-weight: 900;">Salarié:</span><br>
            {{ $remb->salarie->nom }} {{ $remb->salarie->prenom }}<br>
            {{ $remb->salarie->adresse }}<br>
            {{ $remb->salarie->codePostal }} {{ $remb->salarie->ville }}
        </td>
    </tr>
</table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<h2>Prestations</h2>
<table style="width: 100%;" cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th style="width: 50%; text-align: center; background-color: #07d5ff; padding: 15px;">Nom de la prestation</th>
        <th style="width: 25%; text-align: center; background-color: #07d5ff; padding: 15px;">Montant de la prestation</th>
        <th style="width: 25%; text-align: center; background-color: #07d5ff; padding: 15px;">Part CE</th>
    </tr>
    </thead>
    <tbody>
    @foreach($remb->prestations as $prestation)
        <tr>
            <td style="padding: 5px; border: solid 1px;">
                {{ $prestation->prestation }}
            </td>
            <td style="padding: 5px; text-align: right; border: solid 1px;">{{ \App\Http\Controllers\OtherController::euro($prestation->montant) }}</td>
            <td style="padding: 5px; text-align: right; border: solid 1px;">{{ \App\Http\Controllers\OtherController::euro($prestation->partCe) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="2" style="padding: 5px; text-align: right; border: solid 1px; background-color: #0b96e5; font-size: 18px;">Total à remboursé</td>
        <td style="padding: 5px; text-align: right; border: solid 1px; background-color: #0b96e5; font-size: 18px;">{{ \App\Http\Controllers\OtherController::euro($remb->totalRemb) }}</td>
    </tr>
    </tfoot>
</table>
<div class="page-break"></div>
<table style="width: 100%;">
    <tr>
        <td style="width: 33%" class="comite">
            @if($infoCom->logo == 1)
                @if(env('APP_ENV') == 'dev')
                    <img src="{{ url('https://gestion.srice.dev/assets/custom/img/logo/'.$infoCom->id.'.jpg') }}" width="100" alt="...">
                @elseif(env('APP_ENV') == 'testing')
                    <img src="{{ url('https://gestion.srice.ovh/assets/custom/img/logo/'.$infoCom->id.'.jpg') }}" width="100" alt="...">
                @else
                    <img src="{{ url('https://gestion.srice.eu/assets/custom/img/logo/'.$infoCom->id.'.jpg') }}" width="100" alt="...">
                @endif
                <br>
            @endif
            {{ $infoCom->nameComite }}<br>
            {{ $infoCom->adresse }}<br>
            {{ $infoCom->codePostal }} {{ $infoCom->ville }}<br>
            <span style="font-weight: 900;">Tel:</span> {{ $infoCom->telephone }}
        </td>
        <td style="width: 33%;">&nbsp;</td>
        <td style="width: 33%; opacity: 0.5; font-weight: 400; font-size: 24px;">
            Facture de remboursement N°{{ $remb->numRembSalarie }}
        </td>
    </tr>
</table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<table style="width: 100%;">
    <tr>
        <td style="width: 50%;">
            &nbsp;
        </td>
        <td style="width: 50%; border: solid 1px #006600; padding: 15px; border-radius: 5px;">
            <span style="font-weight: 900;">Salarié:</span><br>
            {{ $remb->salarie->nom }} {{ $remb->salarie->prenom }}<br>
            {{ $remb->salarie->adresse }}<br>
            {{ $remb->salarie->codePostal }} {{ $remb->salarie->ville }}
        </td>
    </tr>
</table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<h2>Règlements</h2>
<table style="width: 100%;" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th style="width: 33%; text-align: center; background-color: #07d5ff; padding: 15px;">Numéro de transaction</th>
        <th style="width: 33%; text-align: center; background-color: #07d5ff; padding: 15px;">Date du règlement</th>
        <th style="width: 33%; text-align: center; background-color: #07d5ff; padding: 15px;">Total du Règlement</th>
    </tr>
    </thead>
    <tbody>
    @foreach($remb->reglements as $reglement)
        <tr>
            <td style="padding: 5px; border: solid 1px;text-align: center;">{{ $reglement->numReglement }}</td>
            <td style="padding: 5px; border: solid 1px;text-align: center;">{{ $reglement->dateReg->format('d/m/Y') }}</td>
            <td style="padding: 5px; border: solid 1px;text-align: right;">{{ \App\Http\Controllers\OtherController::euro($reglement->montant) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>