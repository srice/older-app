<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveToAd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ayant_droits', function (Blueprint $table) {
            $table->integer('active')->default(1)->comment("0: Incatif |1: Actif");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ayant_droits', function (Blueprint $table) {
            $table->removeColumn('active');
        });
    }
}
