<?php

namespace App\Model\Fidelity;

use Illuminate\Database\Eloquent\Model;

class FidelityBareme extends Model
{
    protected $guarded = [];

    public function compteur(){
        return $this->belongsTo(FidelityCompteur::class, 'id', 'compteurs_id');
    }
}
