<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $name }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="/assets/custom/css/pdf.css">
    <style type="text/css">
        @font-face {
            font-family: Arial;
        }
        body{
            font-family: Arial, sans-serif;
            font-size: 10px;
        }
        .page-break {
            page-break-after: always;
        }
        .page-break:before{
            content: "Page " counter(page, upper-roman);
        }
    </style>
</head>
<body>
<table style="width: 100%;">
    <tr>
        <td style="width: 33%" class="comite">
            <img src="/assets/custom/images/logos/1.jpg" width="100" alt="...">
            <br>
            Comite de Test<br>
            No Adresse<br>
            010101 No Ville
        </td>
        <td style="width: 33%;">&nbsp;</td>
        <td style="width: 33%; opacity: 0.5; font-weight: 400; font-size: 24px;">
            BALANCE DES OEUVRES SOCIALES & CULTURELLES
        </td>
    </tr>
</table>
<h2 style="text-align: center;">{{ $name }}</h2>
<table style="width: 100%;" cellpadding="0" cellspacing="0">
    <tr>
        <td style="width: 47%;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2" style="width: 75%; text-align: center; background-color: #0d47a1; padding: 15px; color: white">Numéros et noms des comptes</th>
                        <th colspan="2" style="width: 25%; text-align: center; background-color: #0d47a1; padding: 15px; color: white; border: solid 1px;">Soldes</th>
                    </tr>
                    <tr>
                        <th style="text-align: center;">Débiteurs</th>
                        <th style="text-align: center;">Créditeurs</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(\App\Http\Controllers\ComptaAsc\Etat\BalanceController::arrayComptes() as $compte)
                    <tr>
                        <td style="padding-top: 5px;">{{ $compte->numCompte }} - {{ $compte->nameCompte }}</td>
                        <td style="padding-top: 5px; text-align: right; padding-right: 5px;">
                            @if(\App\Http\Controllers\ComptaAsc\Etat\BalanceController::getTotalDebit($compte->numCompte) != 0)
                                {{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Etat\BalanceController::getTotalDebit($compte->numCompte)) }}
                            @endif
                        </td>
                        <td style="padding-top: 5px; text-align: right; padding-right: 5px;">
                            @if(\App\Http\Controllers\ComptaAsc\Etat\BalanceController::getTotalCredit($compte->numCompte) != 0)
                                {{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Etat\BalanceController::getTotalCredit($compte->numCompte)) }}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td style="background-color: #0d47a1; padding-top: 5px; color: white; font-size: 15px;font-weight: bold; text-align: right; padding-right: 10px;">Totaux</td>
                        <td style="background-color: #0d47a1; padding-top: 5px; color: white; font-size: 15px;font-weight: bold; text-align: right; padding-right: 10px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Etat\BalanceController::getDebiteur()) }}</td>
                        <td style="background-color: #0d47a1; padding-top: 5px; color: white; font-size: 15px;font-weight: bold; text-align: right; padding-right: 10px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Etat\BalanceController::getCrediteur()) }}</td>
                    </tr>
                </tfoot>
            </table>
        </td>
    </tr>
</table>
</body>
</html>