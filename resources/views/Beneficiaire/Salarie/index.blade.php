@extends('template')
@section('title')
    Liste des Salariés
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/footable/footable.core.css">
    <link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
    <link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
@stop

@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <button class="btn btn-sm btn-primary" onclick="window.location='{{ route('salaries.create') }}'"><i class="md-accounts-add"></i> Nouveau salarié</button>
                <button class="btn btn-sm bg-purple-500 white" data-toggle="modal" data-target="#allInMasse"><i class="fa fa-lock"></i> Désactiver en masse des salariés</button>
                <button class="btn btn-sm bg-pink-500 white" data-toggle="modal" data-target="#allInMasseActif"><i class="fa fa-lock"></i> Activer en masse des salariés</button>
                <button class="btn btn-sm bg-amber-400" data-toggle="modal" data-target="#importCsv"><i class="fa fa-upload"></i> Importer mes salariés</button>
            </div>
            <h3 class="panel-title"><i class="md-accounts"></i> Liste des salariés</h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="listeSalarie">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Identité</th>
                        <th>Adresse</th>
                        <th>Coordonnées</th>
                        <th>Etat du salarié</th>
                        <th class="text-xs-center"><i class="fa fa-cog"></i></th>
                    </tr>
                    </thead>
                    <div class="form-inline p-b-15">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 text-xs-right">
                                <div class="form-group">
                                    <input id="filteringSearch" type="text" placeholder="Rechercher" class="form-control" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <tbody>
                    @foreach($salaries as $salary)
                        <tr>
                            <td>{{ $salary->id }}
                            @if(!empty($salary->matricule))({{ $salary->matricule }})@endif
                            </td>
                            <td>{{ $salary->nom }} {{ $salary->prenom }}</td>
                            <td>
                                {{ $salary->adresse }}<br>
                                {{ $salary->codePostal }} {{ $salary->ville }}
                            </td>
                            <td>
                                @if(!empty($salary->tel))
                                    <strong><i class="md-phone"></i>: </strong>{{ $salary->tel }}<br>
                                @endif
                                @if(!empty($salary->port))
                                    <strong><i class="md-smartphone"></i>: </strong>{{ $salary->port }}<br>
                                @endif
                                    @if(!empty($salary->email))
                                        <strong><i class="md-inbox"></i>: </strong>{{ $salary->email }}
                                    @endif
                            </td>
                            <td class="text-xs-center">
                                {!! \App\Http\Controllers\Beneficiaire\SalarieOtherController::etatSalarie($salary->active) !!}<br>
                                @if($salary->active == 1)
                                    <a href="{{ route('salaries.inactif', $salary->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-lock"></i> Désactiver le salarié</a>
                                @else
                                    <a href="{{ route('salaries.actif', $salary->id) }}" class="btn btn-xs btn-success"><i class="fa fa-unlock"></i> Activer le salarié</a>
                                @endif
                            </td>
                            <td>
                                {{ Form::model($salary, ["route" => ["salaries.delete", $salary->id], "method" => "DELETE"]) }}
                                <a href="{{ route('salaries.show', $salary->id) }}" class="btn btn-icon btn-sm btn-default"><i class="fa fa-eye"></i></a>
                                <a href="{{ route('salaries.edit', $salary->id) }}" class="btn btn-sm btn-icon btn-primary"><i class="fa fa-edit"></i> </a>
                                <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="7">
                            <div class="text-xs-right">
                                <ul class="pagination"></ul>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div id="allInMasse" class="modal fade" aria-hidden="true" aria-labelledby="exampleOptionalLarge" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times-circle text-danger"></i> </span>
                    </button>
                    <h4 class="modal-title" id="exampleOptionalLarge">Désactivation en masse de salarié</h4>
                </div>
                {{ Form::open(["route" => "Salaries.MassInactif", "class" => "form-horizontal"]) }}
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Identité</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($salarieActifs as $actif)
                            <tr>
                                <td class="text-xs-center">
                                    <input type="checkbox" name="salaries[]" value="{{ $actif->id }}">
                                </td>
                                <td>{{ $actif->nom }} {{ $actif->prenom }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Valider</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div id="allInMasseActif" class="modal fade" aria-hidden="true" aria-labelledby="exampleOptionalLarge" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times-circle text-danger"></i> </span>
                    </button>
                    <h4 class="modal-title" id="exampleOptionalLarge">Activation en masse de salarié</h4>
                </div>
                {{ Form::open(["route" => "Salaries.MassActif", "class" => "form-horizontal"]) }}
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Identité</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($salarieInactifs as $inactif)
                            <tr>
                                <td class="text-xs-center">
                                    <input type="checkbox" name="salaries[]" value="{{ $inactif->id }}">
                                </td>
                                <td>{{ $inactif->nom }} {{ $inactif->prenom }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Valider</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div id="importCsv" class="modal fade" aria-hidden="true" aria-labelledby="exampleOptionalLarge" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times-circle text-danger"></i> </span>
                    </button>
                    <h4 class="modal-title" id="exampleOptionalLarge"><i class="fa fa-upload"></i> Importer la liste de mes salariés</h4>
                </div>
                {{ Form::open(["route" => "Salaries.importCsv", "class" => "form-horizontal", "files" => true]) }}
                <div class="modal-body">
                    <div class="alert alert-info">
                        <div class="row">
                            <div class="col-md-1"><i class="fa fa-info-circle h1"></i> </div>
                            <div class="col-md-11">
                                <h1>Information</h1>
                                <p>Le service d'importation ne prend actuellement en parametre que le nom et le prénom de vos salariés.<br>Plus de champs seront pris en charge prochainement.</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('file', "Mon fichier csv", ["class" => "col-md-3"]) }}
                        <div class="col-md-6">
                            {{ Form::file('csv', null, ["class" => "form-control"]) }}
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Valider</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/footable/footable.all.min.js"></script>
    <script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/assets/custom/js/beneficiaire/salarie/index.js"></script>
@stop