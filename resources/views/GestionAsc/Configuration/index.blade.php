@extends('template')
@section('title')
    Configuration des Oeuvres Sociales
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="panel">
        <header class="panel-header">
            <h3 class="panel-title">Configuration des Prestations</h3>
        </header>
        <div class="panel-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td>Configuration de la validité des prestations<br><i>Si actif, des alertes de validité serons ajoutés.</i></td>
                    <td>
                        @if($presta->validite == 0)
                            <a href="{{ route('configPresta.validite.enable') }}" class="btn btn-icon btn-success"><i class="fa fa-power-off"></i> </a>
                        @else
                            <a href="{{ route('configPresta.validite.disable') }}" class="btn btn-icon btn-danger"><i class="fa fa-power-off"></i> </a>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Configuration du quota des billets attribués<br><i>Si actif, une limitation du nombre de ticket sera appliquer.</i></td>
                    <td>
                        @if($presta->quota == 0)
                            <a href="{{ route('configPresta.quota.enable') }}" class="btn btn-icon btn-success"><i class="fa fa-power-off"></i> </a>
                        @else
                            <a href="{{ route('configPresta.quota.disable') }}" class="btn btn-icon btn-danger"><i class="fa fa-power-off"></i> </a>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Configuration de la prise en charge des stocks<br><i>Si actif, la prise en charge des stocks activera l'achat de prestation</i></td>
                    <td>
                        @if($presta->stock == 0)
                            <a href="{{ route('configPresta.stock.enable') }}" class="btn btn-icon btn-success"><i class="fa fa-power-off"></i> </a>
                        @else
                            <a href="{{ route('configPresta.stock.disable') }}" class="btn btn-icon btn-danger"><i class="fa fa-power-off"></i> </a>
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="panel">
        <header class="panel-header">
            <h3 class="panel-title">Configuration de la billetterie</h3>
        </header>
        <div class="panel-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Gestion des Impayés<br><i>Si actif, des alertes aurons lieu si une ou plusieurs factures sont impayer.</i></td>
                        <td>
                            @if($billet->impayer == 0)
                                <a href="{{ route('configPresta.billet.impayer.enable') }}" class="btn btn-icon btn-success"><i class="fa fa-power-off"></i> </a>
                            @else
                                <a href="{{ route('configPresta.billet.impayer.disable') }}" class="btn btn-icon btn-danger"><i class="fa fa-power-off"></i> </a>
                            @endif
                        </td>
                    </tr>
                    @if($billet->impayer == 1)
                    <tr>
                        <td>Nombre de jours avant qu'une facture soit considérer comme impayer</td>
                        <td>
                            {{ Form::model($billet, ["route" => "configPresta.billet.dayOfImpayer", "method" => "PUT"]) }}
                                {{ Form::text('dayOfImpayer', null, ["class" => "form-control"]) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="panel">
        <header class="panel-header">
            <h3 class="panel-title">Configuration des remboursements</h3>
        </header>
        <div class="panel-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Prise en charge par pourcentage<br><i>Si actif, le montant de prise en charge par le comité sera calculé au prorata d'un pourcentage configurer par le comité</i></td>
                        <td>
                            @if($remb->percent == 0)
                                <a href="{{ route('configPresta.remb.percent.enable') }}" class="btn btn-icon btn-success"><i class="fa fa-power-off"></i> </a>
                            @else
                                <a href="{{ route('configPresta.remb.percent.disable') }}" class="btn btn-icon btn-danger"><i class="fa fa-power-off"></i> </a>
                            @endif
                        </td>
                    </tr>
                    @if($remb->percent == 1)
                    <tr>
                        <td>Prise en charge pour les salariés</td>
                        <td>
                            {{ Form::model($remb, ["route" => "configPresta.remb.percentSalarie", "method" => "PUT"]) }}
                            {{ Form::text('percentSalarie', null, ["class" => "form-control"]) }}
                            <button class="btn btn-xs btn-success"><i class="fa fa-check"></i> Mettre à jour</button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                    <tr>
                        <td>Prise en charge pour les ayants droit</td>
                        <td>
                            {{ Form::model($remb, ["route" => "configPresta.remb.percentAd", "method" => "PUT"]) }}
                            {{ Form::text('percentAd', null, ["class" => "form-control"]) }}
                            <button class="btn btn-xs btn-success"><i class="fa fa-check"></i> Mettre à jour</button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/custom/js/gestionasc/configuration/index.js"></script>
@stop