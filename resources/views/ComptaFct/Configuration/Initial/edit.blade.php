@extends('template')
@section("title")
    Edition du montant initial du compte: <strong>{{ $compte->numCompte }} - {{ \App\Http\Controllers\ComptaFct\Etat\CompteController::getNameCompte($compte->numCompte) }}</strong>
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield("title")</h3>
        </header>
        <div class="panel-body">
            {{ Form::model($compte, ["route" => ["comptaFct.config.initial.update", $compte->numCompte], "class" => "form-horizontal", "method" => "PUT"]) }}
            <div class="form-group row form-material">
                {{ Form::label('debit', "Solde Débiteur", ["class" => "control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('debit', null, ["class" => "form-control"]) }}
                </div>
            </div>
            <div class="form-group row form-material">
                {{ Form::label('credit', "Solde Créditeur", ["class" => "control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('credit', null, ["class" => "form-control"]) }}
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    