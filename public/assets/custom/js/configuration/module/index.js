(function ($) {
    $("#listeModule")
        .on('click', '#btnFicheProjet', function (e) {
            var btn = $(this);
            var ficheProjet = $("#ficheProject"); //modal
            var idProjet = btn.attr('data-id');
            var content = $("#modalProject");
            ficheProjet.modal('show');
            $.get('/config/module/ficheProject/'+idProjet)
                .done(function (data) {
                    content.html(data)
                })
        })
        .on('click', '#btnClefAccess', function (e) {
            var btn = $(this);
            var clefAccess = $("#clefAccess");
            var idProjet = btn.attr('data-id');
            var content = $("#modalClefAccess");
            clefAccess.modal('show');
            $.get('/config/module/clefAccess/'+idProjet)
                .done(function (data) {
                    content.html(data)
                })
        })
        .on('click', '#btnCheckout', function (e) {
            var btn = $(this);
            var clefAccess = $("#checkout");
            var idProjet = btn.attr('data-id');
            var content = $("#modalCheckout");
            clefAccess.modal('show');
            $.get('/config/module/checkout/'+idProjet)
                .done(function (data) {
                    content.html(data)
                })
        })
})(jQuery);