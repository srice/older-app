<?php

namespace App\Http\Controllers\GestionAsc\Prestation;

use App\Model\GestionAsc\Prestation\PrestationTarif;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class StockController extends PrestationController
{
    public static function etatStock($stockLimit, $stockActuel){
        if($stockActuel == 0){
            return '<span class="tag tag-danger"><i class="fa fa-times"></i> Rupture de stock</span>';
        }elseif($stockActuel > 0 && $stockActuel <= $stockLimit){
            return '<span class="tag tag-warning"><i class="fa fa-warning"></i> Limite de stock atteinte</span>';
        }else{
            return '<span class="tag tag-success"><i class="fa fa-check"></i> En stock</span>';
        }
    }

    public function editStock($prestations_id, $stocks_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $stock = PrestationTarif::find($stocks_id);

        return view('GestionAsc.prestation.editStock', compact('config', 'stock', 'prestations_id'));
    }

    public function updateStock(Request $request, $prestations_id, $stock_id){
        $stock = PrestationTarif::find($stock_id)->update([
            "quota"         => $request->quota,
            "stockLimit"    => $request->stockLimit,
            "stockActuel"   => $request->stockActuel
        ]);

        if($stock){
            Toastr::success("Le stock du tarif à été mis à jour !");
            return redirect()->route('prestations.show', $prestations_id);
        }else{
            Toastr::error("Erreur lors de la mise à jour du stock du tarif !");
            return redirect()->back();
        }
    }
}
