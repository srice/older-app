<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class OtherController extends Controller
{
    public static function getDateFormated($date, $format = null){
        $str = strtotime($date);
        $carbon = Carbon::createFromTimestamp($str);
        if($format != null){
            Carbon::setToStringFormat($format);
        }
        return $carbon;
    }

    public static function euro($value){
        return number_format($value, 2, ',', ' ')." &euro;";
    }
}
