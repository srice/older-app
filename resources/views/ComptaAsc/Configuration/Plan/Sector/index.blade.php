@extends('template')
@section("title")
    Listes des secteurs de classe: <strong>{{ $classe->nameClasse }}</strong>
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">@yield('title')</h3>
                </div>
                <div class="col-md-2 ">
                    <a href="#addSector" data-toggle="modal" class="btn btn-block btn-lg btn-primary m-t-5"><i class="fa fa-plus-circle"></i> Ajouter un secteur</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Numéro</th>
                            <th>Désignation</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sectors as $sector)
                        <tr>
                            <td>{{ $sector->numSector }}</td>
                            <td>{{ $sector->nameSector }}</td>
                            <td>
                                <a href="{{ route('comptaAsc.config.plan.compte.index', [$classe->numClasse, $sector->numSector]) }}" class="btn btn-sm btn-icon btn-default" data-toggle="tooltip" data-placement="top" data-title="Voir les comptes"><i class="fa fa-eye"></i> </a>
                                <a href="{{ route('comptaAsc.config.plan.sector.edit', [$classe->numClasse, $sector->numSector]) }}" class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" data-placement="top" data-title="Editer le secteur"><i class="fa fa-edit"></i> </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addSector" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un secteur</h4>
                </div>
                {{ Form::model($classe, ["route" => ["comptaAsc.config.plan.sector.store", $classe->numClasse]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('numSector', "Numéro de secteur", ["class" => "control-label"]) }}
                        {{ Form::text('numSector', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('nameSector', "Nom du secteur", ["class" => "control-label"]) }}
                        {{ Form::text('nameSector', null, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    