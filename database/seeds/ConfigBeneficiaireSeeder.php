<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigBeneficiaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config_beneficiaires')->insert([
            "soldeActive"       => 0,
            "activateur"        => 0,
            "adAgeLimitActive"  => 0
        ]);
    }
}
