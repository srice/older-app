<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $name }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="/assets/custom/css/pdf.css">
    <style type="text/css">
        body{
            font-family: Arial, sans-serif;
            font-size: 10px;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
<table style="width: 100%;">
    <tr>
        <td style="width: 33%" class="comite">
            <img src="/assets/custom/images/logos/1.jpg" width="100" alt="...">
            <br>
            Comite de Test<br>
            No Adresse<br>
            010101 No Ville
        </td>
        <td style="width: 33%;">&nbsp;</td>
        <td style="width: 33%; opacity: 0.5; font-weight: 400; font-size: 24px;">
            COMPTE DE RESULTAT DES OEUVRES SOCIALES & CULTURELLES
        </td>
    </tr>
</table>
<table style="margin-top: 5px; margin-bottom: 5px"></table>
<h2 style="text-align: center;">{{ $name }}</h2>
<table style="width: 100%;" cellpadding="0" cellspacing="0">
    <tr>
        <td style="width: 47%;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width: 75%; text-align: center; background-color: #0d47a1; padding: 15px; color: white">CHARGES</th>
                        <th style="width: 25%; text-align: center; background-color: #0d47a1; padding: 15px; color: white; border: solid 1px;">Soldes</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(\App\Http\Controllers\ComptaAsc\Etat\ResultatController::arrayCompteCharges() as $compte)
                    <tr>
                        <td style="padding: 5px;">{{ $compte->numCompte }} - {{ $compte->nameCompte }}</td>
                        <td style="text-align: right; padding: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Etat\ResultatController::getTotalNCharge($compte->numCompte)) }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td style="text-align: right; background-color: #0d47a1; padding: 15px; color: white;font-size: 15px;font-weight: bold;">Total des Charges</td>
                        <td style="text-align: right; background-color: #0d47a1; padding: 15px; color: white;font-size: 15px;font-weight: bold;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Etat\ResultatController::getTotalCharge()) }}</td>
                    </tr>
                </tfoot>
            </table>
        </td>
    </tr>
</table>
<div class="page-break"></div>
    <table style="width: 100%;">
        <tr>
            <td style="width: 33%" class="comite">
                <img src="/assets/custom/images/logos/1.jpg" width="100" alt="...">
                <br>
                Comite de Test<br>
                No Adresse<br>
                010101 No Ville
            </td>
            <td style="width: 33%;">&nbsp;</td>
            <td style="width: 33%; opacity: 0.5; font-weight: 400; font-size: 24px;">
                COMPTE DE RESULTAT DES OEUVRES SOCIALES & CULTURELLES
            </td>
        </tr>
    </table>
    <table style="margin-top: 25px; margin-bottom: 25px"></table>
    <h2 style="text-align: center;">{{ $name }}</h2>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 47%;">
                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th style="width: 75%; text-align: center; background-color: #0d47a1; padding: 15px; color: white">Produit</th>
                        <th style="width: 25%; text-align: center; background-color: #0d47a1; padding: 15px; color: white; border: solid 1px;">Soldes</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(\App\Http\Controllers\ComptaAsc\Etat\ResultatController::arrayCompteproduits() as $compte)
                        <tr>
                            <td style="padding: 5px;">{{ $compte->numCompte }} - {{ $compte->nameCompte }}</td>
                            <td style="text-align: right; padding: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Etat\ResultatController::getTotalNProduit($compte->numCompte)) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style="text-align: right; background-color: #0d47a1; padding: 15px; color: white;font-size: 15px;font-weight: bold;">Total des Produits</td>
                            <td style="text-align: right; background-color: #0d47a1; padding: 15px; color: white;font-size: 15px;font-weight: bold;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Etat\ResultatController::getTotalProduit()) }}</td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<table style="width: 100%;" cellpadding="0" cellspacing="0">
    <tr>
        <td style="width: 47%;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tfoot>
                    <tr>
                        <td style="text-align: right;background-color: {{ \App\Http\Controllers\ComptaAsc\Etat\ResultatController::getColorResultat() }}; padding: 15px; color: #fff; font-size: 20px;font-weight: bold;">Résultat de l'exploitation</td>
                        <td style="text-align: right;background-color: {{ \App\Http\Controllers\ComptaAsc\Etat\ResultatController::getColorResultat() }}; padding: 15px; color: #fff; font-size: 20px;font-weight: bold;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Etat\ResultatController::getTotalResultat()) }}</td>
                    </tr>
                </tfoot>

            </table>
        </td>
    </tr>
</table>

</body>
</html>