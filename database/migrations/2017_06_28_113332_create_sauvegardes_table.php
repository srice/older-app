<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSauvegardesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sauvegardes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nameSave');
            $table->integer('typeSave')->default(0); //0: Manuel |1: Automatique
            $table->string('sizeSave');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sauvegardes');
    }
}
