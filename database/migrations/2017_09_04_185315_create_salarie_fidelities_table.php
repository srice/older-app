<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalarieFidelitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salarie_fidelities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salaries_id');
            $table->string('numTransaction');
            $table->string('operation');
            $table->string('point');
            $table->string('solde');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salarie_fidelities');
    }
}
