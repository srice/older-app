<?php

namespace App\Model\GestionAsc\Prestation\Achat;

use Illuminate\Database\Eloquent\Model;

class Achat extends Model
{
    protected $guarded = [];

    public function fournisseur(){
        return $this->belongsTo(Fournisseur::class, 'fournisseurs_id');
    }

    public function prestations(){
        return $this->hasMany(AchatPrestation::class, 'achats_id');
    }

    public function reglements(){
        return $this->hasMany(AchatReglement::class, 'achats_id');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateAchat"];
    }
}
