@extends('template')
@section('title')
    Achat de Prestation
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop

@section("content")
    <div class="panel">
        <header class="panel-heading bg-primary-300">
            <div class="row">
                <div class="col-md-9">
                    <h3 class="panel-title">Liste des Fournisseurs</h3>
                </div>
                <div class="col-md-3">
                    <a href="#addFournisseur" class="btn btn-block btn-lg btn-primary" data-toggle="modal"><i class="fa fa-plus-circle"></i> Nouveau fournisseur</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="m-t-30"></div>
            <div class="table-responsive">
                <table id="listeFournisseur" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nom du fournisseur</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($fournisseurs as $fournisseur)
                            <tr>
                                <td>{{ $fournisseur->name }}</td>
                                <td>
                                    {{ Form::model($fournisseur, ["route" => ["fournisseurs.delete", $fournisseur->id], "method" => "DELETE"]) }}
                                    <a href="{{ route('fournisseurs.edit', $fournisseur) }}" class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" data-title="Editer le fournisseur"><i class="fa fa-edit"></i> </a>
                                    <button class="btn btn-sm btn-icon btn-danger" data-toggle="tooltip" data-title="Supprimer le fournisseur"><i class="fa fa-trash"></i> </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="m-t-30"></div>
    <div class="panel">
        <header class="panel-heading bg-primary-300">
            <div class="row">
                <div class="col-md-9">
                    <h3 class="panel-title">Liste des Achats</h3>
                </div>
                <div class="col-md-3">
                    <a href="#addAchat" data-toggle="modal" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Nouvel achat</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="listeAchat" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Numéro d'achat</th>
                            <th>Date d'achat</th>
                            <th>Fournisseur</th>
                            <th>Total des achats</th>
                            <th>Etat des achats</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($achats as $achat)
                        <tr>
                            <td>{{ $achat->numAchat }}</td>
                            <td>{{ $achat->dateAchat->format('d/m/Y') }}</td>
                            <td>{{ $achat->fournisseur->name }}</td>
                            <td>{{ \App\Http\Controllers\OtherController::euro($achat->totalAchat) }}</td>
                            <td>{!! \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatOtherController::etatAchatLabel($achat->etatAchat) !!}</td>
                            <td>
                                {{ Form::model($achat, ["route" => ["achats.delete", $achat->id], "method" => "DELETE"]) }}
                                <a href="{{ route('achats.show', $achat->id) }}" class="btn btn-icon btn-sm btn-default"><i class="fa fa-eye"></i> </a>
                                <a href="{{ route('achats.edit', $achat->id) }}" class="btn btn-icon btn-sm btn-info"><i class="fa fa-edit"></i> </a>
                                <button class="btn btn-icon btn-sm btn-danger"><i class="fa fa-trash"></i> </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addFournisseur" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un fournisseur</h4>
                </div>
                {{ Form::open(["route" => "fournisseurs.store"]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('name', "Nom du fournisseur", ["class" => "control-label"]) }}
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::text('name', null, ["class" => "form-control dateStart"]) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="addAchat" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un achat</h4>
                </div>
                {{ Form::open(["route" => "achats.store"]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('fournisseurs_id', "Fournisseur", ["class" => "control-label"]) }}
                        <div class="row">
                            <div class="col-md-12">
                                <select name="fournisseurs_id" class="form-control">
                                    @foreach($fournisseurs as $fournisseur)
                                        <option value="{{ $fournisseur->id }}">{{ $fournisseur->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('dateAchat', "Date de la facture", ["class" => "control-label"]) }}
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::text('dateAchat', null, ["class" => "form-control date", "data-plugin" => "datepicker"]) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/custom/js/gestionasc/prestation/achat/index.js"></script>
@stop