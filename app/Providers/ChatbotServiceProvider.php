<?php

namespace App\Providers;

use Chatbot;
use Illuminate\Support\ServiceProvider;

class ChatbotServiceProvider extends ServiceProvider
{
    protected $defer = false;
    protected $configName = 'chatbot';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__.'/../config/'.$this->configName.'.php';
        $this->publishes([$configPath => config_path($this->configName . '.php')], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__.'/../../config/'.$this->configName.'.php';
        $this->mergeConfigFrom($configPath, $this->configName);
        $this->app->bind(Chatbot::class, Chatbot::class);
        $this->app->singleton('chatbot', function($app){
            return new Chatbot();
        });

        $this->app->alias('chatbot', Chatbot::class);
    }
}
