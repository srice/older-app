@extends('template')
@section("title")
    Journal de caisse
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/global/vendor/icheck/icheck.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">@yield('title')</h3>
                </div>
                <div class="col-md-2">
                    <a href="#addEcriture" class="btn btn-lg btn-block btn-primary" data-toggle="modal"><i class="fa fa-plus-circle"></i> Nouvelle écriture</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Compte</th>
                        <th>Libelle</th>
                        <th>Débit</th>
                        <th>Crédit</th>
                        <th class="text-xs-center"><i class="fa fa-bars"></i> </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($caisses as $caisse)
                        <tr>
                            <td>{{ $caisse->dateCaisse->format('d/m/Y') }}</td>
                            <td>{{ $caisse->compte->numCompte }} - {{ $caisse->compte->nameCompte }}</td>
                            <td>{!! $caisse->libelleCaisse !!}</td>
                            <td class="text-xs-right">
                                @if($caisse->debitCaisse != 0)
                                    {{ \App\Http\Controllers\OtherController::euro($caisse->debitCaisse) }}
                                @else
                                @endif
                            </td>
                            <td class="text-xs-right">
                                @if($caisse->creditCaisse != 0)
                                    {{ \App\Http\Controllers\OtherController::euro($caisse->creditCaisse) }}
                                @else

                                @endif
                            </td>
                            <td>
                                {{ Form::model($caisse, ["route" => ["comptaFct.caisse.delete", $caisse->numCaisse], "method" => "DELETE"]) }}
                                <button class="btn btn-xs btn-icon btn-danger" data-toggle="tooltip" title="Supprimer l'écriture de caisse"><i class="fa fa-trash"></i> </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addEcriture" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'une écriture</h4>
                </div>
                {{ Form::open(["route" => "comptaFct.caisse.store"]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('typeCaisse', "Type d'entré", ["class" => "control-label"]) }}
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::radio('typeCaisse', 0, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                                {{ Form::label('typeCaisse', "Recette", ["class" => "control-label"]) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::radio('typeCaisse', 1, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                                {{ Form::label('typeCaisse', "Dépense", ["class" => "control-label"]) }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('numCaisse', 'Numérotation', ["class" => "control-label"]) }}
                        {{ Form::text('numCaisse', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('dateCaisse', "Date de l'entrée", ["class" => "control-label"]) }}
                        {{ Form::text('dateCaisse', null, ["class" => "form-control date"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('libelleCaisse', "Libellé de l'écriture", ["class" => "control-label"]) }}
                        {{ Form::text('libelleCaisse', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('montantCaisse', "Montant de l'écriture", ["class" => "control-label"]) }}
                        {{ Form::text('montantCaisse', null, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-select.js"></script>
    <script src="/assets/global/vendor/icheck/icheck.min.js"></script>
    <script src="/assets/global/js/Plugin/icheck.js"></script>
    <script type="text/javascript">
        (function($){
            $('.date').datepicker({
                format: 'dd-mm-yyyy',
                language: 'fr'
            });
        })(jQuery)
    </script>
@stop    