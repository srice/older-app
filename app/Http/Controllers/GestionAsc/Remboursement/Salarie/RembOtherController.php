<?php

namespace App\Http\Controllers\GestionAsc\Remboursement\Salarie;

use App\Model\GestionAsc\Remboursement\Salarie\RembSalarie;

class RembOtherController extends RembController
{
    public static function countRemb(){
        $count = RembSalarie::all()->count();
        return $count;
    }
}
