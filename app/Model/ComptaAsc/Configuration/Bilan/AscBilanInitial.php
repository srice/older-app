<?php

namespace App\Model\ComptaAsc\Configuration\Bilan;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use Illuminate\Database\Eloquent\Model;

class AscBilanInitial extends Model
{
    protected $guarded = [];

    public function comptes(){
        return $this->hasMany(ComptaPlanCompte::class, 'numCompte', 'numCompte');
    }
}
