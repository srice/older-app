@extends('template')
@section('title')
    Création d'une demande
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/summernote/summernote.css">
@stop

@section("content")
    <div class="panel">
        <header class="panel-header">
            <h3 class="panel-title"><i class="fa fa-plus"></i> @yield('title')</h3>
        </header>
        <div class="panel-body">
            {{ Form::open(["route" => "demandes.store", "class" => "form-horizontal"]) }}
            <div class="form-group row">
                {{ Form::label('titleDemande', 'Sujet de la demande *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::text('titleDemande', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('descDemande', 'Description de la demande *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-9">
                    {{ Form::textarea('descDemande', null, ["class" => "form-control round", "id" => "summernote", "data-plugin" => "summernote"]) }}
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/summernote/summernote.min.js"></script>
    <script src="/assets/custom/js/assistance/demande/create.js"></script>
@stop