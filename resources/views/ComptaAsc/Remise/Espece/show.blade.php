@extends('template')
@section("title")
    Remise en banque N°<strong>{{ $remise->numRemise }}</strong>
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-sweetalert/sweetalert.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
    <div class="row">
        <div class="col-md-4">
            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-md-10">
                            <h3 class="panel-title"><i class="fa fa-info-circle text-info"></i> Information</h3>
                        </div>
                        <div class="col-md-2">
                            @if($remise->etatRemise == 0)
                            <button id="checkRemise" data-num="{{ $remise->numRemise }}" class="btn btn-icon btn-xs btn-round btn-success m-t-20"><i class="fa fa-check"></i> Valider</button>
                            @endif
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-body">
                            <a href="{{ route('remise.espece.print', $remise->numRemise) }}" class="btn btn-sm btn-default"><i class="fa fa-print"></i> Voir la remise</a>
                            <a href="{{ route('remise.espece.pdf', $remise->numRemise) }}" class="btn btn-sm btn-default"><i class="fa fa-file-pdf-o"></i> Pdf de la remise</a>
                        </div>
                    </div>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td style="font-weight: bold;">Numéro de la remise</td>
                            <td>{{ $remise->numRemise }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Date de la remise</td>
                            <td>{{ $remise->dateRemise->format('d/m/Y') }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Montant total de la remise</td>
                            <td style="font-size: 18px;font-weight: bold;">{{ \App\Http\Controllers\OtherController::euro($remise->totalRemise) }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Etat de la remise</td>
                            <td>{!! \App\Http\Controllers\ComptaAsc\Remise\RemiseOtherController::etatRemiseLabel($remise->etatRemise) !!}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel">
                <header class="panel-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h3 class="panel-title">Liste des Paiement en espèce de la remise</h3>
                        </div>
                        <div class="col-md-2">
                            @if($remise->etatRemise == 0)
                            <button data-toggle="modal" href="#addEspece" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Ajouter un règlement en espèce</button>
                            @endif
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Numéro de la transaction</th>
                                    <th>Date de la transaction</th>
                                    <th>Dépositaire</th>
                                    <th>Montant</th>
                                    @if($remise->etatRemise == 0)
                                    <th class="text-xs-center"><i class="fa fa-bars"></i> </th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($especes as $cheque)
                                <tr>
                                    <td>{{ $cheque->numTransaction }}</td>
                                    <td>{{ $cheque->reglementSalaries->dateReglement->format('d/m/Y') }}</td>
                                    <td>{{ $cheque->reglementSalaries->depositaireReglement }}</td>
                                    <td>{{ \App\Http\Controllers\OtherController::euro($cheque->reglementSalaries->totalReglement) }}</td>
                                    @if($remise->etatRemise == 0)
                                    <td>
                                        {{ Form::model($cheque, ["route" => ["remise.espece.delEspece", $remise->numRemise, $cheque->numTransaction], "method" => "DELETE"]) }}
                                        <button data-toggle="tooltip" data-placement="top" title="Supprimer" class="btn btn-xs btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                        {{ Form::close() }}
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addEspece" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un paiement en espèce à la remise</h4>
                </div>
                {{ Form::open(["route" => ["remise.espece.addEspece", $remise->numRemise]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('numTransaction', "Transaction", ["control-label"]) }}
                        <select name="numTransaction" class="form-control" data-plugin="selectpicker" title="Selectionner le paiement à ajouter à la remise"
                        data-live-search="true">
                            <optgroup label="Salariés">
                                @foreach($salaries as $salary)
                                    <option value="{{ $salary->numReglementBilletSalarie }}">{{ $salary->numReglementBilletSalarie }} | {{ $salary->depositaireReglement }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Ayants Droit">
                                @foreach($ads as $ad)
                                    <option value="{{ $ad->numReglementBilletAd }}">{{ $ad->numReglementBilletAd }} | {{ $ad->depositaireReglement }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-sweetalert.js"></script>
    <script src="/assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-select.js"></script>
    <script src="/assets/custom/js/comptaasc/remise/cheque/show.js"></script>
@stop    