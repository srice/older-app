<?php

namespace App\Model\GestionAsc\Billetterie;

use App\Model\ComptaAsc\Remise\RemiseBanqueCheque;
use App\Model\ComptaAsc\Remise\RemiseBanqueEspece;
use Illuminate\Database\Eloquent\Model;

class ReglementBilletSalarie extends Model
{
    protected $guarded = [];

    public function billet(){
        return $this->belongsTo(BilletSalarie::class, "billet_salarie_id");
    }

    public function remiseCheques(){
        return $this->hasMany(RemiseBanqueCheque::class, 'numTransaction', 'numReglement');
    }

    public function remiseEspece(){
        return $this->hasMany(RemiseBanqueEspece::class, 'numTransaction', 'numReglement');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateReglement"];
    }
}
