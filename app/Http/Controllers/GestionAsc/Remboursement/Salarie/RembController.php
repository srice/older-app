<?php

namespace App\Http\Controllers\GestionAsc\Remboursement\Salarie;

use App\Http\Controllers\ComptaAsc\Etat\CompteController;
use App\Model\Beneficiaire\Salarie\Salarie;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalDiver;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Model\GestionAsc\Remboursement\Salarie\RembSalarie;
use App\Packages\Srice\Comite;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;
use PDF;

class RembController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'gestionasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function create(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $salaries = Salarie::where('active', 1)->get();

        return view('GestionAsc.Remboursement.Salarie.create', compact('config', 'salaries'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "salaries_id"   => "required",
            "dateRemb"      => "required"
        ]);

        if(RembOtherController::countRemb() == 0){
            $id = 0;
        }else{
            $id = RembOtherController::countRemb() + 1;
        }

        $date = $request->dateRemb;
        $strt = strtotime($date);
        $dateRemb = Carbon::createFromTimestamp($strt);

        $numRembSalarie = Controller::genNumerator("REMBSAL", $id);

        $add = RembSalarie::create([
            "numRembSalarie"    => $numRembSalarie,
            "salaries_id"       => $request->salaries_id,
            "dateRemb"          => $dateRemb,
            "totalRemb"         => 0,
            "etatRemb"          => 0
        ]);

        if($add){
            Toastr::success("La facture de remboursement à été créé");
            return redirect()->route('remboursements.index');
        }else{
            Toastr::error("Erreur lors de la création de la facture de remboursement !");
            return redirect()->back();
        }
    }

    public function show($id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $remb = RembSalarie::find($id)->load('salarie', 'prestations', 'reglements');

        return view('GestionAsc.Remboursement.Salarie.show', compact('config', 'remb'));
    }

    public function edit($id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $rembSalarie = RembSalarie::find($id);
        $salaries = Salarie::where('active', 1)->get();

        return view('GestionAsc.Remboursement.Salarie.edit', compact('config', 'rembSalarie', 'salaries'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            "salaries_id"   => "required",
            "dateRemb"      => "required"
        ]);

        $date = $request->dateRemb;
        $strt = strtotime($date);
        $dateRemb = Carbon::createFromTimestamp($strt);

        $edit = RembSalarie::find($id)->update([
            "salaries_id"   => $request->salaries_id,
            "dateRemb"      => $dateRemb
        ]);

        if($edit){
            Toastr::success("La facture de remboursement à été édité");
            return redirect()->route('remboursements.index');
        }else{
            Toastr::error("Erreur lors de l\'édition de la facture de remboursement");
            return redirect()->back();
        }
    }

    public function delete($id){

    }

    public function check($remb_id){

        $remb = RembSalarie::find($remb_id)->load('salarie');

        $newJournalDiv = AscJournalDiver::create([
            "numDivers"     => $remb->numRembSalarie,
            "dateDivers"    => $remb->dateRemb,
            "numCompte"     => 607,
            "libelleDivers" => "Remboursement sur facture pour <strong>".$remb->salarie->nom." ".$remb->salarie->prenom."</strong>",
            "debitDivers"   => $remb->totalRemb
        ]);

        //Mise à jour des Comptes
        $debit = CompteController::soldeDebit('607') + $remb->totalRemb;
        $credit = CompteController::soldeCredit($remb->salarie->numCompte) + $remb->totalRemb;

        $cptDebit = AscCompte::where('numCompte', '607')->update(["debit" => $debit]);
        $cptCredit = AscCompte::where('numCompte', $remb->salarie->numCompte)->update(["credit" => $credit]);

        $remb = RembSalarie::find($remb_id)->update(["etatRemb" => 1]);

        if($remb && $cptCredit && $cptDebit && $newJournalDiv){
            Toastr::success("La facture de remboursement à été validé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la validation de la facture de remboursement");
            return redirect()->back();
        }
    }

    public function showPdf($remb_id, Comite $comite){
        $remb = RembSalarie::find($remb_id)->load('salarie', 'prestations', 'reglements');
        $infoCom = $comite->comite();
        $name = "Facture de remboursement N°".$remb->numRembSalarie.".pdf";
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('GestionAsc.Remboursement.Salarie.pdf', compact('remb', 'infoCom', 'name'));
        return $pdf->stream($name);
    }

    public function savePdf($remb_id, Comite $comite){
        $remb = RembSalarie::find($remb_id)->load('salarie', 'prestations', 'reglements');
        $infoCom = $comite->comite();
        $name = "Facture de remboursement N°".$remb->numRembSalarie.".pdf";
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('GestionAsc.Remboursement.Salarie.pdf', compact('remb', 'infoCom', 'name'));
        return $pdf->download($name);
    }
}
