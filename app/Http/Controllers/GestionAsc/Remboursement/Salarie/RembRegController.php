<?php

namespace App\Http\Controllers\GestionAsc\Remboursement\Salarie;

use App\Http\Controllers\ComptaAsc\Etat\CompteController;
use App\Http\Controllers\GestionAsc\Billetterie\Salarie\BilletReglementController;
use App\Http\Controllers\GestionAsc\Remboursement\RemboursementController;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalBanque;
use App\Model\ComptaAsc\Journal\AscJournalCaisse;
use App\Model\GestionAsc\Remboursement\Salarie\RembRegSalarie;
use App\Model\GestionAsc\Remboursement\Salarie\RembSalarie;
use App\Packages\Srice\Comite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Kamaln7\Toastr\Facades\Toastr;

class RembRegController extends RembController
{
    public function addReglement(Request $request, $remb_id, Comite $comite){
        $this->validate($request, [
            "numReglement"  => "required",
            "montant"       => "required",
            "dateReg"       => "required"
        ]);
        $com = $comite->comite();
        $remb = RembSalarie::find($remb_id)->load('salarie');

        $date = $request->dateReg;
        $strt = strtotime($date);
        $dateReg = Carbon::createFromTimestamp($strt);

        try {
            $add = RembRegSalarie::create([
                "remb_salarie_id"   => $remb_id,
                "numReglement"      => $request->numReglement,
                "depositaire"       => "Comité de Test",
                "montant"           => $request->montant,
                "dateReg"           => $dateReg
            ]);
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
            try {
                $newJournalbanque = AscJournalBanque::create([
                    "numBanque"     => $request->numReglement,
                    "dateBanque"    => $dateReg,
                    "numCompte"     => 512,
                    "libelleBanque" => "Règlement en <strong>".BilletReglementController::modeReglement($request->modeReglement)."</strong> pour le remboursement N°<strong>".$remb->numRembSalarie."</strong>",
                    "creditBanque"  => $request->montant
                ]);
            }catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }

            // Mise à jour des Comptes
            try {
                $debit = CompteController::soldeDebit($remb->salarie->numCompte) + $request->montant;
                $credit = CompteController::soldeCredit('512') + $request->montant;
            }catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }

            try {
                $cptDebit = AscCompte::where('numCompte', $remb->salarie->numCompte)->update(["debit" => $debit]);
                $cptCredit = AscCompte::where('numCompte', '512')->update(["credit" => $credit]);
            }catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }

        if(RemboursementController::totalReglementByRemboursement($remb_id) == $remb->totalRemb){
            $remb->update(["etatRemb" => 3]);
        }elseif(RemboursementController::totalReglementByRemboursement($remb_id) < $remb->totalRemb && RemboursementController::totalReglementByRemboursement($remb_id) > 0){
            $remb->update(["etatRemb" => 2]);
        }else{
            $remb->update(["etatRemb" => 1]);
        }

        if($add && $cptDebit && $cptCredit){
            Toastr::success("Un règlement à été ajouté à la facture de remboursement");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du règlement dans la facture de remboursement !");
            return redirect()->back();
        }
    }

    public function delReglement($remb_id, $reglements_id){
        $remb = RembSalarie::find($remb_id)->load('salarie');
        $reglement = RembRegSalarie::find($reglements_id);

            $delJournalBanque = AscJournalBanque::where('numBanque', $reglement->numReglement)->delete();

            // Mise à jour des Comptes
            $debit = CompteController::soldeDebit($remb->salarie->numCompte) - $reglement->totalReglement;
            $credit = CompteController::soldeCredit('512') - $reglement->totalReglement;

            $cptDebit = AscCompte::where('numCompte', $remb->salarie->numCompte)->update(["debit" => $debit]);
            $cptCredit = AscCompte::where('numCompte', '512')->update(["credit" => $credit]);

        $reglement->delete();

        if(RemboursementController::totalReglementByRemboursement($remb_id) == $remb->totalRemb){
            $remb->update(["etatRemb" => 3]);
        }elseif(RemboursementController::totalReglementByRemboursement($remb_id) < $remb->totalRemb && RemboursementController::totalReglementByRemboursement($remb_id) > 0){
            $remb->update(["etatRemb" => 2]);
        }else{
            $remb->update(["etatRemb" => 1]);
        }

        if($reglement && $cptCredit && $cptDebit){
            Toastr::success("Un Règlement à été supprimé de la facture de remboursement");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du règlement dans la facture de remboursement !");
            return redirect()->back();
        }

    }
}
