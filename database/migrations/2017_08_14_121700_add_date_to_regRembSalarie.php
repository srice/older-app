<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateToRegRembSalarie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remb_reg_salaries', function (Blueprint $table) {
            $table->timestamp('dateReg')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remb_reg_salaries', function (Blueprint $table) {
            $table->removeColumn('dateReg');
        });
    }
}
