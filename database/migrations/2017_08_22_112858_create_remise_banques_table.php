<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemiseBanquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remise_banques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numRemise');
            $table->timestamp('dateRemise')->nullable();
            $table->integer('typeRemise')->comment('//0: Chèque |1: Espèce');
            $table->string('totalRemise')->default(0);
            $table->integer('etatRemise')->default(0)->comment("0: Brouillon |1: Valider");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remise_banques');
    }
}
