@extends('template')
@section("title")
    Facture de remboursement N° <strong>{{ $remb->numRembSalarie }}</strong>
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
    <div class="panel">
        <div class="panel-body">
            <a href="{{ route('remb.salarie.showPdf', $remb->id) }}" class="btn btn-default"><i class="fa fa-print"></i> Imprimer</a>
            <a href="{{ route('remb.salarie.savePdf', $remb->id) }}" class="btn btn-default"><i class="fa fa-file-pdf-o"></i> Exporter en pdf</a>
            @if($remb->etatRemb == 0)
                <a href="{{ route('remb.salarie.check', $remb->id) }}" class="btn btn-success"><i class="fa fa-check"></i> Valider la facture</a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel bg-info">
                <header class="panel-heading">
                    <h2 class="text-xs-center white p-t-10">Date de la facture</h2>
                </header>
                <div class="panel-body">
                    <h1 class="text-xs-center white p-t-20" style="font-weight: bold;">{{ $remb->dateRemb->format('d/m/Y') }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel bg-info">
                <header class="panel-heading">
                    <h2 class="text-xs-center white p-t-10">Montant à remboursé</h2>
                </header>
                <div class="panel-body">
                    <h1 class="text-xs-center white p-t-20" style="font-weight: bold;">{{ \App\Http\Controllers\OtherController::euro($remb->totalRemb) }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel {{ \App\Http\Controllers\GestionAsc\Remboursement\RemboursementController::etatRembColor($remb->etatRemb) }}">
                <header class="panel-heading">
                    <h2 class="text-xs-center white p-t-10">Etat de la facture</h2>
                </header>
                <div class="panel-body">
                    <h1 class="text-xs-center white p-t-20" style="font-weight: bold;">{!! \App\Http\Controllers\GestionAsc\Remboursement\RemboursementController::etatRembText($remb->etatRemb) !!} </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <header class="panel-heading">
            <div class="nav-tabs-horizontal" data-plugin="tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" data-toggle="tab" href="#prestation" role="tab"><i class="fa fa-cubes"></i> Prestations</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#reglement" role="tab"><i class="fa fa-money"></i> Règlements</a>
                    </li>
                </ul>
            </div>
        </header>
        <div class="panel-body tab-content p-t-20">
            <div class="tab-pane active" id="prestation" role="tabpanel">
                <div class="panel">
                    <header class="panel-header">
                        <div class="row">
                            <div class="col-md-10">
                                <h3 class="panel-title">Liste des Prestations</h3>
                            </div>
                            <div class="col-md-2">
                                @if($remb->etatRemb == 0)
                                    <a href="#addPrestation" class="btn btn-lg btn-block btn-primary" data-toggle="modal"><i class="fa fa-plus-circle"></i> Ajouter une prestation</a>
                                @endif
                            </div>
                        </div>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Prestation</th>
                                    <th>Montant de la prestation</th>
                                    <th>Participation du CE</th>
                                    <th>Montant Remboursable</th>
                                    @if($remb->etatRemb == 0)
                                        <th></th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($remb->prestations as $ligne)
                                    <tr>
                                        <td>
                                            {{ $ligne->prestation }}
                                        </td>
                                        <td>{{ \App\Http\Controllers\OtherController::euro($ligne->montant) }}</td>
                                        <td>{{ \App\Http\Controllers\OtherController::euro($ligne->partCe) }}</td>
                                        <td>{{ \App\Http\Controllers\GestionAsc\Remboursement\RemboursementController::totalRembPrestation($ligne->montant, $ligne->partCe) }}</td>
                                        @if($remb->etatRemb == 0)
                                            <td>
                                                {{ Form::model($ligne, ["route" => ["remb.salarie.delPrestation", $remb->id, $ligne->id], "method" => "DELETE"]) }}
                                                <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                                {{ Form::close() }}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="reglement" role="tabpanel">
                <div class="panel">
                    <header class="panel-heading">
                        <div class="row">
                            <div class="col-md-10">
                                <h3 class="panel-title">Liste des règlements</h3>
                            </div>
                            <div class="col-md-2">
                                @if($remb->etatRemb >= 1)
                                    <a href="#addReglement" class="btn btn-block btn-lg btn-primary" data-toggle="modal"><i class="fa fa-plus-circle"></i> Ajouter un règlement</a>
                                @endif
                            </div>
                        </div>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="listeReglement" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Numéro de transaction</th>
                                    <th>Date du règlement</th>
                                    <th>Montant du règlement</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($remb->reglements as $reglement)
                                    <tr>
                                        <td>{{ $reglement->numReglement }}</td>
                                        <td>{{ $reglement->dateReg->format('d/m/Y') }}</td>
                                        <td>{{ \App\Http\Controllers\OtherController::euro($reglement->montant) }}</td>
                                        <td>
                                            {{ Form::model($reglement, ["route" => ["remb.salarie.delReglement", $remb->id, $reglement->id], "method" => "DELETE"]) }}
                                            <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addPrestation" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'une Prestation</h4>
                </div>
                {{ Form::model($remb, ["route" => ["remb.salarie.addPrestation", $remb->id]]) }}
                <div class="modal-body">
                    <div class="form-group row form-material">
                        {{ Form::label('prestation', 'Prestation', ["class" => "control-label"]) }}
                        {{ Form::text('prestation', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group row form-material">
                        {{ Form::label('montant', 'Montant de la prestation', ["class" => "control-label"]) }}
                        {{ Form::text('montant', null, ["class" => "form-control"]) }}
                    </div>
                    @if($config->configuration['remb']->percent == 0)
                    <div class="form-group row form-material">
                        {{ Form::label('partCe', "Participation du comité", ["class" => "control-label"]) }}
                        {{ Form::text('partCe', null, ["class" => "form-control"]) }}
                    </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="addReglement" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un Règlement</h4>
                </div>
                {{ Form::model($remb, ["route" => ["remb.salarie.addReglement", $remb->id]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('numReglement', "Numéro de la transaction", ["class" => "control-label"]) }}
                        {{ Form::text('numReglement', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('dateReg', 'Date de la transaction', ["class" => "control-label"]) }}
                        {{ Form::text('dateReg', null, ["class" => "form-control date"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('montant', "Montant de la transaction", ["class" => "control-label"]) }}
                        {{ Form::text('montant', null, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/select2/select2.full.min.js"></script>
    <script src="/assets/global/js/Plugin/select2.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/custom/js/gestionasc/remboursement/salarie/show.js"></script>
@stop    