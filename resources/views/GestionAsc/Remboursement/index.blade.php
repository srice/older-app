@extends('template')
@section("title")
    Remboursement sur Facture
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Remboursement Salariés</h3>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('remb.salarie.create') }}" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Nouveau remboursement</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="listeRembSalarie" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Numéro de Facture</th>
                            <th>Date de la facture</th>
                            <th>Identité</th>
                            <th>Total de la facture</th>
                            <th>Etat de la facture</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rembSalaries as $rembSalary)
                        <tr>
                            <td>{{ $rembSalary->numRembSalarie }}</td>
                            <td class="text-xs-center">{{ $rembSalary->dateRemb->format('d/m/Y') }}</td>
                            <td>{{ $rembSalary->salarie->nom }} {{ $rembSalary->salarie->prenom }}</td>
                            <td class="text-xs-right">{{ \App\Http\Controllers\OtherController::euro($rembSalary->totalRemb) }}</td>
                            <td class="text-xs-center">{!! \App\Http\Controllers\GestionAsc\Remboursement\RemboursementController::etatRembLabel($rembSalary->etatRemb) !!}</td>
                            <td>
                                {{ Form::model($rembSalary, ["route" => ["remb.salarie.delete", $rembSalary->id], "method" => "DELETE"]) }}
                                <a href="{{ route('remb.salarie.show', $rembSalary->id) }}" class="btn btn-sm btn-icon btn-default"><i class="fa fa-eye"></i></a>
                                <a href="{{ route('remb.salarie.edit', $rembSalary->id) }}" class="btn btn-sm btn-icon btn-info"><i class="fa fa-edit"></i></a>
                                <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/custom/js/gestionasc/remboursement/index.js"></script>
@stop    