(function ($) {
    getVille();
})(jQuery);

function getVille(){
    var codePostal = $("#codePostal").val();
    var ville = $("#ville");

    $.get('/beneficiaire/salarie/getVille/'+codePostal)
        .done(function (data) {
            ville.html(data)
        })
}