<?php

namespace App\Http\Controllers\Beneficiaire;

use App\Model\Beneficiaire\Ad\AyantDroit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class AdOtherController extends AdController
{
    // Route

    public function inactif($ads_id){
        $ad = AyantDroit::find($ads_id);

        $update = $ad->update(["active" => 0]);

        if($update){
            Toastr::success("L\'ayant Droit <strong>".$ad->nom." ".$ad->prenom."</strong> est maintenant inactif !");
            return redirect()->back();
        }
    }

    public function actif($ads_id){
        $ad = AyantDroit::find($ads_id);

        $update = $ad->update(["active" => 1]);

        if($update){
            Toastr::success("L\'ayant Droit <strong>".$ad->nom." ".$ad->prenom."</strong> est maintenant actif !");
            return redirect()->back();
        }
    }

    // Static

    public static function AdIsActif($value){
        switch ($value){
            case 0: return '<span class="tag bg-danger"><i class="fa fa-lock"></i> Inactif</span>';
            case 1: return '<span class="tag bg-green-500"><i class="fa fa-unlock"></i> Actif</span>';
        }
    }
}
