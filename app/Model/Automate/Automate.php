<?php

namespace App\Model\Automate;

use Illuminate\Database\Eloquent\Model;

class Automate extends Model
{
    protected $guarded = [];
}
