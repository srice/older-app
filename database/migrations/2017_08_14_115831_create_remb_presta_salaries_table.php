<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRembPrestaSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remb_presta_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('remb_salarie_id');
            $table->string('prestation');
            $table->string('montant');
            $table->string('partCe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remb_presta_salaries');
    }
}
