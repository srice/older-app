<?php

namespace App\Http\Controllers\Assistance\Ticket;

use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Comite;
use App\Packages\Srice\Espace;
use App\Packages\Srice\Ticket;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class TicketController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace, Guard $auth)
    {
        $this->auth = $auth;
        $this->sector = '';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(Ticket $ticket){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $tickets = $ticket->tickets();

        return view('Assistance.Ticket.index', compact('config', 'tickets'));
    }

    public function create(Ticket $ticket){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $departements = $ticket->departements()->pluck('nameDepartement', 'id');
        $services = $ticket->services()->pluck('nameService', 'id');

        return view('Assistance.Ticket.create', compact('config', 'departements', 'services'));
    }

    public function store(Request $request, Ticket $ticket){
        $this->validate($request, [
            "message"   => "required"
        ]);
        $post = [
            "sujetTicket"       => $request->sujetTicket,
            "departements_id"   => $request->departements_id,
            "services_id"       => $request->services_id,
            "priorities_id"     => $request->priorities_id,
            "message"           => $request->message
        ];

        $comite = new Comite();
        $tickets = $ticket->postTicket($post);

        if($tickets == true){
            Toastr::success("Votre ticket à été transmis à notre équipe technique !");
            return redirect()->route('tickets.index');
        }else{
            Toastr::error("Erreur lors de la transmission de votre ticket à notre équipe technique !");
            return redirect()->back();
        }
    }

    public function show($id, Ticket $tickets){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $ticket = $tickets->ticket($id);

        return view('Assistance.Ticket.show', compact('config', 'ticket'));

    }

    public function loadChat($id){
        $ticket = new Ticket();
        $chats = $ticket->chats($id);
        ob_start();
        ?>
        <div class="app-message-chats">
            <button type="button" id="historyBtn" class="btn btn-round btn-default">History Messages</button>
            <div class="chats">
                <?php foreach ($chats as $chat): ?>
                    <?php if($chat->group == 0): ?>
                        <div class="chat">
                            <div class="chat-avatar">
                                <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="">
                                    <img src="/assets/template/images/logo.png" alt="June Lane">
                                </a>
                            </div>
                            <div class="chat-body">
                                <div class="chat-content">
                                    <?= $chat->message; ?>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="chat chat-left">
                            <div class="chat-avatar">
                                <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="">
                                    <img src="/assets/template/images/logo.png" alt="June Lane">
                                </a>
                            </div>
                            <div class="chat-body">
                                <div class="chat-content">
                                    <?= $chat->message; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function reply(Request $request, $id){
        $tickets = new Ticket();
        $ticket = $tickets->ticket($id);

        $post = $tickets->postReply($id, $request->message);

        return response()->json();

    }

    public function stateTicket($tickets_id){
        $ticket = new Ticket();
        $st = $ticket->ticket($tickets_id);

        return TicketOtherController::etatTicket($st->etatTicket);
    }

}
