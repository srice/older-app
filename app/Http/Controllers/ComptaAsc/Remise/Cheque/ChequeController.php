<?php

namespace App\Http\Controllers\ComptaAsc\Remise\Cheque;

use App\Http\Controllers\ComptaAsc\Remise\RemiseController;
use App\Model\ComptaAsc\Remise\RemiseBanque;
use App\Model\ComptaAsc\Remise\RemiseBanqueCheque;
use App\Model\GestionAsc\Billetterie\ReglementBilletAd;
use App\Model\GestionAsc\Billetterie\ReglementBilletSalarie;
use App\Packages\Srice\Comite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Kamaln7\Toastr\Facades\Toastr;
use PDF;

class ChequeController extends RemiseController
{
    public function show($numRemise){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $remise = RemiseBanque::where('numRemise', $numRemise)->first();
        $cheques = RemiseBanqueCheque::where('numRemise', $numRemise)->get()->load('reglementSalaries');

        $salaries = ReglementBilletSalarie::where('point', 0)
            ->where('modeReglement', '1')
            ->get();
        $ads = ReglementBilletAd::where('point', 0)
            ->where('modeReglement', '1')
            ->get();

        return view('ComptaAsc.Remise.Cheque.show', compact('config', 'remise', 'cheques', 'salaries', 'ads'));
    }

    public function check($numRemise){
        $check = RemiseBanque::where('numRemise', $numRemise)->update(["etatRemise" => 1]);

        return Response::json($check);
    }

    public function addCheque(Request $request, $numRemise){
        $this->validate($request, [
            "numTransaction"    => "required"
        ]);

        $countSalarie = self::countTransSalarie($request->numTransaction);
        $countAd = self::countTransAd($request->numTransaction);

        $remise = RemiseBanque::where('numRemise', $numRemise)->first();

        $add = RemiseBanqueCheque::create([
            "numRemise"     => $numRemise,
            "numTransaction"=> $request->numTransaction
        ]);

        if($countSalarie == 1){
            $regSalarie = ReglementBilletSalarie::where('numReglementBilletSalarie', $request->numTransaction)->first();
            $up = ReglementBilletSalarie::where('numReglementBilletSalarie', $request->numTransaction)->first()->update(["point" => 1]);

            $newTotal = $remise->totalRemise + $regSalarie->totalReglement;
            $upRemise = $remise->update(["totalRemise" => $newTotal]);
        }elseif($countAd == 1){
            $regAd = ReglementBilletAd::where('numReglementBilletAd', $request->numTransaction)->first();
            $up = ReglementBilletAd::where('numReglementBilletAd', $request->numTransaction)->first()->update(["point" => 1]);

            $newTotal = $remise->totalRemise + $regAd->totalReglement;
            $upRemise = $remise->update(["totalRemise" => $newTotal]);
        }else{
            $up = 0;
            $upRemise = 0;
            Toastr::warning("Selection invalide !");
        }

        if($add && $up && $upRemise){
            Toastr::success("Le chèque à été ajouter à la remise en banque !");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du chèque dans la remise en banque !");
            return redirect()->back();
        }
    }

    public function delCheque($numRemise, $numTransaction){
        $countSalarie = self::countTransSalarie($numTransaction);
        $countAd = self::countTransAd($numTransaction);
        $remise = RemiseBanque::where('numRemise', $numRemise)->first();
        $ligne = RemiseBanqueCheque::where('numTransaction', $numTransaction)->first();

        if($countSalarie == 1){
            $regSalarie = ReglementBilletSalarie::where('numReglementBilletSalarie', $numTransaction)->first();
            $up = ReglementBilletSalarie::where('numReglementBilletSalarie', $numTransaction)->first()->update(["point" => 0]);

            $newTotal = $remise->totalRemise - $regSalarie->totalReglement;
            $upRemise = $remise->update(["totalRemise" => $newTotal]);
        }elseif($countAd == 1){
            $regAd = ReglementBilletAd::where('numReglementBilletAd', $numTransaction)->first();
            $up = ReglementBilletAd::where('numReglementBilletAd', $numTransaction)->first()->update(["point" => 1]);

            $newTotal = $remise->totalRemise - $regAd->totalReglement;
            $upRemise = $remise->update(["totalRemise" => $newTotal]);
        }else{
            $up = 0;
            $upRemise = 0;
            Toastr::warning("Selection Invalide !");
        }

        $ligne->delete();

        if($ligne && $up && $upRemise){
            Toastr::success("La transaction à été supprimé de la remise en banque !");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de la transaction de la remise en banque !");
            return redirect()->back();
        }
    }

    public function print($numRemise, Comite $comite){
        $remise = RemiseBanque::where('numRemise', $numRemise)->first()->load('cheques');
        $infoCom = $comite->comite();
        $name = "Remise de Chèque N°".$numRemise.'.pdf';
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('ComptaAsc.Remise.Cheque.pdf', compact('remise', 'infoCom', 'name'));
        return $pdf->stream($name);
    }

    public function pdf($numRemise, Comite $comite){
        $remise = RemiseBanque::where('numRemise', $numRemise)->first()->load('cheques');
        $infoCom = $comite->comite();
        $name = "Remise de Chèque N°".$numRemise.'.pdf';
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('ComptaAsc.Remise.Cheque.pdf', compact('remise', 'infoCom', 'name'));
        return $pdf->download($name);
    }


    public static function countTransSalarie($numTransaction){
        $count = ReglementBilletSalarie::where('numReglementBilletSalarie', $numTransaction)->get()->count();

        return $count;
    }

    public static function countTransAd($numTransaction){
        $count = ReglementBilletAd::where('numReglementBilletAd', $numTransaction)->get()->count();

        return $count;
    }
}
