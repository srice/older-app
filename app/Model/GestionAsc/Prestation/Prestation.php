<?php

namespace App\Model\GestionAsc\Prestation;

use Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    protected $guarded = [];

    public function famille(){
        return $this->belongsTo(Famille::class, 'familles_id');
    }

    public function dates(){
        return $this->hasMany(PrestationDate::class, 'prestations_id');
    }

    public function locales(){
        return $this->hasMany(PrestationLocal::class, 'prestations_id');
    }

    public function tarifs(){
        return $this->hasMany(PrestationTarif::class, 'prestations_id');
    }

}
