<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFctJournalVentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fct_journal_ventes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numVente');
            $table->timestamp('dateVente')->nullable();
            $table->string('numCompte');
            $table->string('libelleVente');
            $table->string('debitVente')->default(0);
            $table->string('creditVente')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fct_journal_ventes');
    }
}
