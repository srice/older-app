@extends('template')
@section('title')
    Localisation de l'évènement
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="panel">
        <div class="panel-body">
            <div style="width: 100%; height: 600px;">{!! Mapper::render() !!}</div>
        </div>
    </div>
@stop
@section('footer_scripts')
    
@stop