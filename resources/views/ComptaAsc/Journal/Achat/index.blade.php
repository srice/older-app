@extends('template')
@section("title")
    Journal des Achats
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">@yield('title')</h3>
                </div>
                <div class="col-md-2">
                    <a href="#addEcriture" class="btn btn-lg btn-block btn-primary" data-toggle="modal"><i class="fa fa-plus-circle"></i> Nouvelle écriture</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Compte</th>
                            <th>Libelle</th>
                            <th>Débit</th>
                            <th>Crédit</th>
                            <th class="text-xs-center"><i class="fa fa-bars"></i> </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($achats as $achat)
                        <tr>
                            <td>{{ $achat->dateAchat->format('d/m/Y') }}</td>
                            <td>{{ $achat->compte->numCompte }} - {{ $achat->compte->nameCompte }}</td>
                            <td>{!! $achat->libelleAchat !!}</td>
                            <td>
                                @if($achat->debitAchat != 0)
                                    {{ \App\Http\Controllers\OtherController::euro($achat->debitAchat) }}
                                @else

                                @endif
                            </td>
                            <td>
                                @if($achat->creditAchat != 0)
                                    {{ \App\Http\Controllers\OtherController::euro($achat->creditAchat) }}
                                @else

                                @endif
                            </td>
                            <td>
                                @if($achat->numCompte != '601' && $achat->numCompte != '607')
                                    {{ Form::model($achat, ["route" => ["comptaAsc.achat.delete", $achat->numAchat], "method" => "DELETE"]) }}
                                    <button class="btn btn-xs btn-icon btn-danger" data-toggle="tooltip" title="Supprimer l'achat"><i class="fa fa-trash"></i> </button>
                                    {{ Form::close() }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addEcriture" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'une écriture</h4>
                </div>
                {{ Form::open(["route" => "comptaAsc.achat.store"]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('numAchat', 'Numéro de la facture', ["class" => "control-label"]) }}
                        {{ Form::text('numAchat', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('dateAchat', "Date de l'achat", ["class" => "control-label"]) }}
                        {{ Form::text('dateAchat', null, ["class" => "form-control date"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('numCompte', "Compte", ["class" => "control-label"]) }}
                        <select name="numCompte" class="form-control" data-plugin="selectpicker" data-live-search="true">
                            @foreach($sectors as $sector)
                            <optgroup label="{{ $sector->nameSector }}">
                                <?php
                                    $comptes = \App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte::where('numSector', $sector->numSector)->get();
                                    foreach ($comptes as $compte):
                                ?>
                                <option value="{{ $compte->numCompte }}">{{ $compte->nameCompte }}</option>
                                <?php endforeach; ?>
                            </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('libelleAchat', "Libellé de l'écriture", ["class" => "control-label"]) }}
                        {{ Form::text('libelleAchat', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('montantAchat', "Montant de l'écriture", ["class" => "control-label"]) }}
                        {{ Form::text('montantAchat', null, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-select.js"></script>
    <script type="text/javascript">
        (function($){
            $('.date').datepicker({
                format: 'dd-mm-yyyy',
                language: 'fr'
            });
        })(jQuery)
    </script>
@stop    