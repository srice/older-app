<?php

namespace App\Model\ComptaFct\Configuration\Bilan;

use App\Model\ComptaFct\Configuration\Plan\FctPlanCompte;
use Illuminate\Database\Eloquent\Model;

class FctBilanInitial extends Model
{
    protected $guarded = [];

    public function comptes(){
        return $this->hasMany(FctPlanCompte::class, 'numCompte', 'numCompte');
    }
}
