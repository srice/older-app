@extends('template')
@section("title")
    Connexion à un compte utilisateur
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield('title')</h3>
        </header>
        <div class="panel-body">
            {{ Form::open(["route" => "banker.users.connexion", "class" => "form-horizontal"]) }}
            <div class="form-group row form-material">
                {{ Form::label('email', 'Adresse mail', ["class" => "control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::email('email', null, ["class" => "form-control"]) }}
                </div>
            </div>
            <div class="form-group row form-material">
                {{ Form::label('password', "Mot de passe", ["class" => "control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::password('password', ["class" => "form-control"]) }}
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    