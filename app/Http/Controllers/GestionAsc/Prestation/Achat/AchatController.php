<?php

namespace App\Http\Controllers\GestionAsc\Prestation\Achat;

use App\Http\Controllers\ComptaAsc\Etat\CompteController;
use App\Http\Controllers\GestionAsc\Configuration\ModeReglementController;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalAchat;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Model\GestionAsc\Prestation\Achat\Achat;
use App\Model\GestionAsc\Prestation\Achat\AchatPrestation;
use App\Model\GestionAsc\Prestation\Achat\Fournisseur;
use App\Model\GestionAsc\Prestation\Famille;
use App\Model\GestionAsc\Prestation\PrestationTarif;
use App\Packages\Srice\Comite;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;
use PDF;

class AchatController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'gestionasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(){
        Carbon::setToStringFormat('d/m/Y');
        Carbon::setLocale('fr');
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $achats = Achat::all()->load('fournisseur', 'prestations', 'reglements');
        $fournisseurs = Fournisseur::all();


        return view('GestionAsc.Prestation.Achat.index', compact('config', 'achats', 'fournisseurs'));
    }

    public function store(Request $request){

        $this->validate($request, [
            "fournisseurs_id"   => "required",
            "dateAchat"         => "required|date"
        ]);

        if(AchatOtherController::countAchat() == 0){
            $achats_id = 1;
        }else{
            $achats_id = AchatOtherController::countAchat() + 1;
        }

        $date = $request->dateAchat;
        $strt = strtotime($date);
        $dateAchat = Carbon::createFromTimestamp($strt);

        $numAchat = Controller::genNumerator("ACH", $achats_id);

        $achat = Achat::create([
            "numAchat"          => $numAchat,
            "fournisseurs_id"   => $request->fournisseurs_id,
            "dateAchat"         => $dateAchat,
            "totalAchat"        => 0,
            "etatAchat"         => 0
        ]);

        if($achat){
            Toastr::success("La facture N°<strong>".$numAchat."</strong> à été créé !");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la création de la facture d\'achat !");
            return redirect()->back();
        }
    }

    public function show($achats_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $achat = Achat::find($achats_id)->load('fournisseur', 'prestations', 'reglements');

        $familles = Famille::all();

        $modeReglements = ModeReglementController::listeMode();

        return view('GestionAsc.Prestation.Achat.show', compact('config', 'achat', 'familles', 'modeReglements'));
    }

    public function edit($achats_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $achat = Achat::find($achats_id);
        $fournisseurs = Fournisseur::all();

        return view('GestionAsc.Prestation.Achat.edit', compact('config', 'achat', 'fournisseurs'));
    }

    public function update(Request $request, $achats_id){

        $this->validate($request, [
            "fournisseurs_id"   => "required",
            "dateAchat"         => "required|date"
        ]);

        $date = $request->dateAchat;
        $strt = strtotime($date);
        $dateAchat = Carbon::createFromTimestamp($strt);

        $achat = Achat::find($achats_id)->update([
            "fournisseurs_id"   => $request->fournisseurs_id,
            "dateAchat"         => $dateAchat
        ]);

        if($achat){
            Toastr::success("La facture à été édité");
            return redirect()->route('achats.index');
        }else{
            Toastr::error("Erreur lors de l\'édition de la facture d\'achat !");
            return redirect()->back();
        }
    }

    public function delete($achats_id){
        $prestations = AchatPrestation::where('achats_id', $achats_id)->delete();
        $achat = Achat::destroy($achats_id);

        if($achat && $prestations){
            Toastr::success("La facture d\'achat à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de la facture d\'achat !");
            return redirect()->back();
        }
    }

    public function check($achats_id){
        $achat = Achat::find($achats_id)->load('fournisseur');
        $lignes = AchatPrestation::where('achats_id', $achats_id)->get();

        if($this->configuration['presta']->stock == 1){
            foreach ($lignes as $ligne){
                $prestation = PrestationTarif::find($ligne->tarifs_id);
                $newStock = $prestation->stockActuel + $ligne->qte;
                $prestation->update(["stockActuel" => $newStock]);
            }
            $achat->update(["etatAchat" => 1]);
        }else{
            $achat->update(["etatAchat" => 1]);
        }

        $newJournalCharge = AscJournalAchat::create([
            "numAchat"      => $achat->numAchat,
            "dateAchat"     => $achat->dateAchat,
            "numCompte"     => 601,
            "libelleAchat"  => "Achat de prestation au fournisseurs <strong>".$achat->fournisseur->name."</strong>",
            "debitAchat"    => $achat->totalAchat
        ]);

        // Mise à jour des Comptes
        $debit = CompteController::soldeDebit('601') + $achat->totalAchat;
        $credit = CompteController::soldeCredit($achat->fournisseur->numCompte) + $achat->totalAchat;

        $cptDebit = AscCompte::where('numCompte', '601')->update(["debit" => $debit]);
        $cptCredit = AscCompte::where('numCompte', $achat->fournisseur->numCompte)->update(["credit" => $credit]);

        if($achat && $newJournalCharge && $cptDebit && $cptCredit){
            Toastr::success("La facture à été valider");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la validation de la facture !");
            return redirect()->back();
        }

    }

    public function showPdf($achats_id, Comite $comite){
        $achat = Achat::find($achats_id)->load('fournisseur', 'prestations', 'reglements');
        $infoCom = $comite->comite();
        $name = "Facture d'Achat N°".$achat->numAchat;
        $pdf = PDF::loadView('GestionAsc.Prestation.Achat.pdf', compact('achat', 'infoCom', 'name'));
        return $pdf->stream($name);
        //return view('GestionAsc.Prestation.Achat.pdf', compact('achat', 'infoCom', 'name'));
    }
}
class AchatOtherController extends AchatController{

    public static function countAchat(){
        $achats = Achat::all()->count();
        return $achats;
    }

    public static function etatAchatLabel($value){
        switch ($value){
            case 0: return '<span class="tag tag-dark"><i class="fa fa-pencil"></i> Brouillon</span>';
            case 1: return '<span class="tag tag-info"><i class="fa fa-check"></i> Valider</span>';
            case 2: return '<span class="tag tag-warning"><i class="fa fa-clock-o"></i> Partiellement Payer</span>';
            case 3: return '<span class="tag tag-success"><i class="fa fa-check-circle"></i> Payer</span>';
            default: return false;
        }
    }

    public static function etatAchatBackground($value){
        switch ($value){
            case 0: return 'bg-grey-600';
            case 1: return 'bg-blue-600';
            case 2: return 'bg-orange-600';
            case 3: return 'bg-green-600';
            default: return false;
        }
    }

    public static function etatAchatIcon($value){
        switch ($value){
            case 0: return 'fa-pencil';
            case 1: return 'fa-check';
            case 2: return 'fa-clock-o';
            case 3: return 'fa-check-circle';
            default: return false;
        }
    }

    public static function etatAchatText($value){
        switch ($value){
            case 0: return 'Brouillon';
            case 1: return 'Valider';
            case 2: return 'Partiellement Payer';
            case 3: return 'Payer';
            default: return false;
        }
    }

}
