<?php

namespace App\Model\ComptaFct\Configuration\Plan;

use Illuminate\Database\Eloquent\Model;

class FctPlanSector extends Model
{
    public function classe(){
        return $this->belongsTo(FctPlanClasse::class, 'numClasse');
    }

    public function comptes(){
        return $this->hasMany(FctPlanCompte::class, 'numSector');
    }
}
