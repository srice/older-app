<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('sup', function ($attribute, $value, $parameters){
            return $value >= $parameters[0];
        });

        Validator::extend('inf', function ($attribute, $value, $parameters){
            return $value <= $parameters[0];
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
