<?php

namespace App\Http\Controllers\Profil;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfilController extends Controller
{
    public function __construct()
    {

    }

    public function index(){
        $user = Auth::user()->load('role');

        return view('Profil.index', compact('user'));
    }
}
