<?php

namespace App\Model\GestionAsc\Billetterie;

use Illuminate\Database\Eloquent\Model;

class LigneBilletAd extends Model
{
    protected $guarded = [];

    public function billet(){
        return $this->belongsTo(BilletAd::class, 'billet_ads_id');
    }
}
