<?php

namespace App\Model\ComptaAsc\Configuration\Plan;

use App\Model\ComptaAsc\Configuration\Bilan\AscBilanInitial;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalAchat;
use Illuminate\Database\Eloquent\Model;

class ComptaPlanCompte extends Model
{
    protected $guarded = [];

    public function sector(){
        return $this->belongsTo(ComptaPlanSector::class, 'numSector');
    }

    public function achats(){
        return $this->hasMany(AscJournalAchat::class, 'numSector');
    }

    public function asc_compte(){
        return $this->belongsTo(AscCompte::class, 'numCompte');
    }

    public function bilan_initial(){
        return $this->belongsTo(AscBilanInitial::class, 'numCompte');
    }
}
