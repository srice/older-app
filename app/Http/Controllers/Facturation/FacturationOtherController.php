<?php

namespace App\Http\Controllers\Facturation;

use App\Model\Facturation\Facturation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacturationOtherController extends FacturationController
{
    public static function getCaVenteClient(){
        return [
            self::getValueByMontClient('01-01-'.date('Y')),
            self::getValueByMontClient('01-02-'.date('Y')),
            self::getValueByMontClient('01-03-'.date('Y')),
            self::getValueByMontClient('01-04-'.date('Y')),
            self::getValueByMontClient('01-05-'.date('Y')),
            self::getValueByMontClient('01-06-'.date('Y')),
            self::getValueByMontClient('01-07-'.date('Y')),
            self::getValueByMontClient('01-08-'.date('Y')),
            self::getValueByMontClient('01-09-'.date('Y')),
            self::getValueByMontClient('01-10-'.date('Y')),
            self::getValueByMontClient('01-11-'.date('Y')),
            self::getValueByMontClient('01-12-'.date('Y')),
        ];
    }

    public static function getCaFournisseur(){
        return [
            self::getValueByMontFournisseur('01-01-'.date('Y')),
            self::getValueByMontFournisseur('01-02-'.date('Y')),
            self::getValueByMontFournisseur('01-03-'.date('Y')),
            self::getValueByMontFournisseur('01-04-'.date('Y')),
            self::getValueByMontFournisseur('01-05-'.date('Y')),
            self::getValueByMontFournisseur('01-06-'.date('Y')),
            self::getValueByMontFournisseur('01-07-'.date('Y')),
            self::getValueByMontFournisseur('01-08-'.date('Y')),
            self::getValueByMontFournisseur('01-09-'.date('Y')),
            self::getValueByMontFournisseur('01-10-'.date('Y')),
            self::getValueByMontFournisseur('01-11-'.date('Y')),
            self::getValueByMontFournisseur('01-12-'.date('Y')),
        ];
    }

    public static function getValueByMontClient($dateStart){
        $strt = strtotime($dateStart);
        $start = Carbon::createFromTimestamp($strt);
        $end = Carbon::createFromTimestamp($strt)->endOfMonth();

        $select = Facturation::where('typesFacture', 0)
            ->where('dateFacture', '>=', $start)
            ->where('dateFacture', '<=', $end)
            ->get()->sum('totalFacture');

        return $select;
    }

    public static function getValueByMontFournisseur($dateStart){
        $strt = strtotime($dateStart);
        $start = Carbon::createFromTimestamp($strt);
        $end = Carbon::createFromTimestamp($strt)->endOfMonth();

        $select = Facturation::where('typesFacture', 1)
            ->where('dateFacture', '>=', $start)
            ->where('dateFacture', '<=', $end)
            ->get()->sum('totalFacture');

        return $select;
    }
}
