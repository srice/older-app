<?php

namespace App\Model\ComptaAsc\Configuration\Plan;

use Illuminate\Database\Eloquent\Model;

class ComptaPlanClasse extends Model
{
    protected $guarded = [];

    public function sectors(){
        return $this->hasMany(ComptaPlanSector::class, 'numClasse');
    }
}
