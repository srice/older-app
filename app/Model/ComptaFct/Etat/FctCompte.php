<?php

namespace App\Model\ComptaFct\Etat;

use App\Model\ComptaFct\Configuration\Plan\FctPlanCompte;
use Illuminate\Database\Eloquent\Model;

class FctCompte extends Model
{
    protected $guarded = [];

    public function comptes(){
        return $this->hasMany(FctPlanCompte::class, 'numCompte', 'numCompte');
    }
}
