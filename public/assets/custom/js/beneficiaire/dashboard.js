(function ($) {
    countSalarie()
    countAd()
})(jQuery);

function countSalarie(){
    let div = $("#countSalarie");
    $.get('beneficiaire/countSalarie')
        .done(function (data) {
            div.html(data);
        })
}
function countAd(){
    let div = $("#countAd");
    $.get('beneficiaire/countAd')
        .done(function (data) {
            div.html(data);
        })
}