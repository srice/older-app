<?php

namespace App\Model\ComptaAsc\Journal;

use Illuminate\Database\Eloquent\Model;

class AscJournalDiver extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ["created_at", "updated_at", "dateDivers"];
    }
}
