<?php

namespace App\Http\Controllers\Facturation\Facture;

use App\Model\Facturation\Facturation;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FactureController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'facturation';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $factures = Facturation::all()->load('tier', 'lignes', 'reglements');

        return view("Facturation.Facture.index", compact('config', 'factures'));
    }


}
