<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 15/08/2017
 * Time: 20:23
 */

namespace App\Packages\Bankin;


class Config
{
    protected $auth = [
        "clientId"      => null,
        "clientSecret"  => null,
        "endpoint"      => null
    ];

    public function __construct(array $auth = [])
    {
        $auth = collect($auth);

        if($auth->isEmpty() === false){
            $this->auth = collect($auth)->only([
                "clientId",
                "clientSecret",
                "endpoint"
            ]);
        }else{
            $this->loadAuthFromConfig();
        }
    }

    protected function loadAuthFromConfig(){
        $this->auth['clientId']         = config('bankin.clientId');
        $this->auth['clientSecret']     = config('bankin.clientSecret');
        $this->auth['endpoint']         = config('bankin.endpoint');

        return $this;
    }
}