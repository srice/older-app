@extends('template')
@section('title')
    Création d'un ticket d'assistance
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="/assets/global/vendor/summernote/summernote.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop

@section("content")
    <div class="panel">
        <header class="panel-header">
            <h3 class="panel-title"><i class="fa fa-plus"></i> Nouveau ticket d'assistance</h3>
        </header>
        <div class="panel-body">
            {{ Form::open(["route" => "tickets.store", "class" => "form-horizontal"]) }}
            <div class="form-group row">
                {{ Form::label('sujetTicket', 'Sujet du ticket *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::text('sujetTicket', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('departements_id', 'Département *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::select('departements_id', $departements, null, ["class" => "form-control round", "data-plugin" => "select2", "data-placeholder" => "Selectionner le département"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('services_id', 'Service concerné *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::select('services_id', $services, null, ["class" => "form-control round", "data-plugin" => "select2", "data-placeholder" => "Selectionner le département"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('priorities_id', 'Priorité *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    <select class="form-control round" name="priorities_id" data-placeholder="Selectionner la priorité">
                        <option value=""></option>
                        <option value="0">Basse</option>
                        <option value="1">Moyenne</option>
                        <option value="2">Haute</option>
                        <option value="3">Urgente</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('message', 'Votre problème *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-9">
                    {{ Form::textarea('message', null, ["class" => "form-control round", "data-plugin" => "summernote", "id" => "summernote"]) }}
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/select2/select2.full.min.js"></script>
    <script src="/assets/global/js/Plugin/select2.js"></script>
    <script src="/assets/global/vendor/summernote/summernote.min.js"></script>
    <script src="/assets/custom/js/assistance/ticket/create.js"></script>
@stop