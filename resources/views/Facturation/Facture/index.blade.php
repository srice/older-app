@extends('template')
@section("title")
    Listing des factures
@stop
@section("header_styles")
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="/assets/global/vendor/icheck/icheck.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">
            @yield("title")
        </h3>
        <div class="panel-actions">
            <a class="btn btn-primary white" href="#addFacture" data-toggle="modal"><i class="fa fa-plus-circle"></i> Nouvelle facture</a>
        </div>
    </div>
    <div class="panel-body">
        <table id="listeFacture" class="table">
            <thead>
                <tr>
                    <th>Numéro de facture</th>
                    <th>Tiers</th>
                    <th>Date de la facture</th>
                    <th>Total de la facture</th>
                    <th>Etat de la facture</th>
                    <th><i class="fa fa-bars"></i> </th>
                </tr>
            </thead>
            <tbody>
            @foreach($factures as $facture)
                <tr>
                    <td>{{ $facture->numInvoice }}</td>
                    <td>{{ $facture->tier->nomTier }}</td>
                    <td>{{ \App\Http\Controllers\OtherController::getDateFormated($facture->dateFacture, 'd/m/Y') }}</td>
                    <td>{{ \App\Http\Controllers\OtherController::euro($facture->totalFacture) }}</td>
                    <td>
                        <strong>Etat Générale</strong>: {!! \App\Http\Controllers\Facturation\Facture\FactureOtherController::etatGenerale($facture->etatFacture) !!}<br>
                        <strong>Sous Etat</strong>: {!! \App\Http\Controllers\Facturation\Facture\FactureOtherController::subEtat($facture->sousEtatFacture) !!}
                    </td>
                    <td>
                        {{ Form::model($facture, ["route" => ["facturation.facture.delete", $facture->id], "method" => "DELETE"]) }}
                        <a href="{{ route('facturation.facture.show', $facture->id) }}" class="btn btn-sm btn-icon btn-default" data-toggle="tooltip" title="Voir la facture"><i class="fa fa-eye"></i> </a>
                        <a href="{{ route('facturation.facture.edit', $facture->id) }}" class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" title="Editer la facture"><i class="fa fa-edit"></i> </a>
                        <a href="{{ route('facturation.facture.print', $facture->id) }}" class="btn btn-sm btn-icon bg-amber-500" data-toggle="tooltip" title="Imprimer la facture"><i class="fa fa-print"></i> </a>
                        <button class="btn btn-sm btn-icon btn-danger" data-toggle="tooltip" title="Supprimer la facture"><i class="fa fa-trash"></i> </button>
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="addFacture" aria-hidden="true" aria-labelledby="examplePositionSidebar"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Nouvelle facture</h4>
            </div>
            {{ Form::open(["route" => "facturation.tiers.store"]) }}
            <div class="modal-body">
                <div class="form-group form-material">
                    {{ Form::label('typeCompta', "Type de comptabilité", ["class" => "control-label"]) }}
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::radio('typeCompta', 0, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                            {{ Form::label('typeCompta', "Oeuvre Sociale", ["class" => "control-label"]) }}
                        </div>
                        <div class="col-md-6">
                            {{ Form::radio('typeCompta', 1, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                            {{ Form::label('typeCompta', "Budget de fonctionnement", ["class" => "control-label"]) }}
                        </div>
                    </div>
                </div>
                <div class="form-group form-material">
                    {{ Form::label('typeCompta', "Type de comptabilité", ["class" => "control-label"]) }}
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::radio('typeCompta', 0, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                            {{ Form::label('typeCompta', "Oeuvre Sociale", ["class" => "control-label"]) }}
                        </div>
                        <div class="col-md-6">
                            {{ Form::radio('typeCompta', 1, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                            {{ Form::label('typeCompta', "Budget de fonctionnement", ["class" => "control-label"]) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-block">Valider</button>
                <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section("footer_scripts")
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="/assets/global/vendor/icheck/icheck.min.js"></script>
    <script src="/assets/global/js/Plugin/icheck.js"></script>
    <script src="/assets/custom/js/facturation/facture/index.js"></script>
@stop    