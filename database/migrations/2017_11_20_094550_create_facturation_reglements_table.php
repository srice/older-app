<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturationReglementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturation_reglements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('factures_id');
            $table->timestamp('dateReglement')->nullable();
            $table->integer('modeReglementId')->comment("relation: FacturationModeReglement");
            $table->string('montantReglement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturation_reglements');
    }
}
