<?php

namespace App\Model\GestionAsc\Remboursement\Salarie;

use Illuminate\Database\Eloquent\Model;

class RembRegSalarie extends Model
{
    protected $guarded = [];

    public function remboursement(){
        return $this->belongsTo(RembSalarie::class, "remb_salarie_id");
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateReg"];
    }
}
