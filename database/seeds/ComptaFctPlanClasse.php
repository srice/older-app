<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComptaFctPlanClasse extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fct_plan_classes')->insert([
            "numClasse"     => 1,
            "nameClasse"    => "CAPITAUX"
        ]);
        DB::table('fct_plan_classes')->insert([
            "numClasse"     => 2,
            "nameClasse"    => "IMMOBILISATIONS"
        ]);
        DB::table('fct_plan_classes')->insert([
            "numClasse"     => 3,
            "nameClasse"    => "STOCKS"
        ]);
        DB::table('fct_plan_classes')->insert([
            "numClasse"     => 4,
            "nameClasse"    => "TIERS"
        ]);
        DB::table('fct_plan_classes')->insert([
            "numClasse"     => 5,
            "nameClasse"    => "FINANCES"
        ]);
        DB::table('fct_plan_classes')->insert([
            "numClasse"     => 6,
            "nameClasse"    => "CHARGES"
        ]);
        DB::table('fct_plan_classes')->insert([
            "numClasse"     => 7,
            "nameClasse"    => "PRODUITS"
        ]);
        DB::table('fct_plan_classes')->insert([
            "numClasse"     => 8,
            "nameClasse"    => "SPECIAUX"
        ]);
    }
}
