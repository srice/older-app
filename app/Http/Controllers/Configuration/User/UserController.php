<?php

namespace App\Http\Controllers\Configuration\User;

use App\Mail\Configuration\User\CreateUser;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Api;
use App\Packages\Srice\Espace;
use App\Role;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;
use Kamaln7\Toastr\Facades\Toastr;

class UserController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;
    public function __construct(Espace $espace, Mailer $mailer, Guard $auth)
    {
        $this->auth = $auth;
        $this->sector = 'configuration';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];

        $this->mailer = $mailer;
    }

    public function index(){

        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $users = User::where('id', '!=', 1)->get();

        return view('Configuration.User.index', compact('users', 'config'));
    }

    public function create(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        return view('Configuration.User.create', compact('config'));
    }

    public function store(Request $request, Api $api){
        $this->validate($request, [
            "name"      => "required|min:5",
            "email"     => "required|email|unique:users"
        ]);
        $password = UserOtherController::genPassword();
        $passCrypt = bcrypt($password);
        $user = User::create([
            "name"      => $request->name,
            "email"     => $request->email,
            "password"  => $passCrypt,
            "poste"     => $request->poste,
            "etat"      => 1
        ]);
        $this->mailer->to($request->email)->send(new CreateUser($user, $api->getId(), $password));
        if($user){
            Toastr::success("Un utilisateur à été créé !", "Nouvelle Utilisateur");
            return redirect()->route('config.user.index');
        }else{
            Toastr::error("Erreur lors de la création d'un utilisateur !", "Nouvelle Utilisateur");
            return redirect()->back();
        }
    }

    public function show($id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $user = User::find($id);

        return view('Configuration.User.show', compact('config', 'user'));
    }

    public function edit($id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $user = User::find($id);

        return view('Configuration.User.edit', compact('config', 'user'));
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        $this->validate($request, [
            "name"      => "required|min:5",
            "email"     => "required|email"
        ]);
        if(!empty($request->avatar)){
            $this->validate($request, [
                "avatar" => "image"
            ]);
            ImageManagerStatic::make($request->avatar)->fit(150, 150)->save(public_path("/assets/custom/images/profil/{$user->id}.png"));
            $user = User::find($id)->update([
                "avatar"    => true
            ]);
        }else{
            $user = User::find($id)->update([
                "name"  => $request->name,
                "email" => $request->email,
                "poste" => $request->poste
            ]);
        }

        if($user){
            Toastr::success("Un utilisateur à été edité !", "Edition d'un utilisateur");
            return redirect()->route('config.user.index');
        }else{
            Toastr::error("Erreur lors de l'édition d'un utilisateur !", "Edition d'un utilisateur");
            return redirect()->back();
        }
    }

    public function delete($id){
        $user = User::destroy($id);

        if($user){
            Toastr::success("Un utilisateur à été supprimé !", "Suppression d'un utilisateur");
            return redirect()->route('config.user.index');
        }else{
            Toastr::error("Erreur lors de la suppression d'un utilisateur !", "Suppression d'un utilisateur");
            return redirect()->back();
        }
    }

    public function lock($id){
        $user = User::find($id)->update(["etat" => 0]);

        if($user){
            Toastr::success("Un utilisateur à été Bloqué !", "Bloquage d'un utilisateur");
            return redirect()->route('config.user.index');
        }else{
            Toastr::error("Erreur lors du bloquage d'un utilisateur !", "Bloquage d'un utilisateur");
            return redirect()->back();
        }
    }

    public function unlock($id){
        $user = User::find($id)->update(["etat" => 1]);

        if($user){
            Toastr::success("Un utilisateur à été Débloqué !", "Débloquage d'un utilisateur");
            return redirect()->route('config.user.index');
        }else{
            Toastr::error("Erreur lors du débloquage d'un utilisateur !", "Débloquage d'un utilisateur");
            return redirect()->back();
        }
    }
}
class UserOtherController extends UserController{

    public static function genPassword(){
        $str = Str::random(7);
        return $str;
    }

    public static function etatUser($value){
        switch ($value){
            case 0: return '<span class="tag tag-danger"><i class="fa fa-lock"></i> Inactif</span>';
            case 1: return '<span class="tag tag-success"><i class="fa fa-unlock"></i> Actif</span>';
            default: return false;
        }
    }

}
