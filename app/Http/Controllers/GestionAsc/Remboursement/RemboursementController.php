<?php

namespace App\Http\Controllers\GestionAsc\Remboursement;

use App\Http\Controllers\GestionAsc\Remboursement\Salarie\RembOtherController;
use App\Http\Controllers\OtherController;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Model\GestionAsc\Remboursement\Salarie\RembRegSalarie;
use App\Model\GestionAsc\Remboursement\Salarie\RembSalarie;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RemboursementController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'gestionasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];


        $rembSalaries = RembSalarie::all()->load('salarie', 'prestations', 'reglements');

        return view('GestionAsc.Remboursement.index', compact('config', 'rembSalaries'));
    }

    public static function etatRembLabel($value){
        switch ($value){
            case 0: return "<span class='tag tag-default'><i class='fa fa-pencil'></i> Brouillon</span>";
            case 1: return "<span class='tag tag-info'><i class='fa fa-check'></i> Valider</span>";
            case 2: return "<span class='tag tag-warning'><i class='fa fa-clock-o'></i> Partiellement Remboursé</span>";
            case 3: return "<span class='tag tag-success'><i class='fa fa-check-circle'></i> Rembourser</span>";
            default: return false;
        }
    }

    public static function etatRembColor($value){
        switch ($value){
            case 0: return "bg-grey-600";
            case 1: return "bg-blue-600";
            case 2: return "bg-orange-600";
            case 3: return "bg-green-600";
            default: return false;
        }
    }

    public static function etatRembText($value){
        switch ($value){
            case 0: return "<i class='fa fa-pencil'></i> Brouillon";
            case 1: return "<i class='fa fa-check'></i> Valider";
            case 2: return "<i class='fa fa-clock-o'></i> Partiellement remboursé";
            case 3: return "<i class='fa fa-check-circle'></i> Rembourser";
            default: return false;
        }
    }

    public static function totalRembPrestation($montant, $partCe, $euro = true){
        $calc = $partCe;
        if($euro == true){
            return OtherController::euro($calc);
        }else{
            return $calc;
        }
    }

    public static function totalReglementByRemboursement($remb_id){
        $total = RembRegSalarie::where('remb_salarie_id', $remb_id)->get()->sum('montant');
        return $total;
    }
}
