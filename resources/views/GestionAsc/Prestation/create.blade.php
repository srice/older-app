@extends('template')
@section('title')
    Création d'une Prestation
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/summernote/summernote.css">
    <link rel="stylesheet" href="/assets/global/vendor/blueimp-file-upload/jquery.fileupload.css">
    <link rel="stylesheet" href="/assets/global/vendor/dropify/dropify.css">
    <link rel="stylesheet" href="/assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop

@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield('title')</h3>
        </header>
        <div class="panel-body">
            {{ Form::open(["route" => "prestations.store", "class" => "form-horizontal", "files" => "true"]) }}
            <div class="nav-tabs-vertical" data-plugin="tabs">
                <ul class="nav nav-tabs m-r-25" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" data-toggle="tab" href="#info" aria-controls="exampleTabsLeftOne" role="tab"><i class="fa fa-home"></i> Information</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#locale" aria-controls="exampleTabsLeftOne" role="tab"><i class="fa fa-map-marker"></i> Localisation</a>
                    </li>
                    @if($config->configuration['presta']->validite == 1)
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#dispo" aria-controls="exampleTabsLeftOne" role="tab"><i class="fa fa-calendar"></i> Disponibilité</a>
                    </li>
                    @endif
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#tarif" aria-controls="exampleTabsLeftOne" role="tab"><i class="fa fa-euro"></i> Tarification</a>
                    </li>
                    @if($config->moduleMenu[12]->state == 1)
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#fidelity" aria-controls="exampleTabsLeftOne" role="tab"><i class="fa fa-ticket"></i> Fidélité</a>
                    </li>
                    @endif
                </ul>
                <div class="tab-content p-y-15">
                    <div class="tab-pane active" id="info" role="tabpanel">
                        <div class="form-group row">
                            {{ Form::label('familles_id', 'Famille *', ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-6">
                                {{ Form::select('familles_id', $familles, null, ["class" => "form-control"]) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('name', 'Nom de la prestation *', ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-6">
                                {{ Form::text('name', null, ["class" => "form-control"]) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('descriptif', 'Descriptif', ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-6">
                                {{ Form::textarea('descriptif', null, ["class" => "form-control", "id" => "summernote", "data-plugin" => "summernote"]) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('visuel', 'Visuel', ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-3">
                                {{ Form::file('visuel', ["class" => "form-control", "id" => "input-file-now-custom-2", "data-plugin" => "dropify", "data-height" => "350"]) }}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="locale" role="tabpanel">
                        <div class="form-group row">
                            {{ Form::label('type', 'Titre', ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-6">
                                {{ Form::text('type', null, ["class" => "form-control"]) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('adresse', 'Adresse', ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-6">
                                {{ Form::textarea('adresse', null, ["class" => "form-control"]) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('codePostal', 'Code Postal', ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-2">
                                {{ Form::text('codePostal', null, ["class" => "form-control"]) }}
                            </div>
                            {{ Form::label('ville', 'Ville', ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-4">
                                {{ Form::text('ville', null, ["class" => "form-control"]) }}
                            </div>
                        </div>
                    </div>
                    @if($config->configuration['presta']->validite == 1)
                    <div class="tab-pane" id="dispo" role="tabpanel">
                        <div class="form-group row">
                            <div class="col-md-10">
                                {{ Form::checkbox('representative', 1, null, ["data-plugin" => "switchery", "id" => "representative", "onChange" => "appearRepresentative()"]) }}
                                <label class="p-t-3" for="inputBasicOff">Unique représentation</label>
                            </div>
                        </div>
                        <div id="dateRepresentative">
                            <div class="form-group row">
                                {{ Form::label('dateStart', 'Date de la représentation Unique', ["class" => "col-md-3 control-label"]) }}
                                <div class="col-md-2">
                                    {{ Form::text('dateStartUnique', null, ["class" => "form-control dateStartUnique"]) }}
                                </div>
                                <div class="col-md-2">
                                    {{ Form::text('heureStartUnique', null, ["class" => "form-control hour"]) }}
                                </div>
                            </div>
                        </div>
                        <div id="newSeance">
                            <div class="form-group row">
                                {{ Form::label('dateStart', "Date de début", ["class" => "col-md-3 control-label"]) }}
                                <div class="col-md-2">
                                    {{ Form::text('dateStart', null, ["class" => "form-control dateStart"]) }}

                                </div>
                                {{ Form::label('dateEnd', "Date de Fin", ["class" => "col-md-3 control-label"]) }}
                                <div class="col-md-2">
                                    {{ Form::text('dateEnd', null, ["class" => "form-control dateEnd"]) }}

                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="tab-pane" id="tarif" role="tabpanel">
                        <div class="form-group row">
                            {{ Form::label("libelle", "Intitulé du tarif *", ["class" => "col-md-3 control-label required"]) }}
                            <div class="col-md-6">
                                {{ Form::text('libelle', null, ["class" => "form-control"]) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label("price_sal", "Participation Salarié *", ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-2">
                                {{ Form::text('price_sal', null, ["class" => "form-control"]) }}
                                <i>Format: <strong>0.00</strong></i>
                            </div>
                            {{ Form::label("price_ce", "Participation Comité *", ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-2">
                                {{ Form::text('price_ce', null, ["class" => "form-control"]) }}
                                <i>Format: <strong>0.00</strong></i>
                            </div>
                        </div>
                        @if($config->configuration['presta']->quota == 1)
                        <div class="form-group row">
                            {{ Form::label("quota", "Quota", ["class" => "col-md-3 control-label"]) }}
                            <div class="col-md-1">
                                {{ Form::text('quota', null, ["class" => "form-control"]) }} / par salarié
                            </div>
                        </div>
                        <hr>
                        @endif
                        @if($config->configuration['presta']->stock == 1)
                        <div class="form-group row">
                            <div class="col-md-10">
                                {{ Form::checkbox('stock', 1, null, ["data-plugin" => "switchery", "id" => "stock", "onchange" => "appearStock()"]) }}
                                <label class="p-t-3" for="inputBasicOff">Prise en charge du stock</label>
                            </div>
                        </div>
                        <div id="stockInfo">
                            <div class="form-group row">
                                {{ Form::label('stockLimit', "Limite de Stock", ["class" => "col-md-3 control-label"]) }}
                                <div class="col-md-1">
                                    {{ Form::text('stockLimit', null, ["class" => "form-control"]) }}
                                    <i>Pour désactiver la fonction, mettez <strong>0</strong></i>
                                </div>
                                {{ Form::label('stockActuel', "Stock Actuel", ["class" => "col-md-3 control-label"]) }}
                                <div class="col-md-1">
                                    {{ Form::text('stockActuel', null, ["class" => "form-control"]) }}
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    @if($config->moduleMenu[12]->state == 1)
                    <div class="tab-pane" id="fidelity" role="tabpanel">
                        <div class="form-group row">
                            {{ Form::label('compteurs_id', "Compteur de fidélité", ["class" => "control-label col-md-3"]) }}
                            <div class="col-md-6">
                                <div class="example">
                                    <select name="compteurs_id" class="form-control" data-plugin="selectpicker" data-live-search="true">
                                        @foreach($compteurs as $compteur)
                                            <option value="{{ $compteur->id }}" data-content="Compteur {{ $compteur->nameCompteur }}<br>"></option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                    @endif
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/summernote/summernote.min.js"></script>
    <script src="/assets/global/vendor/blueimp-tmpl/tmpl.js"></script>
    <script src="/assets/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
    <script src="/assets/global/vendor/blueimp-load-image/load-image.all.min.js"></script>
    <script src="/assets/global/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
    <script src="/assets/global/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
    <script src="/assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
    <script src="/assets/global/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>
    <script src="/assets/global/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>
    <script src="/assets/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
    <script src="/assets/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
    <script src="/assets/global/vendor/dropify/dropify.min.js"></script>
    <script src="/assets/global/vendor/switchery/switchery.min.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/switchery.js"></script>
    <script src="/assets/global/js/Plugin/dropify.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="/assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-select.js"></script>
    <script src="/assets/custom/js/gestionasc/prestation/create.js"></script>
@stop