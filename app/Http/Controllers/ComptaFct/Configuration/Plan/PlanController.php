<?php

namespace App\Http\Controllers\ComptaFct\Configuration\Plan;

use App\Model\ComptaFct\Configuration\Plan\FctPlanClasse;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptafct';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $classes = FctPlanClasse::all();

        return view('ComptaFct.Configuration.Plan.index', compact('config', 'classes'));
    }
}
