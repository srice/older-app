@extends('template')
@section('title')
    Ticket N°{{ $ticket->numTicket }}
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/summernote/summernote.css">
    <link rel="stylesheet" href="/assets/template/examples/css/apps/message.css">
@stop

@section("content")
    <div class="row">
        <div class="col-md-4">
            <div class="panel">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td style="font-weight: bold;">Numéro du ticket</td>
                            <td>{{ $ticket->numTicket }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Département</td>
                            <td>{{ \App\Http\Controllers\Assistance\Ticket\TicketOtherController::nameDepartement($ticket->departements_id) }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Service</td>
                            <td>{{ \App\Http\Controllers\Assistance\Ticket\TicketOtherController::nameService($ticket->services_id) }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Priorité</td>
                            <td>{{ \App\Http\Controllers\Assistance\Ticket\TicketOtherController::namePriorities($ticket->priorities_id) }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Etat du ticket</td>
                            <td id="stateTicket" data-id="{{ $ticket->id }}"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel">
                <header class="panel-heading">
                    <h3 class="panel-title">{{ $ticket->sujetTicket }}</h3>
                </header>
                <div id="contentChat" data-id="{{ $ticket->id }}"></div>
            </div>
            <div class="panel">
                <div class="panel-body">
                    {{ Form::model($ticket, ["route" => ["tickets.reply", $ticket->id], "class" => "form-horizontal", "id" => "formAddMessage"]) }}
                    {{ Form::hidden('tickets_id', $ticket->id, ["id" => "tickets_id"]) }}
                    <div class="form-group row">
                        <div class="col-md-12">
                            {{ Form::textarea('message', null, ["class" => "form-control round", "data-plugin" => "summernote", "id" => "summernote"]) }}
                        </div>
                    </div>
                    <div class="text-xs-right">
                        @if($ticket->etatTicket == 3)
                            <button class="btn btn-success" disabled>Valider</button>
                        @else
                            <button class="btn btn-success">Valider</button>
                        @endif
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/summernote/summernote.min.js"></script>
    <script src="/assets/custom/js/assistance/ticket/show.js"></script>
@stop