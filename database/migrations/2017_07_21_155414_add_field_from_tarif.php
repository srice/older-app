<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFromTarif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prestation_tarifs', function (Blueprint $table) {
            $table->string('price_sal');
            $table->string('price_ce');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prestation_tarifs', function (Blueprint $table) {
            $table->removeColumn('price_sal');
            $table->removeColumn('price_ce');
        });
    }
}
