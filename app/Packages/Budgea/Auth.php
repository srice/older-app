<?php


namespace App\Packages\Budgea;


class Auth extends Budgea
{
    public function __construct($settings = [])
    {
        parent::__construct($settings);
    }

    public function generateJwtManagerToken($scope = 'config', $duration = 1)
    {
        return $this->fetch('/admin/jwt', [
            "scope" => $scope,
            "duration" => $duration
        ], "POST");
    }

    public function createAnonymousUser()
    {
        return $this->fetch('/auth/init', [
            "client_id" => env("BUDGEA_CLIENT_ID"),
            "client_secret" => env("BUDGEA_CLIENT_SECRET")
        ], 'POST');
    }

    public function generateJwtUserToken($id_user)
    {
        return $this->fetch('/auth/jwt', [
            "client_id" => env("BUDGEA_CLIENT_ID"),
            "client_secret" => env("BUDGEA_CLIENT_SECRET"),
            "id_user"       => $id_user
        ], 'POST');
    }

    public function removeUserAccess()
    {
        return $this->fetch('/auth/token', [], 'DELETE');
    }

    public function createAccessToken($user_code)
    {
        return $this->fetch('/auth/token/access', [
            "client_id" => env("BUDGEA_CLIENT_ID"),
            "client_secret" => env("BUDGEA_CLIENT_SECRET"),
            "code"          => $user_code,
            "redirect_uri"  => env("APP_URL").'/banker'
        ], 'POST');
    }


}