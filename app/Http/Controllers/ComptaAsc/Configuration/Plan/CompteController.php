<?php

namespace App\Http\Controllers\ComptaAsc\Configuration\Plan;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanClasse;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanSector;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class CompteController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptaasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index($classe_id, $sector_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $classe = ComptaPlanClasse::where('numClasse', $classe_id)->first();
        $sector = ComptaPlanSector::where('numSector', $sector_id)->first();
        $comptes = ComptaPlanCompte::where('numSector', $sector_id)->get();

        return view('ComptaAsc.Configuration.Plan.Compte.index', compact('config', 'classe', 'sector', 'comptes'));
    }

    public function store(Request $request, $classes_id, $sectors_id){
        $this->validate($request, [
            "numCompte"     => "required",
            "nameCompte"    => "required|min:2"
        ]);

        $add = ComptaPlanCompte::create([
            "numSector"     => $sectors_id,
            "numCompte"     => $request->numCompte,
            "nameCompte"    => $request->nameCompte
        ]);

        $cpt = AscCompte::create(["numCompte" => $request->numCompte]);

        if($add && $cpt){
            Toastr::success("Un compte à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du compte");
            return redirect()->back();
        }
    }

    public function edit($classes_id, $sectors_id, $comptes_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $classe = ComptaPlanClasse::where('numClasse', $classes_id)->first();
        $sector = ComptaPlanSector::where('numSector', $sectors_id)->first();
        $compte = ComptaPlanCompte::where('numCompte', $comptes_id)->first();

        return view('ComptaAsc.Configuration.Plan.Compte.edit', compact('config', 'classe', 'sector', 'compte'));
    }

    public function update(Request $request, $classes_id, $sectors_id, $comptes_id){
        $this->validate($request, [
            "numCompte"     => "required",
            "nameCompte"    => "required|min:2"
        ]);

        $edit = ComptaPlanCompte::where('numCompte', $comptes_id)->first()->update([
            "numCompte"     => $request->numCompte,
            "nameCompte"    => $request->nameCompte
        ]);

        $cpt = AscCompte::where('numCompte', $comptes_id)->first()->update(["numCompte" => $request->numCompte]);

        if($edit && $cpt){
            Toastr::success("Le compte à été édité");
            return redirect()->route('comptaAsc.config.plan.compte.index', [$classes_id, $sectors_id]);
        }else{
            Toastr::error("Erreur lors de l\'édition du compte");
            return redirect()->back();
        }
    }

    public function delete($classes_id, $sectors_id, $comptes_id){
        $del = ComptaPlanCompte::where('numCompte', $comptes_id)->first()->delete();
        $cpt = AscCompte::where('numCompte', $comptes_id)->first()->delete();

        if($del && $cpt){
            Toastr::success("Le compte à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du compte");
            return redirect()->back();
        }
    }
}
