(function () {
    $(".date").datepicker({
        language: 'fr',
        format: "dd-mm-yyyy"
    });
})(jQuery);

function showNum(){
    var modeReglement = $("#modeReglements option:selected").val();
    if(modeReglement == 1){
        $("#numTransaction").show();
    }else{
        $("#numTransaction").hide();
    }
}