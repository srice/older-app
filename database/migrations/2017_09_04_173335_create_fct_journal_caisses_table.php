<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFctJournalCaissesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fct_journal_caisses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numCaisse');
            $table->timestamp('dateCaisse')->nullable();
            $table->string('numCompte');
            $table->string('libelleCaisse');
            $table->string('debitCaisse')->default(0);
            $table->string('creditCaisse')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fct_journal_caisses');
    }
}
