<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 15/08/2017
 * Time: 22:15
 */

namespace App\Packages\Bankin;


class Items extends Appel
{
    public function connect($bearer, $bankId){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "bank_id"       => $bankId,
            "access_token"  => $bearer
        ];

        $call = $this->get('items/connect?', $params, false, '', true);

        return $call;
    }

    public function listItems($bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "limit"         => 50
        ];

        $call = $this->get('items?', $params, true, $bearer);

        return $call;
    }

    public function item($item_id, $bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
        ];

        $call = $this->get('items/'.$item_id.'?', $params, true, $bearer);

        return $call;
    }

    public function editItem($items_id, $bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "access_token"  => $bearer
        ];

        $call = $this->get('items/'.$items_id.'/edit?', $params, false, '', true);

        return $call;
    }

    public function refreshItem($items_id, $bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
        ];

        $call = $this->post('items/'.$items_id.'/refresh?', $params, true, $bearer);

        return $call;
    }

    public function statusRefresh($items_id, $bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
        ];

        $call = $this->get('items/'.$items_id.'/refresh/status?', $params, true, $bearer);

        return $call;
    }

    public function deleteItem($items_id, $bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
        ];

        $call = $this->delete('items/'.$items_id.'?', $params, true, $bearer);

        return $call;
    }
}