<?php

namespace App\Http\Controllers\Configuration\Comite;

use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Comite;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComiteController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;
    public function __construct(Espace $espace, Mailer $mailer, Guard $auth)
    {
        $this->auth = $auth;
        $this->sector = 'configuration';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];

        $this->mailer = $mailer;
    }

    public function index(Comite $comite, Espace $espace){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $infoComite = $comite->comite();
        $espaceInfo = $espace->espace();
        $licence = $espace->licence();

        return view('Configuration.Comite.index', compact('config', 'infoComite', 'espaceInfo', 'licence'));
    }

    public function timeout(Espace $espace){
        $info = $espace->espace();
        $licence = $espace->licence();

        $dateStart = strtotime($licence->start);
        $dateEnd = strtotime($licence->end);

        $dtst = Carbon::createFromTimestamp($dateStart);
        $dtse = Carbon::createFromTimestamp($dateEnd);
        $now = Carbon::now();

        $diffNow = $dtse->diffInDays($now);
        $diffAll = $dtse->diffInDays($dtst);

        $calc = $diffNow*100/$diffAll;

        if($calc == 0){
            $percent = 0;
        }else{
            $percent = $calc;
        }

        $prec = round($percent, 2);

        ob_start();
        ?>
        <h3 id="" class="text-xs-center">Il reste <?= $diffNow; ?> jours</h3>
        <?php if($percent > 66): ?>
            <div class="progress">
                <div class="progress-bar progress-bar-success progress-bar-striped active" aria-valuenow="<?= $percent; ?>" aria-valuemin="0"
                     aria-valuemax="100" style="width: <?= $percent; ?>%" role="progressbar">
                    <span class="sr-only">90% Complete</span>
                </div>
            </div>
        <?php elseif ($percent <= 66 && $percent >= 33): ?>
            <div class="progress">
                <div class="progress-bar progress-bar-warning progress-bar-striped active" aria-valuenow="<?= $percent; ?>" aria-valuemin="0"
                     aria-valuemax="100" style="width: <?= $percent; ?>%" role="progressbar">
                    <span class="sr-only">90% Complete</span>
                </div>
            </div>
        <?php else: ?>
            <div class="progress">
                <div class="progress-bar progress-bar-danger progress-bar-striped active" aria-valuenow="<?= $percent; ?>" aria-valuemin="0"
                     aria-valuemax="100" style="width: <?= $percent; ?>%" role="progressbar">
                    <span class="sr-only">90% Complete</span>
                </div>
            </div>
        <?php endif; ?>
        <?php
        $content = ob_get_clean();
        return $content;
    }
}
