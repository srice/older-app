@extends('template')
@section("title")
    Edition du tier: <strong>{{ $tier->nomTier }}</strong>
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/icheck/icheck.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Edition du Tier</h3>
    </div>
    {{ Form::model($tier, ["route" => ["facturation.tiers.update", $tier->id], "class" => "form-horizontal", "method" => "PUT", "id" => "formEditTier"]) }}
    <div class="panel-body">
        <div class="form-group row form-material">
            {{ Form::label('typeTier', "Type de tier", ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::radio('typeTier', 0, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                    {{ Form::label('typeTier', "Client", ["class" => "control-label"]) }}
                </div>
                <div class="col-md-3">
                    {{ Form::radio('typeTier', 1, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                    {{ Form::label('typeTier', "Fournisseur", ["class" => "control-label"]) }}
                </div>
        </div>
        <div class="form-group row form-material">
            {{ Form::label('nomTier', "Nom ou Raison Social", ["class" => "form-control-label col-md-3"]) }}
            <div class="col-md-6">
                {{ Form::text('nomTier', null, ["class" => "form-control", "required"]) }}
            </div>
        </div>
        <div class="form-group row form-material">
            {{ Form::label('adresseTier', "Adresse", ["class" => "form-control-label col-md-3"]) }}
            <div class="col-md-6">
                {{ Form::textarea('adresseTier', null, ["class" => "form-control", "required"]) }}
            </div>
        </div>
        <div class="form-group row form-material">
            {{ Form::label('codePostalTier', "Code Postal", ["class" => "form-control-label col-md-3"]) }}
            <div class="col-md-2">
                {{ Form::text('codePostalTier', null, ["class" => "form-control", "required"]) }}
            </div>
        </div>
        <div class="form-group row form-material">
            {{ Form::label('villeTier', "Ville", ["class" => "form-control-label col-md-3"]) }}
            <div class="col-md-6">
                {{ Form::text('villeTier', null, ["class" => "form-control", "required"]) }}
            </div>
        </div>
    </div>
    <div class="panel-footer text-xs-right">
        <button id="btnEditTier" class="btn btn-primary"><i class="fa fa-check"></i> Modifier le tier</button>
    </div>
    {{ Form::close() }}
</div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/icheck/icheck.min.js"></script>
    <script src="/assets/global/js/Plugin/icheck.js"></script>
    <script src="/assets/custom/js/facturation/tiers/edit.js"></script>
@stop    