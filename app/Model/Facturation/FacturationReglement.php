<?php

namespace App\Model\Facturation;

use Illuminate\Database\Eloquent\Model;

class FacturationReglement extends Model
{
    protected $guarded = [];

    public function facture(){
        return $this->belongsTo(Facturation::class);
    }

    public function modeReglement(){
        return $this->belongsTo(FacturationModeReglement::class);
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateReglement"];
    }
}
