@extends('template')
@section("title")
    Gestion des comptes
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-header">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste de mes comptes bancaires</h3>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('banker.accounts.connect') }}" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Ajout un compte</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Désignation du compte</th>
                            <th>Etat</th>
                            <th>Solde</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($accounts as $account)
                            <tr>
                                <td>{{ $account->name }}</td>
                                <td>{{ \App\HelperClass\Banker\BankerAccount::viewType($account->type) }}</td>
                                <td>{!! \App\HelperClass\Banker\BankerAccount::formatSoldeAccount($account->balance) !!}</td>
                                <td>
                                    <a href="{{ route('banker.accounts.show', $account->id) }}" class="btn btn-icon btn-info" data-toggle="tooltip" title="Voir les information du compte"><i class="fa fa-eye"></i> </a>
                                    <a href="" class="btn btn-icon btn-danger" data-toggle="tooltip" title="Supprimer ce compte"><i class="fa fa-trash"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2" style="text-align: right; font-weight: bold; font-size: 15px;">Total de vos comptes bancaire</td>
                            <td style="text-align: right; font-weight: bold; font-size: 15px;">{!! \App\HelperClass\Banker\BankerAccount::getTotalForAccounts($accounts) !!}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    