<?php

namespace App\Model\Beneficiaire\Salarie;

use App\Model\Ancv\AncvVente;
use App\Model\Beneficiaire\Ad\AyantDroit;
use App\Model\GestionAsc\Billetterie\BilletSalarie;
use Illuminate\Database\Eloquent\Model;

class Salarie extends Model
{
    protected $guarded = [];

    public function ad(){
        return $this->hasMany(AyantDroit::class, 'salaries_id');
    }

    public function fidelities(){
        return $this->hasMany(SalarieFidelity::class, 'salaries_id');
    }

    public function billet(){
        return $this->belongsTo(BilletSalarie::class, 'salaries_id');
    }

    public function ancvs()
    {
        return $this->hasMany(AncvVente::class);
    }
}
