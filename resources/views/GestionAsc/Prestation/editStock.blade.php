@extends('template')
@section('title')
    Edition du stock
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield("title")</h3>
        </header>
        <div class="panel-body">
            {{ Form::model($stock, ["route" => ["stock.updateStock", $prestations_id, $stock->id], "class" => "form-horizontal", "method" => "PUT"]) }}
            <div class="form-group form-material row">
                {{ Form::label('stock', "Limite de stock *", ["class" => "control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('stockLimit', null, ["class" => "form-control", "placeholder" => "Limite de stock"]) }}
                </div>
            </div>
            <div class="form-group form-material row">
                {{ Form::label('stock', "Stock Actuel *", ["class" => "control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('stockActuel', null, ["class" => "form-control", "placeholder" => "Limite de stock"]) }}
                </div>
            </div>
            <div class="form-group form-material row">
                {{ Form::label('stock', "Limite de commande *", ["class" => "control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('quota', null, ["class" => "form-control", "placeholder" => "Limite de stock"]) }} /par salarié
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('footer_scripts')
    
@stop