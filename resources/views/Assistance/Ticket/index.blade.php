@extends('template')
@section('title')
    Liste de mes tickets d'assistance
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="panel">
        <header class="panel-header">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title"><i class="fa fa-list"></i> @yield('title')</h3>
                </div>
                <div class="col-md-2 m-t-20">
                    <a href="{{ route('tickets.create') }}" class="btn btn-sm btn-primary btn-block"><i class="fa fa-plus-circle"></i> Nouveau Ticket</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Service</th>
                            <th>Département</th>
                            <th>Sujet</th>
                            <th>Dernier Intervenant</th>
                            <th>Créé le</th>
                            <th>Maj</th>
                            <th>Etat</th>
                            <th><i class="fa fa-cog"></i> </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tickets as $ticket)
                        <tr>
                            <td>{{ $ticket->numTicket }}</td>
                            <td>{{ \App\Http\Controllers\Assistance\Ticket\TicketOtherController::nameService($ticket->services_id) }}</td>
                            <td>{{ \App\Http\Controllers\Assistance\Ticket\TicketOtherController::nameDepartement($ticket->departements_id) }}</td>
                            <td>{{ $ticket->sujetTicket }}</td>
                            <td>{{ \App\Http\Controllers\Assistance\Ticket\TicketOtherController::lastNameChat($ticket->id) }}</td>
                            <td>{{ \App\Http\Controllers\OtherController::getDateFormated($ticket->created_at, 'd/m/Y à H:i') }}</td>
                            <td>{{ \App\Http\Controllers\OtherController::getDateFormated($ticket->updated_at, 'd/m/Y à H:i') }}</td>
                            <td>{!! \App\Http\Controllers\Assistance\Ticket\TicketOtherController::etatTicketTag($ticket->etatTicket) !!}</td>
                            <td>
                                <a href="{{ route('tickets.show', $ticket->id) }}" class="btn btn-sm btn-icon btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Voir le ticket"><i class="fa fa-eye"></i> </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    
@stop