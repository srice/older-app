<?php

namespace App\Http\Controllers\Facturation;

use App\Http\Controllers\OtherController;
use App\Model\Facturation\Facturation;
use App\Model\Facturation\FacturationTier;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Espace;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacturationController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'facturation';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function dashboard(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        /**
         * Nombre de Tiers
         * Total de factures (Euros)
         * Total de factures (Euros non payer)
         * Graphique recette / dépense
         */

        return view('Facturation.dashboard', compact('config'));

    }

    public function widget(){
        sleep(2);
        $tiers = FacturationTier::all()->count();
        $factureAll = Facturation::all()->sum('totalFacture');
        $factureAsUnpaid = Facturation::where('etatFacture', 1)->where('sousEtatFacture', 1)->orWhere('sousEtatFacture', 3)->sum('totalFacture');
        ob_start();
        ?>
        <div class="col-md-4">
            <div class="card card-block p35">
                <div class="pull-xs-left white">
                    <i class="icon icon-circle icon-2x md-accounts-list bg-orange-500"></i>
                </div>
                <div class="counter counter-md counter text-xs-right pull-xs-right">
                    <div class="counter-number-group">
                        <span class="counter-number"><?= $tiers; ?></span>
                        <span class="counter-number-related text-capitalize">Tiers</span>
                    </div>
                    <div class="counter-label text-capitalize font-size-16">enregistrés</div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-block p35">
                <div class="pull-xs-left white">
                    <i class="icon icon-circle icon-2x md-shopping-basket bg-green-500"></i>
                </div>
                <div class="counter counter-md counter text-xs-right pull-xs-right">
                    <div class="counter-number-group">
                        <span class="counter-number"><?= OtherController::euro($factureAll); ?></span>
                    </div>
                    <div class="counter-label text-capitalize font-size-16">de chiffre d'affaire</div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-block p35">
                <div class="pull-xs-left white">
                    <i class="icon icon-circle icon-2x md-shopping-cart bg-red-500"></i>
                </div>
                <div class="counter counter-md counter text-xs-right pull-xs-right">
                    <div class="counter-number-group">
                        <span class="counter-number"><?= OtherController::euro($factureAsUnpaid); ?></span>
                    </div>
                    <div class="counter-label text-capitalize font-size-16">de facture impayer</div>
                </div>
            </div>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function graph(){
        $chart = Charts::multi('line', 'chartjs')
            ->title("Chiffre d'affaire du secteur de facturation")
            ->dimensions(0,600)
            ->template('material')
            ->dataset('Clients', FacturationOtherController::getCaVenteClient())
            ->dataset('Fournisseurs', FacturationOtherController::getCaFournisseur())
            ->elementLabel('Euros')
            ->labels(['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre']);
        ob_start();
        ?>
        <div class="card">
            <?= $chart->render(); ?>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }
}
