(function ($) {
    $("#soldeInitCase").hide();
    appearSolde();
    $("#ageLimitCase").hide();
    appearAge();
})(jQuery)

function appearSolde(){
    if(document.getElementById('inputSoldeActive').checked === true){
        $("#soldeInitCase").show();
    }else{
        $("#soldeInitCase").hide();
    }
}

function appearAge(){
    if(document.getElementById('inputAgeLimit').checked === true){
        $("#ageLimitCase").show();
    }else{
        $("#ageLimitCase").hide();
    }
}