<?php

namespace App\Mail\Automate;

use App\Model\Automate\Automate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AutomatingMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Automate
     */
    public $automate;

    /**
     * Create a new message instance.
     *
     * @param Automate $automate
     */
    public function __construct(Automate $automate)
    {
        //
        $this->automate = $automate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Email.Automate.mail')->subject('Journal d\'automatisation pour l\'espace :'.env("APP_URL"));
    }
}
