<?php

namespace App\Http\Controllers\GestionAsc\Prestation\Achat;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\GestionAsc\Prestation\Achat\Fournisseur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class FournisseurController extends AchatController
{
    public function store(Request $request){

        $this->validate($request, [
            "name"  => "required"
        ]);


        $cond = str_limit($request->name, 3, '');
        $numCompte = "401".$cond;

        $fournisseur = Fournisseur::create([
            "name" => $request->name,
            "numCompte" => $numCompte
        ]);

        $cpt = ComptaPlanCompte::create([
            "numCompte"     => $numCompte,
            "numSector"     => "40",
            "nameCompte"    => $request->name
        ]);

        $compte = AscCompte::create(["numCompte" => $numCompte]);

        if($fournisseur && $cpt && $compte){
            Toastr::success("Fournisseur Ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du fournisseur !");
            return redirect()->back();
        }
    }

    public function edit($id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $fournisseur = Fournisseur::find($id);

        return view('GestionAsc.Prestation.Fournisseur.edit', compact('config', 'fournisseur'));
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            "name"  => "required"
        ]);

        $fournisseur = Fournisseur::find($id);

        $cond = str_limit($request->name, 3, '');
        $numCompte = "401".$cond;


        $cpt = ComptaPlanCompte::where('numCompte', $fournisseur->numCompte)->update([
            "numCompte"     => $numCompte,
            "numSector"     => "40",
            "nameCompte"    => $request->name
        ]);

        $fournisseur->update([
            "name"      => $request->name,
            "numCompte" => $numCompte
        ]);

        $compte = AscCompte::where('numCompte', $fournisseur->numCompte)->update([
            "numCompte" => $numCompte
        ]);

        if($fournisseur && $cpt && $compte){
            Toastr::success("Fournisseur édité");
            return redirect()->route('achats.index');
        }else{
            Toastr::error("Erreur lors de l\'édition du fournisseur !");
            return redirect()->back();
        }
    }

    public function delete($id){
        $fournisseur = Fournisseur::find($id);

        $cond = str_limit($fournisseur->name, 3, '');
        $numCompte = "401".$cond;


        if($this->countAchatOnFournisseur($id) == 0){
            $fournisseur = Fournisseur::destroy($id);
            $cpt = ComptaPlanCompte::where('numCompte', $numCompte)->delete();
            $compte = AscCompte::where('numCompte', $numCompte)->delete();

            if($fournisseur && $cpt && $compte){
                Toastr::success("Fournisseur supprimé");
                return redirect()->back();
            }else{
                Toastr::error("Erreur lors de la suppression du fournisseur !");
                return redirect()->back();
            }
        }else{
            Toastr::warning("Impossible de supprimer le fournisseur car des achats ont été effectués avec celui-ci !");
            return redirect()->back();
        }
    }

    private function countAchatOnFournisseur($fournisseur_id){
        $count = Achat::where('fournisseurs_id', $fournisseur_id)->count();
        return $count;
    }
}
