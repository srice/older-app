<?php

namespace App\Model\GestionAsc\Remboursement\Salarie;

use Illuminate\Database\Eloquent\Model;

class RembPrestaSalarie extends Model
{
    protected $guarded = [];

    public function remboursement(){
        return $this->belongsTo(RembSalarie::class, 'remb_salarie_id');
    }

}
