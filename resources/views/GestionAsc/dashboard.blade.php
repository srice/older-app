@extends('template')
@section('title')
    Tableau de Bord
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/template/examples/css/widgets/statistics.css">
@stop

@section("content")
    <div class="row">
        <div class="col-md-3">
            <div class="card card-block p-20 bg-blue-600">
                <div class="counter counter-lg counter-inverse">
                    <div class="counter-label text-uppercase font-size-16">Il y a</div>
                    <div class="counter-number-group">
                        <span class="counter-number">{{ \App\Http\Controllers\GestionAsc\GestionAscController::countPrestation() }}</span>
                        <span class="counter-icon m-l-10"><i class="icon fa-cubes" aria-hidden="true"></i></span>
                    </div>
                    <div class="counter-label text-uppercase font-size-16">Prestations</div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            @if($config->configuration['presta']->validite == 1)
                <div class="panel">
                    <header class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-warning text-warning"></i> Prestations dont la validité est passé</h3>
                    </header>
                    <div class="panel-body">
                        @if(\App\Http\Controllers\GestionAsc\GestionAscController::countPrestaOutdate() == 0)
                            <h1><i class="icon wb-thumb-up text-success"></i> Aucune Prestation hors de la validation</h1>
                        @else
                            <ul>
                                @foreach($prestationValidites as $validite)
                                    <li>
                                        <a href="{{ route('prestations.show', $validite->prestations_id) }}">{{ $validite->prestation->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            @endif
            @if($config->configuration['presta']->stock == 1)
                <div class="panel">
                    <header class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-warning text-warning"></i> Etat des Stock</h3>
                    </header>
                    <div class="panel-body">
                        @if(\App\Http\Controllers\GestionAsc\GestionAscController::countPrestationUnpack() == 0)
                            <h1><i class="icon wb-thumb-up text-success"></i> Aucune Prestation en rupture de stock</h1>
                        @else
                            <ul>
                                @foreach($prestationStocks as $stock)
                                <li>
                                    <a href="">{{ $stock->libelle }} ({{ $stock->prestation->name }})</a>
                                </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/custom/js/gestionasc/dashboard.js"></script>
@stop