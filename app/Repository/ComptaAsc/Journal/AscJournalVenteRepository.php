<?php
namespace App\Repository\ComptaAsc\Journal;

use App\Model\ComptaAsc\Journal\AscJournalVente;

class AscJournalVenteRepository
{
    /**
     * @var AscJournalVente
     */
    private $ascJournalVente;

    /**
     * AscJournalVenteRepository constructor.
     * @param AscJournalVente $ascJournalVente
     */

    public function __construct(AscJournalVente $ascJournalVente)
    {
        $this->ascJournalVente = $ascJournalVente;
    }

    public function create($id, $created_at, int $numCompte, string $libelleVente, $montant)
    {
        return $this->ascJournalVente->newQuery()
            ->create([
                "numVente"  => "ANCV".$id,
                "dateVente" => $created_at,
                "numCompte" => $numCompte,
                "libelleVente" => $libelleVente,
                "debitVente"=> 0,
                "creditVente" => $montant
            ]);
    }

}

        