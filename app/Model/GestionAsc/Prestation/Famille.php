<?php

namespace App\Model\GestionAsc\Prestation;

use Illuminate\Database\Eloquent\Model;

class Famille extends Model
{
    protected $guarded = [];

    public function prestations(){
        return $this->hasMany(Prestation::class, 'familles_id');
    }
}
