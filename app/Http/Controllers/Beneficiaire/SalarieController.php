<?php

namespace App\Http\Controllers\Beneficiaire;

use App\Model\Beneficiaire\Ad\AyantDroit;
use App\Model\Beneficiaire\Salarie\Salarie;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\GestionAsc\Billetterie\BilletSalarie;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Model\GestionAsc\Remboursement\Salarie\RembSalarie;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class SalarieController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'beneficiaire';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $salaries = Salarie::all();
        $salarieActifs = Salarie::where('active', 1)->get();
        $salarieInactifs = Salarie::where('active', 0)->get();

        return view('Beneficiaire.Salarie.index', compact('config', 'salaries', 'salarieActifs', 'salarieInactifs'));
    }

    public function create(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        return view('Beneficiaire.Salarie.create', compact('config'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "nom"   => "required",
            "prenom"=> "required"
        ]);

        $str = str_limit($request->nom, 3, '');
        $numCompte = "410".$str;

        $salarie = Salarie::create([
            "nom"       => $request->nom,
            "prenom"    => $request->prenom,
            "adresse"   => $request->adresse,
            "codePostal"=> $request->codePostal,
            "ville"     => $request->ville,
            "tel"       => $request->tel,
            "port"      => $request->port,
            "email"     => $request->email,
            "numCompte" => $numCompte
        ]);

        $cpt = ComptaPlanCompte::create([
            "numCompte"     => $numCompte,
            "numSector"     => 41,
            "nameCompte"    => $request->nom.' '.$request->prenom
        ]);

        $compte = AscCompte::create(["numCompte" => $numCompte]);

        if($salarie && $cpt && $compte){
            Toastr::success("Un salarié à été ajouté");
            return redirect()->route('salaries.index');
        }else{
            Toastr::error("Erreur lors de l\'ajout du salarié !");
            return redirect()->back();
        }
    }

    public function show($id){
        Carbon::setLocale('fr');
        Carbon::setToStringFormat('d/m/Y');
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];
        $salarie = Salarie::find($id)->load('ad', 'fidelities');
        $chart = Charts::multi('line', 'chartjs')
            // Setup the chart settings
            ->title("Chiffre d'affaire du salarié")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400) // Width x Height
            // This defines a preset of colors already done:)
            ->template("material")
            // You could always set them manually
            // ->colors(['#2196F3', '#F44336', '#FFC107'])
            // Setup the diferent datasets (this is a multi chart)
            ->dataset('Vente de Billetterie', SalarieOtherController::getCaVenteChart($id))
            ->dataset('Remboursement', SalarieOtherController::getCaRembChart($id))
            ->elementLabel('Euros')
            // Setup what the values mean
            ->labels(['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre']);

        $billets = BilletSalarie::where('salaries_id', $salarie->id)->get();
        $rembs = RembSalarie::where('salaries_id', $salarie->id)->get();

        return view('Beneficiaire.Salarie.show', compact('config', 'salarie', 'chart', 'billets', 'rembs'));
    }

    public function edit($id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];
        $salarie = Salarie::find($id);

        return view('Beneficiaire.Salarie.edit', compact('config', 'salarie'));
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            "nom"   => "required",
            "prenom"=> "required"
        ]);

        $salarie = Salarie::find($id);
        $cpt = ComptaPlanCompte::where('numCompte', $salarie->numCompte)->first();
        $compte = AscCompte::where('numCompte', $salarie->numCompte)->first();

        $str = str_limit($request->nom, 3, '');
        $numCompte = "410".$str;

        $salarie->update([
            "nom"       => $request->nom,
            "prenom"    => $request->prenom,
            "adresse"   => $request->adresse,
            "codePostal"=> $request->codePostal,
            "ville"     => $request->ville,
            "tel"       => $request->tel,
            "port"      => $request->port,
            "email"     => $request->email,
            "numCompte" => $numCompte
        ]);

        $cpt->update([
            "numCompte"     => $numCompte,
            "numSector"     => 41,
            "nameCompte"    => $request->nom.' '.$request->prenom
        ]);

        $compte->update(["numCompte" => $numCompte]);

        if($salarie && $cpt && $compte){
            Toastr::success("Un salarié à été édité");
            return redirect()->route('salaries.index');
        }else{
            Toastr::error("Erreur lors de l\'édition du salarié !");
            return redirect()->back();
        }
    }

    public function delete($id){
        $salarie = Salarie::find($id);
        $cpt = ComptaPlanCompte::where('numCompte', $salarie->numCompte)->first();
        $ads = AyantDroit::where('salaries_id', $id)->get();
        $compte = AscCompte::where('numCompte', $salarie->numCompte)->first();

        foreach ($ads as $ad) {
            $cptAd = ComptaPlanCompte::where('numCompte', $ad->numCompte)->delete();
            $compteAd = AscCompte::where('numCompte', $ad->numCompte)->delete();
            $ad->delete();
        }

        $salarie = Salarie::destroy($id);
        $cpt->delete();
        $compte->delete();

        if($salarie && $ads && $cpt){
            Toastr::success("Un salarié à été supprimé");
            return redirect()->route('salaries.index');
        }else{
            Toastr::error("Erreur lors de la suppression du salarié !");
            return redirect()->back();
        }
    }
}
