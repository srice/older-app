<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestationTarifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestation_tarifs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prestations_id');
            $table->string('libelle');
            $table->string('price');
            $table->string('quota');
            $table->string('stock')->default(0); // Désactiver |1: Activer
            $table->string('stockLimit');
            $table->string('stockActuel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestation_tarifs');
    }
}
