<?php

namespace App\Http\Controllers\Facturation\Facture;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FactureOtherController extends FactureController
{
    // Route


    // Static

    public static function typeFacturation($value){
        switch ($value){
            case 0: return "Oeuvres Sociales";
            case 1: return "Budget de Fonctionnement";
            default: return "Inconnue";
        }
    }

    public static function etatGenerale($value){
        switch ($value){
            case 0: return '<span class="tag tag-default"><i class="fa fa-edit"></i> Brouillon</span>';
            case 1: return '<span class="tag tag-primary"><i class="fa fa-check"></i> Valider</span>';
            case 2: return '<span class="tag tag-success"><i class="fa fa-check-square"></i> Terminer</span>';
        }
    }

    public static function subEtat($value){
        switch ($value){
            case 0: return '<span class="tag tag-primary"><i class="fa fa-refresh"></i> En attente de validation</span>';
            case 1: return '<span class="tag tag-warning"><i class="fa fa-warning"></i> En attente de paiement</span>';
            case 2: return '<span class="tag tag-success"><i class="fa fa-check"></i> Payer</span>';
            case 3: return '<span class="tag tag-danger"><i class="fa fa-times-circle"></i> Impayer</span>';

        }
    }
}
