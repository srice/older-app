@extends('template')
@section("title")
    Liste des modes de règlements du module de Facturation
    @parent
@stop
@section("header_styles")

@stop
@section("content")
<div class="panel">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-9">
                <h3 class="panel-title">Liste des modes de règlements</h3>
            </div>
            <div class="col-md-3">
                <a data-toggle="modal" href="#addMode" class="btn btn-primary "><i class="fa fa-plus-circle"></i> Ajouter un mode de règlement</a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Libellé</th>
                    <th><i class="fa fa-bars"></i> </th>
                </tr>
            </thead>
            <tbody>
                @foreach($modes as $mode)
                    <tr>
                        <td>{{ $mode->libelle }}</td>
                        <td>
                            {{ Form::model($mode, ["route" => ["facturation.modeReglement.delete", $mode->id], "method" => "DELETE"]) }}
                            <a href="{{ route('facturation.modeReglement.edit', $mode->id) }}" class="btn btn-icon btn-primary" data-toggle="tooltip" title="Editer le mode de paiement"><i class="fa fa-edit"></i> </a>
                            <button class="btn btn-icon btn-danger" data-toggle="tooltip" title="Supprimer le mode de paiement"><i class="fa fa-trash"></i> </button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="addMode" aria-hidden="true" aria-labelledby="examplePositionSidebar"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un mode de règlement</h4>
            </div>
            {{ Form::open(["route" => "facturation.modeReglement.store"]) }}
            <div class="modal-body">
                <div class="form-group form-material">
                    {{ Form::label('libelle', "Libelle", ["class" => "control-label"]) }}
                    {{ Form::text('libelle', null, ["class" => "form-control", "required"]) }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-block">Valider</button>
                <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section("footer_scripts")

@stop    