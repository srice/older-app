<?php

namespace App\Model\ComptaAsc\Etat;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use Illuminate\Database\Eloquent\Model;

class AscCompte extends Model
{
    protected $guarded = [];

    public function comptes(){
        return $this->hasMany(ComptaPlanCompte::class, 'numCompte', 'numCompte');
    }
}
