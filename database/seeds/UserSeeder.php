<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');
        if(env('APP_ENV') == 'local'){
            $user = User::create([
                "name"      => "Support Srice",
                "email"     => "support@srice.eu",
                "password"  => bcrypt('1992_Maxime'),
                "poste"     => "Support technique",
                "etat"      => 1
            ]);

            for($i=0; $i <= 5; $i++){
                DB::table('users')->insert([
                    "name"      => $faker->name,
                    "email"     => $faker->email,
                    "password"  => bcrypt('secret'),
                    "poste"     => "Testeur",
                    "etat"      => 1
                ]);
            }
        }

        if(env('APP_ENV') == 'test'){
            $user = User::create([
                "name"      => "Support Srice",
                "email"     => "support@srice.eu",
                "password"  => bcrypt('1992_Maxime'),
                "poste"     => "Support technique",
                "etat"      => 1
            ]);

            for($i=0; $i <= 5; $i++){
                DB::table('users')->insert([
                    "name"      => $faker->name,
                    "email"     => $faker->email,
                    "password"  => bcrypt('secret'),
                    "poste"     => "Testeur",
                    "etat"      => 1
                ]);
            }
        }

        if(env('APP_ENV') == 'prod'){
            $user = User::create([
                "name"      => "Support Srice",
                "email"     => "support@srice.eu",
                "password"  => bcrypt('1992_Maxime'),
                "poste"     => "Support technique",
                "etat"      => 1
            ]);

            $user = User::create([
                "name"      => "Compte de Test",
                "email"     => "test@srice.eu",
                "password"  => bcrypt('TJdw06o'),
                "poste"     => "Compte de test",
                "etat"      => 1
            ]);
        }
    }
}
