@extends('template')
@section("title")
    Gestion des Utilisateurs
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-header">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste des Accès utilisateurs</h3>
                </div>
                <div class="col-md-2">
                    <a href="#addAccess" data-toggle="modal" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Ajout un accès</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Adresse Email</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tabs->resources as $user)
                            <tr>
                                <td>{{$user->email}}</td>
                                <td>
                                    <a href="{{ route('banker.users.login') }}" class="btn btn-sm btn-icon bg-purple-500"><i class="fa fa-lock"></i> </a>
                                    <a href="{{ route('banker.users.step', $user->uuid) }}" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addAccess" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un accès</h4>
                </div>
                {{ Form::open(["route" => "banker.users.store"]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('email', "Adresse Mail", ["class" => "control-label"]) }}
                        {{ Form::email('email', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('password', "Mot de Passe", ["class" => "control-label"]) }}
                        {{ Form::password('password', ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="delAccess" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-remove"></i> Suppression d'un accès</h4>
                </div>
                {{ Form::open(["route" => "banker.users.store"]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('email', "Adresse Mail", ["class" => "control-label"]) }}
                        {{ Form::email('email', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('password', "Mot de Passe", ["class" => "control-label"]) }}
                        {{ Form::password('password', ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    