<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigAscBilletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config_asc_billets')->insert([
            "impayer"       => 0,
            "dayOfImpayer"  => 7
        ]);
    }
}
