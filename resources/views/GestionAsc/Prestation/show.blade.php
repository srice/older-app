@extends('template')
@section('title')
    Prestation: <strong>{{ $prestation->name }}</strong>
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="/assets/template/examples/css/uikit/modals.css">
@stop

@section("content")
    <div class="row">
        <div class="col-md-4">
            <div class="panel">
                <header class="panel-header">
                    <div class="row">
                        <div class="col-md-12 text-xs-right">
                            {{ Form::model($prestation, ["route" => ["prestations.delete", $prestation->id], "method" => "DELETE"]) }}
                            <button class="btn btn-sm btn-icon btn-danger" data-toggle="tooltip" title="Supprimer la prestation"><i class="fa fa-times"></i> </button>
                            {{ Form::close() }}
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="text-center">
                        @if($prestation->visuel == 1)
                            <img src="/assets/custom/images/prestation/{{ str_slug($prestation->famille->name) }}/{{ $prestation->id }}.png" class="img-responsive" style="max-width: 500px; max-height: 600px;" alt="">
                        @else
                            <img src="{{ public_path()."/assets/global/photos/placeholder.png" }}" class="img-responsive" style="max-width: 500px; max-height: 600px;" alt="">
                        @endif
                    </div>
                    <div class="m-t-30"></div>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td style="font-weight: bold;">Nom de la prestation</td>
                                <td>{{ $prestation->name }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Famille de la prestation</td>
                                <td>{{ $prestation->famille->name }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Date de création de la prestation</td>
                                <td>{{ $prestation->created_at->format('d/m/Y') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel">
                <header class="panel-heading">
                    <div class="nav-tabs-horizontal" data-plugin="tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" data-toggle="tab" href="#description" aria-controls="exampleTabsFour" role="tab"><i class="fa fa-search"></i> Description</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#localisation" aria-controls="exampleTabsFour" role="tab"><i class="fa fa-map-marker"></i> Localisation</a>
                            </li>
                            @if($config->configuration['presta']->validite == 1)
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#seance" aria-controls="exampleTabsFour" role="tab"><i class="fa fa-calendar"></i> Dates & Séances</a>
                            </li>
                            @endif
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#tarif" aria-controls="exampleTabsFour" role="tab"><i class="fa fa-euro"></i> Tarification</a>
                            </li>
                            @if($config->configuration['presta']->stock == 1)
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#stock" aria-controls="exampleTabsFour" role="tab"><i class="fa fa-truck"></i> Stock</a>
                            </li>
                            @endif
                            <!--<li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#stat" aria-controls="exampleTabsFour" role="tab"><i class="fa fa-line-chart"></i> Statistique</a>
                            </li>-->
                            @if($config->moduleMenu[12]->state == 1)
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#fidelity" aria-controls="exampleTabsFour" role="tab"><i class="fa fa-shopping-basket"></i> Fidélité</a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="tab-content p-t-20">
                        <div class="tab-pane active" id="description" role="tabpanel">
                            <h3>DESCRIPTION</h3>
                            <div class="m-t-20"></div>
                            {!! $prestation->descriptif !!}
                        </div>
                        <div class="tab-pane" id="localisation" role="tabpanel">
                            <h3>LOCALISATION</h3>
                            <div class="m-t-30"></div>
                            <div class="panel">
                                <header class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <h3 class="panel-title">Liste des localisations</h3>
                                        </div>
                                        <div class="col-md-2">
                                            <a class="btn btn-block btn-sm btn-primary" data-toggle="modal" href="#addLocation"><i class="fa fa-plus-circle"></i> Ajouter une localisation</a>
                                        </div>
                                    </div>
                                </header>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="tableLocalize" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Titre</th>
                                                <th>Adresse</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($prestation->locales as $locale)
                                                <tr>
                                                    <td>{{ $locale->type }}</td>
                                                    <td>{{ $locale->adresse }}<br>{{ $locale->codePostal }} {{ $locale->ville }}</td>
                                                    <td>
                                                        {{ Form::model($locale, ["route" => ["localize.delete", $prestation->id, $locale->id], "method" => "DELETE"]) }}
                                                        <a href="{{ route('localize.map', [$prestation->id, $locale->id]) }}" class="btn btn-icon btn-primary" data-toggle="tooltip" data-title="Ouvrir la map"><i class="fa fa-map"></i> </a>
                                                        <button class="btn btn-icon btn-danger" data-toggle="tooltip" data-title="Supprimer"><i class="fa fa-trash"></i> </button>
                                                        {{ Form::close() }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($config->configuration['presta']->validite == 1)
                        <div class="tab-pane" id="seance" role="tabpanel">
                            <h3>Dates & Séances</h3>
                            <div class="m-t-30"></div>
                            <div class="panel">
                                <header class="panel-header bg-primary-300">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h3 class="panel-title">Liste des dates & des séances</h3>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="#addSeance" class="btn btn-primary btn-lg btn-block" data-toggle="modal"><i class="fa fa-plus-circle"></i> Nouvelle séance</a>
                                        </div>
                                    </div>
                                </header>
                                <div class="panel-body">
                                    @if(\App\Http\Controllers\GestionAsc\Prestation\DateController::countDate($prestation->id) > 1)
                                        <div class="table-responsive">
                                            <table id="listeDate" class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Date de début</th>
                                                    <th>Date de Fin</th>
                                                    <th>Dans...</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($prestation->dates as $date)
                                                        <tr>
                                                            <td>{{ $date->dateStart->format('d/m/Y') }}</td>
                                                            <td>{{ $date->dateEnd->format('d/m/Y') }}</td>
                                                            <td>
                                                                @if($date->dateStart >= \Carbon\Carbon::now())
                                                                    <span class="tag tag-success">{{ $date->dateEnd->diffForHumans() }}</span>
                                                                @else
                                                                    <span class="tag tag-danger">Evenement terminer</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{ Form::model($date, ["route" => ["seance.deleteDate", $prestation->id, $date->id], "method" => "DELETE"]) }}
                                                                <button class="btn btn-icon btn-danger" data-toggle="tooltip" data-title="Supprimer la séance"><i class="fa fa-trash"></i></button>
                                                                {{ Form::close() }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <div class="panel bg-amber-100">
                                            <div class="panel-body text-center">
                                                Unique représentation du <strong>{{ $prestation->dates[0]->dateStart->format('d/m/Y à H:i') }}</strong> au <strong>{{ $prestation->dates[0]->dateEnd->format('d/m/Y à H:i') }}</strong>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="tab-pane" id="tarif" role="tabpanel">
                            <h3>TARIFICATION</h3>
                            <div class="m-t-30"></div>
                            <div class="panel">
                                <header class="panel-heading bg-primary-300">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h3 class="panel-title">Liste des tarifs</h3>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="#addTarif" data-toggle="modal" class="btn btn-block btn-lg btn-primary"><i class="fa fa-plus-circle"></i> Ajouter un tarif</a>
                                        </div>
                                    </div>
                                </header>
                                <div class="panel-body">
                                    <table id="listeTarif" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Libelle</th>
                                                <th>Tarification</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($prestation->tarifs as $tarif)
                                            <tr>
                                                <td>{{ $tarif->libelle }}</td>
                                                <td class="text-xs-center">
                                                    <strong>Tarif du billet:</strong> {{ \App\Http\Controllers\OtherController::euro($tarif->price) }}
                                                    <div class="row">
                                                        <div class="col-md-6 text-xs-center">
                                                            <strong>Participation salarié</strong><br>{{ \App\Http\Controllers\OtherController::euro($tarif->price_sal) }}
                                                        </div>
                                                        <div class="col-md-6 text-xs-center">
                                                            <strong>Participation comité</strong><br>{{ \App\Http\Controllers\OtherController::euro($tarif->price_ce) }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    {{ Form::model($tarif, ["route" => ["tarif.deleteTarif", $prestation->id, $tarif->id], "method" => "DELETE"]) }}
                                                    <button class="btn btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @if($config->configuration['presta']->stock == 1)
                        <div class="tab-pane" id="stock" role="tabpanel">
                            <h3>Gestion des Stocks</h3>
                            <div class="m-t-30"></div>
                            <div class="panel">
                                <header class="panel-heading bg-primary-300">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h3 class="panel-title">Liste des mouvements de stock</h3>
                                        </div>
                                    </div>
                                </header>
                                <div class="panel-body">
                                    <table id="listeStock" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Libelle</th>
                                                @if($config->configuration['presta']->quota == 1)
                                                <th>Quota</th>
                                                @endif
                                                <th>Stock</th>
                                                <th>Etat du stock</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($prestation->tarifs as $tarif)
                                            <tr>
                                                <td>{{ $tarif->libelle }}</td>
                                                @if($config->configuration['presta']->quota == 1)
                                                <td>{{ $tarif->quota }}/ par salarié</td>
                                                @endif
                                                <td>
                                                    <strong>Limite de stock:</strong> {{ $tarif->stockLimit }}<br>
                                                    <strong>Stock Actuel:</strong> {{ $tarif->stockActuel }}
                                                </td>
                                                <td>{!! \App\Http\Controllers\GestionAsc\Prestation\StockController::etatStock($tarif->stockLimit, $tarif->stockActuel) !!}</td>
                                                <td>
                                                    <a href="{{ route('stock.editStock', [$prestation->id, $tarif->id]) }}" class="btn btn-icon btn-primary" data-toggle="tooltip" data-title="Editer le stock"><i class="fa fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($config->moduleMenu[12]->state == 1)
                        <div class="tab-pane" id="fidelity" role="tabpanel">
                            <h3>Compteur Affilié</h3>
                            <div class="m-t-30"></div>
                            <div class="panel">
                                <header class="panel-heading">
                                    <h3 class="panel-title">Compteur affilié à la prestation</h3>
                                </header>
                                <div class="panel-body">
                                    <strong>Nom du compteur:</strong> {{ $compteur->nameCompteur }}<br>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>A partir de</th>
                                                    <th>A</th>
                                                    <th>Nombre de point attribué</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($compteur->baremes as $bareme)
                                                <tr>
                                                    <td>{{ \App\Http\Controllers\OtherController::euro($bareme->subStart) }}</td>
                                                    <td>{{ \App\Http\Controllers\OtherController::euro($bareme->subEnd) }}</td>
                                                    <td>{{ $bareme->nbPoint }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="addLocation" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-map-marker"></i> Ajout d'une localisation</h4>
                </div>
                {{ Form::open(["route" => ["localize.store", $prestation->id]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('type', "Désignation de la localisation", ["class" => "control-label"]) }}
                        {{ Form::text('type', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('adresse', "Adresse", ["class" => "control-label"]) }}
                        {{ Form::text('adresse', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::label('codePostal', "Code Postal", ["class" => "control-label"]) }}
                                {{ Form::text('codePostal', null, ["class" => "form-control", "id" => "codePostal", "onBlur" => "getVille()"]) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::label('ville', "Ville", ["class" => "control-label"]) }}
                                <select id="ville" name="ville" class="form-control round">
                                    <option disabled selected>Selectionner une ville</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="addSeance" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-calendar"></i> Ajout d'une séance</h4>
                </div>
                {{ Form::open(["route" => ["seance.storeDate", $prestation->id]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('dateStart', "Date de la séance", ["class" => "control-label"]) }}
                        <div class="row">
                            <div class="col-md-3">
                                {{ Form::text('dateStart', null, ["class" => "form-control dateStart"]) }}
                            </div>
                            <div class="col-md-3">
                                {{ Form::text('heureStart', null, ["class" => "form-control hour"]) }}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="addTarif" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-euro"></i> Ajout d'un Tarif</h4>
                </div>
                {{ Form::open(["route" => ["tarif.storeTarif", $prestation->id]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('libelle', "Libelle du tarif *", ["class" => "control-label"]) }}
                        {{ Form::text('libelle', null, ["class" => "form-control", "required"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('participation', "Participation *", ["class" => "control-label"]) }}
                        <div class="row">
                            <div class="col-md-3">
                                {{ Form::text('price_sal', null, ["class" => "form-control", "required", "placeholder" => "Participation du salarié"]) }}
                            </div>
                            <div class="col-md-3">
                                {{ Form::text('price_ce', null, ["class" => "form-control", "required", "placeholder" => "Participation du comité"]) }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-material">
                        {{ Form::checkbox('stock', 1, null, ["data-plugin" => "switchery", "id" => "stock"]) }}
                        <label class="p-t-3" for="inputBasicOff">Prise en charge du stock</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- End Modal -->
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/switchery/switchery.min.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/global/js/Plugin/switchery.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="/assets/custom/js/gestionasc/prestation/show.js"></script>
    <script src="/assets/custom/js/beneficiaire/salarie/create.js"></script>
@stop