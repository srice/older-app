<?php

namespace App\Http\Controllers\Ancv\Vente;

use App\HelperClass\Ancv\Ancv;
use App\Packages\Srice\Espace;
use App\Repository\Ancv\AncvVenteRepository;
use App\Repository\Beneficiaire\Salarie\SalarieRepository;
use App\Repository\ComptaAsc\Etat\AscCompteRepository;
use App\Repository\ComptaAsc\Journal\AscJournalVenteRepository;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Kamaln7\Toastr\Facades\Toastr;

class AncvVenteController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;
    /**
     * @var Espace
     */
    private $espace;
    /**
     * @var AncvVenteRepository
     */
    private $ancvVenteRepository;
    /**
     * @var SalarieRepository
     */
    private $salarieRepository;
    /**
     * @var AscJournalVenteRepository
     */
    private $ascJournalVenteRepository;
    /**
     * @var AscCompteRepository
     */
    private $ascCompteRepository;

    /**
     * AncvVenteController constructor.
     * @param Espace $espace
     * @param Guard $auth
     * @param AncvVenteRepository $ancvVenteRepository
     * @param SalarieRepository $salarieRepository
     * @param AscJournalVenteRepository $ascJournalVenteRepository
     * @param AscCompteRepository $ascCompteRepository
     */
    public function __construct(
        Espace $espace,
        Guard $auth,
        AncvVenteRepository $ancvVenteRepository,
        SalarieRepository $salarieRepository,
        AscJournalVenteRepository $ascJournalVenteRepository,
        AscCompteRepository $ascCompteRepository
    )
    {
        $this->auth = $auth;
        $this->sector = 'ancv';
        $this->modules = $espace->listeModule();
        $this->espace = $espace;
        $this->ancvVenteRepository = $ancvVenteRepository;
        $this->salarieRepository = $salarieRepository;
        $this->ascJournalVenteRepository = $ascJournalVenteRepository;
        $this->ascCompteRepository = $ascCompteRepository;
    }

    public function index()
    {
        $config = (object)[
            "sector" => $this->sector,
            "moduleMenu" => $this->modules,
            "configuration" => $this->configuration,
            "parent" => 0
        ];

        return view("Ancv.Vente.index", [
            "config" => $config,
            "ventes" => $this->ancvVenteRepository->list()
        ]);
    }

    public function create()
    {
        $config = (object)[
            "sector" => $this->sector,
            "moduleMenu" => $this->modules,
            "configuration" => $this->configuration,
            "parent" => 1
        ];

        return view("Ancv.Vente.create", [
            "config" => $config,
            "salaries" => $this->salarieRepository->list(),
            "solutions" => Ancv::solutionArray()
        ]);
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $calc = $request->part_ce + $request->part_salarie;
        $salarie = $this->salarieRepository->get($request->salarie_id);

        try {
            $ancv = $this->ancvVenteRepository->create(
                $request->salarie_id,
                Ancv::getSolutionArray($request->solution_id),
                $calc,
                $request->part_ce,
                $request->part_salarie
            );


        } catch (\Exception $exception) {
            Log::error($exception->getMessage());

            Toastr::error("Erreur de la création de la vente ! Contactez un administrateur", "Création de la vente");
            return redirect()->back();
        }

        // Enregistrement dans le journal de vente
        try {
            $journal = $this->ascJournalVenteRepository->create(
                $ancv->id,
                $ancv->created_at,
                7061,
                "Vente de " . $request->solution_id . " pour " . $salarie->nom . " " . $salarie->prenom,
                $calc
            );

        } catch (\Exception $exception) {
            Log::error($exception->getMessage());

            Toastr::error("Erreur lors de l'inscription dans le journal de vente ! Contactez un administrateur", "Journal de vente");
            return redirect()->back();
        }

        // Import des informations de compte
        try {
            $compte = $this->ascCompteRepository->getCompte(7061);
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());

            Toastr::error("Erreur lors de la récupération du compte ! Contactez un administrateur", "GET Compte");
            return redirect()->back();
        }

        // Mise à jour du compte
        try {
            $credit = $compte->credit + $calc;

            $this->ascCompteRepository->updateCredit(
                7061,
                $credit
            );

            Toastr::success("La vente ANCV à été créer", "Succès");
            return redirect()->route('Ancv.Vente.index');
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());

            Toastr::error("Erreur lors de l'inscription en compte ! Contactez un administrateur", "Inscription en compte");
            return redirect()->back();
        }
    }
}
