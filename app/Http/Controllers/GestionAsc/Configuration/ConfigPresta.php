<?php

namespace App\Http\Controllers\GestionAsc\Configuration;

use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class ConfigPresta extends Controller
{
    public function validiteEnable(){
        $up = ConfigAscPresta::find(1)->update([
            "validite"  => 1
        ]);

        if($up){
            Toastr::success("Validité Activé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }
    public function validiteDisable(){
        $up = ConfigAscPresta::find(1)->update([
            "validite"  => 0
        ]);

        if($up){
            Toastr::success("Validité Désactivé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function quotaEnable(){
        $up = ConfigAscPresta::find(1)->update([
            "quota"  => 1
        ]);

        if($up){
            Toastr::success("Quota Activé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function quotaDisable(){
        $up = ConfigAscPresta::find(1)->update([
            "quota"  => 0
        ]);

        if($up){
            Toastr::success("Quota Désactivé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function stockEnable(){
        $up = ConfigAscPresta::find(1)->update([
            "stock"  => 1
        ]);

        if($up){
            Toastr::success("Stock Activé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function stockDisable(){
        $up = ConfigAscPresta::find(1)->update([
            "stock"  => 0
        ]);

        if($up){
            Toastr::success("Stock Désactivé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function stateImpayer(){
        $config = ConfigAscBillet::find(1);
        $state = $config->impayer;

        return $state;
    }

    public function dayOfImpayer(Request $request){
        $up = ConfigAscBillet::find(1)->update(["dayOfImpayer" => $request->dayOfImpayer]);

        if($up){
            Toastr::success("Configuration effectuer");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function impayerEnable(){
        $up = ConfigAscBillet::find(1)->update([
            "impayer" => 1
        ]);

        if($up){
            Toastr::success("Impayer Activé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function impayerDisable(){
        $up = ConfigAscBillet::find(1)->update([
            "impayer" => 0
        ]);

        if($up){
            Toastr::success("Impayer Désactivé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function percentEnable(){
        $up = ConfigAscRemb::find(1)->update([
            "percent" => 1
        ]);

        if($up){
            Toastr::success("Pise en charge par pourcentage activé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function percentDisable(){
        $up = ConfigAscRemb::find(1)->update([
            "percent" => 0
        ]);

        if($up){
            Toastr::success("Pise en charge par pourcentage désactivé");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function percentSalarie(Request $request){
        $up = ConfigAscRemb::find(1)->update([
            "percentSalarie" => $request->percentSalarie
        ]);

        if($up){
            Toastr::success("Mise à jour effectué");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function percentAd(Request $request){
        $up = ConfigAscRemb::find(1)->update([
            "percentAd" => $request->percentAd
        ]);

        if($up){
            Toastr::success("Mise à jour effectué");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }
}
