<?php

namespace App\Http\Controllers\GestionAsc\Prestation;

use App\Model\Fidelity\FidelityCompteur;
use App\Model\GestionAsc\Billetterie\LigneBilletAd;
use App\Model\GestionAsc\Billetterie\LigneBilletSalarie;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Model\GestionAsc\Prestation\Achat\AchatPrestation;
use App\Model\GestionAsc\Prestation\Famille;
use App\Model\GestionAsc\Prestation\Prestation;
use App\Model\GestionAsc\Prestation\PrestationDate;
use App\Model\GestionAsc\Prestation\PrestationLocal;
use App\Model\GestionAsc\Prestation\PrestationTarif;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Kamaln7\Toastr\Facades\Toastr;
use Spatie\Geocoder\Google\Geocoder;

class PrestationController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'gestionasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(){
        Carbon::setToStringFormat('d/m/Y');
        Carbon::setLocale('fr');
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $familles = Famille::all();

        return view('GestionAsc.Prestation.index', compact('config', 'familles'));
    }

    public function create(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $familles = Famille::all()->pluck('name', 'id');
        $compteurs = FidelityCompteur::all()->load('baremes');

        return view('GestionAsc.Prestation.create', compact('config', 'familles', 'compteurs'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "familles_id"   => "required",
            "name"          => "required"
        ]);
        if(isset($request->representative)){
            $representative = 1;
        }else{
            $representative = 0;
        }

        if($request->compteurs_id != null){
            //Création de la prestation
            $prestation = Prestation::create([
                "familles_id"   => $request->familles_id,
                "name"          => $request->name,
                "visuel"        => 0,
                "representative"=> $representative,
                "descriptif"    => $request->descriptif,
                "compteurs_id"  => $request->compteurs_id
            ]);
        }else{
            //Création de la prestation
            $prestation = Prestation::create([
                "familles_id"   => $request->familles_id,
                "name"          => $request->name,
                "visuel"        => 0,
                "representative"=> $representative,
                "descriptif"    => $request->descriptif
            ]);
        }

        $famille = Famille::find($request->familles_id);

        //Sauvegarde du visuel
        if(is_object($request->visuel) && $request->visuel->isValid()){
            $storage = $request->file('visuel')->move(public_path()."/assets/custom/images/prestation/".str_slug($famille->name)."/", $prestation->id.".png");
            $up = Prestation::all()->last()->update([
                "visuel"    => 1
            ]);
            if(!$up){
                Toastr::warning("Impossible de mettre à jour le visuel de la prestation !");
            }
        }

        //Ajout du lieu de la prestation
        $locales = PrestationLocal::create([
            "prestations_id"    => $prestation->id,
            "type"      => $request->type,
            "adresse"   => $request->adresse,
            "codePostal"=> $request->codePostal,
            "ville"     => $request->ville
        ]);

        //Ajout de la disponibilité par default

        if($representative == 1){
            $date = $request->dateStartUnique.' '.$request->heureStartUnique;
            $strtStart = strtotime($date);
            $dateStart = Carbon::createFromTimestamp($strtStart);
            $dateEnd = Carbon::createFromTimestamp($strtStart);
        }else{
            $start = $request->dateStart;
            $strtStart = strtotime($start);
            $dateStart = Carbon::createFromTimestamp($strtStart);
            $end = $request->dateEnd;
            $strtEnd = strtotime($end);
            $dateEnd = Carbon::createFromTimestamp($strtEnd);
        }

        $dispo = PrestationDate::create([
            "prestations_id"    => $prestation->id,
            "dateStart"         => $dateStart,
            "dateEnd"           => $dateEnd
        ]);

        //Ajout du tarif par default
        $price = $request->price_sal+$request->price_ce;
        if($request->stock == 1){
            $stockLimit = $request->stockLimit;
            $stockActuel = $request->stockActuel;
        }else{
            $stockLimit = 0;
            $stockActuel = 0;
        }

        $tarif = PrestationTarif::create([
            "prestations_id"    => $prestation->id,
            "libelle"           => $request->libelle,
            "price_sal"         => $request->price_sal,
            "price_ce"          => $request->price_ce,
            "price"             => $price,
            "quota"             => $request->quota,
            "stock"             => $request->stock,
            "stockLimit"        => $stockLimit,
            "stockActuel"       => $stockActuel
        ]);



        if($prestation && $locales && $dispo && $tarif){
            Toastr::success("Une prestation à été créé");
            return redirect()->route('prestations.index');
        }else{
            Toastr::error("Erreur lors de la création de la prestation !");
            return redirect()->back();
        }
    }

    public function show($id){
        Carbon::setToStringFormat('d/m/Y');
        Carbon::setLocale('fr');
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        if($config->moduleMenu[12]->state == 1){
            $prestation = Prestation::find($id)->load('famille', 'dates', 'locales', 'tarifs');
            $compteur = FidelityCompteur::find($prestation->compteurs_id)->load('baremes');

            return view('GestionAsc.Prestation.show', compact('config', 'prestation', 'compteur'));
        }else{
            $prestation = Prestation::find($id)->load('famille', 'dates', 'locales', 'tarifs');

            return view('GestionAsc.Prestation.show', compact('config', 'prestation'));
        }
    }




    public function localize($adresse){
        $result = app('geocoder')->using('chain')->geocode('1600 Pennsylvania Ave., Washington, DC USA')->all();
        $lng = $result[0]->getLongitude();
        $lat = $result[0]->getLatitude();



    }

    public function storeLocalize(Request $request, $id){
        $localize = PrestationLocal::create([
            "prestations_id"    => $id,
            "type"              => $request->type,
            "adresse"           => $request->adresse,
            "codePostal"        => $request->codePostal,
            "ville"             => $request->ville
        ]);

        if($localize){
            Toastr::success("Localisation ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout de la localisation !");
            return redirect()->back();
        }
    }

    public function deleteLocalize($prestations_id, $localize_id){
        $localize = PrestationLocal::destroy($localize_id);

        if($localize){
            Toastr::success("Localisation supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de la localisation !");
            return redirect()->back();
        }
    }

    public function delete($prestations_id){
        if(self::isAchat($prestations_id) == true && self::isVenteSalarie($prestations_id) == true && self::isVenteAd($prestations_id) == true){
            $tarifs = PrestationTarif::where('prestations_id', $prestations_id)->delete();
            $locals = PrestationLocal::where('prestations_id', $prestations_id)->delete();
            $dates = PrestationDate::where('prestations_id', $prestations_id)->delete();
            $prestation = Prestation::find($prestations_id)->delete();

            if($tarifs && $locals && $dates && $prestation){
                Toastr::success("La prestation à bien été supprimé");
                return redirect()->route('prestations.index');
            }else{
                Toastr::error("Erreur lors de la suppression de la prestation !");
                return redirect()->back();
            }

        }else{
            Toastr::warning("Impossible de supprimer la prestation car elle soit acheter, soit vendue !");
            return redirect()->back();
        }
    }

    public static function isAchat($prestations_id){
        $count = AchatPrestation::where('prestations_id', $prestations_id)->get()->count();

        if($count == 0){
            return true;
        }else{
            return false;
        }
    }

    public static function isVenteSalarie($prestations_id){
        $count = LigneBilletSalarie::where('prestations_id', $prestations_id)->get()->count();
        if($count == 0){
            return true;
        }else{
            return false;
        }
    }

    public static function isVenteAd($prestations_id){
        $count = LigneBilletAd::where('prestations_id', $prestations_id)->get()->count();
        if($count == 0){
            return true;
        }else{
            return false;
        }
    }
}
