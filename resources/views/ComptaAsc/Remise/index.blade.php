@extends('template')
@section("title")
    Remise en banque
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
<div class="panel">
    <header class="panel-header">
        <div class="row">
            <div class="col-md-10">
                <h3 class="panel-title">@yield('title')</h3>
            </div>
            <div class="col-md-2">
                <a href="#addRemise" class="btn btn-lg btn-block btn-primary m-t-5" data-toggle="modal"><i class="fa fa-plus-circle"></i> Nouvelle remise</a>
            </div>
        </div>
    </header>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Numéro de la remise</th>
                        <th>Date de la remise</th>
                        <th>Type de Remise</th>
                        <th>Montant de la remise</th>
                        <th>Etat de la remise</th>
                        <th class="text-xs-center"><i class="fa fa-bars"></i> </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($remises as $remise)
                    <tr>
                        <td>{{ $remise->numRemise }}</td>
                        <td>{{ $remise->dateRemise->format('d/m/Y') }}</td>
                        <td>{!! \App\Http\Controllers\ComptaAsc\Remise\RemiseOtherController::getNameTypeRemise($remise->typeRemise) !!}</td>
                        <td>{{ \App\Http\Controllers\OtherController::euro($remise->totalRemise) }}</td>
                        <td>{!! \App\Http\Controllers\ComptaAsc\Remise\RemiseOtherController::etatRemiseLabel($remise->etatRemise) !!}</td>
                        <td>
                            {{ Form::model($remise, ["route" => ["remise.delete", $remise->numRemise], "method" => "DELETE"]) }}
                            @if($remise->typeRemise == 0)
                                <a href="{{ route('remise.cheque.show', $remise->numRemise) }}" class="btn btn-sm btn-icon btn-default"><i class="fa fa-eye"></i> </a>
                            @else
                                <a href="{{ route('remise.espece.show', $remise->numRemise) }}" class="btn btn-sm btn-icon btn-default"><i class="fa fa-eye"></i> </a>
                            @endif
                            @if($remise->etatRemise == 0)
                            <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                            @endif
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="addRemise" aria-hidden="true" aria-labelledby="examplePositionSidebar"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'une Remise en banque</h4>
            </div>
            {{ Form::open(["route" => "remise.store"]) }}
            <div class="modal-body">
                <div class="form-group form-material">
                    {{ Form::label('numRemise', 'Numéro de la remise', ["class" => "control-label"]) }}
                    {{ Form::text('numRemise', null, ["class" => "form-control"]) }}
                </div>
                <div class="form-group form-material">
                    {{ Form::label('typeRemise', "Type de remise", ["class" => "control-label"]) }}
                    <select name="typeRemise" class="form-control" data-plugin="selectpicker" title="Selectionnez le type de remise en banque">
                        @foreach($typeRemises as $typeRemise)
                            <option value="{{ $typeRemise['id'] }}">{{ $typeRemise["designation"] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group form-material">
                    {{ Form::label('dateRemise', "Date de la remise", ["class" => "control-label"]) }}
                    <div class="input-group input-group-icon">
                        <div class="input-group-addon">
                            <i class="icon md-calendar"></i>
                        </div>
                        {{ Form::text('dateRemise', null, ["class" => "form-control date"]) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-block">Valider</button>
                <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-select.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/custom/js/comptaasc/remise/index.js"></script>
@stop    