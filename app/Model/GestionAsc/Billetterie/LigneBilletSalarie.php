<?php

namespace App\Model\GestionAsc\Billetterie;

use Illuminate\Database\Eloquent\Model;

class LigneBilletSalarie extends Model
{
    protected $guarded = [];

    public function billet(){
        return $this->belongsTo(BilletSalarie::class, 'billet_salarie_id');
    }

}
