<?php

namespace App\Http\Controllers\GestionAsc\Billetterie\Salarie;

use App\Model\Fidelity\FidelityCompteur;
use App\Model\GestionAsc\Billetterie\BilletSalarie;
use App\Model\GestionAsc\Billetterie\LigneBilletSalarie;
use App\Model\GestionAsc\Prestation\Prestation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BilletOtherController extends BilletController
{
    public static function countBillet(){
        $count = BilletSalarie::all()->count();
        return $count;
    }

    public static function countQuantity($billets_id){
        $count = LigneBilletSalarie::where('billet_salarie_id', $billets_id)->get()->sum('qte');
        return $count;
    }

    public static function newFidelities($prestations_id, $lignes_id){
        $prestation = Prestation::find($prestations_id);
        $compteur = FidelityCompteur::find($prestation->compteurs_id)->load('baremes');
        $ligne = LigneBilletSalarie::find($lignes_id);

        $fidelity = 0;
        foreach ($compteur->baremes as $bareme){
            $fidelity = $ligne->total / $bareme->subEnd;
        }

        return $fidelity;
    }
}
