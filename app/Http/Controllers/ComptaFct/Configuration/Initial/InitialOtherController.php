<?php

namespace App\Http\Controllers\ComptaFct\Configuration\Initial;

use App\Model\ComptaFct\Configuration\Bilan\FctBilanInitial;
use App\Model\ComptaFct\Configuration\Plan\FctPlanCompte;
use App\Model\ComptaFct\Etat\FctCompte;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class InitialOtherController extends InitialController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function validation(Request $request){
        $debit = self::getTotalDebit();
        $credit = self::getTotalCredit();

        $total = $credit - $debit;

        if($total > 0){
            $update = FctCompte::where('numCompte', '110')->update([
                "credit"    => $total
            ]);
            $update = FctCompte::where('numCompte', '101')->update([
                "debit"    => $total
            ]);
        }else{
            $update = FctCompte::where('numCompte', '119')->update([
                "debit"    => $total
            ]);
            $update = FctCompte::where('numCompte', '101')->update([
                "credit"    => $total
            ]);
        }

        if($update){
            Toastr::success("Le bilan initial à été valider");
            return redirect()->route('comptaAsc.config.initial.index');
        }
    }

    // Static

    public static function soldeDebit($numCompte){
        $compte = FctBilanInitial::where('numCompte', $numCompte)->sum('debit');
        return $compte;
    }

    public static function soldeCredit($numCompte){
        $compte = FctBilanInitial::where('numCompte', $numCompte)->sum('credit');
        return $compte;
    }

    public static function getNameCompte($numCompte){
        $compte = FctPlanCompte::where('numCompte', $numCompte)->first();
        return $compte->nameCompte;
    }

    public static function getTotalDebit(){
        $compte = FctBilanInitial::all()->sum('debit');
        return $compte;
    }

    public static function getTotalCredit(){
        $compte = FctBilanInitial::all()->sum('credit');
        return $compte;
    }
}
