@extends('template')
@section("title")
    Gestion des chèques vacances (ANCV)
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/footable/footable.core.css">
    <link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
    <link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste des Ventes</h3>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('Ancv.Vente.create') }}" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Nouvelle Vente</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="listeVente" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Numéro de Facture</th>
                        <th>Solution</th>
                        <th>Date de la facture</th>
                        <th>Identité</th>
                        <th>Total de la facture</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ventes as $vente)
                        <tr>
                            <td>{{ $vente->id }}</td>
                            <td>{{ \App\HelperClass\Ancv\Ancv::solution($vente->solution_id) }}</td>
                            <td>{{ $vente->created_at->format("d/m/Y") }}</td>
                            <td>
                                {{ $vente->salarie->nom }} {{ $vente->salarie->prenom }}
                            </td>
                            <td>
                                {{ formatCurrency($vente->total_ancv) }}
                            </td>
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/footable/footable.all.min.js"></script>
    <script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
    <script>
        (function ($) {
            $(".table").dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/French.json"
                }
            })
        })(jQuery)
        function tableVente(){
            var filtering = $('#listeVente');
            filtering.footable().on('footable_filtering', function(e) {
                var selected = $('#filteringStatus').find(':selected').val();
                e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                e.clear = !e.filter;
            });

            // Filter status
            $('#filteringStatus').change(function(e) {
                e.preventDefault();
                filtering.trigger('footable_filter', {
                    filter: $(this).val()
                });
            });

            // Search input
            $('#filteringSearch').on('input', function(e) {
                e.preventDefault();
                filtering.trigger('footable_filter', {
                    filter: $(this).val()
                });
            });
        }
    </script>
@stop    