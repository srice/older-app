<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRembRegSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remb_reg_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('remb_salarie_id');
            $table->string('numReglement');
            $table->string('depositaire');
            $table->string('montant');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remb_reg_salaries');
    }
}
