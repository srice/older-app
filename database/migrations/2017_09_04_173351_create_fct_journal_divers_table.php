<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFctJournalDiversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fct_journal_divers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numDivers');
            $table->timestamp('dateDivers')->nullable();
            $table->string('numCompte');
            $table->string('libelleDivers');
            $table->string('debitDivers')->default(0);
            $table->string('creditDivers')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fct_journal_divers');
    }
}
