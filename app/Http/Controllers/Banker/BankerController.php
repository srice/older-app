<?php

namespace App\Http\Controllers\Banker;

use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankerController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'banker';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function dashboard(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];


        return view('Banker.dashboard', compact('config'));
    }
}
