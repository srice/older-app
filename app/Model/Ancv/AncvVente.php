<?php

namespace App\Model\Ancv;

use App\Model\Beneficiaire\Salarie\Salarie;
use Illuminate\Database\Eloquent\Model;

class AncvVente extends Model
{
    protected $guarded = [];

    public function salarie()
    {
        return $this->belongsTo(Salarie::class, 'salarie_id');
    }
}
