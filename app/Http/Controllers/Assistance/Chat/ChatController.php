<?php

namespace App\Http\Controllers\Assistance\Chat;

use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{
    public $sector;
    public $modules;
    public function __construct(Espace $espace)
    {
        $this->sector = '';
        $this->modules = $espace->listeModule();
    }

    public function index(){

    }
}
