<?php

namespace App\Model\GestionAsc\Prestation\Achat;

use Illuminate\Database\Eloquent\Model;

class Fournisseur extends Model
{
    protected $guarded = [];

    public function achats(){
        return $this->hasMany(Achat::class);
    }
}
