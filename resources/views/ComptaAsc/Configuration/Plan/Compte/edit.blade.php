@extends('template')
@section("title")
    Edition du compte
    @parent
@stop
@section("header_styles")

@stop
@section("content")
<div class="panel">
    <header class="panel-heading">
        <h3 class="panel-title">@yield('title')</h3>
    </header>
    <div class="panel-body">
        {{ Form::model($compte, ["route" => ["comptaAsc.config.plan.compte.update", $classe->numClasse, $sector->numSector, $compte->numCompte], "class" => "form-horizontal", "method" => "PUT"]) }}
        <div class="form-group form-material row">
            {{ Form::label('numCompte', "Numéro de Compte", ["class" => "control-label col-md-3"]) }}
            <div class="col-md-6">
                {{ Form::text('numCompte', null, ["class" => "form-control"]) }}
            </div>
        </div>
        <div class="form-group form-material row">
            {{ Form::label('nameCompte', "Nom du compte", ["class" => "control-label col-md-3"]) }}
            <div class="col-md-6">
                {{ Form::text('nameCompte', null, ["class" => "form-control"]) }}
            </div>
        </div>
        <div class="text-xs-right">
            <button class="btn btn-success">Valider</button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop
@section("footer_scripts")

@stop    