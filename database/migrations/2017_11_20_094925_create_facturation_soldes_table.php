<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturationSoldesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturation_soldes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tiers_id')->comment("Relation: FacturationTier");
            $table->string('numEcriture');
            $table->string('designationEcriture');
            $table->integer('typeEcriture')->comment("0: Facture |1: Reglement");
            $table->string("debitEcriture")->comment("Ecriture de facture");
            $table->string('creditEcriture')->comment("Ecriture de Règlement");
            $table->string('soldeEcriture')->comment("Diff entre le solde précédent et l'écriture de débit ou de crédit");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturation_soldes');
    }
}
