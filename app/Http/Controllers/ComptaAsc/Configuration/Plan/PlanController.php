<?php

namespace App\Http\Controllers\ComptaAsc\Configuration\Plan;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanClasse;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptaasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $classes = ComptaPlanClasse::all();

        return view('ComptaAsc.Configuration.Plan.index', compact('config', 'classes'));
    }
}
