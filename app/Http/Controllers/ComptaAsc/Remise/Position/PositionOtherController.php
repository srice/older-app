<?php

namespace App\Http\Controllers\ComptaAsc\Remise\Position;

use App\Model\ComptaAsc\Remise\RemiseBanque;
use App\Model\GestionAsc\Billetterie\ReglementBilletAd;
use App\Model\GestionAsc\Billetterie\ReglementBilletSalarie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PositionOtherController extends PositionController
{
    public static function getNameType($types_id){
        switch ($types_id){
            case 0: return "Chèque";
            case 1: return "Espèce";
            default: return false;
        }
    }

    public static function sumReglement(){
        $salaries = ReglementBilletSalarie::where('modeReglement', 1)
            ->orWhere('modeReglement', 2)
            ->sum('totalReglement');

        $ads = ReglementBilletAd::where('modeReglement', 1)
            ->orWhere('modeReglement', 2)
            ->sum('totalReglement');

        $remises = RemiseBanque::where('etatRemise', 1)->sum('totalRemise');

        $calc = ($salaries + $ads) - $remises;

        return $calc;
    }
}
