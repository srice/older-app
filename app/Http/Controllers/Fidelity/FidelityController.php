<?php

namespace App\Http\Controllers\Fidelity;

use App\Model\Fidelity\FidelityBareme;
use App\Model\Fidelity\FidelityCompteur;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class FidelityController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'fidelity';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];


    }

    public function dashboard(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $compteurs = FidelityCompteur::all()->load('baremes');


        return view('Fidelity.dashboard', compact('config', 'compteurs'));
    }

    public function store(Request $request){

        $compteur = FidelityCompteur::create([
            "nameCompteur"  => $request->nameCompteur
        ]);

        if($compteur){
            Toastr::success("Le compteur à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du compteur !");
            return redirect()->back();
        }
    }

    public function delete($compteurs_id){
        $baremes = FidelityBareme::where('compteurs_id', $compteurs_id)->delete();
        $compteur = FidelityCompteur::find($compteurs_id)->delete();

        if($baremes && $compteur){
            Toastr::success("Le compteur et les barêmes ont été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du compteur !");
            return redirect()->back();
        }
    }
}
class FidelityOtherController extends FidelityController{
    public static function countBareme($compteurs_id){
        $bareme = FidelityBareme::where('compteurs_id', $compteurs_id)->get()->count();
        return $bareme;
    }
}
