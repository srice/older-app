<?php

namespace App\Http\Controllers\Assistance\Demande;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DemandeOtherController extends DemandeController
{
    public static function etatDemande($value){
        switch ($value){
            case 0: return '<span class="tag tag-warning">En considération</span>';
            case 1: return '<span class="tag tag-success">Terminer</span>';
            case 2: return '<span class="tag tag-danger">Refuser</span>';
            case 3: return '<span class="tag tag-info">Planifier</span>';
            case 4: return '<span class="tag tag-default">En cours...</span>';
            default: return false;
        }
    }
}
