<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReglementBilletSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reglement_billet_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numReglementBilletSalarie');
            $table->integer('billet_salarie_id');
            $table->integer('modeReglement');
            $table->timestamp('dateReglement')->nullable();
            $table->string('totalReglement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reglement_billet_salaries');
    }
}
