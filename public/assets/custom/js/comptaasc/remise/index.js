(function($){
    $(".date").datepicker({
        language: 'fr',
        format: 'dd-mm-yyyy',
        btnToday: true
    })
})(jQuery);