<?php


namespace App\Packages\Budgea;


class Banks extends Budgea
{
    public function __construct($settings = [])
    {
        parent::__construct($settings);
    }

    public function listAllBanks()
    {
        return $this->get('/banks');
    }

    public function getAccountType()
    {
        return $this->get('/account_types');
    }

    public function getAAccountType($id)
    {
        return $this->get('/account_types/'.$id);
    }

    public function createCategoryBank($name)
    {
        return $this->fetch('/banks/categories', [
            "name" => $name
        ], "POST");
    }
}