<?php

namespace App\Mail\Configuration\User;

use App\Packages\Srice\Comite;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUser extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var User
     */
    public $user;
    /**
     * @var
     */
    public $password;
    /**
     * @var
     */
    public $idComite;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param $idComite
     * @param $password
     * @internal param Comite $comite
     */
    public function __construct(User $user, $idComite, $password)
    {
        //
        $this->user = $user;
        $this->password = $password;
        $this->idComite = $idComite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Email.Configuration.User.create')
            ->subject('[SRICE] - Nouvelle accès au logiciel');
    }
}
