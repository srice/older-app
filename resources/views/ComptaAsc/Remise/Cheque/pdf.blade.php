<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $name }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="/assets/custom/css/pdf.css">
    <style type="text/css">
        body{
            font-family: Arial, sans-serif;
            font-size: 15px;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
<table style="width: 100%;">
    <tr>
        <td style="width: 33%" class="comite">
            <img src="/assets/custom/images/logos/1.jpg" width="100" alt="...">
            <br>
            Comite de Test<br>
            No Adresse<br>
            010101 No Ville
        </td>
        <td style="width: 33%;">&nbsp;</td>
        <td style="width: 33%; opacity: 0.5; font-weight: 400; font-size: 24px;">
            Remise en banque N°{{ $remise->numRemise }}
        </td>
    </tr>
</table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<h2>Prestations</h2>
<table style="width: 100%;" cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th style="width: 25%; text-align: center; background-color: #07d5ff; padding: 15px;">Numéro de Transaction</th>
        <th style="width: 25%; text-align: center; background-color: #07d5ff; padding: 15px;">Numéro de la facture</th>
        <th style="width: 25%; text-align: center; background-color: #07d5ff; padding: 15px;">Date du règlement</th>
        <th style="width: 25%; text-align: center; background-color: #07d5ff; padding: 15px;">Montant</th>
    </tr>
    </thead>
    <tbody>
    @foreach($remise->cheques as $cheque)
        <?php
            $salaries = \App\Model\GestionAsc\Billetterie\ReglementBilletSalarie::where('numReglementBilletSalarie', $cheque->numTransaction)->get()->load('billet');
            foreach ($salaries as $salary):
        ?>
        <tr>
            <td style="padding: 5px; border: solid 1px; text-align: center;">
                {{ $salary->numReglementBilletSalarie }}
            </td>
            <td style="padding: 5px; text-align: center; border: solid 1px;">{{ $salary->billet->numBilletSalarie }}</td>
            <td style="padding: 5px; text-align: center; border: solid 1px;">{{ $salary->dateReglement->format('d/m/Y') }}</td>
            <td style="padding: 5px; text-align: right; border: solid 1px;">{{ \App\Http\Controllers\OtherController::euro($salary->totalReglement) }}</td>
        </tr>
        <?php endforeach; ?>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="3" style="padding: 5px; text-align: right; border: solid 1px; background-color: #0b96e5; font-size: 18px;">Total de la remise</td>
        <td style="padding: 5px; text-align: right; border: solid 1px; background-color: #0b96e5; font-size: 18px;">{{ \App\Http\Controllers\OtherController::euro($remise->totalRemise) }}</td>
    </tr>
    </tfoot>
</table>
</body>
</html>