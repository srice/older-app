@extends('template')
@section("title")
    Configuration des Bénéficiaires
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
<div class="nav-tabs-horizontal" data-plugin="tabs">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" data-toggle="tab" href="#solde" aria-controls="solde" role="tab">Solde des salariés</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" data-toggle="tab" href="#activateur" aria-controls="activateur" role="tab">Activateur/Désactivateur</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" data-toggle="tab" href="#ageLimit" aria-controls="ageLimit" role="tab">Age Limite</a>
                </li>
            </ul>
        </div>
        <div class="card-block">
            <div class="tab-content p-t-20">
                <div class="tab-pane active" id="solde" role="tabpanel">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Prise en charge d'un solde pour les salariés</td>
                                <td class="text-xs-right">
                                    <input type="checkbox"
                                           id="inputSoldeActive"
                                           name="soldeActive"
                                           data-plugin="switchery"
                                           @if(\App\Http\Controllers\Beneficiaire\Configuration\ConfigurationOtherController::soldeIsActive($config->configuration['beneficiaire']->soldeActive) == true) checked @endif
                                    />
                                </td>
                            </tr>
                            <tr id="soldeInitCase">
                                <td>Solde Initial<br><i>Solde attribuer lors de la création ou de l'import du salarié</i></td>
                                <td>
                                    <input type="text"
                                           id="soldeInit"
                                           class="form-control"
                                           name="soldeInit"
                                           value="{{ $config->configuration['beneficiaire']->soldeInit }}"
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="activateur" role="tabpanel">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Activateur / Désactivateur de salariés</td>
                                <td>
                                    <input type="checkbox"
                                           id="inputActivateur"
                                           name="activateur"
                                           data-plugin="switchery"
                                           @if(\App\Http\Controllers\Beneficiaire\Configuration\ConfigurationOtherController::activateurIsActive($config->configuration['beneficiaire']->activateur) == true) checked @endif
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="ageLimit" role="tabpanel">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Prise en charge d'un age limit pour les ayants droit</td>
                                <td>
                                    <input type="checkbox"
                                           id="inputAgeLimit"
                                           name="adAgeLimitActive"
                                           data-plugin="switchery"
                                           @if(\App\Http\Controllers\Beneficiaire\Configuration\ConfigurationOtherController::ageLimitIsActive($config->configuration['beneficiaire']->adAgeLimitActive) == true) checked @endif
                                    />
                                </td>
                            </tr>
                            <tr id="ageLimitCase">
                                <td>Age Limite</td>
                                <td>
                                    <input type="text"
                                           id="adAgeLimit"
                                           class="form-control"
                                           name="adAgeLimit"
                                           value="{{ $config->configuration['beneficiaire']->adAgeLimit }}"
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/switchery/switchery.min.js"></script>
    <script src="/assets/custom/js/beneficiaire/configuration/index.js"></script>
@stop    