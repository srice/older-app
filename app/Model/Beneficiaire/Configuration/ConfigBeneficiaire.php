<?php

namespace App\Model\Beneficiaire\Configuration;

use Illuminate\Database\Eloquent\Model;

class ConfigBeneficiaire extends Model
{
    protected $guarded = [];
}
