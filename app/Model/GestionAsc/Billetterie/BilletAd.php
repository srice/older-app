<?php

namespace App\Model\GestionAsc\Billetterie;

use App\Model\Beneficiaire\Ad\AyantDroit;
use Illuminate\Database\Eloquent\Model;

class BilletAd extends Model
{
    protected $guarded = [];

    public function ad(){
        return $this->belongsTo(AyantDroit::class, "ads_id");
    }

    public function lignes(){
        return $this->hasMany(LigneBilletAd::class, 'billet_ads_id');
    }

    public function reglements(){
        return $this->hasMany(ReglementBilletAd::class, 'billet_ads_id');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateBillet"];
    }
}
