<?php

namespace App\Http\Controllers\ComptaAsc\Journal\Banque;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanSector;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalBanque;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class BanqueController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptaasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $banques = AscJournalBanque::all()->load('compte');
        $sectors = ComptaPlanSector::where('numClasse', 5)->get();

        return view('ComptaAsc.Journal.Banque.index', compact('config', 'banques', 'sectors'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "typeBanque"        => "required",
            "numCompte"         => "required",
            "dateBanque"        => "required",
            "libelleBanque"     => "required|min:5",
            "montantBanque"     => "required"
        ]);

        //Mise en forme de la date
        $date = strtotime($request->dateBanque);
        $dateBanque = Carbon::createFromTimestamp($date);

        //Numéro de Mouvement Bancaire
        if(empty($request->numBanque)){
            $numBanque = BanqueOtherController::genNumMouvement();
        }else{
            $numBanque = $request->numBanque;
        }

        //Choix de recette ou de dépense
        if($request->typeBanque == 0){
            $debitBanque = $request->montantBanque;
            $creditBanque = 0;
        }else{
            $creditBanque = $request->montantBanque;
            $debitBanque = 0;
        }

        $banque = AscJournalBanque::create([
            "numBanque"     => $numBanque,
            "dateBanque"    => $dateBanque,
            "numCompte"     => $request->numCompte,
            "libelleBanque" => $request->libelleBanque,
            "debitBanque"   => $debitBanque,
            "creditBanque"  => $creditBanque
        ]);

        //Mise à jour des comptes
        if($request->typeBanque == 0){
            $client = AscCompte::where('numCompte', '410DIV')->first();
            $cptBanque = AscCompte::where('numCompte', $request->numCompte)->first();

            $creditClient = $request->montantBanque;
            $newCredit = $client->credit + $creditClient;
            $client->update(["credit" => $newCredit]);

            $debitCptBanque = $request->montantBanque;
            $newDebit = $cptBanque->debit + $debitCptBanque;
            $cptBanque->update(["debit" => $newDebit]);
        }else{
            $four = AscCompte::where('numCompte', '401')->first();
            $cptBanque = AscCompte::where('numCompte', $request->numCompte)->first();

            $debitFour = $request->montantBanque;
            $newDebit = $four->debit + $debitFour;
            $four->update(["debit" => $newDebit]);

            $creditCptbanque = $request->montantBanque;
            $newCredit = $cptBanque->credit + $creditCptbanque;
            $cptBanque->update(["credit" => $newCredit]);
        }

        if($banque && $cptBanque){
            Toastr::success("Le mouvement bancaire à été ajouter");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l'ajout du mouvement bancaire");
            return redirect()->back();
        }

    }

    public function delete($numBanque){
        $banque = AscJournalBanque::where('numBanque', $numBanque)->first();

        if($banque->debitBanque > 0){
            $client = AscCompte::where('numCompte', '410DIV')->first();
            $cptBanque = AscCompte::where('numCompte', $banque->numCompte)->first();

            $creditClient = $client->credit;
            $newCredit = $creditClient - $banque->debitBanque;
            $client->update(["credit" => $newCredit]);

            $debitCptbanque = $cptBanque->debit;
            $newDebit = $debitCptbanque - $banque->debitBanque;
            $cptBanque->update(["debit" => $newDebit]);
        }else{
            $four = AscCompte::where('numCompte', '401')->first();
            $cptBanque = AscCompte::where('numCompte', $banque->numCompte)->first();

            $debitFour = $four->debit;
            $newDebit = $debitFour - $banque->creditBanque;
            $four->update(["debit" => $newDebit]);

            $creditCptBanque = $cptBanque->credit;
            $newCredit = $creditCptBanque - $banque->creditBanque;
            $cptBanque->update(["credit" => $newCredit]);
        }

        $banque->delete();

        if($banque && $cptBanque){
            Toastr::success("Le mouvement bancaire à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du mouvement Bancaire !");
            return redirect()->back();
        }
    }
}
class BanqueOtherController extends BanqueController{
    public static function genNumMouvement(){
        return "BQMOUV".date('Ym').rand(0, 999);
    }
}
