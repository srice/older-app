@extends('template')
@section("title")
    Tableau de configuration du module
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste des Compteurs</h3>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#addCompteur"><i class="fa fa-plus-circle"></i> Ajouter un compteur</button>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="80%">Nom du compteur</th>
                            <th width="10%">Nombre de barêmes</th>
                            <th width="10%" class="text-xs-center"><i class="fa fa-bars"></i> </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($compteurs as $compteur)
                        <tr>
                            <td>{{ $compteur->nameCompteur }}</td>
                            <td class="text-xs-center">{{ \App\Http\Controllers\Fidelity\FidelityOtherController::countBareme($compteur->id) }}</td>
                            <td>
                                {{ Form::model($compteur, ["route" => ["fidelity.delete", $compteur->id], "method" => "DELETE"]) }}
                                <a href="{{ route('fidelity.bareme.index', $compteur) }}" class="btn btn-icon btn-sm btn-primary"><i class="fa fa-eye"></i> </a>
                                <button class="btn btn-icon btn-sm btn-danger"><i class="fa fa-trash"></i> </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addCompteur" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un Compteur</h4>
                </div>
                {{ Form::open(["route" => "fidelity.store"]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('nameCompteur', 'Nom du compteur', ["class" => "control-label"]) }}
                        {{ Form::text('nameCompteur', null, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    