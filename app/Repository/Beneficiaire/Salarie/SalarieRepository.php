<?php
namespace App\Repository\Beneficiaire\Salarie;

use App\Model\Beneficiaire\Salarie\Salarie;

class SalarieRepository
{
    /**
     * @var Salarie
     */
    private $salarie;

    /**
     * SalarieRepository constructor.
     * @param Salarie $salarie
     */

    public function __construct(Salarie $salarie)
    {
        $this->salarie = $salarie;
    }

    public function list()
    {
        return $this->salarie->newQuery()
            ->get()
            ->load('ad', 'fidelities', 'billet', 'ancvs');
    }

    public function get($salarie_id)
    {
        return $this->salarie->newQuery()
            ->find($salarie_id)
            ->load('ad', 'fidelities', 'billet', 'ancvs');
    }

}

        