<?php

namespace App\Http\Controllers\Automate;

use App\Http\Controllers\Configuration\Database\DatabaseOtherController;
use App\Mail\Automate\AutomatingMail;
use App\Model\Automate\Automate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class DailyController extends Controller
{
    public static function exec(){
        DatabaseOtherController::SavingAutoDatabase();

        $now = strtotime(date('d-m-Y'));
        Mail::to('technique@cridip.com')->send(new AutomatingMail(Automate::where("created_at", Carbon::createFromTimestamp($now))->get()));
    }
}
