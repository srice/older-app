(function ($) {
widget();
graph()
})(jQuery);

function widget() {
    let div = $("#widget");

    div.html('<div class="text-center"><i class="fa fa-spinner fa-5x spinner"></i> </div>');

    $.get('/facturation/widget')
        .done(function(data){
            div.html(data);
        })
        .fail(function (data) {
            toastr.error("Erreur lors de l'affichage des données de widget")
        })
}

function graph() {
    let div = $("#graph");

    div.html('<div class="text-center"><i class="fa fa-spinner fa-5x spinner"></i> </div>');

    $.get('/facturation/graph')
        .done(function (data) {
            div.html(data)
        })
        .fail(function (data) {
            toastr.error("Erreur lors de l'affichage du graphique");
        })
}

