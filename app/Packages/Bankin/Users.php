<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 15/08/2017
 * Time: 21:33
 */

namespace App\Packages\Bankin;


class Users extends Appel
{
    public function createUser($email, $password){
        $params = [
            "email"         => $email,
            "password"      => $password,
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret']
        ];

        $call = $this->post('users?', $params);

        return $call;
    }

    public function authUser($email, $password){
        $params = [
            "email"         => $email,
            "password"      => $password,
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret']
        ];

        $call = $this->post('authenticate?', $params);

        return $call;
    }

    public function logoutUser($email, $password, $bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret']
        ];

        $call = $this->post('logout?', $params, true, $bearer);

        return $call;
    }

    public function listUsers(){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "limit"         => 50
        ];

        $call = $this->get('users?', $params);

        return $call;
    }

    public function editUser($uuid, $currentPassword, $newPassword){
        $params = [
            "client_id"         => $this->auth['clientId'],
            "client_secret"     => $this->auth['clientSecret'],
            "current_password"  => $currentPassword,
            "new_password"      => $newPassword
        ];

        $call = $this->put('users/'.$uuid.'/password?', $params);

        return $call;
    }

    public function deleteUser($uuid, $password){
        $params = [
            "client_id"         => $this->auth['clientId'],
            "client_secret"     => $this->auth['clientSecret'],
            "password"          => $password
        ];

        $call = $this->delete('users/'.$uuid.'?', $params);

        return $call;
    }
}