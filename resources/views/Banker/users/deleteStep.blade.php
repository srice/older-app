@extends('template')
@section("title")
    Confirmation de la suppression
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield("title")</h3>
        </header>
        <div class="panel-body">
            {{ Form::model($uuid, ["route" => ["banker.users.delete", $uuid], "method" => "DELETE"]) }}
            <div class="form-group form-material">
                {{ Form::label('password', "Mot de passe de l'utilisateur", ["class" => "control-label"]) }}
                {{ Form::password('password', ["class" => "form-control"]) }}
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    