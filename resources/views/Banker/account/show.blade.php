@extends('template')
@section("title")
    Vue du compte <strong>{{ $account->name }}</strong>
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-header">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste des mouvements</h3>
                </div>
                <!--<div class="col-md-2">
                    <a href="#addAccount" data-toggle="modal" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Ajout un compte</a>
                </div>-->
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center"><i class="fa fa-clock-o" data-toggle="tooltip" title="En attente"></i></th>
                            <th>Date de mouvement</th>
                            <th>Désignation</th>
                            <th>Débit</th>
                            <th>Crédit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transactions as $transaction)
                            <tr>
                                <td>
                                    @if($transaction->is_future == 1)
                                        <i class="fa fa-clock-o" data-toggle="tooltip" title="En attente"></i>
                                    @endif
                                </td>
                                <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($transaction->date))->format('d-m-Y') }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src="{{ \App\HelperClass\Banker\BankerCategory::getIconCategory($transaction->category->id) }}"
                                                 alt="" width="24" height="24" class="img-responsive img-rounded img-bordered-primary" data-toggle="tooltip" title="{{ \App\HelperClass\Banker\BankerCategory::getNameCategory($transaction->category->id) }}">
                                        </div>
                                        <div class="col-md-11">
                                            <strong>{{ $transaction->description }}</strong><br>
                                            <i>{{ $transaction->raw_description }}</i>
                                        </div>
                                    </div>
                                </td>
                                <td style="text-align: right;">
                                    @if($transaction->amount < 0)
                                        {{ formatCurrency($transaction->amount) }}
                                    @endif
                                </td>
                                <td style="text-align: right">
                                    @if($transaction->amount > 0 || $transaction->amount == 0)
                                        {{ formatCurrency($transaction->amount) }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    