<?php

namespace App\Model\Facturation;

use Illuminate\Database\Eloquent\Model;

class FacturationSolde extends Model
{
    protected $guarded = [];

    public function tier(){
        return $this->belongsTo(FacturationTier::class);
    }
}
