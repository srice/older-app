@extends('template')
@section("title")
    Edition d'un mode de règlement
@stop
@section("header_styles")

@stop
@section("content")
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">@yield("title")</h3>
    </div>
    {{ Form::model($mode, ["route" => ["facturation.modeReglement.update", $mode->id], "method" => "PUT"]) }}
    <div class="panel-body">
        <div class="form-group form-material">
            {{ Form::label('libelle', "Libelle", ["class" => "control-label"]) }}
            {{ Form::text('libelle', null, ["class" => "form-control", "required"]) }}
        </div>
    </div>
    <div class="panel-footer text-xs-right">
        <button id="btnEditTier" class="btn btn-primary"><i class="fa fa-check"></i> Modifier le mode de règlement</button>
    </div>
    {{ Form::close() }}
</div>
@stop
@section("footer_scripts")

@stop    