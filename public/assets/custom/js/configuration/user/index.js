(function ($) {
    tableUser();
})(jQuery);
function tableUser(){
    var filtering = $('#listeUser');
    filtering.footable().on('footable_filtering', function(e) {
        var selected = $('#filteringStatus').find(':selected').val();
        e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
        e.clear = !e.filter;
    });

    // Filter status
    $('#filteringStatus').change(function(e) {
        e.preventDefault();
        filtering.trigger('footable_filter', {
            filter: $(this).val()
        });
    });

    // Search input
    $('#filteringSearch').on('input', function(e) {
        e.preventDefault();
        filtering.trigger('footable_filter', {
            filter: $(this).val()
        });
    });
}