<?php


namespace App\Packages\Budgea;



class Budgea
{
    /**
     * @var string
     */
    private $endpoint;
    /**
     * @var string
     */
    private $client_id;
    /**
     * @var string
     */
    private $client_secret;

    public function __construct()
    {
        $this->endpoint = env("BUDGEA_ENDPOINT");
        $this->client_id = env("BUDGEA_CLIENT_ID");
        $this->client_secret = env("BUDGEA_CLIENT_SECRET");

    }

}