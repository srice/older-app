<?php


namespace App\HelperClass\Banker;


class BankerAccount
{
    public static function viewType($type)
    {
        switch ($type)
        {
            case 'checking': return 'Compte courant'; break;
            case 'savings': return 'Compte Epargne'; break;
            case 'securities': return 'Compte Titre'; break;
            case 'card': return 'Carte de crédit ou de débit'; break;
            case 'load': return 'Pret'; break;
            case 'share_savings_plan': return 'PEA français, Plan d\'Epargne en Actions'; break;
            case 'pending': return 'Compte virtuel'; break;
            case 'life_insurance': return 'Assurance-vie'; break;
            case 'unknown': return 'Inconnu'; break;
            default: return 'Inconnu';
        }
    }

    public static function formatSoldeAccount($solde)
    {
        if($solde < 0)
        {
            return '<span class="text-danger">'.$solde.' €</span>';
        }elseif($solde == 0)
        {
            return '<span class="text-info">'.$solde.' €</span>';
        }else{
            return '<span class="text-success">+ '.$solde.' €</span>';
        }
    }

    public static function getTotalForAccounts($resources)
    {
        $total = 0;

        foreach ($resources as $resource)
        {
            $new_total = $resource->balance + $total;
        }

        return self::formatSoldeAccount($new_total);
    }
}