@extends('template')
@section("title")
    Tableau des Barême pour le compteur: <strong>{{ $compteur->nameCompteur }}</strong>
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste des barême</h3>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#addBareme"><i class="fa fa-plus-circle"></i> Nouveau barême</button>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Montant de départ</th>
                            <th>Montant de fin</th>
                            <th>Nombre de point rapporter</th>
                            <th class="text-xs-center"><i class="fa fa-bars"></i> </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($baremes as $bareme)
                        <tr>
                            <td class="text-xs-center">{{ \App\Http\Controllers\OtherController::euro($bareme->subStart) }}</td>
                            <td class="text-xs-center">{{ \App\Http\Controllers\OtherController::euro($bareme->subEnd) }}</td>
                            <td class="text-xs-center">{{ $bareme->nbPoint }}</td>
                            <td class="text-xs-center">
                                {{ Form::model($bareme, ["route" => ["fidelity.bareme.delete", $compteur->id, $bareme->id], "method" => "DELETE"]) }}
                                <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addBareme" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un bareme</h4>
                </div>
                {{ Form::model($compteur, ["route" => ["fidelity.bareme.store", $compteur->id]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('subStart', 'Montant de départ', ["class" => "control-label"]) }}
                        {{ Form::text('subStart', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('subEnd', 'Montant de fin', ["class" => "control-label"]) }}
                        {{ Form::text('subEnd', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('nbPoint', 'Nombre de point accordée', ["class" => "control-label"]) }}
                        {{ Form::text('nbPoint', null, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    