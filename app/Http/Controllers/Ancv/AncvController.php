<?php

namespace App\Http\Controllers\Ancv;

use App\Packages\Srice\Espace;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AncvController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;

    public function __construct(Espace $espace, Guard $auth)
    {
        $this->auth = $auth;
        $this->sector = 'ancv';
        $this->modules = $espace->listeModule();
    }

    public function dashboard()
    {
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        return view ("Ancv.dashboard", [
            "config"    => $config
        ]);
    }
}
