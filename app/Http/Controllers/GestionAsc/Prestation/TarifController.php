<?php

namespace App\Http\Controllers\GestionAsc\Prestation;

use App\Model\GestionAsc\Prestation\PrestationTarif;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class TarifController extends PrestationController
{
    public function storeTarif(Request $request, $prestations_id){

        $price = $request->price_sal+$request->price_ce;
        if(isset($request->stock)){
            $stock = 1;
        }else{
            $stock = 0;
        }


        $tarif = PrestationTarif::create([
            "prestations_id"    => $prestations_id,
            "libelle"           => $request->libelle,
            "price"             => $price,
            "quota"             => 0,
            "stock"             => $stock,
            "stockLimit"        => 0,
            "stockActuel"       => 0,
            "price_sal"         => $request->price_sal,
            "price_ce"          => $request->price_ce
        ]);

        if($tarif){
            Toastr::success("Tarification Ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la création du Tarif !");
            return redirect()->back();
        }
    }

    public function deleteTarif($prestations_id, $tarif_id){
        $tarif = PrestationTarif::destroy($tarif_id);

        if($tarif){
            Toastr::success("Tarification supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du Tarif !");
            return redirect()->back();
        }
    }


}
