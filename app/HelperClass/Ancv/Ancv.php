<?php


namespace App\HelperClass\Ancv;


use Illuminate\Support\Arr;

class Ancv
{
    public static function solution($solution)
    {
        switch ($solution)
        {
            case 0: return "Chèques Vancance"; break;
            case 1: return "Coupon sport"; break;
            case 2: return "Départ 18-25"; break;
        }
    }

    public static function solutionArray()
    {
        return [
            [
                "id"    => 0,
                "name"  => "Chèques vacances"
            ],
            [
                "id"    => 1,
                "name"  => "Coupon Sport"
            ],
            [
                "id"    => 2,
                "name"  => "Départ 18-25"
            ]
        ];
    }

    public static function getSolutionArray($designation)
    {
        switch ($designation){
            case 'Chèques vacances': return 0;
            case 'Coupon Sport': return 1;
            case 'Départ 18-25': return 2;
        }
    }
}