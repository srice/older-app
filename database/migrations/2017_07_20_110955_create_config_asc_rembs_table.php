<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigAscRembsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_asc_rembs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('percent')->default(0);
            $table->string('percentSalarie')->default(0);
            $table->string('percentAd')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_asc_rembs');
    }
}
