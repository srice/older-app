(function ($) {
    $("#stockInfo").hide();
    $("#dateRepresentative").hide();
    $("#newSeance").show();
    $("#summernote").summernote({
        height: 350,
    });
    $(".dateStart").datepicker({
        language: 'fr',
        format: "dd-mm-yyyy"
    });
    $(".dateStartUnique").datepicker({
        language: 'fr',
        format: "dd-mm-yyyy"
    });
    $(".dateEnd").datepicker({
        language: 'fr',
        format: "dd-mm-yyyy"
    });
    $(".hour").timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '0',
        maxTime: '23:45',
        defaultTime: '00',
        startTime: '00:00am',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    })
})(jQuery);

function appearStock(){
    if(document.getElementById("stock").checked === true){
        $("#stockInfo").show();
    }else{
        $("#stockInfo").hide();
    }
}

function appearRepresentative(){
    if(document.getElementById("representative").checked === true){
        $("#newSeance").hide();
        $("#dateRepresentative").show();
    }else{
        $("#newSeance").show();
        $("#dateRepresentative").hide();
    }
}