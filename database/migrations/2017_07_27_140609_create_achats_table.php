<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numAchat');
            $table->integer('fournisseurs_id');
            $table->timestamp('dateAchat')->nullable();
            $table->string('totalAchat');
            $table->integer('etatAchat'); //0: Brouillon |1: Valider |2: Partiellement Payer |3: Payer
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achats');
    }
}
