<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBilletAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billet_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numBilletAd');
            $table->integer('ads_id');
            $table->timestamp('dateBillet')->nullable();
            $table->string('totalBillet');
            $table->integer('etatBillet');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billet_ads');
    }
}
