@extends('template')
@section('title')
    Facturation Immédiate
@stop
@section('header_styles')
@stop

@section("content")
    <div class="row">
        <div id="widget"></div>
    </div>
    <div id="graph"></div>
@stop
@section('footer_scripts')
    <script src="/assets/custom/js/facturation/dashboard.js"></script>
@stop