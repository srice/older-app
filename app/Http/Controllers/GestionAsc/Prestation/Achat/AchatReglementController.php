<?php

namespace App\Http\Controllers\GestionAsc\Prestation\Achat;

use App\Http\Controllers\ComptaAsc\Etat\CompteController;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalBanque;
use App\Model\ComptaAsc\Journal\AscJournalCaisse;
use App\Model\GestionAsc\Prestation\Achat\Achat;
use App\Model\GestionAsc\Prestation\Achat\AchatReglement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class AchatReglementController extends AchatController
{
    public static function verifSoldeFacture($achats_id){
        $achat = Achat::find($achats_id);
        $sum = AchatReglement::where('achats_id', $achats_id)->sum('totalReglement');

        if($achat->totalAchat < $sum){
            return true;
        }else{
            return false;
        }
    }

    public static function totalReglement($achats_id){
        $sum = AchatReglement::where('achats_id', $achats_id)->get()->sum('totalReglement');
        return $sum;
    }

    public static function countAllReglement(){
        $count = AchatReglement::all()->count();
        return $count;
    }

    public static function countReglement($achats_id){
        $count = AchatReglement::where('achats_id', $achats_id)->get()->count();
        return $count;
    }

    public static function modeReglement($modes_id){
        switch ($modes_id){
            case 1: return "Chèque";
            case 2: return "Espèce";
            case 3: return "Carte Bancaire";
            case 4: return "Virement bancaire";
            case 5: return "Prémèvement Bancaire";
            default: return false;
        }
    }

    public function addReglement(Request $request, $achats_id){
        $this->validate($request, [
            "modeReglement" => "required",
            "dateReglement" => "required",
            "totalReglement"=> "required"
        ]);

        $achat = Achat::find($achats_id)->load('fournisseur');

        if(self::countAllReglement() == 0){
            $id = 1;
        }else{
            $reglement = AchatReglement::all()->last();
            $id = $reglement->id;
        }

        if($request->modeReglement != 1){
            $numReglement = Controller::genNumerator("RGLTACH", $id);
        }else{
            $numReglement = $request->numReglement;
        }

        $date = $request->dateReglement;
        $strt = strtotime($date);
        $dateReglement = Carbon::createFromTimestamp($strt);

        $addReglement = AchatReglement::create([
            "achats_id"     => $achats_id,
            "numReglement"  => $numReglement,
            "dateReglement" => $dateReglement,
            "modeReglement" => $request->modeReglement,
            "totalReglement"=> $request->totalReglement
        ]);

        //Inscription dans le journal
        if($request->modeReglement == 2){
            $newJournalCaisse = AscJournalCaisse::create([
                "numCaisse"     => $numReglement,
                "dateCaisse"    => $dateReglement,
                "numCompte"     => 530,
                "libelleCaisse" => "Règlement en espèce pour l'achat N°<strong>".$achat->numAchat."</strong>",
                "creditCaisse"  => $request->totalReglement
            ]);

            // Mise à jour des Comptes
            $debit = CompteController::soldeDebit($achat->fournisseur->numCompte) + $request->totalReglement;
            $credit = CompteController::soldeCredit('530') + $request->totalReglement;

            $cptDebit = AscCompte::where('numCompte', $achat->fournisseur->numCompte)->update(["debit" => $debit]);
            $cptCredit = AscCompte::where('numCompte', '530')->update(["credit" => $credit]);
        }else{
            $newJournalbanque = AscJournalBanque::create([
                "numBanque"     => $numReglement,
                "dateBanque"    => $dateReglement,
                "numCompte"     => 512,
                "libelleBanque" => "Règlement en <strong>".self::modeReglement($request->modeReglement)."</strong> pour l'achat N°<strong>".$achat->numAchat."</strong>",
                "creditBanque"  => $request->totalReglement
            ]);

            // Mise à jour des Comptes
            $debit = CompteController::soldeDebit($achat->fournisseur->numCompte) + $request->totalReglement;
            $credit = CompteController::soldeCredit('512') + $request->totalReglement;

            $cptDebit = AscCompte::where('numCompte', $achat->fournisseur->numCompte)->update(["debit" => $debit]);
            $cptCredit = AscCompte::where('numCompte', '512')->update(["credit" => $credit]);
        }


        if(self::totalReglement($achats_id) < $achat->totalAchat){
            $achat->update(["etatAchat" => 2]);
        }elseif(self::totalReglement($achats_id) == $achat->totalAchat){
            $achat->update(["etatAchat" => 3]);
        }else{
            $achat->update(["etatAchat" => 1]);
        }

        if($achat && $addReglement && $cptDebit && $cptCredit && ($newJournalbanque || $newJournalCaisse)){
            Toastr::success("Le règlement à été ajouté !");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du règlement !");
            return redirect()->back();
        }

    }

    public function delReglement($achats_id, $reglements_id){
        $achat = Achat::find($achats_id);
        $reglement = AchatReglement::find($reglements_id);

        if($reglement->modeReglement == 2){
            $delJournalCaisse = AscJournalCaisse::where('numCaisse', $reglement->numReglement)->delete();

            // Mise à jour des Comptes
            $debit = CompteController::soldeDebit($achat->fournisseur->numCompte) - $reglement->totalReglement;
            $credit = CompteController::soldeCredit('530') - $reglement->totalReglement;

            $cptDebit = AscCompte::where('numCompte', $achat->fournisseur->numCompte)->update(["debit" => $debit]);
            $cptCredit = AscCompte::where('numCompte', '530')->update(["credit" => $credit]);
        }else{
            $delJournalBanque = AscJournalBanque::where('numBanque', $reglement->numReglement)->delete();

            // Mise à jour des Comptes
            $debit = CompteController::soldeDebit($achat->fournisseur->numCompte) - $reglement->totalReglement;
            $credit = CompteController::soldeCredit('512') - $reglement->totalReglement;

            $cptDebit = AscCompte::where('numCompte', $achat->fournisseur->numCompte)->update(["debit" => $debit]);
            $cptCredit = AscCompte::where('numCompte', '512')->update(["credit" => $credit]);
        }


        $reglement->delete();



        if(self::totalReglement($achats_id) < $achat->totalAchat){
            $achat->update(["etatAchat" => 2]);
        }elseif(self::totalReglement($achats_id) == $achat->totalAchat){
            $achat->update(["etatAchat" => 3]);
        }elseif(self::totalReglement($achats_id) == 0){
            $achat->update(["etatAchat" => 1]);
        }else{
            $achat->update(["etatAchat" => 1]);
        }

        if($reglement && $achat && $cptDebit && $cptCredit && ($delJournalCaisse || $delJournalBanque)){
            Toastr::success("Le règlement à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du règlement !");
            return redirect()->back();
        }
    }
}
