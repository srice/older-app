<?php

namespace App\Http\Controllers\Facturation\Tiers;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaFct\Configuration\Plan\FctPlanCompte;
use App\Model\ComptaFct\Etat\FctCompte;
use App\Model\Facturation\Facturation;
use App\Model\Facturation\FacturationLigne;
use App\Model\Facturation\FacturationReglement;
use App\Model\Facturation\FacturationSolde;
use App\Model\Facturation\FacturationTier;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Espace;
use ComptaFctCompte;
use ComptaFctPlanCompte;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class TiersController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'facturation';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $tiers = FacturationTier::all();

        return view('Facturation.Tiers.index', compact('config', 'tiers'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $this->validate($request, [
            "typeTier"      => "required",
            "nomTier"       => "required",
            "adresseTier"   => "required",
            "codePostalTier"=> "required|max:5",
            "villeTier"     => "required"
        ]);

        $str = str_limit($request->nomTier, 3, '');
        if($request->typeTier == 0){
            $codeCompta = "410".$str;
            $sector = 41;
        }else{
            $codeCompta = "401".$str;
            $sector = 40;
        }


        $numTier    = rand(1000,10000);

        $tier = FacturationTier::create([
            "codeCompta"    => $codeCompta,
            "typeTier"      => $request->typeTier,
            "numTier"       => $numTier,
            "nomTier"       => $request->nomTier,
            "adresseTier"   => $request->adresseTier,
            "codePostalTier"=> $request->codePostalTier,
            "villeTier"     => $request->villeTier,
            "soldeTier"     => 0
        ]);

        $asc = ComptaPlanCompte::create([
            "numCompte"     => $codeCompta,
            "numSector"     => $sector,
            "nameCompte"    => $request->nomTier
        ]);
        $ascCompte = AscCompte::create([
            "numCompte"     => $codeCompta
        ]);

        $fct = FctPlanCompte::create([
            "numCompte"     => $codeCompta,
            "numSector"     => $sector,
            "nameCompte"    => $request->nomTier
        ]);
        $fctCompte = FctCompte::create([
            "numCompte"     => $codeCompta
        ]);

        if($tier && $asc && $ascCompte && $fct && $fctCompte){
            Toastr::success("Le tier à été ajouter !");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du tier");
            return redirect()->back();
        }
    }

    public function show($tiers_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $tier = FacturationTier::find($tiers_id)->load('factures', 'soldes');

        return view('Facturation.Tiers.show', compact('config', 'tier'));
    }

    public function edit($tiers_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $tier = FacturationTier::find($tiers_id);

        return view('Facturation.Tiers.edit', compact('config', 'tier'));
    }

    public function update(Request $request, $tiers_id){
        $this->validate($request, [
            "typeTier"      => "required",
            "nomTier"       => "required",
            "adresseTier"   => "required",
            "codePostalTier"=> "required|max:5",
            "villeTier"     => "required"
        ]);

        $tier = FacturationTier::find($tiers_id);

        $str = str_limit($request->nomTier, 3, '');
        if($request->typeTier == 0){
            $codeCompta = "410$str";
            $sector = 41;
        }else{
            $codeCompta = "401$str";
            $sector = 40;
        }

        $tier = FacturationTier::find($tiers_id);

        $upAscPlan = ComptaPlanCompte::where('numCompte', $tier->codeCompta)->update(["numCompte" => $codeCompta, "nameCompte" => $request->nomTier]);
        $upAscCompte = AscCompte::where('numCompte', $tier->codeCompta)->update(["numCompte" => $codeCompta]);

        $upFctPlan = FctPlanCompte::where('numCompte', $tier->codeCompta)->update(["numCompte" => $codeCompta, "nameCompte" => $request->nomTier]);

        $upTier = $tier->update([
            "typeTier"      => $request->typeTier,
            "codeCompta"    => $codeCompta,
            "nomTier"       => $request->nomTier,
            "adresseTier"   => $request->adresseTier,
            "codePostalTier"=> $request->codePostalTier,
            "villeTier"     => $request->villeTier
        ]);

        if($upTier && $upAscPlan && $upAscCompte && $upFctPlan){
            Toastr::success("Le tier à été modifié");
            return redirect()->route('facturation.tiers');
        }else{
            Toastr::error("Erreur lors de la modification du tier !");
            return redirect()->back();
        }

    }

    public function delete($tiers_id){
        $tier = FacturationTier::find($tiers_id);
        $factures = Facturation::where('tiers_id', $tiers_id)->get();
        $soldes = FacturationSolde::where('tiers_id', $tiers_id)->get();

        $delAscPlan = ComptaPlanCompte::where('numCompte', $tier->codeCompta)->first()->delete();
        $delAscCompte = AscCompte::where('numCompte', $tier->codeCompta)->first()->delete();
        $delFctPlan = FctPlanCompte::where('numCompte', $tier->codeCompta)->first()->delete();
        $delFctCompte = FctCompte::where('numCompte', $tier->codeCompta)->first()->delete();

        foreach ($factures as $facture){
            $ligne = FacturationLigne::where('factures_id', $facture->id)->get()->destroy();
            $rglt = FacturationReglement::where('factures_id', $facture->id)->get()->delete();
        }

        $delSolde = $soldes->delete();
        $delfacture = $factures->delete();

        $delTier = $tier->delete();

        if($delAscCompte && $delAscPlan && $delFctCompte && $delFctPlan && $delTier && $delSolde && $delfacture){
            Toastr::success("Le tier à été supprimé !");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du tier !");
            return redirect()->back();
        }
    }
}
