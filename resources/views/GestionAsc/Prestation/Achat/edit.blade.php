@extends('template')
@section("title")
    Edition d'une facture d'achat
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
    <div class="panel">
        <header class="panel-header">
            <h3 class="panel-title">@yield("title")</h3>
        </header>
        <div class="panel-body">
            {{ Form::model($achat, ["route" => ["achats.update", $achat->id], "class" => "form-horizontal", "method" => "PUT"]) }}
            <div class="form-group form-material row">
                {{ Form::label('fournisseurs_id', "Fournisseur", ["class" => "col-md-3 control-label"]) }}
                <div class="col-md-6">
                    {{ Form::select('fournisseurs_id', $fournisseurs->pluck('name', 'id'), $achat->fournisseurs_id, ["class" => "form-control"]) }}
                </div>
            </div>
            <div class="form-group form-material row">
                {{ Form::label('dateAchat', "Date de la facture", ["class" => "col-md-3 control-label"]) }}
                <div class="col-md-3">
                    {{ Form::text("dateAchat", $achat->dateAchat->format('d-m-Y'), ["class" => "form-control date"]) }}
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/custom/js/gestionasc/prestation/achat/edit.js"></script>
@stop    