<?php

namespace App\Http\Controllers\ComptaAsc\Configuration\Initial;

use App\Model\ComptaAsc\Configuration\Bilan\AscBilanInitial;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use App\Model\ComptaAsc\Etat\AscCompte;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class InitialOtherController extends InitialController
{
    // Route

    public function validation(Request $request){
        $debit = self::getTotalDebit();
        $credit = self::getTotalCredit();

        $total = $credit - $debit;

        if($total > 0){
            $update = AscCompte::where('numCompte', '110')->update([
                "credit"    => $total
            ]);
            $update = AscCompte::where('numCompte', '101')->update([
                "debit"    => $total
            ]);
        }else{
            $update = AscCompte::where('numCompte', '119')->update([
                "debit"    => $total
            ]);
            $update = AscCompte::where('numCompte', '101')->update([
                "credit"    => $total
            ]);
        }

        if($update){
            Toastr::success("Le bilan initial à été valider");
            return redirect()->route('comptaAsc.config.initial.index');
        }
    }

    // Static

    public static function soldeDebit($numCompte){
        $compte = AscBilanInitial::where('numCompte', $numCompte)->sum('debit');
        return $compte;
    }

    public static function soldeCredit($numCompte){
        $compte = AscBilanInitial::where('numCompte', $numCompte)->sum('credit');
        return $compte;
    }

    public static function getNameCompte($numCompte){
        $compte = ComptaPlanCompte::where('numCompte', $numCompte)->first();
        return $compte->nameCompte;
    }

    public static function getTotalDebit(){
        $compte = AscBilanInitial::all()->sum('debit');
        return $compte;
    }

    public static function getTotalCredit(){
        $compte = AscBilanInitial::all()->sum('credit');
        return $compte;
    }
}
