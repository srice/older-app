<div class="page-header">
    <h1 class="page-title">@yield("title")</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../index.html">{{ env('APP_NAME') }}</a></li>
        @if(!empty($config->sector))
            <li class="breadcrumb-item"><a href="javascript:void(0)">{{ $config->sector }}</a></li>
        @endif
        <li class="breadcrumb-item active">@yield('title')</li>
    </ol>
    @if($config->parent == 1)
        <div class="page-header-actions">
            <button class="btn btn-sm btn-primary btn-round" onclick="history.back()">
                <i class="icon md-arrow-left" aria-hidden="true"></i>
                <span class="hidden-sm-down">Retour</span>
            </button>
        </div>
    @endif
</div>
