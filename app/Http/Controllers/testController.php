<?php

namespace App\Http\Controllers;


use App\HelperClass\Ancv\Ancv;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Bankin\Categories;
use App\Packages\Srice\Espace;
use Illuminate\Contracts\Auth\Guard;

class testController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;
    public function __construct(Espace $espace, Guard $auth)
    {
        $this->auth = $auth;
        $this->sector = '';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function test(){
        dd(Ancv::getSolutionArray('Coupon Sport'));
    }
}
