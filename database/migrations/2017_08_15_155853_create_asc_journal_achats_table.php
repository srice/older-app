<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAscJournalAchatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asc_journal_achats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numAchat');
            $table->timestamp('dateAchat')->nullable();
            $table->string('numCompte');
            $table->string('libelleAchat');
            $table->string('debitAchat')->default(0);
            $table->string('creditAchat')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asc_journal_achats');
    }
}
