<?php
if(!function_exists('sourceImage')){
    function sourceImage($images){
        if(env('APP_ENV') == 'production')
        {
            return \Illuminate\Support\Facades\Storage::disk('s3')->url($images);
        }else{
            return \Illuminate\Support\Facades\Storage::disk('public')->url($images);
        }
    }
}
