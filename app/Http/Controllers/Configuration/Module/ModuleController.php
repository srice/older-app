<?php

namespace App\Http\Controllers\Configuration\Module;

use App\Mail\Configuration\Module\AcceptClef;
use App\Mail\Configuration\Module\DemandeClef;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Comite;
use App\Packages\Srice\Commande;
use App\Packages\Srice\Espace;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class ModuleController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;
    public function __construct(Espace $espace, Mailer $mailer, Guard $auth)
    {
        $this->auth = $auth;
        $this->sector = 'configuration';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];

        $this->mailer = $mailer;
    }

    public function index(Espace $espace){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];


        $modules = $espace->listeModule();

        return view('Configuration.Module.index', compact('config', 'modules'));
    }

    public function ficheProject($id, Espace $espace){
        $module = $espace->module($id);
        ob_start();
        ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Module: <?= $module->designation; ?></h4>
        </div>
        <div class="modal-body">
            <?php if($module->release == 4): ?>
                <div class="alert alert-info">
                    <i class="fa fa-info-circle"></i> Le module est actuellement en <strong>BETA</strong>, il vous est donc possible de demander une clef de test.
                </div>
            <?php endif; ?>
            <?php if($module->release == 5): ?>
                <div class="alert alert-info">
                    <i class="fa fa-info-circle"></i> Le module est actuellement en <strong>GAMMA</strong>, il vous est donc possible de demander une clef de test.<br>
                    Le module sera disponible prochainement !
                </div>
            <?php endif; ?>
            <?php if($module->release == 6): ?>
                <div class="alert alert-info">
                    <i class="fa fa-info-circle"></i> Le module est actuellement en <strong>RELEASE CANDIDATE</strong>, il vous est donc possible de demander une licence d'utilisation.
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-6 text-xs-center" style="vertical-align: middle">
                    <img src="//gestion.<?= env('APP_DOMAIN') ?>/assets/custom/img/modules/<?= $module->image ?>" class="img-responsive img-rounded img-bordered img-bordered-green" width="120">
                </div>
                <div class="col-md-6">
                    <strong>Nom du module:</strong> <?= $module->designation ?><br>
                    <strong>Version du module:</strong> <a href="<?= env('APP_BASE_URL') ?>/module/<?= $module->id; ?>/version/<?= $module->version ?>"><?= $module->version ?></a><br>
                    <strong>Etape du module:</strong> <?= ModuleOtherController::releaseModuleTag($module->release) ?><br>
                    <strong>Description:</strong><br><br><?= $module->description; ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }
    public function clefAccess($id, Espace $espace){
        $info = $espace->espace();
        $module = $espace->module($id);
        ob_start();
        ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Module: <?= $module->designation; ?></h4>
        </div>
        <div class="modal-body">
            <div class="text-xs-center">
                <img src="//gestion.<?= env('APP_DOMAIN') ?>/assets/custom/img/modules/<?= $module->image ?>" class="img-responsive img-rounded img-bordered img-bordered-green" width="120">
                <p>En demandant une clef d'accès à ce logiciel, vous acceptez les conditions générales temporaire de ce module disponible sur le site
                    <a href="//srice.eu/cgt">Srice.eu</a></p>
            </div>
        </div>
        <div class="modal-footer">
            <form action="<?= route('config.module.acceptClef', [$info->id, $module->modules_id]); ?>" method="post">
                <?= csrf_field(); ?>
                <button class="btn btn-success">Demander ma clef d'accès</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </form>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }
    public function checkout($id, Espace $espace){
        $info = $espace->espace();
        $module = $espace->module($id);
        ob_start();
        ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Module: <?= $module->designation; ?></h4>
        </div>
        <div class="modal-body">
            <div class="text-xs-center">
                <img src="//gestion.<?= env('APP_DOMAIN') ?>/assets/custom/img/modules/<?= $module->image ?>" class="img-responsive img-rounded img-bordered img-bordered-green" width="120">
                <p>En passant la commande de ce module, vous acceptez les conditions générales de vente de ce module disponible sur le site
                    <a href="//srice.eu/cgt">Srice.eu</a></p>
            </div>
        </div>
        <div class="modal-footer">
            <form action="<?= route('config.module.acceptCheckout', [$info->id, $module->modules_id]); ?>" method="post">
                <?= csrf_field(); ?>
                <button class="btn btn-success">Passer la commande</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </form>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }
    public function acceptClef($espaces_id, $modules_id, Espace $espace){
        $comite = new Comite();
        $contact = $comite->firstContact();
        $this->mailer->to('clef@srice.eu')->send(new DemandeClef($espace, $modules_id));
        $this->mailer->to($contact->email)->send(new AcceptClef($espace, $modules_id));
        Toastr::success("Votre demande de clef à été transmis à nos services !");
        return redirect()->back();
    }
    public function acceptCheckout($espaces_id, $modules_id, Espace $espace){
        $comite = new Comite();
        $com = $comite->comite();
        $commande = new Commande();
        $newCommande = $commande->postCommande($com->id, $modules_id);
        if($newCommande == true){
            Toastr::success("Votre commande à bien été prise en compte !");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors du passage de la commande !");
            return redirect()->back();
        }
    }
    public function active($modules_id, Espace $espace){
        $active = $espace->activeModule($modules_id);
        if($active == true){
            Toastr::success("Le module à été activé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'activation du module");
            return redirect()->back();
        }
    }
    public function desactive($modules_id, Espace $espace){
        $active = $espace->desactiveModule($modules_id);
        if($active == true){
            Toastr::success("Le module à été désactivé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la désactivation du module");
            return redirect()->back();
        }
    }
}
class ModuleOtherController extends ModuleController{

    public static function releaseModuleTag($value){
        switch ($value){
            case 0: return '<span class="tag bg-yellow-500 black" data-toggle="tooltip" data-placement="bottom" data-title="Le module est au stade d\'idée">IDEA</span>';
            case 1: return '<span class="tag bg-orange-500" data-toggle="tooltip" data-placement="bottom" data-title="Le module est au stade de cahier des charges">POC</span>';
            case 2: return '<span class="tag bg-red-500" data-toggle="tooltip" data-placement="bottom" data-title="Le module est au stade de développement primaire">MVPP</span>';
            case 3: return '<span class="tag bg-grey-500" data-toggle="tooltip" data-placement="bottom" data-title="Le module est au stade ALPHA, des tests en interne sont en cours">ALPHA</span>';
            case 4: return '<span class="tag bg-purple-500" data-toggle="tooltip" data-placement="bottom" data-title="Le module est au stade BETA, des tests publique sont en cours (instable)">BETA</span>';
            case 5: return '<span class="tag bg-pink-500" data-toggle="tooltip" data-placement="bottom" data-title="Le module est au stade GAMMA, des tests publique sont en cours (stable)">GAMMA</span>';
            case 6: return '<span class="tag bg-green-500" data-toggle="tooltip" data-placement="bottom" data-title="Le module est au stade END, il est disponible pour tous !">END</span>';
            default: return false;
        }
    }

}
