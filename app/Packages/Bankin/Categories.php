<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 15/08/2017
 * Time: 23:19
 */

namespace App\Packages\Bankin;


class Categories extends Appel
{
    public function listCategories(){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "limit"         => 100
        ];

        $call = $this->get('categories?', $params);

        return $call;
    }

    public function category($categories_id){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            ''
        ];

        $call = $this->get('categories/'.$categories_id.'?', $params);

        return $call;
    }
}