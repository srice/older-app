@extends('template')
@section('title')
    Création d'un utilisateur
    @parent
@stop

@section('header_styles')

@stop

@section("content")
    <div class="panel">
        <header class="panel-header">
            <h3 class="panel-title"><i class="md-account-add"></i> Ajout d'un utilisateur</h3>
        </header>
        <div class="panel-body">
            {{ Form::open(["route" => "config.user.store", "class" => "form-horizontal"]) }}
            <div class="form-group row">
                {{ Form::label('name *', 'Nom', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::text('name', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('email *', 'Email', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::email('email', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('poste', 'Poste', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::text('poste', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('footer_scripts')
    
@stop