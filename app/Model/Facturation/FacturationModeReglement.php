<?php

namespace App\Model\Facturation;

use Illuminate\Database\Eloquent\Model;

class FacturationModeReglement extends Model
{
    protected $guarded = [];

    public function facture(){
        return $this->hasOne(FacturationReglement::class);
    }
}
