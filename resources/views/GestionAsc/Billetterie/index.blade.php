@extends('template')
@section("title")
    Gestion de la billetterie
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste des Ventes Salariés</h3>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('billet.salarie.create') }}" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Nouvelle Vente</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="listeBilletSalarie" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Numéro de Facture</th>
                            <th>Date de la facture</th>
                            <th>Identité</th>
                            <th>Total de la facture</th>
                            <th>Etat de la facture</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($billetSalaries as $billetSalary)
                        <tr>
                            <td>{{ $billetSalary->numBilletSalarie }}</td>
                            <td>{{ $billetSalary->dateBillet->format('d/m/Y') }}</td>
                            <td>{{ $billetSalary->salarie->nom }} {{ $billetSalary->salarie->prenom }}</td>
                            <td>{{ \App\Http\Controllers\OtherController::euro($billetSalary->totalBillet) }}</td>
                            <td>{!! \App\Http\Controllers\GestionAsc\Billetterie\BilletterieController::etatBilletLabel($billetSalary->etatBillet) !!}</td>
                            <td>
                                {{ Form::model($billetSalary, ["route" => ["billet.salarie.delete", $billetSalary->id], "method" => "DELETE"]) }}
                                <a href="{{ route('billet.salarie.show', $billetSalary) }}" class="btn btn-sm btn-icon btn-default"><i class="fa fa-eye"></i> </a>
                                <a href="" class="btn btn-sm btn-icon btn-primary"><i class="fa fa-edit"></i> </a>
                                <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste de ventes Ayant Droits</h3>
                </div>
                <div class="col-md-2">
                    <a href="" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Nouvelle Vente</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="listeBilletAd" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Numéro de la facture</th>
                            <th>Date de la facture</th>
                            <th>Identité</th>
                            <th>Total de la facture</th>
                            <th>Etat de la facture</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($billetAds as $billetAd)
                        <tr>
                            <td>{{ $billetAd->numBilletAd }}</td>
                            <td>{{ $billetAd->dateBillet->format('d/m/Y') }}</td>
                            <td>{{ $billetAd->ad->nom }} {{ $billetAd->ad->prenom }}</td>
                            <td>{{ \App\Http\Controllers\OtherController::euro($billetAd->totalBillet) }}</td>
                            <td>{{ $billetAd->etatBillet }}</td>
                            <td></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    