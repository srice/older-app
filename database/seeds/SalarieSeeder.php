<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;

class SalarieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     * @internal param Faker $faker
     */
    public function run()
    {
        if(env('APP_ENV') == 'local'){
            $faker = \Faker\Factory::create('fr_FR');
            $config = \App\Model\Beneficiaire\Configuration\ConfigBeneficiaire::find(1);
            if($config->soldeActive == 1){
                $fidelity = $config->soldeInit;
            }else{
                $fidelity = 0;
            }
            for($i=0; $i <= 100; $i++){
                DB::table('salaries')->insert([
                    "matricule"     => $faker->randomNumber(),
                    "nom"           => $faker->firstName,
                    "prenom"        => $faker->lastName,
                    "adresse"       => $faker->streetAddress,
                    "codePostal"    =>str_replace(' ', '', $faker->postcode),
                    "ville"         => $faker->city,
                    "tel"           => $faker->phoneNumber,
                    "email"         => $faker->email,
                    "fidelityPoint" => $fidelity
                ]);
            }
        }

        if(env('APP_ENV') == 'test'){
            $faker = \Faker\Factory::create('fr_FR');
            $config = \App\Model\Beneficiaire\Configuration\ConfigBeneficiaire::find(1);
            if($config->soldeActive == 1){
                $fidelity = $config->soldeInit;
            }else{
                $fidelity = 0;
            }
            for($i=0; $i <= 100; $i++){
                DB::table('salaries')->insert([
                    "matricule"     => $faker->randomNumber(),
                    "nom"           => $faker->firstName,
                    "prenom"        => $faker->lastName,
                    "adresse"       => $faker->streetAddress,
                    "codePostal"    =>str_replace(' ', '', $faker->postcode),
                    "ville"         => $faker->city,
                    "tel"           => $faker->phoneNumber,
                    "email"         => $faker->email,
                    "fidelityPoint" => $fidelity
                ]);
            }
        }
    }
}
