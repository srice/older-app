<?php


namespace App\Packages\Bankin;


class Bridge extends Appel
{
    public function connectItem($access_token)
    {
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "country"       => 'fr'
        ];

        $call = $this->get('connect/items/add/url?', $params, true, $access_token);

        return $call->redirect_url;
    }

    public function editConnectItem($account_id)
    {
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "item_id"       => $account_id
        ];

        $call = $this->get('connect/items/edit/url?', $params, true, session('access_token'));

        return $call->redirect_url;

    }
}