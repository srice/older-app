<?php

namespace App\Model\Facturation;

use Illuminate\Database\Eloquent\Model;

class FacturationLigne extends Model
{
    protected $guarded = [];

    public function facture(){
        return $this->belongsTo(FacturationLigne::class);
    }
}
