<?php

namespace App\Http\Controllers\Banker\Users;

use App\Packages\Bankin\Users;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class BankerUserController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'banker';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index(Users $users){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $tabs = $users->listUsers();



        return view('Banker.users.index', compact('config', 'tabs'));
    }

    public function store(Request $request, Users $users){
        $this->validate($request, [
            "email"     => "required|email",
            "password"  => "required|min:6|alpha_dash"
        ]);

        $add = $users->createUser($request->email, $request->password);

        if($add){
            Toastr::success("Un accès à été créée");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la création de l'accès !");
            return redirect()->back();
        }

    }

    public function deleteStep($uuid){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        return view('banker.users.deleteStep', compact('config', 'uuid'));
    }

    public function delete(Request $request, $uuid, Users $users){
        $del = $users->deleteUser($uuid, $request->password);

        if($del == null){
            Toastr::success("L\'accès à été supprimé");
            return redirect()->route('banker.users.index');
        }else{
            Toastr::error("Erreur lors de la suppression de l\'accès !");
            return redirect()->back();
        }
    }

    public function loginUser(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        return view('Banker.users.login', compact('config'));
    }

    public function connexion(Request $request, Users $users){
        $this->validate($request, [
            "email"     => "required",
            "password"  => "required"
        ]);

        $login = $users->authUser($request->email, $request->password);
        $request->session()->put('access_token', $login->access_token);
        if($request->session()->exists('access_token')){
            Toastr::success("Vous êtes à présent connecté");
            return redirect()->route('banker.users.index');
        }else{
            Toastr::error("Erreur lors de la connexion !");
            return redirect()->back();
        }
    }
}
