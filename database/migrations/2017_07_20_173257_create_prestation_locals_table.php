<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestationLocalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestation_locals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prestations_id');
            $table->string('type')->nullable();
            $table->string('adresse')->nullable();
            $table->string('codePostal', 5)->nullable();
            $table->string('ville');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestation_locals');
    }
}
