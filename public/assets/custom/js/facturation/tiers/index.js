(function ($) {
    $("#listeTier").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/French.json"
        },
        buttons: [
            'copy', 'excel', 'pdf'
        ]
    });
})(jQuery)