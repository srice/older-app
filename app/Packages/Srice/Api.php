<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 27/06/2017
 * Time: 00:20
 */

namespace App\Packages\Srice;


use Illuminate\Support\Facades\DB;

class Api
{
    protected $auth = [
        "api_key"       => null,
        'api_endpoint'  => null,
        'api_comite_id' => null
    ];

    protected $db;

    public function __construct(array $auth = [])
    {
        $auth = collect($auth);

        if($auth->isEmpty() === false){
            $this->auth = collect($auth)->only([
                'api_key',
                'api_endpoint',
                'api_comite_id'
            ]);
        }else{
            $this->loadAuthFromConfig();
        }

        $this->db = DB::connection('gestion');
    }

    protected function loadAuthFromConfig(){
        $this->auth['api_key']          = config('api.api_key');
        $this->auth['api_endpoint']     = config('api.api_endpoint');
        $this->auth['api_comite_id']    = config('api.api_comite_id');

        return $this;
    }

    public function getInfo(){
        return $this->auth;
    }

    public function getId(){
        return $this->auth['api_comite_id'];
    }
}