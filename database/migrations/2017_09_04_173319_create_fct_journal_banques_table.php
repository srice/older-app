<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFctJournalBanquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fct_journal_banques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numBanque');
            $table->timestamp('dateBanque')->nullable();
            $table->string('numCompte');
            $table->string('libelleBanque');
            $table->string('debitBanque')->default(0);
            $table->string('creditBanque')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fct_journal_banques');
    }
}
