
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2013_11_26_161501_create_currency_table',1),(2,'2014_10_12_000000_create_users_table',1),(3,'2014_10_12_100000_create_password_resets_table',1),(4,'2017_06_28_094019_add_avatar_to_users',1),(5,'2017_06_28_113332_create_sauvegardes_table',1),(6,'2017_07_10_221817_create_salaries_table',1),(7,'2017_07_11_121647_add_avatar_to_salarie',1),(8,'2017_07_11_142716_create_ayant_droits_table',1),(9,'2017_07_20_110839_create_config_asc_prestas_table',1),(10,'2017_07_20_110942_create_config_asc_billets_table',1),(11,'2017_07_20_110955_create_config_asc_rembs_table',1),(12,'2017_07_20_172133_create_familles_table',1),(13,'2017_07_20_172216_create_prestations_table',1),(14,'2017_07_20_173133_create_prestation_dates_table',1),(15,'2017_07_20_173257_create_prestation_locals_table',1),(16,'2017_07_20_173737_create_prestation_tarifs_table',1),(17,'2017_07_21_155414_add_field_from_tarif',1),(18,'2017_07_25_204737_add_representative_to_prestations',1),(19,'2017_07_27_140609_create_achats_table',1),(20,'2017_07_27_140656_create_fournisseurs_table',1),(21,'2017_07_27_140721_create_achat_prestations_table',1),(22,'2017_07_27_140734_create_achat_reglements_table',1),(23,'2017_07_27_151757_add_date_to_reglement',1),(24,'2017_08_02_111642_create_bouncer_tables',1),(25,'2017_08_03_120536_add_tarif_into_achat',1),(26,'2017_08_07_175126_create_billet_salaries_table',1),(27,'2017_08_07_175141_create_ligne_billet_salaries_table',1),(28,'2017_08_07_175156_create_reglement_billet_salaries_table',1),(29,'2017_08_07_175208_create_billet_ads_table',1),(30,'2017_08_07_175222_create_ligne_billet_ads_table',1),(31,'2017_08_07_175236_create_reglement_billet_ads_table',1),(32,'2017_08_08_181300_add_salaries_to_lignePresta',1),(33,'2017_08_10_175300_add_depositaire_to_billetReglement',1),(34,'2017_08_14_115700_create_remb_salaries_table',1),(35,'2017_08_14_115831_create_remb_presta_salaries_table',1),(36,'2017_08_14_115931_create_remb_reg_salaries_table',1),(37,'2017_08_14_121700_add_date_to_regRembSalarie',1),(38,'2017_08_15_113328_create_compta_plan_sectors_table',1),(39,'2017_08_15_113441_create_compta_plan_classes_table',1),(40,'2017_08_15_113455_create_compta_plan_comptes_table',1),(41,'2017_08_15_155853_create_asc_journal_achats_table',1),(42,'2017_08_15_160029_create_asc_journal_ventes_table',1),(43,'2017_08_15_160044_create_asc_journal_banques_table',1),(44,'2017_08_15_160449_create_asc_journal_divers_table',1),(45,'2017_08_22_112858_create_remise_banques_table',1),(46,'2017_08_22_112937_create_remise_banque_cheques_table',1),(47,'2017_08_22_112958_create_remise_banque_especes_table',1),(48,'2017_08_22_180926_addPointToReglement',1),(49,'2017_08_25_124632_addInfoCompteToFournisseur',1),(50,'2017_08_25_125904_addInfoCompteToSalarie',1),(51,'2017_08_25_125929_addInfoCompteToAd',1),(52,'2017_08_25_153142_create_asc_comptes_table',1),(53,'2017_08_25_170945_create_asc_journal_caisses_table',1),(54,'2017_09_04_173014_create_fct_comptes_table',1),(55,'2017_09_04_173146_create_fct_journal_achats_table',1),(56,'2017_09_04_173319_create_fct_journal_banques_table',1),(57,'2017_09_04_173335_create_fct_journal_caisses_table',1),(58,'2017_09_04_173351_create_fct_journal_divers_table',1),(59,'2017_09_04_173409_create_fct_journal_ventes_table',1),(60,'2017_09_04_173602_create_fct_plan_classes_table',1),(61,'2017_09_04_173620_create_fct_plan_comptes_table',1),(62,'2017_09_04_173636_create_fct_plan_sectors_table',1),(63,'2017_09_04_185143_addStartFidelityToSalaries',1),(64,'2017_09_04_185315_create_salarie_fidelities_table',1),(65,'2017_09_04_190306_create_fidelity_compteurs_table',1),(66,'2017_09_04_190332_create_fidelity_baremes_table',1),(67,'2017_09_05_134537_addCompteurToPrestation',1),(68,'2017_09_29_180332_create_asc_bilan_initials_table',1),(69,'2017_09_29_184215_create_fct_bilan_initials_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

