<?php

namespace App\Http\Controllers\ComptaAsc\Journal\Achat;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanSector;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalAchat;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class AchatController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptaasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public static function genNumAchat(){
        return "JAG".date('Ym').rand(0,999);
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $achats = AscJournalAchat::all()->load('compte');
        $sectors = ComptaPlanSector::where('numClasse', 6)->get();

        return view('ComptaAsc.Journal.Achat.index', compact('config', 'achats', 'sectors'));
    }

    public function store(Request $request){

        $this->validate($request, [
            "dateAchat"     => "required",
            "numCompte"     => "required",
            "libelleAchat"  => "required|min:5",
            "montantAchat"  => "required"
        ]);

        //Mise en forme de la date
        $date = $request->dateAchat;
        $strt = strtotime($date);
        $dateAchat = Carbon::createFromTimestamp($strt);

        //Numéro de facture
        if(empty($request->numAchat)){
            $numAchat = self::genNumAchat();
        }else{
            $numAchat = $request->numAchat;
        }

        //Choix du débit ou crédit
        if($request->montantAchat >= 0){
            $debitAchat = $request->montantAchat;
            $creditAchat = 0;
        }else{
            $debitAchat = 0;
            $creditAchat = str_replace('-', '', $request->montantAchat);
        }

        $achat = AscJournalAchat::create([
            "numAchat"  => $numAchat,
            "dateAchat" => $dateAchat,
            "numCompte" => $request->numCompte,
            "libelleAchat"   => $request->libelleAchat,
            "debitAchat"    => $debitAchat,
            "creditAchat"   => $creditAchat
        ]);

        //Mise à jour des Comptes
        $charge = AscCompte::where('numCompte', $request->numCompte)->first();
        $fournisseur = AscCompte::where('numCompte', '401')->first();

        if($request->montantAchat >= 0){
            $debitAchat = $request->montantAchat;
            $newDebit = $charge->debit + $debitAchat;
            $charge->update(["debit" => $newDebit]);
        }else{
            $creditAchat = str_replace('-', '', $request->montantAchat);
            $newCredit = $charge->credit + $creditAchat;
            $charge->update(["credit" => $newCredit]);
        }

        if($request->montantAchat >= 0){
            $creditAchat = $request->montantAchat;
            $newCredit = $fournisseur->credit + $creditAchat;
            $fournisseur->update(["credit" => $newCredit]);
        }else{
            $debitAchat = str_replace('-', '', $request->montantAchat);
            $newDebit = $fournisseur->debit + $debitAchat;
            $fournisseur->update(["debit" => $newDebit]);
        }

        if($fournisseur && $charge && $achat){
            Toastr::success("L\'achat à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout de l\'achat !");
            return redirect()->back();
        }
    }

    public function delete($numAchat){
        $achat = AscJournalAchat::where('numAchat', $numAchat)->first();
        $charge = AscCompte::where('numCompte', $achat->numCompte)->first();
        $fournisseur = AscCompte::where('numCompte', '401')->first();

        if($achat->debitAchat > 0){
            $debitCharge = $charge->debit;
            $newDebit = $debitCharge - $achat->debitAchat;
            $charge->update(["debit" => $newDebit]);

            $creditFour = $fournisseur->credit;
            $newCredit = $creditFour - $achat->debitAchat;
            $fournisseur->update(["credit" => $newCredit]);
        }else{
            $creditCharge = $charge->credit;
            $newCredit = $creditCharge - $achat->creditAchat;
            $charge->update(["credit" => $newCredit]);

            $debitFour = $fournisseur->debit;
            $newDebit = $debitFour - $achat->creditAchat;
            $fournisseur->update(["debit" => $newDebit]);
        }


        $achat->delete();

        if($achat && $charge && $fournisseur){
            Toastr::success("L\'achat à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de l\'achat !");
            return redirect()->back();
        }
    }
}
