<?php

namespace App\Model\GestionAsc\Prestation;

use Illuminate\Database\Eloquent\Model;

class PrestationLocal extends Model
{
    protected $guarded = [];

    public function prestation(){
        return $this->belongsTo(Prestation::class, 'prestations_id');
    }
}
