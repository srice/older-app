<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLigneBilletAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ligne_billet_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('billet_ads_id');
            $table->integer('prestations_id');
            $table->integer('tarifs_id');
            $table->string('qte');
            $table->string('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ligne_billet_ads');
    }
}
