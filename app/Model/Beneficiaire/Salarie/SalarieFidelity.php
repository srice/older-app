<?php

namespace App\Model\Beneficiaire\Salarie;

use Illuminate\Database\Eloquent\Model;

class SalarieFidelity extends Model
{
    protected $guarded = [];

    public function fidelity(){
        return $this->belongsTo(Salarie::class, 'salaries_id');
    }
}
