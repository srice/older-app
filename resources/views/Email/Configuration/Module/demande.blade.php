<h2>Nouvelle Demande de clef d'accès aux modules</h2>
<p>Une nouvelle demande vient de vous parvenir pour <strong>Obtention d'une clef d'accès pour le module {{ $module->designation }}</strong></p>
<p>Les informations de la demande sont indiquer ci-dessous:</p>
<ul>
    <li><strong>Comité:</strong> {{ $comite->nameComite }}</li>
    <li><strong>Module:</strong> {{ $module->designation }}</li>
    <li><strong>Etat du Module:</strong> {{ $module->releaseModule }}</li>
</ul>