<?php

namespace App\Http\Controllers\GestionAsc\Billetterie;

use App\Model\GestionAsc\Billetterie\BilletAd;
use App\Model\GestionAsc\Billetterie\BilletSalarie;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BilletterieController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'gestionasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $billetSalaries = BilletSalarie::all()->load('salarie', 'lignes', 'reglements');
        $billetAds = BilletAd::all()->load('ad', 'lignes', 'reglements');

        return view('GestionAsc.Billetterie.index', compact('config', 'billetSalaries', 'billetAds'));
    }

    public static function etatBilletLabel($value){
        switch ($value){
            case 0: return '<span class="tag tag-default"><i class="fa fa-pencil"></i> Brouillon</span>';
            case 1: return '<span class="tag tag-info"><i class="fa fa-check-circle"></i> Valider</span>';
            case 2: return '<span class="tag tag-warning"><i class="fa fa-clock-o"></i> Partiellement Payer</span>';
            case 3: return '<span class="tag tag-success"><i class="fa fa-check"></i> Payer</span>';
            case 4: return '<span class="tag tag-danger"><i class="fa fa-times"></i> Impayer</span>';
            default: return false;
        }
    }

    public static function etatBilletBg($value){
        switch ($value){
            case 0: return 'bg-grey-300';
            case 1: return 'bg-blue-300';
            case 2: return 'bg-orange-500';
            case 3: return 'bg-green-500';
            case 4: return 'bg-red-500';
            default: return false;
        }
    }

    public static function etatBilletText($value){
        switch ($value){
            case 0: return '<i class="fa fa-pencil"></i> Brouillon';
            case 1: return '<i class="fa fa-check-circle"></i> Valider';
            case 2: return '<i class="fa fa-clock-o"></i> Partiellement payer';
            case 3: return '<i class="fa fa-check"></i> Payer';
            case 4: return '<i class="fa fa-times"></i> Impayer';
            default: return false;
        }
    }
}
