(function ($) {
    tableUser();
    lastSave();
    stateBdd();
    stateCluster();
    stateInerance();
    $("#purgeDatabase").on('click', function (e) {
        e.preventDefault();
        $("#purge").modal('show');
    });
    $("#ineranceDatabase").on('click', function (e) {
        e.preventDefault();
        var content = $("#modalIneranceInfo");
        $("#inerance").modal('show');
        $.get('/config/bdd/infoInerance')
            .done(function (data) {
                content.html(data)
            })
    })
})(jQuery);
function tableUser(){
    var filtering = $('#listeSave');
    filtering.footable().on('footable_filtering', function(e) {
        var selected = $('#filteringStatus').find(':selected').val();
        e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
        e.clear = !e.filter;
    });

    // Filter status
    $('#filteringStatus').change(function(e) {
        e.preventDefault();
        filtering.trigger('footable_filter', {
            filter: $(this).val()
        });
    });

    // Search input
    $('#filteringSearch').on('input', function(e) {
        e.preventDefault();
        filtering.trigger('footable_filter', {
            filter: $(this).val()
        });
    });
}
function lastSave(){
    var div = $("#lastSave");
    div.html('<i class="fa fa-spinner fa-spin"></i>');
    $.get('/config/bdd/lastSave')
        .done(function(data){
            div.html(data);
        })
}
function stateBdd(){
    var div = $("#stateBdd");
    div.html('<i class="fa fa-spinner icon-spin"></i>');
    $.get('/config/bdd/stateBdd')
        .done(function (data) {
            div.html(data);
        })
}
function stateCluster() {
    var div = $("#stateCluster");
    div.html('<i class="fa fa-spinner icon-spin"></i>');
    $.get('/config/bdd/stateCluster')
        .done(function (data) {
            div.html(data);
        })
}

function stateInerance(){
    var div = $("#stateInerance");
    div.html('<i class="fa fa-spinner icon-spin"></i>');
    $.get('/config/bdd/stateInerance')
        .done(function (data) {
            div.html(data)
        })
}

