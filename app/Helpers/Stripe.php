<?php
if(!function_exists('currentPeriodeStart')){
    function currentPeriodeStart($subscription_id){
        $subscriptions = new \App\Packages\Stripe\Subscription();

        $sub = $subscriptions->retrieve($subscription_id);

        return $sub["current_period_start"];

    }
}

if(!function_exists('currentPeriodeEnd')){
    function currentPeriodeEnd($subscription_id){
        $subscriptions = new \App\Packages\Stripe\Subscription();

        $sub = $subscriptions->retrieve($subscription_id);

        return $sub["current_period_end"];

    }
}

if(!function_exists('abonnementStart')){
    function abonnementStart($subscription_id){
        $subscriptions = new \App\Packages\Stripe\Subscription();

        $sub = $subscriptions->retrieve($subscription_id);

        return $sub["start"];

    }
}

if(!function_exists('generateServiceImg')){
    function generateServiceImg($subscription_id){
        $subscriptions = new \App\Packages\Stripe\Subscription();

        $sub = $subscriptions->retrieve($subscription_id);

        switch ($sub["plan"]["product"])
        {
            case "prod_EZltfDmEzDofRc": return "/storage/service/tickets.png";
            case "prod_EZly5VP5xoB49k": return "/storage/service/annuaire.png";
            case "prod_EZm1FqeiFXotMS": return "/storage/service/travel.png";
            case "prod_EVevWzeS340sEg": return "/storage/service/tent.png";
            case "prod_EZm58c0F5SFBAo": return "/storage/service/multi.png";
            default: return null;
        }
    }
}

if(!function_exists('generateAccessImg')){
    function generateAccessImg($prog_id){


        switch ($prog_id)
        {
            case "1": return "/storage/service/tickets.png";
            case "2": return "/storage/service/annuaire.png";
            case "3": return "/storage/service/travel.png";
            case "4": return "/storage/service/tent.png";
            default: return null;
        }
    }
}

if(!function_exists('typeTarif')){
    function typeTarif($type)
    {
        if($type == 0)
        {
            return '<div class="sale-flash">Tarif CSE</div>';
        }else{
            return '<div class="sale-flash">Tarif TPE</div>';
        }
    }
}

if(!function_exists('statusSubscription')){
    function statusSubscription($subscription_id)
    {

        $subscriptions = new \App\Packages\Stripe\Subscription();

        $sub = $subscriptions->retrieve($subscription_id);

        switch ($sub["status"])
        {
            case 'trialing': return '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded"><i class="fa fa-hourglass-half"></i> Période d\'essai</span>';
            case 'active': return '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded"><i class="fa fa-check"></i> Actif</span>';
            case 'past_due': return '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded"><i class="fa fa-warning"></i> En retard</span>';
            case 'canceled': return '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded"><i class="fa fa-recycle"></i> Annuler</span>';
            case 'unpaid': return '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded"><i class="fa fa-credit-card"></i> Non Payer</span>';
            default: return null;
        }
    }
}

if(!function_exists('countAboFam')){
    function countAboFam(){
        $subscription = new \App\Packages\Stripe\Subscription();

        $count1 = count($subscription->listForPlan('bill_fam_month'));
        $count2 = count($subscription->listForPlan('bill_fam_ann'));
        $count2 += count($subscription->listForPlan('bill_free_access'));
        $count2 += count($subscription->listForPlan('multi_fam_month'));
        $count2 += count($subscription->listForPlan('multi_fam_ann'));
        $count2 += count($subscription->listForPlan('multi_free'));

        return $count1 + $count2;

    }
}

if(!function_exists('getNamedPlan')){
    function getNamedPlan($subscription_id){
        $subscriptions = new \App\Packages\Stripe\Subscription();

        $sub = $subscriptions->retrieve($subscription_id);

        switch ($sub["plan"]["id"])
        {
            case "bill_fam_month": return "Abonnement Familliale Mensuel";
            case "bill_perso_ann": return "Abonnement Personnel Annuel";
            case "bill_fam_ann": return "Abonnement Familliale Annuel";
            case "bill_perso_month": return "Abonnement Personnel Mensuel";
            case "bill_free": return "Abonnement Gratuit Universel";
            case "ann_opt_video": return "Abonnement Annuaire Vidéo";
            case "ann_opt_visu3d": return "Abonnement Annuaire Visite 3D";
            case "ann_standard": return "Abonnement Parution annuaire";
            case "bill_pro_month": return "Abonnement Professionnel Mensuel (Usage Charge)";
            case "voy_month": return "Abonnement Voyage Mensuel";
            case "voy_annual": return "Abonnement Voyage Annuel";
            case "voy_free": return "Abonnement Voyage Gratuit";
            case "camp_month": return "Abonnement Camping Mensuel";
            case "camp_ann": return "Abonnement Camping Annuel";
            case "multi_fam_ann": return "Abonnement Multi Famille Annuel";
            case "multi_perso_annual": return "Abonnement Multi Personnel Annuel";
            case "multi_fam_month": return "Abonnement Multi Famille Mensuel";
            case "mult_perso_month": return "Abonnement Multi Personnel Mensuel";
            case "multi_free": return "Abonnement Multi Gratuit";
            default: return null;
        }
    }
}

if(!function_exists('getProgrammeText')){
    function getProgrammeText($prog_id){
        switch ($prog_id)
        {
            case "1": return "Billetterie";
            case "2": return "Annuaire";
            case "3": return "Voyage";
            case "4": return "Camping";

            default: return null;
        }
    }
}