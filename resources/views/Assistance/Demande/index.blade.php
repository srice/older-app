@extends('template')
@section('title')
    Liste de mes demandes
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <div class="row">
                    <div class="col-md-10">
                        <h3 class="panel-title"><i class="fa fa-list"></i> @yield('title')</h3>
                    </div>
                    <div class="col-md-2 m-t-20">
                        <a href="{{ route('demandes.create') }}" class="btn btn-sm btn-block btn-primary"><i class="fa fa-plus-circle"></i> Nouvelle Demande</a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Sujet</th>
                                <th>Etat</th>
                                <th><i class="fa fa-cog"></i> </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($demandes as $demande)
                            <tr>
                                <td>{{ $demande->id }}</td>
                                <td>{{ $demande->titleDemande }}</td>
                                <td>{!! \App\Http\Controllers\Assistance\Demande\DemandeOtherController::etatDemande($demande->etatDemande) !!}</td>
                                <td>
                                    <a href="{{ route('demandes.show', $demande->id) }}" class="btn btn-sm btn-icon btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Voir ma demande"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    
@stop