<?php

namespace App\Http\Controllers\ComptaAsc\Etat;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use App\Model\ComptaAsc\Etat\AscCompte;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompteController extends Controller
{
    public static function soldeDebit($numCompte){
        $compte = AscCompte::where('numCompte', $numCompte)->sum('debit');
        return $compte;
    }

    public static function soldeCredit($numCompte){
        $compte = AscCompte::where('numCompte', $numCompte)->sum('credit');
        return $compte;
    }

    public static function getNameCompte($numCompte){
        $compte = ComptaPlanCompte::where('numCompte', $numCompte)->first();
        return $compte->nameCompte;
    }

    public static function getTotalDebit(){
        $compte = AscCompte::all()->sum('debit');
        return $compte;
    }

    public static function getTotalCredit(){
        $compte = AscCompte::all()->sum('credit');
        return $compte;
    }
}
