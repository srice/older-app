<?php

namespace App\Http\Controllers\Configuration\Database;

use App\Http\Controllers\Automate\AutomateController;
use App\Model\Automate\Automate;
use App\Model\Beneficiaire\Salarie\Salarie;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanClasse;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanSector;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaFct\Configuration\Plan\FctPlanCompte;
use App\Model\Configuration\Database\Sauvegarde;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Model\GestionAsc\Prestation\Achat\Achat;
use App\Model\GestionAsc\Prestation\Famille;
use App\Packages\Srice\Espace;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Kamaln7\Toastr\Facades\Toastr;

class DatabaseController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;
    public function __construct(Espace $espace, Mailer $mailer, Guard $auth)
    {
        $this->auth = $auth;
        $this->sector = 'configuration';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];

        $this->mailer = $mailer;
    }

    public function index(){
        Carbon::setToStringFormat('d/m/Y à H:i');
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $saves = Sauvegarde::all();

        return view('Configuration.Bdd.index', compact('config', 'saves'));
    }

    public function lastSave(){
        $save = Sauvegarde::all()->last();
        Carbon::setToStringFormat('d/m/Y à H:i');
        ob_start();
        ?>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title text-xs-center">Date de la dernière sauvegarde</h3>
            </div>
            <div class="card-block text-xs-center bg-green-500">
                <h2 class="white">
                    <?php
                    if($save == null){
                        echo "Aucunes sauvegardes";
                    }else{
                        echo $save->created_at;
                    }
                    ?>
                </h2>
            </div>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function stateBdd(){

        $state = DatabaseOtherController::testBdd();
        ob_start();
        ?>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title text-xs-center">Etat de la base de donnée</h3>
            </div>
            <div class="card-block text-xs-center <?= DatabaseOtherController::stateBddBg($state); ?>">
                <h2 class="white"><?= DatabaseOtherController::stateBddText($state) ?></h2>
            </div>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function stateCluster(){
        if(env('APP_ENV') == 'local'){
            $state = Storage::disk('local')->exists('saving/save.service');
        }elseif(env("APP_ENV") == 'test'){
            $state = Storage::disk('ftp')->exists('save.service');
        }else{
            $state = Storage::disk('ftp')->exists('save.service');
        }
        ob_start();
        ?>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title text-xs-center">Etat du cluster de sauvegarde</h3>
            </div>
            <div class="card-block text-xs-center <?= DatabaseOtherController::stateClusterBg($state) ?>">
                <h2 class="white"><?= DatabaseOtherController::stateClusterText($state) ?></h2>
            </div>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function stateInerance(){
        $state = DatabaseOtherController::testInerance();
        ob_start();
        ?>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title text-xs-center">Inérance de la base de donnée</h3>
            </div>
            <div class="card-block text-xs-center <?= DatabaseOtherController::stateIneranceBg($state) ?>">
                <h2 class="white"><?= DatabaseOtherController::stateIneranceText($state) ?></h2>
            </div>
        </div>

        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function infoInerance(){
        $statement = DatabaseOtherController::testInfoInerance();
        ob_start();
        ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Information sur l'inérance de la base de donnée</h4>
        </div>
        <div class="modal-body">
            <?= $statement['text1']; ?>
            <?= $statement['text2']; ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function save(){
        $dateSaveBackup = date("Y-m-d-H-i-s").".zip";
        $dump = exec('cd '.base_path()." && php artisan backup:run --only-db");
        if($dump){
            $file = storage_path()."/app/".env("APP_NAME")."/".date("Y-m-d-H-i-s").".zip";
            if($file){
                $storage = DatabaseOtherController::SavingDatabase($file);

                if($storage == true){
                    if(env("APP_ENV") == 'local'){
                        $sizeSave = Storage::disk('local')->size($file);
                    }elseif(env("APP_ENV") == 'test'){
                        $sizeSave = Storage::disk('ftp')->size($file);
                    }else{
                        $sizeSave = Storage::disk('ftp')->size($file);
                    }

                    $save = Sauvegarde::create([
                        "nameSave"  => $dateSaveBackup.'.sql',
                        "typeSave"  => "1",
                        "sizeSave"  => DatabaseOtherController::formatBytes($sizeSave, 2)
                    ]);

                    if($save && $file){
                        Toastr::success("La base de donnée à été sauvegarder !");
                        return redirect()->back();
                    }else{
                        Toastr::error("Erreur lors de la sauvegarde de la base de donnée");
                        return redirect()->back();
                    }
                }else{
                    Toastr::error("Erreur lors du transfère de la sauvegarde sur le serveur de sauvegarde !");
                    return redirect()->back();
                }
            }else{
                Toastr::error("Erreur lors de l'écriture de la sauvegarde dans le fichier local !");
                return redirect()->back();
            }
        }else{
            Toastr::error("Erreur lors de la création du fichier de sauvegarde !");
            return redirect()->back();
        }

    }

    public function purgeDatabaseGestion(){
        DB::table('achats')->truncate();
        DB::table('achat_prestations')->truncate();
        DB::table('achat_reglements')->truncate();
        DB::table('billet_ads')->truncate();
        DB::table('billet_salaries')->truncate();
        DB::table('ligne_billet_ads')->truncate();
        DB::table('ligne_billet_salaries')->truncate();
        DB::table('reglement_billet_ads')->truncate();
        DB::table('reglement_billet_salaries')->truncate();
        DB::table('remb_presta_salaries')->truncate();
        DB::table('remb_reg_salaries')->truncate();
        DB::table('remb_salaries')->truncate();

        Toastr::success("La base de donnée de la gestion des Oeuvres Sociales à été purger");
        return redirect()->back();
    }

    public function purgeDatabaseComptaAsc(){
        DB::table('asc_journal_achats')->truncate();
        DB::table('asc_journal_banques')->truncate();
        DB::table('asc_journal_caisses')->truncate();
        DB::table('asc_journal_divers')->truncate();
        DB::table('asc_journal_ventes')->truncate();
        DB::table('remise_banques')->truncate();
        DB::table('remise_banque_cheques')->truncate();
        DB::table('remise_banque_especes')->truncate();

        $comptes = ComptaPlanCompte::all();
        foreach ($comptes as $compte){
            DB::table('asc_comptes')->where('numCompte', $compte->numCompte)->update(["debit" => 0, "credit" => 0]);
        }

        Toastr::success("La base de donnée de la comptabilité des Oeuvres sociales à été purger !");
        return redirect()->back();

    }

    public function purgeDatabaseComptaFct(){
        DB::table('fct_journal_achats')->truncate();
        DB::table('fct_journal_banques')->truncate();
        DB::table('fct_journal_caisses')->truncate();
        DB::table('fct_journal_divers')->truncate();
        DB::table('fct_journal_ventes')->truncate();

        $comptes = FctPlanCompte::all();
        foreach ($comptes as $compte){
            DB::table('fct_comptes')->where('numCompte', $compte->numCompte)->update(["debit" => 0, "credit" => 0]);
        }

        Toastr::success("La base de donnée de la comptabilité du budget de fonctionnement à été purger !");
        return redirect()->back();

    }

    public function purgeDatabaseFidelity(){
        DB::table('fidelity_baremes')->truncate();
        DB::table('fidelity_compteurs')->truncate();
        DB::table('salarie_fidelities')->truncate();

        $salaries = Salarie::all();
        foreach ($salaries as $salary){
            $salary->update(["fidelityPoint" => 0]);
        }

        Toastr::success("La base de donnée de la fidélité à été purger !");
        return redirect()->back();

    }
}
class DatabaseOtherController extends DatabaseController{

    public static function stateBddBg($value){
        switch ($value){
            case 0: return 'bg-red-500';
            case 1: return 'bg-orange-500';
            case 2: return 'bg-green-500';
            default: return 'bg-grey-500';
        }
    }

    public static function stateBddText($value){
        switch ($value){
            case 0: return '<i class="fa fa-times-circle"></i> Erreur !';
            case 1: return '<i class="fa fa-exclamation-circle"></i> Attention !';
            case 2: return '<i class="fa fa-check-circle"></i> OK';
            default: return '<i class="fa fa-question-circle"></i> Données Indisponibles';
        }
    }

    public static function stateClusterBg($value){
        if($value == true){
            return 'bg-green-500';
        }else{
            return 'bg-red-500';
        }
    }

    public static function stateClusterText($value){
        if($value == true){
            return '<i class="fa fa-check-circle"></i> En Ligne';
        }else{
            return '<i class="fa fa-times-circle"></i> Hors Ligne';
        }
    }

    public static function stateIneranceBg($value){
        switch ($value){
            case 0: return 'bg-red-500';
            case 1: return 'bg-orange-500';
            case 2: return 'bg-green-500';
            default: return 'bg-grey-500';
        }
    }

    public static function stateIneranceText($value){
        switch ($value){
            case 0: return '<i class="fa fa-times-circle"></i> Critique';
            case 1: return '<i class="fa fa-warning"></i> Mineur';
            case 2: return '<i class="fa fa-check-circle"></i> OK';
            default: return '<i class="fa fa-question-circle"></i> Données Indisponibles';
        }
    }

    public static function formatBytes($bytes, $prec = 2){
        $units = ["O", "Ko", "Mo", "Gb", "Tb"];

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        return round($bytes, $prec).' '.$units[$pow];
    }

    public static function typeSaveLabel($value){
        switch ($value){
            case 0: return 'AUTOMATIQUE';
            case 1: return 'MANUEL';
            default: return "INCONNUE";
        }
    }

    public static function testBdd(){
        //Test de connection
        $i = 0;
        if(DB::connection('mysql')){
            $i = $i + 1;
        }else{
            $i = $i - 1;
        }

        //Test de communication avec la base de gestion
        if(DB::connection('gestion')){
            $i = $i + 1;
        }else{
            $i = $i - 1;
        }

        // Test des données pré-rempli
        $famille = Famille::all()->count();
        $user = User::all()->count();
        $configAscBillet = ConfigAscBillet::all()->count();
        $configAscPresta = ConfigAscPresta::all()->count();
        $configAscRemb = ConfigAscRemb::all()->count();
        $comptaCompte = ComptaPlanCompte::all()->count();
        $comptaClass = ComptaPlanClasse::all()->count();
        $comptaSector = ComptaPlanSector::all()->count();

        $total = $famille + $user + $configAscBillet + $configAscPresta + $configAscRemb + $comptaSector + $comptaClass + $comptaCompte;

        if($total == 90){
            $i = $i + 1;
        }else{
            $i = $i - 1;
        }

        if($i == 0 || $i == 1){
            return 0;
        }elseif($i == 2){
            return 1;
        }else{
            return 2;
        }
    }

    public static function testInerance(){
        $i = 0;
        // Test de la relation des comptes ASC
        $ascCompte = AscCompte::all()->count();
        $planCompte = ComptaPlanCompte::all()->count();

        if($ascCompte == $planCompte){
            $i = $i + 1;
        }else{
            $i = $i - 1;
        }

        // Balance Exacte ASC
        $debit = AscCompte::all()->sum('debit');
        $credit = AscCompte::all()->sum('credit');

        if($debit == $credit){
            $i = $i + 1;
        }else{
            $i = $i - 1;
        }

        if($i == 0){
            return 0;
        }elseif($i == 1){
            return 1;
        }else{
            return 2;
        }
    }

    public static function testInfoInerance(){
        $error = [];
        $ascCompte = AscCompte::all()->count();
        $planCompte = ComptaPlanCompte::all()->count();

        if($ascCompte != $planCompte){
            $error = array_add($error, 'text1', "Nombre de compte: Non égale<br>");
        }else{
            $error = array_add($error, 'text1', "Nombre de compte: Egale<br>");
        }

        $debit = AscCompte::all()->sum('debit');
        $credit = AscCompte::all()->sum('credit');

        if($debit != $credit){
            $error = array_add($error, 'text2', "Etat de la balance: La balance n'est pas égale !<br>");
        }else{
            $error = array_add($error, 'text2', 'Etat de la balance: La balance est égale<br>');
        }

        return $error;


    }

    public static function SavingDatabase($file){
        $fp = file_get_contents($file);
        if(env('APP_ENV') == 'local'){
            $saving = Storage::disk('local')->put(env("DB_DATABASE").'/'.date("Y-d-m-H-i-s").'.sql.zip', $fp);
            if($saving){
                return true;
            }else{
                return false;
            }
        }elseif(env('APP_ENV') == 'test'){
            $saving = Storage::disk('ftp')->put(env("DB_DATABASE").'/'.date("Y-d-m-H-i-s").'.sql.zip', $fp);
            if($saving){
                return true;
            }else{
                return false;
            }
        }else{
            $saving = Storage::disk('ftp')->put(env("DB_DATABASE").'/'.date("Y-d-m-H-i-s").'.sql.zip', $fp);
            if($saving){
                return true;
            }else{
                return false;
            }
        }
    }

    public static function SavingAutoDatabase(){
        $dateSaveBackup = date("Y-m-d-H-i-s").".zip";
        $dump = exec('cd '.base_path()." && php artisan backup:run --only-db");
        if($dump){
            $file = storage_path()."/app/".env("APP_NAME")."/".date("Y-m-d-H-i-s").".zip";
            if($file){
                $storage = DatabaseOtherController::SavingDatabase($file);

                if($storage == true){
                    if(env("APP_ENV") == 'local'){
                        $sizeSave = Storage::disk('local')->size($file);
                    }elseif(env("APP_ENV") == 'test'){
                        $sizeSave = Storage::disk('ftp')->size($file);
                    }else{
                        $sizeSave = Storage::disk('ftp')->size($file);
                    }

                    $save = Sauvegarde::create([
                        "nameSave"  => $dateSaveBackup.'.sql',
                        "typeSave"  => "0",
                        "sizeSave"  => DatabaseOtherController::formatBytes($sizeSave, 2)
                    ]);

                    if($save && $file){
                         return AutomateController::datastore('SavingAutoDatabase', 'Sauvegarde automatique de la base de donnée réussi !', 2);

                    }else{
                         return AutomateController::datastore('SavingAutoDatabase', 'Erreur lors de la sauvegarde de la base ed donnée !', 0);
                    }
                }else{
                     return AutomateController::datastore('SavingAutoDatabase', 'Echec de transfere !', 0);
                }
            }else{
                 return AutomateController::datastore('SavingAutoDatabase', 'Echec d\'inscription du fichier local !', 0);
            }
        }else{
             return AutomateController::datastore('SavingAutoDatabase', 'Echec de création du fichier local !', 0);
        }

    }

}
