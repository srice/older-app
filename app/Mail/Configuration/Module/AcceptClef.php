<?php

namespace App\Mail\Configuration\Module;

use App\Packages\Srice\Comite;
use App\Packages\Srice\Espace;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AcceptClef extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Espace
     */
    public $espace;

    public $module;

    public $comite;

    /**
     * Create a new message instance.
     *
     * @param Espace $espace
     */
    public function __construct(Espace $espace, $idModule)
    {
        //
        $comite = new Comite();
        $this->espace = $espace;
        $this->module = $this->espace->module($idModule);
        $this->comite = $comite->comite();

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Email.Configuration.Module.accept')
            ->subject('[SRICE] - Votre demande de clef d\'accès !');
    }
}
