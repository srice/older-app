@extends('template')
@section("title")
    Facture de Vente salarié N° <strong>{{ $billet->numBilletSalarie }}</strong>
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">

@stop
@section("content")
    <div class="panel">
        <div class="panel-body">
            <a href="{{ route('billet.salarie.showPdf', $billet->id) }}" class="btn btn-default"><i class="fa fa-print"></i> Imprimer</a>
            <a href="{{ route('billet.salarie.savePdf', $billet->id) }}" class="btn btn-default"><i class="fa fa-file-pdf-o"></i> Exporter en pdf</a>
            @if($billet->etatBillet == 0)
            <a href="{{ route('billet.salarie.check', $billet->id) }}" class="btn btn-success"><i class="fa fa-check"></i> Valider la facture</a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel bg-info">
                <header class="panel-heading">
                    <h2 class="text-xs-center white p-t-10">Date de la facture</h2>
                </header>
                <div class="panel-body">
                    <h1 class="text-xs-center white p-t-20" style="font-weight: bold;">{{ $billet->dateBillet->format('d/m/Y') }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel bg-info">
                <header class="panel-heading">
                    <h2 class="text-xs-center white p-t-10">Total de la facture</h2>
                </header>
                <div class="panel-body">
                    <h1 class="text-xs-center white p-t-20" style="font-weight: bold;">{{ \App\Http\Controllers\OtherController::euro($billet->totalBillet) }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel {{ \App\Http\Controllers\GestionAsc\Billetterie\BilletterieController::etatBilletBg($billet->etatBillet) }}">
                <header class="panel-heading">
                    <h2 class="text-xs-center white p-t-10">Etat de la facture</h2>
                </header>
                <div class="panel-body">
                    <h1 class="text-xs-center white p-t-20" style="font-weight: bold;">{!! \App\Http\Controllers\GestionAsc\Billetterie\BilletterieController::etatBilletText($billet->etatBillet) !!} </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <header class="panel-heading">
            <div class="nav-tabs-horizontal" data-plugin="tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" data-toggle="tab" href="#prestation" role="tab"><i class="fa fa-cubes"></i> Prestations</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#reglement" role="tab"><i class="fa fa-money"></i> Règlements</a>
                    </li>
                </ul>
            </div>
        </header>
        <div class="panel-body tab-content p-t-20">
            <div class="tab-pane active" id="prestation" role="tabpanel">
                <div class="panel">
                    <header class="panel-header">
                        <div class="row">
                            <div class="col-md-10">
                                <h3 class="panel-title">Liste des Prestations</h3>
                            </div>
                            <div class="col-md-2">
                                @if($billet->etatBillet == 0)
                                <a href="#addPrestation" class="btn btn-lg btn-block btn-primary" data-toggle="modal"><i class="fa fa-plus-circle"></i> Ajouter une prestation</a>
                                @endif
                            </div>
                        </div>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Prestation</th>
                                        <th>Prix Unitaire</th>
                                        <th>Quantité</th>
                                        <th>Total</th>
                                        @if($billet->etatBillet == 0)
                                        <th></th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($billet->lignes as $ligne)
                                    <tr>
                                        <td>
                                            <strong>{{ \App\Http\Controllers\GestionAsc\Billetterie\Salarie\LigneBilletController::getNamePrestation($ligne->prestations_id) }}</strong><br>
                                            <i><strong>TARIF:</strong> {{ \App\Http\Controllers\GestionAsc\Billetterie\Salarie\LigneBilletController::getNameTarif($ligne->tarifs_id) }}</i>
                                        </td>
                                        <td>{{ \App\Http\Controllers\GestionAsc\Billetterie\Salarie\LigneBilletController::getUnitPrice($ligne->tarifs_id) }}</td>
                                        <td>{{ $ligne->qte }}</td>
                                        <td>{{ \App\Http\Controllers\GestionAsc\Billetterie\Salarie\LigneBilletController::getTotal($ligne->tarifs_id, $ligne->qte) }}</td>
                                        @if($billet->etatBillet == 0)
                                        <td>
                                            {{ Form::model($ligne, ["route" => ["billet.salarie.delPrestation", $billet->id, $ligne->id], "method" => "DELETE"]) }}
                                            <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                            {{ Form::close() }}
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="reglement" role="tabpanel">
                <div class="panel">
                    <header class="panel-heading">
                        <div class="row">
                            <div class="col-md-10">
                                <h3 class="panel-title">Liste des règlements</h3>
                            </div>
                            <div class="col-md-2">
                                @if($billet->etatBillet >= 1)
                                <a href="#addReglement" class="btn btn-block btn-lg btn-primary" data-toggle="modal"><i class="fa fa-plus-circle"></i> Ajouter un règlement</a>
                                @endif
                            </div>
                        </div>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="listeReglement" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Numéro de transaction</th>
                                        <th>Date du règlement</th>
                                        <th>Mode de règlement</th>
                                        <th>Montant du règlement</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($billet->reglements as $reglement)
                                    <tr>
                                        <td>{{ $reglement->numReglementBilletSalarie }}</td>
                                        <td>{{ $reglement->dateReglement->format('d/m/Y') }}</td>
                                        <td>{{ \App\Http\Controllers\GestionAsc\Billetterie\Salarie\BilletReglementController::modeReglement($reglement->modeReglement) }}</td>
                                        <td>{{ \App\Http\Controllers\OtherController::euro($reglement->totalReglement) }}</td>
                                        <td>
                                            {{ Form::model($reglement, ["route" => ["billet.salarie.delReglement", $billet->id, $reglement->id], "method" => "DELETE"]) }}
                                            <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addPrestation" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'une Prestation</h4>
                </div>
                {{ Form::model($billet, ["route" => ["billet.salarie.addPrestation", $billet->id]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('tarifs_id', "Prestation *", ["class" => "control-label"]) }}
                        <select name="tarifs_id" id="" class="form-control" data-plugin="select2">
                            <option value=""></option>
                            @foreach($familles as $famille)
                                <optgroup label="{{ $famille->name }}">
                                    <?php
                                    $prestations = \App\Model\GestionAsc\Prestation\Prestation::where('familles_id', $famille->id)->get();
                                    foreach ($prestations as $prestation):
                                    $tarifs = \App\Model\GestionAsc\Prestation\PrestationTarif::where('prestations_id', $prestation->id)->get();
                                    foreach ($tarifs as $tarif):
                                    ?>
                                    <option value="{{ $tarif->id }}">{{ $prestation->name }} - {{ $tarif->libelle }}</option>
                                    <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('qte', "Quantité *", ["class" => "control-label"]) }}
                        {{ Form::text('qte', null, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="addReglement" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un Règlement</h4>
                </div>
                {{ Form::model($billet, ["route" => ["billet.salarie.addReglement", $billet->id]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('modeReglement', "Mode de règlement", ["class" => "control-label"]) }}
                        <select id="modeReglements" class="form-control" name="modeReglement" data-plugin="select2" onchange="showNum()">
                            <option value=""></option>
                            @foreach($modeReglements as $modeReglement)
                                <option value="{{ $modeReglement['id'] }}">{{ $modeReglement['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="numTransaction">
                        <div class="form-group form-material">
                            {{ Form::label('numReglementBilletSalarie', "Numéro du Chèque", ["class" => "control-label"]) }}
                            {{ Form::text("numReglementBilletSalarie", null, ["class" => "form-control"]) }}
                        </div>
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('depositaireReglement', "Payeur", ["class" => "control-label"]) }}
                        {{ Form::text('depositaireReglement', $billet->salarie->nom.' '.$billet->salarie->prenom, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('dateReglement', "Date du règlement", ["class" => "control-label"]) }}
                        {{ Form::text('dateReglement', null, ["class" => "form-control date"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('totalReglement', "Total du règlement", ["class" => "control-label"]) }}
                        {{ Form::text('totalReglement', $billet->totalBillet, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/select2/select2.full.min.js"></script>
    <script src="/assets/global/js/Plugin/select2.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/custom/js/gestionasc/billetterie/salarie/show.js"></script>
@stop    