<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAncvAchatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ancv_achats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solution_id')->default(0)->comment("0: Chèque Vacances |1: Coupon Sport |2: Départ 18-25");
            $table->string("total");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ancv_achats');
    }
}
