<?php

namespace App\Model\ComptaAsc\Remise;

use Illuminate\Database\Eloquent\Model;

class RemiseBanque extends Model
{
    protected $guarded = [];

    public function cheques(){
        return $this->hasMany(RemiseBanqueCheque::class, 'numRemise', 'numRemise');
    }

    public function especes(){
        return $this->hasMany(RemiseBanqueEspece::class, 'numRemise', 'numRemise');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateRemise"];
    }
}
