(function ($) {
    window.setInterval(function () {
        contentChat();
        stateTicket();
    }, 1000);

    $("#formAddMessage").on('submit', function(e){
        e.preventDefault();
        var form = $(this);
        var id = $("#tickets_id").val();
        $.ajax({
            url: id+'/reply',
            type: "POST",
            data: form.serializeArray(),
            success: function (data) {
                toastr.success("Message Posté");
                $("#summernote").val('');
            },
            fail: function (data) {
                toastr.error("Erreur lors du post !")
            }
        })
    })
})(jQuery);

function contentChat(){
    var div = $("#contentChat");
    var tickets_id = div.attr('data-id');

    $.get(tickets_id+'/loadChat')
        .done(function (data) {
            div.html(data)
        })
}

function stateTicket(){
    var div = $("#stateTicket");
    var tickets_id = div.attr('data-id');
    $.get(tickets_id+'/stateTicket')
        .done(function (data) {
            div.html(data)
        })
}