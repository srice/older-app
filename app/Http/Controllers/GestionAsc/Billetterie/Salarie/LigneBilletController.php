<?php

namespace App\Http\Controllers\GestionAsc\Billetterie\Salarie;

use App\Http\Controllers\OtherController;
use App\Model\GestionAsc\Billetterie\BilletSalarie;
use App\Model\GestionAsc\Billetterie\LigneBilletSalarie;
use App\Model\GestionAsc\Prestation\Prestation;
use App\Model\GestionAsc\Prestation\PrestationTarif;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class LigneBilletController extends BilletController
{
    public static function getNamePrestation($prestations_id){
        $prestation = Prestation::find($prestations_id);
        return $prestation->name;
    }

    public static function getNameTarif($tarifs_id){
        $prestation = PrestationTarif::find($tarifs_id);
        return $prestation->libelle;
    }

    public static function getUnitPrice($tarifs_id, $euro = true){
        $key = PrestationTarif::find($tarifs_id);
        if($euro == true){
            return OtherController::euro($key->price_sal);
        }else{
            return $key->price_sal;
        }
    }

    public static function getTotal($tarifs_id, $qte, $euro = true){
        $tarif = PrestationTarif::find($tarifs_id);
        $calc = $tarif->price_sal * $qte;

        if($euro == true){
            return OtherController::euro($calc);
        }else{
            return $calc;
        }
    }

    public static function verifQuotaSalarieByTarif($salaries_id = null, $tarifs_id = null){
        $sum = LigneBilletSalarie::where('tarifs_id', $tarifs_id)->where('salaries_id', $salaries_id)->sum('qte');
        $quota = PrestationTarif::find($tarifs_id);


        $limitQuota = $quota->quota;
        $sumQte = $sum;

        if($sumQte > $limitQuota){
            return true;
        }else{
            return false;
        }
    }

    public static function verifQuotaSalarieByQte($tarifs_id = null, $qte){
        $quota = PrestationTarif::find($tarifs_id);

        if($qte > $quota->quota){
            return true;
        }else{
            return false;
        }
    }

    public function addPrestation(Request $request, $billets_id){
        $this->validate($request, [
            "tarifs_id" => "required",
            "qte"       => "required|numeric"
        ]);
        $billet = BilletSalarie::find($billets_id);
        $tarif = PrestationTarif::find($request->tarifs_id);

        if($this->configuration['presta']->stock == 1){
            if($tarif->stockActuel < $request->qte){
                Toastr::warning("Attention la quantité demander est supérieurs au stock actuel !");
                return redirect()->back();
            }
        }

        if($this->configuration['presta']->quota == 1){
            if(self::verifQuotaSalarieByTarif($billet->salaries_id, $request->tarifs_id) == true){
                Toastr::warning("Attention, ce salarié ne peut plus commander de cette prestation car il à atteint le quota !");
                return redirect()->back();
            }
            if(self::verifQuotaSalarieByQte($request->tarifs_id, $request->qte) == true){
                Toastr::warning("Attention, la quantité entrer est supérieur au quota enregistrer pour ce tarif !");
                return redirect()->back();
            }
        }

        $newTotal = $billet->totalBillet + self::getTotal($request->tarifs_id, $request->qte, false);
        $totalLigne = self::getTotal($request->tarifs_id, $request->qte, false);

        $ligne = LigneBilletSalarie::create([
            "billet_salarie_id" => $billets_id,
            "prestations_id"    => $tarif->prestations_id,
            "tarifs_id"         => $request->tarifs_id,
            "qte"               => $request->qte,
            "total"             => $totalLigne,
            "salaries_id"       => $billet->salaries_id
        ]);

        $billet->update([
            "totalBillet"   => $newTotal
        ]);

        if($ligne && $billet){
            Toastr::success("La Prestation à été ajouté.");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'inscription de la prestation dans la facture de vente !");
            return redirect()->back();
        }


    }

    public function delPrestation($billets_id, $id){
        $billet = BilletSalarie::find($billets_id);
        $ligne = LigneBilletSalarie::find($id);

        $newTotal = $billet->totalBillet - $ligne->total;

        $ligne->delete();

        $billet->update([
            "totalBillet"   => $newTotal
        ]);

        if($billet && $ligne){
            Toastr::success("La prestation à été supprimé de la facture de vente");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de la prestation dans la facture de vente !");
            return redirect()->back();
        }
    }
}
