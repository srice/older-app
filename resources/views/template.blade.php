<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@section('title')
            | SRICE
        @show</title>
    <link rel="apple-touch-icon" href="/assets/template/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="/assets/template/images/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="/assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="/assets/template/css/site.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="/assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="/assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="/assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="/assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="/assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="/assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="/assets/global/vendor/waves/waves.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    @yield('header_styles')
    <!-- Fonts -->
    <link rel="stylesheet" href="/assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="/assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel="stylesheet" href="/assets/global/fonts/web-icons/web-icons.css">
    <link rel="stylesheet" href="/assets/global/fonts/font-awesome/font-awesome.css">
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    {!! Charts::assets() !!}
    <!--[if lt IE 9]>
    <script src="/assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="/assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="/assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="/assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <span class="navbar-brand-text hidden-xs-down"> SRICE</span>
        </div>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search"
                data-toggle="collapse">
            <span class="sr-only">Toggle Search</span>
            <i class="icon md-search" aria-hidden="true"></i>
        </button>
    </div>
    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <ul class="nav navbar-toolbar">
                <li class="nav-item hidden-float" id="toggleMenubar">
                    <a class="nav-link" data-toggle="menubar" href="#" role="button">
                        <i class="icon hamburger hamburger-arrow-left">
                            <span class="sr-only">Toggle menubar</span>
                            <span class="hamburger-bar"></span>
                        </i>
                    </a>
                </li>
            </ul>
            <!-- End Navbar Toolbar -->
            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false" data-animation="scale-up" role="button">
                        <span>Assistance</span>
                    </a>
                    <div class="dropdown-menu" role="menu">
                        <span class="dropdown-header">Mes Tickets</span>
                        <a href="{{ route('tickets.index') }}" class="dropdown-item" role="menuitem">Mes tickets d'assistance</a>
                        <a href="{{ route('tickets.create') }}" class="dropdown-item" role="menuitem">Nouveau ticket</a>
                        <span class="dropdown-header">Mes Demandes</span>
                        <a href="{{ route('demandes.index') }}" class="dropdown-item" role="menuitem">Liste de mes demandes</a>
                        <a href="{{ route('demandes.create') }}" class="dropdown-item" role="menuitem">Poster une demande</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="scale-up" role="button">
                      <span class="avatar">
                        @if(\Illuminate\Support\Facades\Auth::user()->avatar == 0)
                              <img src="{{ \Creativeorange\Gravatar\Facades\Gravatar::get(\Illuminate\Support\Facades\Auth::user()->email, 'secure') }}" alt="...">
                          @else
                              <img src="/assets/custom/images/profil/{{ \Illuminate\Support\Facades\Auth::user()->id }}.png" alt="...">
                          @endif
                        <i></i>
                      </span>
                    </a>
                    <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="{{ route('profil.index') }}" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Mon profil</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Déconnexion</a>
                    </div>
                </li>
            </ul>
            <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->
    </div>
</nav>
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                @if(empty($config->sector))
                    <ul class="site-menu" data-plugin="menu">
                        <li class="site-menu-category">Bienvenue</li>
                        <li class="site-menu-item {!! (Route::is('home.dashboard') ? 'active':"") !!}">
                            <a class="animsition-link" href="{{ route('home.dashboard') }}">
                                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                <span class="site-menu-title">Tableau de Bord</span>
                            </a>
                        </li>
                    </ul>
                @endif
                @if($config->sector == 'configuration')
                        <ul class="site-menu" data-plugin="menu">
                            <li class="site-menu-category">CONFIGURATION</li>
                            <li class="site-menu-item {!! (Route::is('config.dashboard') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('config.dashboard') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Tableau de Bord</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('config.user.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('config.user.index') }}">
                                    <i class="site-menu-icon md-accounts-list" aria-hidden="true"></i>
                                    <span class="site-menu-title">Gestion des Utilisateurs</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('config.bdd.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('config.bdd.index') }}">
                                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                                    <span class="site-menu-title">Gestion BDD</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('config.comite.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('config.comite.index') }}">
                                    <i class="site-menu-icon md-cake" aria-hidden="true"></i>
                                    <span class="site-menu-title">Gestion du comité</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('config.module.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('config.module.index') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Gestion des modules</span>
                                </a>
                            </li>
                        </ul>
                @endif
                    @if($config->sector == 'beneficiaire')
                        <ul class="site-menu" data-plugin="menu">
                            <li class="site-menu-category">BENEFICIAIRE</li>
                            <li class="site-menu-item {!! (Route::is('beneficiaire.dashboard') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('beneficiaire.dashboard') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Tableau de Bord</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('salaries.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('salaries.index') }}">
                                    <i class="site-menu-icon md-accounts" aria-hidden="true"></i>
                                    <span class="site-menu-title">Gestion des salariés</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('beneficiaire.config.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('beneficiaire.config.index') }}">
                                    <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                                    <span class="site-menu-title">Configuration</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                @if($config->sector == 'gestionasc')
                        <ul class="site-menu" data-plugin="menu">
                            <li class="site-menu-category">GESTION DES ASC</li>
                            <li class="site-menu-item {!! (Route::is('gestion_asc.dashboard') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('gestion_asc.dashboard') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Tableau de Bord</span>
                                </a>
                            </li>
                            <li class="site-menu-item has-sub {!! (Route::is('achats.index') || Route::is('prestations.index') ? 'open':"") !!}">
                                <a href="javascript:void(0)">
                                    <i class="site-menu-icon fa-cubes" aria-hidden="true"></i>
                                    <span class="site-menu-title">Prestations</span>
                                    <span class="site-menu-arrow"></span>
                                </a>
                                <ul class="site-menu-sub">
                                    <li class="site-menu-item {!! (Route::is('prestations.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('prestations.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Liste des Prestations</span>
                                        </a>
                                    </li>
                                    @if($config->configuration['presta']->stock == 1)
                                    <li class="site-menu-item {!! (Route::is('achats.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('achats.index') }}">
                                            <i class="site-menu-icon fa-eur" aria-hidden="true"></i>
                                            <span class="site-menu-title">Achat de Prestations</span>
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </li>
                            <li class="site-menu-item {!! (Route::is('billetteries.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('billetteries.index') }}">
                                    <i class="site-menu-icon md-ticket-star" aria-hidden="true"></i>
                                    <span class="site-menu-title">Billetterie</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('remboursements.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('remboursements.index') }}">
                                    <i class="site-menu-icon md-transform" aria-hidden="true"></i>
                                    <span class="site-menu-title">Remboursement</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('configasc.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('configasc.index') }}">
                                    <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                                    <span class="site-menu-title">Configuration</span>
                                </a>
                            </li>
                        </ul>
                @endif
                @if($config->sector == 'comptaasc')
                        <ul class="site-menu" data-plugin="menu">
                            <li class="site-menu-category">COMPTA ASC</li>
                            <li class="site-menu-item {!! (Route::is('compta_asc.dashboard') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('compta_asc.dashboard') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Tableau de Bord</span>
                                </a>
                            </li>
                            <li class="site-menu-item has-sub {!! (Route::is('remise.index') ? 'open':"") !!}">
                                <a href="javascript:void(0)">
                                    <i class="site-menu-icon fa-bank" aria-hidden="true"></i>
                                    <span class="site-menu-title">Remise en banque</span>
                                    <span class="site-menu-arrow"></span>
                                </a>
                                <ul class="site-menu-sub">
                                    <li class="site-menu-item {!! (Route::is('remise.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('remise.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Bordereau de remise</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('remise.position.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('remise.position.index') }}">
                                            <i class="site-menu-icon fa-balance-scale" aria-hidden="true"></i>
                                            <span class="site-menu-title">Position de caisse</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="site-menu-item has-sub {!! (Route::is('comptaAsc.achat.index') || Route::is('comptaAsc.vente.index') || Route::is('comptaAsc.banque.index')
                             || Route::is('comptaAsc.divers.index') ? 'open':"") !!}">
                                <a href="javascript:void(0)">
                                    <i class="site-menu-icon fa-book" aria-hidden="true"></i>
                                    <span class="site-menu-title">Jounaux</span>
                                    <span class="site-menu-arrow"></span>
                                </a>
                                <ul class="site-menu-sub">
                                    <li class="site-menu-item {!! (Route::is('comptaAsc.achat.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.achat.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal des achats</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaAsc.vente.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.vente.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal des ventes</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaAsc.banque.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.banque.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal bancaire</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaAsc.caisse.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.caisse.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal de caisse</span>
                                        </a>
                                    </li>
                                    <!--<li class="site-menu-item {!! (Route::is('comptaAsc.divers.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.divers.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal des opérations diverses</span>
                                        </a>
                                    </li>-->
                                </ul>
                            </li>
                            <li class="site-menu-item has-sub {!! (Route::is('comptaAsc.bilan') || Route::is('comptaAsc.resultat') || Route::is('comptaAsc.balance') ? 'open':"") !!}">
                                <a href="javascript:void(0)">
                                    <i class="site-menu-icon fa-file" aria-hidden="true"></i>
                                    <span class="site-menu-title">Etat</span>
                                    <span class="site-menu-arrow"></span>
                                </a>
                                <ul class="site-menu-sub">
                                    <li class="site-menu-item {!! (Route::is('comptaAsc.bilan') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.bilan') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Bilan</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaAsc.resultat') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.resultat') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Compte de résultat</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaAsc.balance') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.balance') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Balance</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="site-menu-item has-sub {!! (Route::is('comptaAsc.config.index') ? 'open':"") !!}">
                                <a href="javascript:void(0)">
                                    <i class="site-menu-icon fa-cogs" aria-hidden="true"></i>
                                    <span class="site-menu-title">Configuration</span>
                                    <span class="site-menu-arrow"></span>
                                </a>
                                <ul class="site-menu-sub">
                                    <li class="site-menu-item {!! (Route::is('comptaAsc.config.initial.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.config.initial.index') }}">
                                            <i class="site-menu-icon fa-eur" aria-hidden="true"></i>
                                            <span class="site-menu-title">Bilan Initial</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaAsc.config.plan.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaAsc.config.plan.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Plan Comptable</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                @endif
                    @if($config->sector == 'comptafct')
                        <ul class="site-menu" data-plugin="menu">
                            <li class="site-menu-category">COMPTA DE FONCTIONNEMENT</li>
                            <li class="site-menu-item {!! (Route::is('compta_fct.dashboard') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('compta_fct.dashboard') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Tableau de Bord</span>
                                </a>
                            </li>
                            <li class="site-menu-item has-sub {!! (Route::is('comptaFct.achat.index') || Route::is('comptaFct.vente.index') || Route::is('comptaFct.banque.index')
                             || Route::is('comptaFct.divers.index') ? 'open':"") !!}">
                                <a href="javascript:void(0)">
                                    <i class="site-menu-icon fa-book" aria-hidden="true"></i>
                                    <span class="site-menu-title">Jounaux</span>
                                    <span class="site-menu-arrow"></span>
                                </a>
                                <ul class="site-menu-sub">
                                    <li class="site-menu-item {!! (Route::is('comptaFct.achat.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.achat.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal des achats</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaFct.vente.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.vente.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal des ventes</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaFct.banque.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.banque.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal bancaire</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaFct.caisse.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.caisse.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal de caisse</span>
                                        </a>
                                    </li>
                                    <!--<li class="site-menu-item {!! (Route::is('comptaFct.divers.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.divers.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Journal des opérations diverses</span>
                                        </a>
                                    </li>-->
                                </ul>
                            </li>
                            <li class="site-menu-item has-sub {!! (Route::is('comptaFct.bilan') || Route::is('comptaFct.resultat') || Route::is('comptaFct.balance') ? 'open':"") !!}">
                                <a href="javascript:void(0)">
                                    <i class="site-menu-icon fa-file" aria-hidden="true"></i>
                                    <span class="site-menu-title">Etat</span>
                                    <span class="site-menu-arrow"></span>
                                </a>
                                <ul class="site-menu-sub">
                                    <li class="site-menu-item {!! (Route::is('comptaFct.bilan') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.bilan') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Bilan</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaFct.resultat') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.resultat') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Compte de résultat</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaFct.balance') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.balance') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Balance</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="site-menu-item has-sub {!! (Route::is('comptaFct.config.index') ? 'open':"") !!}">
                                <a href="javascript:void(0)">
                                    <i class="site-menu-icon fa-cogs" aria-hidden="true"></i>
                                    <span class="site-menu-title">Configuration</span>
                                    <span class="site-menu-arrow"></span>
                                </a>
                                <ul class="site-menu-sub">
                                    <li class="site-menu-item {!! (Route::is('comptaFct.config.initial.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.config.initial.index') }}">
                                            <i class="site-menu-icon fa-eur" aria-hidden="true"></i>
                                            <span class="site-menu-title">Bilan Initial</span>
                                        </a>
                                    </li>
                                    <li class="site-menu-item {!! (Route::is('comptaFct.config.plan.index') ? 'active':"") !!}">
                                        <a class="animsition-link" href="{{ route('comptaFct.config.plan.index') }}">
                                            <i class="site-menu-icon fa-list" aria-hidden="true"></i>
                                            <span class="site-menu-title">Plan Comptable</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    @endif
                @if($config->sector == 'banker')
                        <ul class="site-menu" data-plugin="menu">
                            <li class="site-menu-category">BANKER</li>
                            <li class="site-menu-item {!! (Route::is('banker.dashboard') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('banker.dashboard') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Tableau de Bord</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('banker.users.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('banker.users.index') }}">
                                    <i class="site-menu-icon md-accounts" aria-hidden="true"></i>
                                    <span class="site-menu-title">Liste des utilisateurs</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('banker.accounts.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('banker.accounts.index') }}">
                                    <i class="site-menu-icon fa-bank" aria-hidden="true"></i>
                                    <span class="site-menu-title">Liste des comptes</span>
                                </a>
                            </li>
                        </ul>
                @endif
                    @if($config->sector == 'ancv')
                        <ul class="site-menu" data-plugin="menu">
                            <li class="site-menu-category">Chèques vancances</li>
                            <li class="site-menu-item {!! (Route::is('ancv.dashboard') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('ancv.dashboard') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Tableau de Bord</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('Ancv.Vente.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('Ancv.Vente.index') }}">
                                    <i class="site-menu-icon md-shopping-cart" aria-hidden="true"></i>
                                    <span class="site-menu-title">Ventes</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('Ancv.Achat.index') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('Ancv.Achat.index') }}">
                                    <i class="site-menu-icon md-shopping-basket" aria-hidden="true"></i>
                                    <span class="site-menu-title">Achats</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                    @if($config->sector == 'fidelity')
                        <ul class="site-menu" data-plugin="menu">
                            <li class="site-menu-category">Bienvenue</li>
                            <li class="site-menu-item {!! (Route::is('fidelity.dashboard') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('fidelity.dashboard') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Tableau de Bord</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                    @if($config->sector == 'facturation')
                        <ul class="site-menu" data-plugin="menu">
                            <li class="site-menu-category">FACTURATION IMMEDIATE</li>
                            <li class="site-menu-item {!! (Route::is('facturation.dashboard') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('facturation.dashboard') }}">
                                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Tableau de Bord</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('facturation.tiers') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('facturation.tiers') }}">
                                    <i class="site-menu-icon md-accounts" aria-hidden="true"></i>
                                    <span class="site-menu-title">Gestion des Tiers</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('facturation.facture') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('facturation.facture') }}">
                                    <i class="site-menu-icon md-balance-wallet" aria-hidden="true"></i>
                                    <span class="site-menu-title">Gestion des Factures</span>
                                </a>
                            </li>
                            <li class="site-menu-item {!! (Route::is('facturation.modeReglement') ? 'active':"") !!}">
                                <a class="animsition-link" href="{{ route('facturation.modeReglement') }}">
                                    <i class="site-menu-icon md-format-list-bulleted" aria-hidden="true"></i>
                                    <span class="site-menu-title">Gestion des modes de règlements</span>
                                </a>
                            </li>
                        </ul>
                    @endif
            </div>
        </div>
    </div>
    <div class="site-menubar-footer">
        <a href="{{ route('config.dashboard') }}" class="fold-show" data-placement="top" data-toggle="tooltip"
           data-original-title="Configuration">
            <span class="icon md-settings" aria-hidden="true"></span>
        </a>
        <a href="{{ route('logout') }}" data-placement="top" data-toggle="tooltip" data-original-title="Déconnexion">
            <span class="icon md-power" aria-hidden="true"></span>
        </a>
    </div>
</div>
<div class="site-gridmenu">
    <div>
        <div>
            <ul>
                @foreach($config->moduleMenu as $menu)
                    @if($menu->state == 1)
                        <li>
                            <a href="{{ route($menu->path.'.dashboard') }}">
                                <img src="{{ env("GESTION_URL_PROD") }}/storage/media/img/module/{{ $menu->image }}" width="70" class="img-responsive">
                                <span>{{ $menu->designation }}</span>
                            </a>
                        </li>
                    @endif    
                @endforeach
                <li>
                    <a href="{{ route('config.dashboard') }}">
                        <i class="icon md-settings" style="font-size: 30px;"></i>
                        <span>Configuration</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- Page -->
<div class="page">
    @include('includes.pageHeader')
    <div class="page-content">
        <div class="alert alert-warning">
            <strong>Attention</strong><br>
            Vous êtes sur une version de test, toutes les informations sont supprimer toutes les 1H30.
        </div>
        <div class="alert alert-info">
            <strong>Information</strong><br>
            Pour accéder à un module, veuillez cliquer sur le logo de SRICE en haut à gauche.
        </div>

        @include('error')
        @yield("content")
    </div>
</div>
<!-- End Page -->
<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">© 2017 SRICE</div>
    <div class="site-footer-right">
        <strong>Version:</strong> {{ env('APP_VERSION') }}
    </div>
</footer>
<!-- Core  -->
<script src="/assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
<script src="/assets/global/vendor/jquery/jquery.js"></script>
<script src="/assets/global/vendor/tether/tether.js"></script>
<script src="/assets/global/vendor/bootstrap/bootstrap.js"></script>
<script src="/assets/global/vendor/animsition/animsition.js"></script>
<script src="/assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="/assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
<script src="/assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
<script src="/assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
<script src="/assets/global/vendor/waves/waves.js"></script>
<!-- Plugins -->
<script src="/assets/global/vendor/switchery/switchery.min.js"></script>
<script src="/assets/global/vendor/intro-js/intro.js"></script>
<script src="/assets/global/vendor/screenfull/screenfull.js"></script>
<script src="/assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!-- Scripts -->
<script src="/assets/global/js/State.js"></script>
<script src="/assets/global/js/Component.js"></script>
<script src="/assets/global/js/Plugin.js"></script>
<script src="/assets/global/js/Base.js"></script>
<script src="/assets/global/js/Config.js"></script>
<script src="/assets/template/js/Section/Menubar.js"></script>
<script src="/assets/template/js/Section/GridMenu.js"></script>
<script src="/assets/template/js/Section/Sidebar.js"></script>
<script src="/assets/template/js/Section/PageAside.js"></script>
<script src="/assets/template/js/Plugin/menu.js"></script>
<script src="/assets/global/js/config/colors.js"></script>
<script src="/assets/template/js/config/tour.js"></script>
<script>
    Config.set('assets', '/assets/template/');
</script>
<!-- Page -->
<script src="/assets/template/js/Site.js"></script>
<script src="/assets/global/js/Plugin/asscrollable.js"></script>
<script src="/assets/global/js/Plugin/slidepanel.js"></script>
<script src="/assets/global/js/Plugin/switchery.js"></script>
<script src="/assets/custom/js/chat.js"></script>
<script src="https://wchat.freshchat.com/js/widget.js"></script>
@yield("footer_scripts")
<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
</script>
<script>

    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>
<!--Start of Tawk.to Script-->
<script>
    function initFreshChat() {
        window.fcWidget.init({
            token: "ff809629-e14f-46b7-a3a5-bdff0c08619a",
            host: "https://wchat.freshchat.com"
        });
    }
    function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script>
<!--End of Tawk.to Script-->
{!! Toastr::render() !!}
</body>
</html>