<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutomatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('automates', function (Blueprint $table) {
            $table->increments('id');
            $table->string("commandAutomate");
            $table->text('descAutomate');
            $table->integer('etatAutomate')->comment("0: Echec|1: Warning|2: Réussi");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('automates');
    }
}
