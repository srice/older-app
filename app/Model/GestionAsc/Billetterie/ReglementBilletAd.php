<?php

namespace App\Model\GestionAsc\Billetterie;

use Illuminate\Database\Eloquent\Model;

class ReglementBilletAd extends Model
{
    protected $guarded = [];

    public function billet(){
        return $this->belongsTo(BilletAd::class, 'billet_ads_id');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateReglement"];
    }
}
