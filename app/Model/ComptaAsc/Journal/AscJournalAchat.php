<?php

namespace App\Model\ComptaAsc\Journal;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use Illuminate\Database\Eloquent\Model;

class AscJournalAchat extends Model
{
    protected $guarded = [];

    public function compte(){
        return $this->belongsTo(ComptaPlanCompte::class, 'numCompte', 'numCompte');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateAchat"];
    }
}
