@extends('template')
@section("title")
    Edition du secteur
    @parent
@stop
@section("header_styles")

@stop
@section("content")
<div class="panel">
    <header class="panel-heading">
        <h3 class="panel-title">@yield('title')</h3>
    </header>
    <div class="panel-body">
        {{ Form::model($sector, ["route" => ["comptaFct.config.plan.sector.update", $classe->numClasse, $sector->numSector], "class" => "form-horizontal", "method" => "PUT"]) }}
        <div class="form-group form-material row">
            {{ Form::label('numSector', "Numéro de Secteur", ["class" => "control-label col-md-3"]) }}
            <div class="col-md-6">
                {{ Form::text('numSector', null, ["class" => "form-control"]) }}
            </div>
        </div>
        <div class="form-group form-material row">
            {{ Form::label('nameSector', "Nom du secteur", ["class" => "control-label col-md-3"]) }}
            <div class="col-md-6">
                {{ Form::text('nameSector', null, ["class" => "form-control"]) }}
            </div>
        </div>
        <div class="text-xs-right">
            <button class="btn btn-success">Valider</button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop
@section("footer_scripts")

@stop    