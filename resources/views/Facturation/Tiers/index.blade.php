@extends('template')
@section("title")
    Gestion des Tiers
@stop
@section("header_styles")
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="/assets/global/vendor/icheck/icheck.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
<div class="panel">
    <div class="panel-header">
        <div class="row">
            <div class="col-md-10">
                <h3 class="panel-title">Liste des tiers</h3>
            </div>
            <div class="col-md-2">
                <a href="#addTiers" data-toggle="modal" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Nouveau tiers</a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table id="listeTier" class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Identité</th>
                    <th>Adresse</th>
                    <th><i class="fa fa-bars"></i> </th>
                </tr>
            </thead>
            <tbody>
                @foreach($tiers as $tier)
                <tr>
                    <td>{{ $tier->id }}</td>
                    <td>{{ $tier->nomTier }}</td>
                    <td>
                        {{ $tier->adresseTier }}<br>
                        {{ $tier->codePostalTier }} {{ $tier->villeTier }}
                    </td>
                    <td>
                        {{ Form::model($tier, ["route" => ["facturation.tiers.delete", $tier->id], "method" => "DELETE"]) }}
                        <a href="{{ route('facturation.tiers.show', $tier->id) }}" class="btn btn-icon btn-default" data-toggle="tooltip" data-title="Voir la fiche du tiers"><i class="fa fa-eye"></i> </a>
                        <a href="{{ route('facturation.tiers.edit', $tier->id) }}" class="btn btn-icon btn-primary" data-toggle="tooltip" data-title="Editer le tier"><i class="fa fa-edit"></i> </a>
                        <button class="btn btn-icon btn-danger" data-toggle="tooltip" data-title="Supprimer le tier"><i class="fa fa-times"></i> </button>
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="addTiers" aria-hidden="true" aria-labelledby="examplePositionSidebar"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un Tiers</h4>
            </div>
            {{ Form::open(["route" => "facturation.tiers.store"]) }}
            <div class="modal-body">
                <div class="form-group form-material">
                    {{ Form::label('typeTier', "Type de tier", ["class" => "control-label"]) }}
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::radio('typeTier', 0, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                            {{ Form::label('typeTier', "Client", ["class" => "control-label"]) }}
                        </div>
                        <div class="col-md-6">
                            {{ Form::radio('typeTier', 1, false, ["class" => "icheckbox-primary", "data-plugin" => "iCheck", "data-radio-class" => "iradio_flat-blue"]) }}
                            {{ Form::label('typeTier', "Fournisseur", ["class" => "control-label"]) }}
                        </div>
                    </div>
                </div>
                <div class="form-group form-material">
                    {{ Form::label('nomTier', "Nom ou Raison Social", ["class" => "control-label"]) }}
                    {{ Form::text('nomTier', null, ["class" => "form-control", "required"]) }}
                </div>
                <div class="form-group form-material">
                    {{ Form::label('adresseTier', "Adresse", ["class" => "control-label"]) }}
                    {{ Form::textarea('adresseTier', null, ["class" => "form-control", "required"]) }}
                </div>
                <div class="form-group form-material">
                    {{ Form::label('codePostalTier', "Code Postal", ["class" => "control-label"]) }}
                    {{ Form::text('codePostalTier', null, ["class" => "form-control", "required"]) }}
                </div>
                <div class="form-group form-material">
                    {{ Form::label('villeTier', "Ville", ["class" => "control-label"]) }}
                    {{ Form::text('villeTier', null, ["class" => "form-control", "required"]) }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-block">Valider</button>
                <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section("footer_scripts")
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="/assets/global/vendor/icheck/icheck.min.js"></script>
    <script src="/assets/global/js/Plugin/icheck.js"></script>
    <script src="/assets/custom/js/facturation/tiers/index.js"></script>
@stop    