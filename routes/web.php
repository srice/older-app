<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::group(["middleware" => ["access", "auth"]], function (){
        Route::get('/', ["as" => "home.dashboard", "uses" => "HomeController@index"]);

        Route::group(["prefix" => "profil", "namespace" => "Profil"], function (){
            Route::get('/', ["as" => "profil.index", "uses" => "ProfilController@index"]);
        });

        Route::group(["prefix" => "assistance", "namespace" => "Assistance"], function (){
            Route::group(["prefix" => "demande", "namespace" => "Demande"], function (){
                route::get('/', ["as" => "demandes.index", "uses" => "DemandeController@index"]);
                route::get('create', ["as" => "demandes.create", "uses" => "DemandeController@create"]);
                route::post('create', ["as" => "demandes.store", "uses" => "DemandeController@store"]);
                route::get('{id}', ["as" => "demandes.show", "uses" => "DemandeController@show"]);
            });
            Route::group(["prefix" => "ticket", "namespace" => "Ticket"], function (){
                Route::get('/', ["as" => "tickets.index", "uses" => "TicketController@index"]);
                Route::get('create', ["as" => "tickets.create", "uses" => "TicketController@create"]);
                Route::post('create', ["as" => "tickets.store", "uses" => "TicketController@store"]);
                Route::get('{id}', ["as" => "tickets.show", "uses" => "TicketController@show"]);

                Route::get('{id}/loadChat', "TicketController@loadChat");
                Route::post('{id}/reply', ["as" => "tickets.reply", "uses" => "TicketController@reply"]);
                Route::get('{id}/stateTicket', "TicketController@stateTicket");
            });
            Route::group(["prefix" => "chat", "namespace" => "Chat"], function (){
                Route::get('/', ["as" => "chat.index", "uses" => "ChatController@index"]);
            });
        });

        Route::group(["prefix" => "config", "namespace" => "Configuration"], function (){
            Route::get('/', ["as" => "config.dashboard", "uses" => "ConfigController@dashboard"]);
            Route::group(["prefix" => "users", "namespace" => "User"], function (){
                Route::get('/', ["as" => "config.user.index", "uses" => "UserController@index"]);
                Route::get('create', ["as" => "config.user.create", "uses" => "UserController@create"]);
                Route::post('create', ["as" => "config.user.store", "uses" => "UserController@store"]);
                Route::get('{id}', ["as" => "config.user.show", "uses" => "UserController@show"]);
                Route::get('{id}/edit', ["as" => "config.user.edit", "uses" => "UserController@edit"]);
                Route::put('{id}/edit', ["as" => "config.user.update", "uses" => "UserController@update"]);
                Route::delete('{id}/delete', ["as" => "config.user.delete", "uses" => "UserController@delete"]);

                Route::get('{id}/lock', ["as" => "config.user.lock", "uses" => "UserController@lock"]);
                Route::get('{id}/unlock', ["as" => "config.user.unlock", "uses" => "UserController@unlock"]);
                Route::get('{id}/mergePass', ["as" => "config.user.mergePass", "uses" => "UserController@mergePass"]);
            });
            Route::group(["prefix" => "bdd", "namespace" => "Database"], function (){
                Route::get('/', ["as" => "config.bdd.index", "uses" => "DatabaseController@index"]);
                Route::get('/lastSave', 'DatabaseController@lastSave');
                Route::get('/stateBdd', 'DatabaseController@stateBdd');
                Route::get('/stateCluster', 'DatabaseController@stateCluster');
                Route::get('/stateInerance', 'DatabaseController@stateInerance');
                Route::get('/infoInerance', 'DatabaseController@infoInerance');

                Route::get('/save', ["as" => "config.bdd.save", "uses" => "DatabaseController@save"]);
                Route::get('/purge/gestion', ["as" => "config.bdd.purge.gestion", "uses" => "DatabaseController@purgeDatabaseGestion"]);
                Route::get('/purge/comptaasc', ["as" => "config.bdd.purge.comptaasc", "uses" => "DatabaseController@purgeDatabaseComptaAsc"]);
                Route::get('/purge/comptafct', ["as" => "config.bdd.purge.comptafct", "uses" => "DatabaseController@purgeDatabaseComptaFct"]);
                Route::get('/purge/fidelity', ["as" => "config.bdd.purge.fidelity", "uses" => "DatabaseController@purgeDatabaseFidelity"]);
            });
            Route::group(["prefix" => "comite", "namespace" => "Comite"], function (){
                Route::get('/', ["as" => "config.comite.index", "uses" => "ComiteController@index"]);
                Route::get('/timeout', 'ComiteController@timeout');
            });
            Route::group(["prefix" => "module", "namespace" => "Module"], function (){
                Route::get('/', ["as" => "config.module.index", "uses" => "ModuleController@index"]);
                Route::get('/ficheProject/{id}', 'ModuleController@ficheProject');
                Route::get('/clefAccess/{id}', 'ModuleController@clefAccess');
                Route::post('/clefAccess/{espaces_id}/{modules_id}', ["as" => "config.module.acceptClef", "uses" => 'ModuleController@acceptClef']);
                Route::get('/checkout/{modules_id}', 'ModuleController@checkout');
                Route::post('/checkout/{espaces_id}/{modules_id}', ["as" => "config.module.acceptCheckout", "uses" => 'ModuleController@acceptCheckout']);

                Route::get('{modules_id}/active', ["as" => "config.module.activeModule", "uses" => "ModuleController@active"]);
                Route::get('{modules_id}/desactive', ["as" => "config.module.desactiveModule", "uses" => "ModuleController@desactive"]);
            });
        });

        Route::group(["prefix" => "beneficiaire", "namespace" => "Beneficiaire"], function (){
            Route::get('/', ["as" => "beneficiaire.dashboard", "uses" => "BeneficiaireController@dashboard"]);
            Route::get('/countSalarie', 'BeneficiaireController@countSalarie');
            Route::get('/countAd', 'BeneficiaireController@countAd');
            Route::group(["prefix" => "salarie"], function (){
                Route::get('/', ["as" => "salaries.index", "uses" => "SalarieController@index"]);
                Route::get('create', ["as" => "salaries.create", "uses" => "SalarieController@create"]);
                Route::post('create', ["as" => "salaries.store", "uses" => "SalarieController@store"]);
                Route::get('{id}', ["as" => "salaries.show", "uses" => "SalarieController@show"]);
                Route::get('{id}/edit', ["as" => "salaries.edit", "uses" => "SalarieController@edit"]);
                Route::put('{id}/edit', ["as" => "salaries.update", "uses" => "SalarieController@update"]);
                Route::delete('{id}', ["as" => "salaries.delete", "uses" => "SalarieController@delete"]);

                Route::get('{salaries_id}/inactif', ["as" => "salaries.inactif", "uses" => "SalarieOtherController@inactif"]);
                Route::get('{salaries_id}/actif', ["as" => "salaries.actif", "uses" => "SalarieOtherController@actif"]);
                Route::post('/MassInactif', ["as" => "Salaries.MassInactif", "uses" => "SalarieOtherController@massInactif"]);
                Route::post('/MassActif', ["as" => "Salaries.MassActif", "uses" => "SalarieOtherController@massActif"]);
                Route::post('/importCsv', ["as" => "Salaries.importCsv", "uses" => "SalarieOtherController@importCsv"]);

                Route::get('getVille/{codePostal}', "SalarieOtherController@getVille");
            });
            Route::group(["prefix" => "{salaries_id}/ad"], function (){
                Route::get('create', ["as" => "ad.create", "uses" => "AdController@create"]);
                Route::post('create', ["as" => "ad.store", "uses" => "AdController@store"]);
                Route::get('{ad_id}/edit', ["as" => "ad.edit", "uses" => "AdController@edit"]);
                Route::put('{ad_id}/edit', ["as" => "ad.update", "uses" => "AdController@update"]);
                Route::delete('{ad_id}/delete', ["as" => "ad.delete", "uses" => "AdController@delete"]);

                Route::get('{ad_id}/inactif', ["as" => "ad.inactif", "uses" => "AdOtherController@inactif"]);
                Route::get('{ad_id}/actif', ["as" => "ad.actif", "uses" => "AdOtherController@actif"]);
            });
            Route::group(["prefix" => "config", "namespace" => "Configuration"], function (){
                Route::get('/', ["as" => "beneficiaire.config.index", "uses" => "ConfigurationController@index"]);
            });
        });

        Route::group(["prefix" => "gestionAsc", "namespace" => "GestionAsc"], function (){
            Route::get('/', ["as" => "gestion_asc.dashboard", "uses" => "GestionAscController@dashboard"]);
            Route::group(["prefix" => "prestation", "namespace" => "Prestation"], function (){
                Route::get('/', ["as" => "prestations.index", "uses" => "PrestationController@index"]);
                Route::get('create', ["as" => "prestations.create", "uses" => "PrestationController@create"]);
                Route::post('create', ["as" => "prestations.store", "uses" => "PrestationController@store"]);
                Route::get('{prestations_id}', ["as" => "prestations.show", "uses" => "PrestationController@show"]);
                Route::get('{prestations_id}/edit', ["as" => "prestations.edit", "uses" => "PrestationController@edit"]);
                Route::put('{prestations_id}/edit', ["as" => "prestations.update", "uses" => "PrestationController@update"]);
                Route::delete('{prestations_id}', ["as" => "prestations.delete", "uses" => "PrestationController@delete"]);
                Route::post('{prestations_id}/localize', ["as" => "localize.store", "uses" => "LocalisationController@storeLocalize"]);
                Route::delete('{prestations_id}/localize/{localize_id}', ["as" => "localize.delete", "uses" => "LocalisationController@deleteLocalize"]);
                Route::get('{prestations_id}/localize/{localize_id}/map', ["as" => "localize.map", "uses" => "LocalisationController@mapper"]);

                Route::post('{prestations_id}/seance', ["as" => "seance.storeDate", "uses" => "DateController@storeDate"]);
                Route::delete('{prestations_id}/seance/{seance_id}', ["as" => "seance.deleteDate", "uses" => "DateController@deleteDate"]);

                Route::post('{prestations_id}/tarif', ["as" => "tarif.storeTarif", "uses" => "TarifController@storeTarif"]);
                Route::delete('{prestations_id}/tarif/{tarif_id}', ["as" => "tarif.deleteTarif", "uses" => "TarifController@deleteTarif"]);

                Route::get('{prestations_id}/stock/{id}', ["as" => "stock.editStock", "uses" => "StockController@editStock"]);
                Route::put('{prestations_id}/stock/{id}', ["as" => "stock.updateStock", "uses" => "StockController@updateStock"]);


            });
            Route::group(["prefix" => "achat", "namespace" => "Prestation\Achat"], function (){
                Route::get('/', ["as" => "achats.index", "uses" => "AchatController@index"]);
                Route::post('/', ["as" => "achats.store", "uses" => "AchatController@store"]);
                Route::get('{achats_id}', ["as" => "achats.show", "uses" => "AchatController@show"]);
                Route::get('{achats_id}/edit', ["as" => "achats.edit", "uses" => "AchatController@edit"]);
                Route::put('{achats_id}/edit', ["as" => "achats.update", "uses" => "AchatController@update"]);
                Route::delete('{achats_id}', ["as" => "achats.delete", "uses" => "AchatController@delete"]);

                Route::post('{achats_id}/addPrestation', ["as" => "achats.addPrestation", "uses" => "AchatPrestationController@addPrestation"]);
                Route::delete('{achats_id}/{prestations_id}', ["as" => "achats.deletePrestation", "uses" => "AchatPrestationController@deletePrestation"]);

                Route::get('{achats_id}/check', ["as" => "achats.check", "uses" => "AchatController@check"]);

                Route::post('{achats_id}/addReglement', ["as" => "achats.addReglement", "uses" => "AchatReglementController@addReglement"]);
                Route::delete('{achats_id}/reglement/{reglements_id}', ["as" => "achats.delReglement", "uses" => "AchatReglementController@delReglement"]);

                Route::get('{achats_id}/showPdf', ["as" => "achats.showPdf", "uses" => "AchatController@showPdf"]);
                Route::get('{achats_id}/savePdf', ["as" => "achats.savePdf", "uses" => "AchatController@savePdf"]);

                Route::post('fournisseur', ["as" => "fournisseurs.store", "uses" => "FournisseurController@store"]);
                Route::get('fournisseur/{id}/edit', ["as" => "fournisseurs.edit", "uses" => "FournisseurController@edit"]);
                Route::put('fournisseur/{id}/edit', ["as" => "fournisseurs.update", "uses" => "FournisseurController@update"]);
                Route::delete('fournisseur/{id}', ["as" => "fournisseurs.delete", "uses" => "FournisseurController@delete"]);
            });
            Route::group(["prefix" => "billetterie", "namespace" => "Billetterie"], function (){
                Route::get('/', ["as" => "billetteries.index", "uses" => "BilletterieController@index"]);
                Route::group(["prefix" => "salarie", "namespace" => "Salarie"], function (){
                    Route::get('/create', ["as" => "billet.salarie.create", "uses" => "BilletController@create"]);
                    Route::post('/create', ["as" => "billet.salarie.store", "uses" => "BilletController@store"]);
                    Route::get('{id}', ["as" => "billet.salarie.show", "uses" => "BilletController@show"]);
                    Route::get('{id}/edit', ["as" => "billet.salarie.edit", "uses" => "BilletController@edit"]);
                    Route::put('{id}/edit', ["as" => "billet.salarie.update", "uses" => "BilletController@update"]);
                    Route::delete('{id}', ["as" => "billet.salarie.delete", "uses" => "BilletController@delete"]);
                    Route::get('{id}/check', ["as" => "billet.salarie.check", "uses" => "BilletController@check"]);
                    Route::get('{id}/print', ["as" => "billet.salarie.print", "uses" => "BilletController@print"]);
                    Route::get('{id}/pdf', ["as" => "billet.salarie.pdf", "uses" => "BilletController@pdf"]);

                    Route::post('{id}/addPrestation', ["as" => "billet.salarie.addPrestation", "uses" => "LigneBilletController@addPrestation"]);
                    Route::delete('{id}/prestation/{prestations_id}', ["as" => "billet.salarie.delPrestation", "uses" => "LigneBilletController@delPrestation"]);

                    Route::post('{id}/addReglement', ["as" => "billet.salarie.addReglement", "uses" => "BilletReglementController@addReglement"]);
                    Route::delete('{id}/{reglements_id}', ["as" => "billet.salarie.delReglement", "uses" => "BilletReglementController@delReglement"]);

                    Route::get('{id}/showPdf', ["as" => "billet.salarie.showPdf", "uses" => "BilletController@showPdf"]);
                    Route::get('{id}/savePdf', ["as" => "billet.salarie.savePdf", "uses" => "BilletController@savePdf"]);

                });
            });
            Route::group(["prefix" => "remboursement", "namespace" => "Remboursement"], function (){
                Route::get('/', ["as" => "remboursements.index", "uses" => "RemboursementController@index"]);

                Route::group(["prefix" => "salarie", "namespace" => "Salarie"], function (){
                    Route::get('create', ["as" => "remb.salarie.create", "uses" => "RembController@create"]);
                    Route::post('create', ["as" => "remb.salarie.store", "uses" => "RembController@store"]);
                    Route::get('{id}', ["as" => "remb.salarie.show", "uses" => "RembController@show"]);
                    Route::get('{id}/edit', ["as" => "remb.salarie.edit", "uses" => "RembController@edit"]);
                    Route::put('{id}/edit', ["as" => "remb.salarie.update", "uses" => "RembController@update"]);
                    Route::delete('{id}', ["as" => "remb.salarie.delete", "uses" => "RembController@delete"]);

                    Route::get('{id}/check', ["as" => "remb.salarie.check", "uses" => "RembController@check"]);

                    Route::post('{id}/addPrestation', ["as" => "remb.salarie.addPrestation", "uses" => "RembPrestaController@addPrestation"]);
                    Route::delete('{id}/delPrestation/{prestations_id}', ["as" => "remb.salarie.delPrestation", "uses" => "RembPrestaController@delPrestation"]);

                    Route::post('{id}/addReglement', ["as" => "remb.salarie.addReglement", "uses" => "RembRegController@addReglement"]);
                    Route::delete('{id}/delReglement/{reglements_id}', ["as" => "remb.salarie.delReglement", "uses" => "RembRegController@delReglement"]);

                    Route::get('{id}/showPdf', ["as" => "remb.salarie.showPdf", "uses" => "RembController@showPdf"]);
                    Route::get('{id}/savePdf', ["as" => "remb.salarie.savePdf", "uses" => "RembController@savePdf"]);

                });
            });
            Route::group(["prefix" => "configuration", "namespace" => "Configuration"], function (){
                Route::get('/', ["as" => "configasc.index", "uses" => "ConfigAscController@index"]);

                Route::get('/presta/validite/enable', ["as" => "configPresta.validite.enable", "uses" => "ConfigPresta@validiteEnable"]);
                Route::get('/presta/validite/disable', ["as" => "configPresta.validite.disable", "uses" => "ConfigPresta@validiteDisable"]);
                Route::get('/presta/quota/enable', ["as" => "configPresta.quota.enable", "uses" => "ConfigPresta@quotaEnable"]);
                Route::get('/presta/quota/disable', ["as" => "configPresta.quota.disable", "uses" => "ConfigPresta@quotaDisable"]);
                Route::get('/presta/stock/enable', ["as" => "configPresta.stock.enable", "uses" => "ConfigPresta@stockEnable"]);
                Route::get('/presta/stock/disable', ["as" => "configPresta.stock.disable", "uses" => "ConfigPresta@stockDisable"]);

                Route::get('/billet/state', 'ConfigPresta@stateImpayer');
                Route::put('/billet/dayOfImpayer', ["as" => "configPresta.billet.dayOfImpayer", "uses" => "ConfigPresta@dayOfImpayer"]);
                Route::get('/billet/impayer/enable', ["as" => "configPresta.billet.impayer.enable", "uses" => "ConfigPresta@impayerEnable"]);
                Route::get('/billet/impayer/disable', ["as" => "configPresta.billet.impayer.disable", "uses" => "ConfigPresta@impayerDisable"]);

                Route::get('/remb/percent/enable', ["as" => "configPresta.remb.percent.enable", "uses" => "ConfigPresta@percentEnable"]);
                Route::get('/remb/percent/disable', ["as" => "configPresta.remb.percent.disable", "uses" => "ConfigPresta@percentDisable"]);
                Route::put('/remb/percentSalarie', ["as" => "configPresta.remb.percentSalarie", "uses" => "ConfigPresta@percentSalarie"]);
                Route::put('/remb/percentAd', ["as" => "configPresta.remb.percentAd", "uses" => "ConfigPresta@percentAd"]);
            });
        });

        Route::group(["prefix" => "comptaAsc", "namespace" => "ComptaAsc"], function (){
            Route::get('/', ["as" => "compta_asc.dashboard", "uses" => "ComptaAscController@dashboard"]);

            Route::group(["prefix" => "remise", "namespace" => "Remise"], function (){
                Route::get('/', ["as" => "remise.index", "uses" => "RemiseController@index"]);
                Route::post('/', ["as" => "remise.store", "uses" => "RemiseController@store"]);
                Route::delete('{numRemise}', ["as" => "remise.delete", "uses" => "RemiseController@delete"]);

                Route::group(["prefix" => "cheque", "namespace" => "Cheque"], function (){
                    Route::get('{numRemise}', ["as" => "remise.cheque.show", "uses" => "ChequeController@show"]);
                    Route::get('{numRemise}/check', ["uses" => "ChequeController@check"]);
                    Route::post('{numRemise}/addCheque', ["as" => "remise.cheque.addCheque", "uses" => "ChequeController@addCheque"]);
                    Route::delete('{numRemise}/delCheque/{numTransaction}', ["as" => "remise.cheque.delCheque", "uses" => "ChequeController@delCheque"]);

                    Route::get('{numRemise}/print', ["as" => "remise.cheque.print", "uses" => "ChequeController@print"]);
                    Route::get('{numRemise}/pdf', ["as" => "remise.cheque.pdf", "uses" => "ChequeController@pdf"]);
                });

                Route::group(["prefix" => "espece", "namespace" => "Espece"], function (){
                    Route::get('{numRemise}', ["as" => "remise.espece.show", "uses" => "EspeceController@show"]);
                    Route::get('{numRemise}/check', ["uses" => "EspeceController@check"]);
                    Route::post('{numRemise}/addEspece', ["as" => "remise.espece.addEspece", "uses" => "EspeceController@addEspece"]);
                    Route::delete('{numRemise}/delEspece/{numTransaction}', ["as" => "remise.espece.delEspece", "uses" => "EspeceController@delEspece"]);

                    Route::get('{numRemise}/print', ["as" => "remise.espece.print", "uses" => "EspeceController@print"]);
                    Route::get('{numRemise}/pdf', ["as" => "remise.espece.pdf", "uses" => "EspeceController@pdf"]);
                });

                Route::group(["prefix" => "position", "namespace" => "Position"], function (){
                    Route::get('/', ["as" => "remise.position.index", "uses" => "PositionController@index"]);
                });
            });

            Route::group(["prefix" => "journal", "namespace" => "Journal"], function (){
                Route::group(["prefix" => "achat", "namespace" => "Achat"], function (){
                    Route::get('/', ["as" => "comptaAsc.achat.index", "uses" => "AchatController@index"]);
                    Route::post('/', ["as" => "comptaAsc.achat.store", "uses" => "AchatController@store"]);
                    Route::delete('{numAchat}', ["as" => "comptaAsc.achat.delete", "uses" => "AchatController@delete"]);
                });
                Route::group(["prefix" => "vente", "namespace" => "Vente"], function (){
                    Route::get('/', ["as" => "comptaAsc.vente.index", "uses" => "VenteController@index"]);
                    Route::post('/', ["as" => "comptaAsc.vente.store", "uses" => "VenteController@store"]);
                    Route::delete('{numVente}', ["as" => "comptaAsc.vente.delete", "uses" => "VenteController@delete"]);
                });
                Route::group(["prefix" => "banque", "namespace" => "Banque"], function (){
                    Route::get('/', ["as" => "comptaAsc.banque.index", "uses" => "BanqueController@index"]);
                    Route::post('/', ["as" => "comptaAsc.banque.store", "uses" => "BanqueController@store"]);
                    Route::delete('{numBanque}', ["as" => "comptaAsc.banque.delete", "uses" => "BanqueController@delete"]);
                });
                Route::group(["prefix" => "divers", "namespace" => "Divers"], function (){
                    Route::get('/', ["as" => "comptaAsc.divers.index", "uses" => "DiversController@index"]);
                });
                Route::group(["prefix" => "caisse", "namespace" => "Caisse"], function (){
                    Route::get('/', ["as" => "comptaAsc.caisse.index", "uses" => "CaisseController@index"]);
                    Route::post('/', ["as" => "comptaAsc.caisse.store", "uses" => "CaisseController@store"]);
                    Route::delete('{numCaisse}', ["as" => "comptaAsc.caisse.delete", "uses" => "CaisseController@delete"]);
                });
            });

            Route::group(["prefix" => "etat", "namespace" => "Etat"], function (){
                Route::get('bilan', ["as" => "comptaAsc.bilan", "uses" => "EtatController@bilan"]);
                Route::get('resultat', ["as" => "comptaAsc.resultat", "uses" => "EtatController@resultat"]);
                Route::get('balance', ["as" => "comptaAsc.balance", "uses" => "EtatController@balance"]);
            });

            Route::group(["prefix" => "configuration", "namespace" => "Configuration"], function (){
                Route::get('/', ["as" => "comptaAsc.config.index", "uses" => "ConfigurationController@index"]);
                Route::group(["prefix" => "initial", "namespace" => "Initial"], function (){
                    Route::get('/', ["as" => "comptaAsc.config.initial.index", "uses" => "InitialController@index"]);
                    Route::get('{numCompte}', ["as" => "comptaAsc.config.initial.edit", "uses" => "InitialController@edit"]);
                    Route::put('{numCompte}', ["as" => "comptaAsc.config.initial.update", "uses" => "InitialController@update"]);
                    Route::post('/validate', ["as" => "comptaAsc.config.initial.validation", "uses" => "InitialOtherController@validation"]);
                });
                Route::group(["prefix" => "plan", "namespace" => "Plan"], function (){
                    Route::get('/', ["as" => "comptaAsc.config.plan.index", "uses" => "PlanController@index"]);

                    Route::get('{classes_id}/sector', ["as" => "comptaAsc.config.plan.sector.index", "uses" => "SectorController@index"]);
                    Route::post('{classes_id}/sector', ["as" => "comptaAsc.config.plan.sector.store", "uses" => "SectorController@store"]);
                    Route::get('{classes_id}/sector/{sectors_id}', ["as" => "comptaAsc.config.plan.sector.edit", "uses" => "SectorController@edit"]);
                    Route::put('{classes_id}/sector/{sectors_id}', ["as" => "comptaAsc.config.plan.sector.update", "uses" => "SectorController@update"]);

                    Route::get('{classes_id}/sector/{sectors_id}/compte', ["as" => "comptaAsc.config.plan.compte.index", "uses" => "CompteController@index"]);
                    Route::post('{classes_id}/sector/{sectors_id}/compte', ["as" => "comptaAsc.config.plan.compte.store", "uses" => "CompteController@store"]);
                    Route::get('{classes_id}/sector/{sectors_id}/compte/{comptes_id}', ["as" => "comptaAsc.config.plan.compte.edit", "uses" => "CompteController@edit"]);
                    Route::put('{classes_id}/sector/{sectors_id}/compte/{comptes_id}', ["as" => "comptaAsc.config.plan.compte.update", "uses" => "CompteController@update"]);
                    Route::delete('{classes_id}/sector/{sectors_id}/compte/{comptes_id}', ["as" => "comptaAsc.config.plan.compte.delete", "uses" => "CompteController@delete"]);
                });
            });
        });

        Route::group(["prefix" => "comptaFct", "namespace" => "ComptaFct"], function (){
            Route::get('/', ["as" => "compta_fct.dashboard", "uses" => "ComptaFctController@dashboard"]);

            Route::group(["prefix" => "journal", "namespace" => "Journal"], function (){
                Route::group(["prefix" => "achat", "namespace" => "Achat"], function (){
                    Route::get('/', ["as" => "comptaFct.achat.index", "uses" => "AchatController@index"]);
                    Route::post('/', ["as" => "comptaFct.achat.store", "uses" => "AchatController@store"]);
                    Route::delete('{numAchat}', ["as" => "comptaFct.achat.delete", "uses" => "AchatController@delete"]);
                });
                Route::group(["prefix" => "vente", "namespace" => "Vente"], function (){
                    Route::get('/', ["as" => "comptaFct.vente.index", "uses" => "VenteController@index"]);
                    Route::post('/', ["as" => "comptaFct.vente.store", "uses" => "VenteController@store"]);
                    Route::delete('{numVente}', ["as" => "comptaFct.vente.delete", "uses" => "VenteController@delete"]);
                });
                Route::group(["prefix" => "banque", "namespace" => "Banque"], function (){
                    Route::get('/', ["as" => "comptaFct.banque.index", "uses" => "BanqueController@index"]);
                    Route::post('/', ["as" => "comptaFct.banque.store", "uses" => "BanqueController@store"]);
                    Route::delete('{numBanque}', ["as" => "comptaFct.banque.delete", "uses" => "BanqueController@delete"]);
                });
                Route::group(["prefix" => "divers", "namespace" => "Divers"], function (){
                    Route::get('/', ["as" => "comptaFct.divers.index", "uses" => "DiversController@index"]);
                });
                Route::group(["prefix" => "caisse", "namespace" => "Caisse"], function (){
                    Route::get('/', ["as" => "comptaFct.caisse.index", "uses" => "CaisseController@index"]);
                    Route::post('/', ["as" => "comptaFct.caisse.store", "uses" => "CaisseController@store"]);
                    Route::delete('{numCaisse}', ["as" => "comptaFct.caisse.delete", "uses" => "CaisseController@delete"]);
                });
            });

            Route::group(["prefix" => "etat", "namespace" => "Etat"], function (){
                Route::get('bilan', ["as" => "comptaFct.bilan", "uses" => "EtatController@bilan"]);
                Route::get('resultat', ["as" => "comptaFct.resultat", "uses" => "EtatController@resultat"]);
                Route::get('balance', ["as" => "comptaFct.balance", "uses" => "EtatController@balance"]);
            });

            Route::group(["prefix" => "configuration", "namespace" => "Configuration"], function (){
                Route::get('/', ["as" => "comptaFct.config.index", "uses" => "ConfigurationController@index"]);
                Route::group(["prefix" => "initial", "namespace" => "Initial"], function (){
                    Route::get('/', ["as" => "comptaFct.config.initial.index", "uses" => "InitialController@index"]);
                    Route::get('{numCompte}', ["as" => "comptaFct.config.initial.edit", "uses" => "InitialController@edit"]);
                    Route::put('{numCompte}', ["as" => "comptaFct.config.initial.update", "uses" => "InitialController@update"]);
                    Route::post('/validate', ["as" => "comptaFct.config.initial.validation", "uses" => "InitialOtherController@validation"]);
                });
                Route::group(["prefix" => "plan", "namespace" => "Plan"], function (){
                    Route::get('/', ["as" => "comptaFct.config.plan.index", "uses" => "PlanController@index"]);

                    Route::get('{classes_id}/sector', ["as" => "comptaFct.config.plan.sector.index", "uses" => "SectorController@index"]);
                    Route::post('{classes_id}/sector', ["as" => "comptaFct.config.plan.sector.store", "uses" => "SectorController@store"]);
                    Route::get('{classes_id}/sector/{sectors_id}', ["as" => "comptaFct.config.plan.sector.edit", "uses" => "SectorController@edit"]);
                    Route::put('{classes_id}/sector/{sectors_id}', ["as" => "comptaFct.config.plan.sector.update", "uses" => "SectorController@update"]);

                    Route::get('{classes_id}/sector/{sectors_id}/compte', ["as" => "comptaFct.config.plan.compte.index", "uses" => "CompteController@index"]);
                    Route::post('{classes_id}/sector/{sectors_id}/compte', ["as" => "comptaFct.config.plan.compte.store", "uses" => "CompteController@store"]);
                    Route::get('{classes_id}/sector/{sectors_id}/compte/{comptes_id}', ["as" => "comptaFct.config.plan.compte.edit", "uses" => "CompteController@edit"]);
                    Route::put('{classes_id}/sector/{sectors_id}/compte/{comptes_id}', ["as" => "comptaFct.config.plan.compte.update", "uses" => "CompteController@update"]);
                    Route::delete('{classes_id}/sector/{sectors_id}/compte/{comptes_id}', ["as" => "comptaFct.config.plan.compte.delete", "uses" => "CompteController@delete"]);
                });
            });
        });

        Route::group(["prefix" => "ancv", "namespace" => "Ancv"], function (){
            Route::get('/', ["as" => "ancv.dashboard", "uses" => "AncvController@dashboard"]);

            Route::group(["prefix" => "vente", "namespace" => "Vente"], function () {
                Route::get('/', ["as" => "Ancv.Vente.index", "uses" => "AncvVenteController@index"]);
                Route::get('create', ["as" => "Ancv.Vente.create", "uses" => "AncvVenteController@create"]);
                Route::post('create', ["as" => "Ancv.Vente.store", "uses" => "AncvVenteController@store"]);
            });

            Route::group(["prefix" => "achat", "namespace" => "Achat"], function () {
                Route::get('/', ["as" => "Ancv.Achat.index", "uses" => "AncvAchatController@index"]);
                Route::get('create', ["as" => "Ancv.Achat.create", "uses" => "AncvAchatController@create"]);
                Route::post('create', ["as" => "Ancv.Achat.store", "uses" => "AncvAchatController@store"]);
            });
        });

        Route::group(["prefix" => "banker", "namespace" => "Banker"], function (){
            Route::get('/', ["as" => "banker.dashboard", "uses" => "BankerController@dashboard"]);
            Route::group(["prefix" => "users", "namespace" => "Users"], function (){
                Route::get('/', ["as" => "banker.users.index", "uses" => "BankerUserController@index"]);
                Route::post('/', ["as" => "banker.users.store", "uses" => "BankerUserController@store"]);

                Route::get('{uuid}/delete/step', ["as" => "banker.users.step", "uses" => "BankerUserController@deleteStep"]);
                Route::delete('{uuid}/delete/step', ["as" => "banker.users.delete", "uses" => "BankerUserController@delete"]);

                Route::get('connexion', ["as" => "banker.users.login", "uses" => "BankerUserController@loginUser"]);
                Route::post('connexion', ["as" => "banker.users.connexion", "uses" => "BankerUserController@connexion"]);
            });
            Route::group(["prefix" => "banks", "namespace" => "Banks"], function (){
                Route::get('/', ["as" => "banker.banks.index", "uses" => "BankerBankController@index"]);
            });
            Route::group(["prefix" => "accounts", "namespace" => "Accounts"], function (){
                Route::get('/', ["as" => "banker.accounts.index", "uses" => "BankerAccountController@index"]);
                Route::get('/connect', ["as" => "banker.accounts.connect", "uses" => "BankerAccountController@connect"]);
                Route::get('/show/{account_id}', ["as" => "banker.accounts.show", "uses" => "BankerAccountController@show"]);
            });
            Route::group(["prefix" => "items", "namespace" => "Items"], function (){
                Route::get('/', ["as" => "banker.items.index", "uses" => "BankerItemsController@index"]);
            });
        });

        Route::group(["prefix" => "communication", "namespace" => "Communication"], function (){
            Route::get('/', ["as" => "communication.dashboard", "uses" => "CommunicationController@dashboard"]);
        });

        Route::group(["prefix" => "loca", "namespace" => "Loca"], function (){
            Route::get('/', ["as" => "loca.dashboard", "uses" => "LocaController@dashboard"]);
        });

        Route::group(["prefix" => "pretce", "namespace" => "PretCe"], function (){
            Route::get('/', ["as" => "pretce.dashboard", "uses" => "PretCeController@dashboard"]);
        });

        Route::group(["prefix" => "restogest", "namespace" => "Restogest"], function (){
            Route::get('/', ["as" => "restogest.dashboard", "uses" => "RestogestController@dashboard"]);
        });

        Route::group(["prefix" => "showce", "namespace" => "ShowCe"], function (){
            Route::get('/', ["as" => "showce.dashboard", "uses" => "ShowCeController@dashboard"]);
        });

        Route::group(["prefix" => "fidelity", "namespace" => "Fidelity"], function (){
            Route::get('/', ["as" => "fidelity.dashboard", "uses" => "FidelityController@dashboard"]);
            Route::post('/', ["as" => "fidelity.store", "uses" => "FidelityController@store"]);
            Route::delete('{compteurs_id}', ["as" => "fidelity.delete", "uses" => "FidelityController@delete"]);

            Route::get('{compteurs_id}/bareme', ["as" => "fidelity.bareme.index", "uses" => "BaremeController@index"]);
            Route::post('{compteurs_id}/bareme', ["as" => "fidelity.bareme.store", "uses" => "BaremeController@store"]);
            Route::delete('{compteurs_id}/bareme/{baremes_id}', ["as" => "fidelity.bareme.delete", "uses" => "BaremeController@delete"]);

        });
        Route::group(["prefix" => "facturation", "namespace" => "Facturation"], function (){
            Route::get('/', ["as" => "facturation.dashboard", "uses" => "FacturationController@dashboard"]);

            Route::get('widget', "FacturationController@widget");
            Route::get('graph', "FacturationController@graph");

            Route::group(["prefix" => "tiers", "namespace" => "Tiers"], function (){
                Route::get('/', ["as" => "facturation.tiers", "uses" => "TiersController@index"]);
                Route::post('/', ["as" => "facturation.tiers.store", "uses" => "TiersController@store"]);
                Route::get('{tiers_id}', ["as" => "facturation.tiers.show", "uses" => "TiersController@show"]);
                Route::get('{tiers_id}/edit', ["as" => "facturation.tiers.edit", "uses" => "TiersController@edit"]);
                Route::put('{tiers_id}/edit', ["as" => "facturation.tiers.update", "uses" => "TiersController@update"]);
                Route::delete('{tiers_id}', ["as" => "facturation.tiers.delete", "uses" => "TiersController@delete"]);

                Route::get('{tiers_id}/soldePrint', ["as" => "facturation.tiers.soldePrint", "uses" => "TiersSoldeController@soldePrint"]);
            });

            Route::group(["prefix" => "facture", "namespace" => "Facture"], function (){
                Route::get('/', ["as" => "facturation.facture", "uses" => "FactureController@index"]);
                Route::post('/', ["as" => "facturation.facture.store", "uses" => "FactureController@store"]);
                Route::get('{factures_id}', ["as" => "facturation.facture.show", ["as" => "FactureController@show"]]);
                Route::get('{factures_id}/edit', ["as" => "facturation.facture.edit", ["as" => "FactureController@edit"]]);
                Route::put('{factures_id}/edit', ["as" => "facturation.facture.update", ["as" => "FactureController@update"]]);
                Route::delete('{factures_id}', ["as" => "facturation.facture.delete", ["as" => "FactureController@delete"]]);

                Route::get('{factures_id}/print', ["as" => "facturation.facture.print", "uses" => "FactureOtherController@print"]);
            });

            Route::group(["prefix" => "mode", "namespace" => "Mode"], function (){
                Route::get('/', ["as" => "facturation.modeReglement", "uses" => "ModeController@index"]);
                Route::post('/', ["as" => "facturation.modeReglement.store", "uses" => "ModeController@store"]);
                Route::get('{modes_id}/edit', ["as" => "facturation.modeReglement.edit", "uses" => "ModeController@edit"]);
                Route::put('{modes_id}/edit', ["as" => "facturation.modeReglement.update", "uses" => "ModeController@update"]);
                Route::delete('{modes_id}', ["as" => "facturation.modeReglement.delete", "uses" => "ModeController@delete"]);
            });
        });
    });

    Route::get('decompose','\Lubusin\Decomposer\Controllers\DecomposerController@index');

Auth::routes();

Route::get('test', 'testController@test');
Route::get('logout', ["as" => "logout", "uses" => 'Auth\LoginController@logout']);
Route::get('weather', 'WeatherController@get');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Auth::routes();
