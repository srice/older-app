(function ($) {
    $("#checkRemise").on('click', function (e) {
        e.preventDefault();
        var btn = $(this);
        var numRemise = btn.attr('data-num');
        swal({
            title: "Etes-vous sur de valider la remise "+numRemise+" ?",
            text: "En validant la remise, aucune modification ou suppression ne pourra être effectuer.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#44dd32",
            confirmButtonText: "Oui, Valider la remise",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            $.get(numRemise+'/check')
                .done(function (data) {
                    swal("Remise valider !", "La remise "+numRemise+" à été valider avec succès !", "success");
                    window.setTimeout(function () {
                        window.refresh();
                    }, 1200)
                })
                .fail(function (data) {
                    swal("Erreur", "La remise "+numRemise+" n'à pas pus être valider !", "error")
                })
        })
    })
})(jQuery);