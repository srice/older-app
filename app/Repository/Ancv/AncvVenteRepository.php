<?php
namespace App\Repository\Ancv;

use App\Model\Ancv\AncvVente;

class AncvVenteRepository
{
    /**
     * @var AncvVente
     */
    private $ancvVente;

    /**
     * AncvVenteRepository constructor.
     * @param AncvVente $ancvVente
     */

    public function __construct(AncvVente $ancvVente)
    {
        $this->ancvVente = $ancvVente;
    }

    public function list()
    {
        return $this->ancvVente->newQuery()
            ->get()
            ->load('salarie');
    }

    public function create($salarie_id, $getSolutionArray, $calc, $part_ce, $part_salarie)
    {
        return $this->ancvVente->newQuery()
            ->create([
                "salarie_id"    => $salarie_id,
                "solution_id"   => $getSolutionArray,
                "total_ancv"    => $calc,
                "part_ce"       => $part_ce,
                "part_salarie"  => $part_salarie
            ]);
    }

}

        