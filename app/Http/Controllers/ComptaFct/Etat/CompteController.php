<?php

namespace App\Http\Controllers\ComptaFct\Etat;

use App\Model\ComptaFct\Configuration\Plan\FctPlanCompte;
use App\Model\ComptaFct\Etat\FctCompte;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompteController extends Controller
{
    public static function soldeDebit($numCompte){
        $compte = FctCompte::where('numCompte', $numCompte)->sum('debit');
        return $compte;
    }

    public static function soldeCredit($numCompte){
        $compte = FctCompte::where('numCompte', $numCompte)->sum('credit');
        return $compte;
    }

    public static function getNameCompte($numCompte){
        $compte = FctPlanCompte::where('numCompte', $numCompte)->first();
        return $compte->nameCompte;
    }

    public static function getTotalDebit(){
        $compte = FctCompte::all()->sum('debit');
        return $compte;
    }

    public static function getTotalCredit(){
        $compte = FctCompte::all()->sum('credit');
        return $compte;
    }
}
