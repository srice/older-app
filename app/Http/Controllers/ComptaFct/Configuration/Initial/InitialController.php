<?php

namespace App\Http\Controllers\ComptaFct\Configuration\Initial;

use App\Model\ComptaFct\Configuration\Bilan\FctBilanInitial;
use App\Model\ComptaFct\Etat\FctCompte;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class InitialController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptafct';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $comptes = FctBilanInitial::all()->load('comptes');

        return view('ComptaFct.Configuration.Initial.index', compact('config', 'comptes'));
    }

    public function edit($numCompte){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $compte = FctBilanInitial::where('numCompte', $numCompte)->first();

        return view('ComptaFct.Configuration.Initial.edit', compact('config', 'compte'));
    }

    public function update(Request $request, $numCompte){
        $compte = FctBilanInitial::where('numCompte', $numCompte)->first()->update([
            "debit" => $request->debit,
            "credit"=> $request->credit
        ]);

        if($compte){
            Toastr::success("Le compte à été mis à jour !");
            return redirect()->route('comptaFct.config.initial.index');
        }else{
            Toastr::error("Erreur lors de la mise à jour du compte !");
            return redirect()->back();
        }
    }
}
