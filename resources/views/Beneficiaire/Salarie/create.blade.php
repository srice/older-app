@extends('template')
@section('title')
    Création d'un salarié
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title"><i class="md-accounts-add"></i> @yield('title')</h3>
        </header>
        <div class="panel-body">
            {{ Form::open(["route" => "salaries.store", "class" => "form-horizontal"]) }}
            <div class="form-group row">
                {{ Form::label('matricule', 'Matricule', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-9">
                    {{ Form::text('matricule', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('nom', 'Nom *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('nom', null, ["class" => "form-control round"]) }}
                </div>
                {{ Form::label('prenom', 'Prenom *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('prenom', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('adresse', 'Adresse', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-9">
                    {{ Form::text('adresse', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('codePostal', 'Code Postal', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-2">
                    {{ Form::text('codePostal', null, ["class" => "form-control round", "id" => "codePostal", "onBlur" => "getVille()"]) }}
                </div>
                {{ Form::label('ville', 'Ville', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-4">
                    <select id="ville" name="ville" class="form-control round">
                        <option disabled selected>Selectionner une ville</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('tel', 'N° de Téléphone', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('tel', null, ["class" => "form-control round"]) }}
                </div>
                {{ Form::label('port', 'N° de Portable', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('port', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('email', 'Adresse Email', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-9">
                    {{ Form::text('email', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            @if($config->moduleMenu[12]->state == 1)
            <div class="form-group row">
                {{ Form::label('fidelityPoint', "Nombre de Point de fidélité de départ", ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('fidelityPoint', \App\Http\Controllers\Beneficiaire\SalarieOtherController::AverageByFidelity(), ["class" => "form-control round"]) }}
                </div>
            </div>
            @endif
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/custom/js/beneficiaire/salarie/create.js"></script>
@stop