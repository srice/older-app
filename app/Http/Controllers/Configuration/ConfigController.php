<?php

namespace App\Http\Controllers\Configuration;

use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Espace;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\BouncerFacade;

class ConfigController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;
    public function __construct(Espace $espace, Guard $auth)
    {
        $this->auth = $auth;
        $this->sector = 'configuration';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function dashboard(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        return view('Configuration.dashboard', compact('config'));

    }
}
