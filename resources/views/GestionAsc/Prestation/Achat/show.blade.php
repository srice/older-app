@extends('template')
@section("title")
    Facture N°<strong>{{ $achat->numAchat }}</strong>
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/template/examples/css/forms/advanced.css">
@stop
@section("content")
    <div class="panel">
        <div class="panel-body">
            <a href="{{ route('achats.showPdf', $achat) }}" class="btn btn-sm btn-icon btn-round btn-default"><i class="fa fa-print"></i> Imprimer</a>
            <a href="{{ route('achats.savePdf', $achat) }}" class="btn btn-sm btn-icon btn-round btn-default"><i class="fa fa-file-pdf-o"></i> Exporter en PDF</a>
            @if($achat->etatAchat == 0)
            <a href="{{ route('achats.check', $achat->id) }}" class="btn btn-sm btn-icon btn-round btn-success white"><i class="fa fa-check"></i> Valider la facture</a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel bg-info">
                <header class="panel-heading">
                    <h2 class="text-xs-center white p-t-10">Date de la facture</h2>
                </header>
                <div class="panel-body">
                    <h1 class="text-xs-center white p-t-20" style="font-weight: bold;">{{ $achat->dateAchat->format('d/m/Y') }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel bg-info">
                <header class="panel-heading">
                    <h2 class="text-xs-center white p-t-10">Total de la facture</h2>
                </header>
                <div class="panel-body">
                    <h1 class="text-xs-center white p-t-20" style="font-weight: bold;">{{ \App\Http\Controllers\OtherController::euro($achat->totalAchat) }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel {{ \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatOtherController::etatAchatBackground($achat->etatAchat) }}">
                <header class="panel-heading">
                    <h2 class="text-xs-center white p-t-10">Etat de la facture</h2>
                </header>
                <div class="panel-body">
                    <h1 class="text-xs-center white p-t-20" style="font-weight: bold;"><i class="fa {{ \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatOtherController::etatAchatIcon($achat->etatAchat) }}"></i> {{ \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatOtherController::etatAchatText($achat->etatAchat) }} </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <header class="panel-header">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste des prestations</h3>
                </div>
                <div class="col-md-2">
                    <a href="#addPrestation" class="btn btn-block btn-lg btn-primary m-t-5" data-toggle="modal"><i class="fa fa-plus-circle"></i> Ajouter une Prestation</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="listePrestation" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Prestation</th>
                            <th>Prix Unitaire</th>
                            <th>Quantité</th>
                            <th>Total</th>
                            @if($achat->etatAchat == 0)
                            <th></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($achat->prestations as $prestation)
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-1">
                                        <img src="/assets/custom/images/prestation/{{ \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatPrestationController::getNameFamilleSlug($prestation->prestations_id) }}/{{ $prestation->prestations_id }}.png" class="img-responsive" width="80" alt="">
                                    </div>
                                    <div class="col-md-10 p-l-30">
                                        {{ \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatPrestationController::getName($prestation->prestations_id) }}<br>
                                        <strong>Tarif:</strong> <i>{{ \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatPrestationController::getNameTarif($prestation->tarifs_id) }}</i>
                                    </div>
                                </div>
                            </td>
                            <td class="text-xs-right">{{ \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatPrestationController::getPrice($prestation->tarifs_id) }}</td>
                            <td class="text-xs-center">{{ $prestation->qte }}</td>
                            <td class="text-xs-right">{{ \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatPrestationController::getTotalLigne($prestation->tarifs_id, $prestation->qte, true) }}</td>
                            @if($achat->etatAchat == 0)
                            <td>
                                {{ Form::model($prestation, ["route" => ["achats.deletePrestation", $achat->id, $prestation->id], "method" => "DELETE"]) }}
                                <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                {{ Form::close() }}
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">Liste des règlements</h3>
                </div>
                <div class="col-md-2">
                    @if(\App\Http\Controllers\GestionAsc\Prestation\Achat\AchatReglementController::verifSoldeFacture($achat->id) == false && $achat->etatAchat >= 1 && $achat->etatAchat < 3)
                    <a href="#addReglement" class="btn btn-block btn-lg btn-primary m-t-5" data-toggle="modal"><i class="fa fa-plus-circle"></i> Ajouter un règlement</a>
                    @endif
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="listeReglement" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Numéro de Transaction</th>
                            <th>Date du Règlement</th>
                            <th>Mode de Règlement</th>
                            <th>Total du Règlement</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($achat->reglements as $reglement)
                            <tr>
                                <td>{{ $reglement->numReglement }}</td>
                                <td>{{ $reglement->dateReglement->format('d/m/Y') }}</td>
                                <td>{{ \App\Http\Controllers\GestionAsc\Prestation\Achat\AchatReglementController::modeReglement($reglement->modeReglement) }}</td>
                                <td>{{ \App\Http\Controllers\OtherController::euro($reglement->totalReglement) }}</td>
                                <td>
                                    {{ Form::model($reglement, ["route" => ["achats.delReglement", $achat->id, $reglement->id], "method" => "DELETE"]) }}
                                    <button class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash"></i> </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addPrestation" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'une Prestation</h4>
                </div>
                {{ Form::model($achat, ["route" => ["achats.addPrestation", $achat->id]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('tarifs_id', "Prestation *", ["class" => "control-label"]) }}
                        <select name="tarifs_id" id="" class="form-control" data-plugin="select2" required>
                            <option value=""></option>
                            @foreach($familles as $famille)
                            <optgroup label="{{ $famille->name }}">
                                <?php
                                    $prestations = \App\Model\GestionAsc\Prestation\Prestation::where('familles_id', $famille->id)->get();
                                    foreach ($prestations as $prestation):
                                        $tarifs = \App\Model\GestionAsc\Prestation\PrestationTarif::where('prestations_id', $prestation->id)->get();
                                        foreach ($tarifs as $tarif):
                                ?>
                                    <option value="{{ $tarif->id }}">{{ $prestation->name }} - {{ $tarif->libelle }}</option>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('qte', "Quantité *", ["class" => "control-label"]) }}
                        {{ Form::text('qte', null, ["class" => "form-control", "required"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="addReglement" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un Règlement</h4>
                </div>
                {{ Form::model($achat, ["route" => ["achats.addReglement", $achat->id]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('modeReglement', "Mode de règlement", ["class" => "control-label"]) }}
                        <select class="form-control" name="modeReglement" id="modeReglements" onchange="showNum()">
                            @foreach($modeReglements as $modeReglement)
                                <option value="{{ $modeReglement['id'] }}">{{ $modeReglement['name'] }}</option>
                            @endforeach    
                        </select>
                    </div>
                    <div id="numTransaction">
                        <div class="form-group form-material">
                            {{ Form::label('numReglement', "Numéro du Chèque", ["class" => "control-label"]) }}
                            {{ Form::text("numReglement", null, ["class" => "form-control"]) }}
                        </div>
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('dateReglement', "Date du règlement", ["class" => "control-label"]) }}
                        {{ Form::text('dateReglement', $achat->dateAchat->format('d-m-Y'), ["class" => "form-control date"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('totalReglement', "Montant du règlement", ["class" => "control-label"]) }}
                        {{ Form::text('totalReglement', $achat->totalAchat, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/select2/select2.full.min.js"></script>
    <script src="/assets/global/js/Plugin/select2.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/custom/js/gestionasc/prestation/achat/show.js"></script>
@stop    