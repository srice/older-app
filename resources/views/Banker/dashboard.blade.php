@extends('template')
@section("title")
    Agrégateur Bancaire
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <?php if(request('success') == 'false'): ?>
    {{ \Kamaln7\Toastr\Facades\Toastr::error("Vous avez annuler la connection à votre banque", "Annulation de la connection") }}
    <?php endif ?>
    <?php if(request('success') == 'true'): ?>
    {{ \Kamaln7\Toastr\Facades\Toastr::success("Votre compte bancaire à été ajouter", "Ajout d'un compte bancaire") }}
    <?php endif ?>
@stop
@section("footer_scripts")

@stop    