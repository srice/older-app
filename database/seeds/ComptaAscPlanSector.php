<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComptaAscPlanSector extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 10,
            "numClasse"     => 1,
            "nameSector"    => "CAPITAL ET RESERVES"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 11,
            "numClasse"     => 1,
            "nameSector"    => "REPORT A NOUVEAUX"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 12,
            "numClasse"     => 1,
            "nameSector"    => "RESULTAT DE L'EXERCICE"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 16,
            "numClasse"     => 1,
            "nameSector"    => "EMPRUNTS ET DETTES ASSIMILEES"
        ]);

        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 20,
            "numClasse"     => 2,
            "nameSector"    => "IMMOBILISATION INCORPORELLES"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 21,
            "numClasse"     => 2,
            "nameSector"    => "IMMOBILISATION CORPORELLES"
        ]);

        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 34,
            "numClasse"     => 3,
            "nameSector"    => "EN-COURS DE PRODUCTION DE SERVICE"
        ]);

        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 40,
            "numClasse"     => 4,
            "nameSector"    => "FOURNISSEURS"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 41,
            "numClasse"     => 4,
            "nameSector"    => "CLIENTS"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 47,
            "numClasse"     => 4,
            "nameSector"    => "COMPTE D'ATTENTE"
        ]);

        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 51,
            "numClasse"     => 5,
            "nameSector"    => "BANQUE ET ETABLISSEMENT FINANCIERS"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 53,
            "numClasse"     => 5,
            "nameSector"    => "CAISSES"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 58,
            "numClasse"     => 5,
            "nameSector"    => "VIREMENT INTERNE"
        ]);

        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 60,
            "numClasse"     => 6,
            "nameSector"    => "ACHATS"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 61,
            "numClasse"     => 6,
            "nameSector"    => "SERVICES EXTERIEURS"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 62,
            "numClasse"     => 6,
            "nameSector"    => "AUTRE SERVICES EXTERIEURS"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 63,
            "numClasse"     => 6,
            "nameSector"    => "IMPOTS"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 65,
            "numClasse"     => 6,
            "nameSector"    => "AUTRES CHARGES DE GESTION COURANTE"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 66,
            "numClasse"     => 6,
            "nameSector"    => "CHARGES FINANCIERES"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 67,
            "numClasse"     => 6,
            "nameSector"    => "CHARGES EXCEPTIONNELLES"
        ]);

        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 70,
            "numClasse"     => 7,
            "nameSector"    => "VENTE DE PRODUITS FINIS, PRESTATIONS DE SERVICES ET MARCHANDISES"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 74,
            "numClasse"     => 7,
            "nameSector"    => "SUBVENTION D'EXPLOITATION"
        ]);
        DB::table('compta_plan_sectors')->insert([
            "numSector"     => 77,
            "numClasse"     => 7,
            "nameSector"    => "PRODUITS EXCEPTIONNELLES"
        ]);
    }
}
