<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 27/06/2017
 * Time: 00:41
 */

namespace App\Packages\Srice;


use Carbon\Carbon;
use Kamaln7\Toastr\Facades\Toastr;

class Commande extends Api
{
    public function commandes(){
        $commandes = $this->db->table('commandes')->where('comites_id', $this->auth['api_comite_id'])->get();
        return $commandes;
    }

    public function commande($commandes_id){
        $commande = $this->db->table('commandes')->find($commandes_id);
        return $commande;
    }

    public function lignes($commandes_id){
        $lignes = $this->db->table('commande_lignes')->where('commandes_id', $commandes_id)->get();
        return $lignes;
    }

    public function reglements($commandes_id){
        $rglt = $this->db->table('commande_reglements')->where('commandes_id', $commandes_id)->get();
        return $rglt;
    }

    protected function lastCommande(){
        $cmd = $this->db->table('commandes')->get()->last();
        return $cmd;
    }

    public function postCommande($comites_id, $modules_id){
        $srv = new Service();
        $service = $srv->service(CommandeOther::getServicesId($modules_id));
        $unitPrice = $service->priceService;
        $totalLigne = $unitPrice;


        $commande = $this->lastCommande();
        if($commande == null){
            $numCommande = 1;
        }else{
            $numCommande = $commande->id+1;
        }
        $dateCommande = Carbon::now();
        $newCommande = $this->db->table('commandes')->insert([
            "numCommande"   => CommandeOther::genNumCommande($numCommande),
            "comites_id"    => $comites_id,
            "dateCommande"  => $dateCommande
        ]);
        $commande = $this->lastCommande();
        $lastTotal = $commande->totalCommande;
        $newTotal = $lastTotal+$totalLigne;
        $up = $this->db->table('commandes')->where('id', $commande->id)->update(["totalCommande" => $newTotal]);

        $ligne = $this->db->table('commande_lignes')->insert([
            "commandes_id"  => $commande->id,
            "services_id"   => CommandeOther::getServicesId($modules_id),
            "descLigne"     => "Achat effectuer par l'espace du client",
            "qte"           => 1,
            "totalLigne"    => $totalLigne
        ]);
        return $newCommande;
    }

}
class CommandeOther extends Commande{

    public static function genNumCommande($id){
        return "CMD".date('Ymd').$id;
    }

    public static function getServicesId($modules_id){
        switch ($modules_id){
            case 6: return 9;
            case 7: return 7;
            case 8: return 5;
            case 9: return 10;
            case 10: return 8;
            case 11: return 6;
            case 12: return 11;
            default: return false;
        }
    }

}