<?php

namespace App\Http\Controllers\Beneficiaire;

use App\Model\Beneficiaire\Ad\AyantDroit;
use App\Model\Beneficiaire\Salarie\Salarie;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Espace;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BeneficiaireController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;

    /**
     * BeneficiaireController constructor.
     * @param Espace $espace
     * @param Guard $auth
     */
    public function __construct(Espace $espace, Guard $auth)
    {
        $this->auth = $auth;
        $this->sector = 'beneficiaire';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function dashboard(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];


        return view('Beneficiaire.dashboard', compact('config'));
    }

    public function countSalarie(){
        return Salarie::all()->count();
    }

    public function countAd(){
        return AyantDroit::all()->count();
    }
}
