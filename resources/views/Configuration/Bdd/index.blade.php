@extends('template')
@section('title')
    Gestion de la base de donnée
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/footable/footable.core.css">
@stop

@section("content")
    <div class="alert alert-warning">
        <strong>Attention</strong><br>
        Le système de restauration n'est pas encore disponible.<br>
        Pour restaurer une sauvegarde, veuillez contacter le support technique .
    </div>
    <div class="row">
        <div class="col-md-3">
            <div id="lastSave"></div>
        </div>
        <div class="col-md-3">
            <div id="stateBdd"></div>
        </div>
        <div class="col-md-3">
            <div id="stateCluster"></div>
        </div>
        <div class="col-md-3">
            <div id="stateInerance"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="panel">
                <header class="panel-header">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-md-9"><i class="fa fa-database"></i> Liste des Sauvegardes</div>
                            <div class="col-md-3">

                            </div>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="listeSave">
                            <thead>
                                <tr>
                                    <th>Date de la sauvegarde</th>
                                    <th>Nom</th>
                                    <th>Type</th>
                                    <th>Taille</th>
                                    <th class="text-xs-center"><i class="fa fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($saves as $save)
                                <tr>
                                    <td>{{ $save->created_at }}</td>
                                    <td>{{ $save->nameSave }}</td>
                                    <td>{{ \App\Http\Controllers\Configuration\Database\DatabaseOtherController::typeSaveLabel($save->typeSave) }}</td>
                                    <td>{{ $save->taille }}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <a href="{{ route('config.bdd.save') }}" class="btn btn-lg btn-icon btn-primary white p-50 m-r-5 m-t-5" data-toggle="tooltip" data-placement="bottom" data-title="Sauvegarder la base de donnée"><br><i class="fa fa-save" style="font-size: 90px;"></i></a>
            <a href="#inerance" id="ineranceDatabase" class="btn btn-lg btn-icon btn-primary white p-50 m-r-5 m-t-5" data-toggle="tooltip modal" data-placement="bottom" data-title="Vérifier l'inérance de la base de donnée"><br><i class="fa fa-exchange" style="font-size: 90px;"></i></a>
            <a href="#purge" id="purgeDatabase" class="btn btn-lg btn-icon btn-danger white p-50 m-r-5 m-t-5" data-toggle="tooltip modal" data-placement="bottom" data-title="Purger la base de donnée"><br><i class="fa fa-trash" style="font-size: 90px;"></i></a>
        </div>
    </div>
    <div class="modal fade" id="purge" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-trash"></i> Purge de la base de donné</h4>
                </div>
                <div class="modal-body">
                    <a href="{{ route('config.bdd.purge.gestion') }}" class="btn btn-lg btn-block btn-info"><i class="fa fa-cubes"></i> Purge de la base de <strong>Gestion des ASC</strong></a>
                    <a href="{{ route('config.bdd.purge.comptaasc') }}" class="btn btn-lg btn-block btn-info"><i class="fa fa-line-chart"></i> Purge de la base de <strong>Comptabilité des ASC</strong></a>
                    <a href="{{ route('config.bdd.purge.comptafct') }}" class="btn btn-lg btn-block btn-info"><i class="fa fa-euro"></i> Purge de la base de <strong>Comptabilité de fonctionnement</strong></a>
                    @if($config->moduleMenu[12]->state == 1)
                        <a href="{{ route('config.bdd.purge.fidelity') }}" class="btn btn-lg btn-block btn-info"><i class="fa fa-shopping-basket"></i> Purge de la base de <strong>Fidélité</strong></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="inerance" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content" id="modalIneranceInfo">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-question"></i> Inérance de la base de donnée</h4>
                </div>
                <div class="modal-body">
                    <span class="text-center h2">La base de donnée est saine</span>
                </div>
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/footable/footable.all.min.js"></script>
    <script src="/assets/custom/js/configuration/bdd/index.js"></script>
@stop