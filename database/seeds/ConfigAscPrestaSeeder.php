<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigAscPrestaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config_asc_prestas')->insert([
            "validite"  => 0,
            "quota"     => 0,
            "stock"     => 0
        ]);
    }
}
