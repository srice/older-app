<?php

namespace App\Model\GestionAsc\Prestation;

use Illuminate\Database\Eloquent\Model;

class PrestationDate extends Model
{
    protected $guarded = [];

    public function prestation(){
        return $this->belongsTo(Prestation::class, 'prestations_id');
    }



    public function getDates()
    {
        return ["created_at", "updated_at", "dateStart", "dateEnd"];
    }
}
