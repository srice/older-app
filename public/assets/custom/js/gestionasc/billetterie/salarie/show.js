(function ($) {
    $(".date").datepicker({
        language: 'fr',
        format: "dd-mm-yyyy",
        todayHighlight: true
    });
})(jQuery);

function showNum(){
    var modeReglement = $("#modeReglements option:selected").val();
    if(modeReglement == 1){
        $("#numTransaction").show();
    }else{
        $("#numTransaction").hide();
    }
}