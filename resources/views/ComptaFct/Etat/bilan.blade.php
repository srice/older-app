<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $name }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="/assets/custom/css/pdf.css">
    <style type="text/css">
        body{
            font-family: Arial, sans-serif;
            font-size: 10px;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
<table style="width: 100%;">
    <tr>
        <td style="width: 33%" class="comite">
            <img src="/assets/custom/images/logos/1.jpg" width="100" alt="...">
            <br>
            Comite de Test<br>
            No Adresse<br>
            010101 No Ville
        </td>
        <td style="width: 33%;">&nbsp;</td>
        <td style="width: 33%; opacity: 0.5; font-weight: 400; font-size: 24px;">
            BILAN DE FONCTIONNEMENT
        </td>
    </tr>
</table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<h2 style="text-align: center;">{{ $name }}</h2>
<table style="width: 100%;" cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th style="width: 75%; text-align: center; background-color: #0d47a1; padding: 15px; color: white">ACTIF</th>
        <th style="width: 25%; text-align: center; background-color: #0d47a1; padding: 15px; color: white">N</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding: 15px; font-weight: bold;">IMMOBILISATION</td>
        <td style="width: 25%; border-left: solid 1px; border-right: solid 1px; padding: 15px;"></td>
    </tr>
    @foreach(\App\Http\Controllers\ComptaFct\Etat\BilanController::arrayCompteImmos() as $immo)
        <tr>
            <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding-top: 5px; padding-left: 15px; padding-bottom: 5px;">{{ $immo->numCompte }} - {{ $immo->nameCompte }}</td>
            <td style="width: 25%; border-right: solid 1px; text-align: right; padding-right: 5px; padding-bottom: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalN($immo->numCompte)) }}</td>
        </tr>
    @endforeach
    <tr>
        <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding: 15px; font-weight: bold;">STOCKS</td>
        <td style="width: 25%; border-left: solid 1px; border-right: solid 1px; padding: 15px;"></td>
    </tr>
    @foreach(\App\Http\Controllers\ComptaFct\Etat\BilanController::arrayCompteStocks() as $stock)
        <tr>
            <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding-top: 5px; padding-left: 15px; padding-bottom: 5px;">{{ $stock->numCompte }} - {{ $stock->nameCompte }}</td>
            <td style="width: 25%; border-right: solid 1px; text-align: right; padding-right: 5px; padding-bottom: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalN($stock->numCompte)) }}</td>
        </tr>
    @endforeach
    <tr>
        <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding: 15px; font-weight: bold;">SALARIES & AYANT DROITS</td>
        <td style="width: 25%; border-left: solid 1px; border-right: solid 1px; padding: 15px;"></td>
    </tr>
    @foreach(\App\Http\Controllers\ComptaFct\Etat\BilanController::arrayCompteSalaries() as $salary)
        <tr>
            <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding-top: 5px; padding-left: 15px; padding-bottom: 5px;">{{ $salary->numCompte }} - {{ $salary->nameCompte }}</td>
            <td style="width: 25%; border-right: solid 1px; text-align: right; padding-right: 5px; padding-bottom: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalN($salary->numCompte)) }}</td>
        </tr>
    @endforeach
    <tr>
        <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding: 15px; font-weight: bold;">FINANCES</td>
        <td style="width: 25%; border-left: solid 1px; border-right: solid 1px; padding: 15px;"></td>
    </tr>
    @foreach(\App\Http\Controllers\ComptaFct\Etat\BilanController::arrayCompteFinance() as $finance)
        <tr>
            <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding-top: 5px; padding-left: 15px; padding-bottom: 5px;">{{ $finance->numCompte }} - {{ $finance->nameCompte }}</td>
            <td style="width: 25%; border-right: solid 1px; text-align: right; padding-right: 5px; padding-bottom: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalN($finance->numCompte)) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td style="width: 75%; background-color: #0d47a1; padding: 15px; color: white; font-size: 15px; font-weight: bold;">Total de l'actif</td>
        <td style="width: 25%; text-align: right; background-color: #0d47a1; padding: 15px; color: white; font-size: 15px; font-weight: bold;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalActif()) }}</td>
    </tr>
    </tfoot>
</table>
<div class="page-break"></div>
<table style="width: 100%;">
    <tr>
        <td style="width: 33%" class="comite">
            @if($infoCom->logo == 1)
                @if(env('APP_ENV') == 'dev')
                    <img src="{{ url('https://gestion.srice.dev/assets/custom/img/logo/'.$infoCom->id.'.jpg') }}" width="100" alt="...">
                @elseif(env('APP_ENV') == 'testing')
                    <img src="{{ url('https://gestion.srice.ovh/assets/custom/img/logo/'.$infoCom->id.'.jpg') }}" width="100" alt="...">
                @else
                    <img src="{{ url('https://gestion.srice.eu/assets/custom/img/logo/'.$infoCom->id.'.jpg') }}" width="100" alt="...">
                @endif
                <br>
            @endif
            {{ $infoCom->nameComite }}<br>
            {{ $infoCom->adresse }}<br>
            {{ $infoCom->codePostal }} {{ $infoCom->ville }}<br>
            <span style="font-weight: 900;">Tel:</span> {{ $infoCom->telephone }}
        </td>
        <td style="width: 33%;">&nbsp;</td>
        <td style="width: 33%; opacity: 0.5; font-weight: 400; font-size: 24px;">
            BILAN DES OEUVRES SOCIALES & CULTURELLES
        </td>
    </tr>
</table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<h2 style="text-align: center;">{{ $name }}</h2>
<table style="width: 100%;" cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th style="width: 75%; text-align: center; background-color: #0d47a1; padding: 15px; color: white">PASSIF</th>
        <th style="width: 25%; text-align: center; background-color: #0d47a1; padding: 15px; color: white">N</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding: 15px; font-weight: bold;">CAPITAUX PROPRES</td>
        <td style="width: 25%; border-left: solid 1px; border-right: solid 1px; padding: 15px;"></td>
    </tr>
    @foreach(\App\Http\Controllers\ComptaFct\Etat\BilanController::arrayCompteCapitaux() as $capitaux)
        <tr>
            <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding-top: 5px; padding-left: 15px; padding-bottom: 5px;">{{ $capitaux->numCompte }} - {{ $capitaux->nameCompte }}</td>
            <td style="width: 25%; border-right: solid 1px; text-align: right; padding-right: 5px; padding-bottom: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalN($capitaux->numCompte)) }}</td>
        </tr>
    @endforeach
    <tr>
        <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding: 15px; font-weight: bold;">PROVISIONS</td>
        <td style="width: 25%; border-left: solid 1px; border-right: solid 1px; padding: 15px;"></td>
    </tr>
    @foreach(\App\Http\Controllers\ComptaFct\Etat\BilanController::arrayCompteProvision() as $capitaux)
        <tr>
            <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding-top: 5px; padding-left: 15px; padding-bottom: 5px;">{{ $capitaux->numCompte }} - {{ $capitaux->nameCompte }}</td>
            <td style="width: 25%; border-right: solid 1px; text-align: right; padding-right: 5px; padding-bottom: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalN($capitaux->numCompte)) }}</td>
        </tr>
    @endforeach
    <tr>
        <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding: 15px; font-weight: bold;">RESULTAT DE L'EXPLOITATION</td>
        <td style="width: 25%; border-left: solid 1px; border-right: solid 1px; padding: 15px;"></td>
    </tr>
    @foreach(\App\Http\Controllers\ComptaFct\Etat\BilanController::arrayCompteProvision() as $capitaux)
        <tr>
            <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding-top: 5px; padding-left: 15px; padding-bottom: 5px;">Résultat</td>
            <td style="width: 25%; border-right: solid 1px; text-align: right; padding-right: 5px; padding-bottom: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalResultat()) }}</td>
        </tr>
    @endforeach
    <tr>
        <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding: 15px; font-weight: bold;">DETTES ET ASSIMILES</td>
        <td style="width: 25%; border-left: solid 1px; border-right: solid 1px; padding: 15px;"></td>
    </tr>
    @foreach(\App\Http\Controllers\ComptaFct\Etat\BilanController::arrayCompteDettes() as $capitaux)
        <tr>
            <td style="width: 75%; border-left: solid 1px; border-right: solid 1px; padding-top: 5px; padding-left: 15px; padding-bottom: 5px;">{{ $capitaux->numCompte }} - {{ $capitaux->nameCompte }}</td>
            <td style="width: 25%; border-right: solid 1px; text-align: right; padding-right: 5px; padding-bottom: 5px;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalN($capitaux->numCompte)) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td style="width: 75%; background-color: #0d47a1; padding: 15px; color: white; font-size: 15px; font-weight: bold;">Total du passif</td>
        <td style="width: 25%; text-align: right; background-color: #0d47a1; padding: 15px; color: white; font-size: 15px; font-weight: bold;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Etat\BilanController::getTotalPassif()) }}</td>
    </tr>
    </tfoot>
</table>
</body>
</html>