<?php

namespace App\Http\Controllers\ComptaFct\Configuration\Plan;

use App\Model\ComptaFct\Configuration\Plan\FctPlanClasse;
use App\Model\ComptaFct\Configuration\Plan\FctPlanCompte;
use App\Model\ComptaFct\Configuration\Plan\FctPlanSector;
use App\Model\ComptaFct\Etat\FctCompte;
use App\Packages\Srice\Espace;
use ComptaFctPlanClasse;
use ComptaFctPlanCompte;
use ComptaFctPlanSector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class CompteController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptafct';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    /**
     * @param $classe_id
     * @param $sector_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($classe_id, $sector_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $classe = FctPlanClasse::where('numClasse', $classe_id)->first();
        $sector = FctPlanSector::where('numSector', $sector_id)->first();
        $comptes = FctPlanCompte::where('numSector', $sector_id)->get();

        return view('ComptaAsc.Configuration.Plan.Compte.index', compact('config', 'classe', 'sector', 'comptes'));
    }

    public function store(Request $request, $classes_id, $sectors_id){
        $this->validate($request, [
            "numCompte"     => "required",
            "nameCompte"    => "required|min:2"
        ]);

        $add = FctPlanCompte::create([
            "numSector"     => $sectors_id,
            "numCompte"     => $request->numCompte,
            "nameCompte"    => $request->nameCompte
        ]);

        $cpt = FctCompte::create(["numCompte" => $request->numCompte]);

        if($add && $cpt){
            Toastr::success("Un compte à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du compte");
            return redirect()->back();
        }
    }

    public function edit($classes_id, $sectors_id, $comptes_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $classe = FctPlanClasse::where('numClasse', $classes_id)->first();
        $sector = FctPlanSector::where('numSector', $sectors_id)->first();
        $compte = FctPlanCompte::where('numCompte', $comptes_id)->first();

        return view('ComptaFct.Configuration.Plan.Compte.edit', compact('config', 'classe', 'sector', 'compte'));
    }

    public function update(Request $request, $classes_id, $sectors_id, $comptes_id){
        $this->validate($request, [
            "numCompte"     => "required",
            "nameCompte"    => "required|min:2"
        ]);

        $edit = FctPlanCompte::where('numCompte', $comptes_id)->first()->update([
            "numCompte"     => $request->numCompte,
            "nameCompte"    => $request->nameCompte
        ]);

        $cpt = FctCompte::where('numCompte', $comptes_id)->first()->update(["numCompte" => $request->numCompte]);

        if($edit && $cpt){
            Toastr::success("Le compte à été édité");
            return redirect()->route('comptaFct.config.plan.compte.index', [$classes_id, $sectors_id]);
        }else{
            Toastr::error("Erreur lors de l\'édition du compte");
            return redirect()->back();
        }
    }

    public function delete($classes_id, $sectors_id, $comptes_id){
        $del = FctPlanCompte::where('numCompte', $comptes_id)->first()->delete();
        $cpt = FctCompte::where('numCompte', $comptes_id)->first()->delete();

        if($del && $cpt){
            Toastr::success("Le compte à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du compte");
            return redirect()->back();
        }
    }
}
