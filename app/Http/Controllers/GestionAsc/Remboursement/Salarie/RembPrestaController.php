<?php

namespace App\Http\Controllers\GestionAsc\Remboursement\Salarie;

use App\Model\GestionAsc\Remboursement\Salarie\RembPrestaSalarie;
use App\Model\GestionAsc\Remboursement\Salarie\RembSalarie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class RembPrestaController extends RembController
{
    public function addPrestation(Request $request, $remb_id){
        if($this->configuration['remb']->percent == 0){
            $this->validate($request, [
                "prestation"    => "required",
                "montant"       => "required",
                "partCe"        => "required"
            ]);
            $partCe = $request->partCe;
        }else{
            $this->validate($request, [
                "prestation"    => "required",
                "montant"       => "required"
            ]);

            $partCe = $request->montant * $this->configuration['remb']->percentSalarie / 100;
        }

        $add = RembPrestaSalarie::create([
            "remb_salarie_id"   => $remb_id,
            "prestation"        => $request->prestation,
            "montant"           => $request->montant,
            "partCe"            => $partCe
        ]);

        $remb = RembSalarie::find($remb_id);
        $newTotal = $remb->totalRemb + $partCe;

        $remb->update(["totalRemb" => $newTotal]);


        if($add){
            Toastr::success("Une prestation à été ajouté au remboursement");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l'ajout de la prestation au remboursement !");
            return redirect()->back();
        }
    }

    public function delPrestation($remb_id, $prestations_id){
        $remb = RembSalarie::find($remb_id);
        $prestation = RembPrestaSalarie::find($prestations_id);

        $newTotal = $remb->totalRemb - $prestation->partCe;

        $del = $prestation->delete();
        $remb->update(["totalRemb" => "$newTotal"]);

        if($del && $remb){
            Toastr::success("La Prestation à été supprimé de la facture de remboursement");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de la prestation dans la facture de remboursement !");
            return redirect()->back();
        }
    }
}
