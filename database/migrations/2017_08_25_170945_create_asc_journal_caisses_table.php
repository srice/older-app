<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAscJournalCaissesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asc_journal_caisses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numCaisse');
            $table->timestamp('dateCaisse')->nullable();
            $table->string('numCompte');
            $table->string('libelleCaisse');
            $table->string('debitCaisse')->default(0);
            $table->string('creditCaisse')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asc_journal_caisses');
    }
}
