<?php

namespace App\Model\Fidelity;

use Illuminate\Database\Eloquent\Model;

class FidelityCompteur extends Model
{
    protected $guarded = [];

    public function baremes(){
        return $this->hasMany(FidelityBareme::class, 'compteurs_id', 'id');
    }
}
