@extends('template')
@section('title')
    Fiche utilisateur
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/template/examples/css/pages/profile.css">
@stop

@section("content")
    <div class="row">
        <div class="col-md-3">
            <div class="card card-shadow text-xs-center">
                <div class="card-block">
                    <a class="avatar avatar-lg" href="javascript:void(0)">
                        @if($user->avatar == 0)
                            <img src="{{ \Creativeorange\Gravatar\Facades\Gravatar::get($user->email, 'secure') }}" alt="...">
                        @else
                            <img src="/assets/custom/images/profil/{{ $user->id }}.png" alt="...">
                        @endif
                    </a>
                    <h4 class="profile-user">{{ $user->name }}</h4>
                    <p class="profile-job">{{ $user->poste }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel">
                <header class="panel-header">
                    <h3 class="panel-title"><i class="fa fa-info-circle text-info"></i> Information de l'utilisateur</h3>
                    <div class="panel-actions">
                        @if($user->etat == 0)
                            <a href="{{ route('config.user.unlock', $user->id) }}" class="btn btn-success btn-round white"><i class="fa fa-unlock"></i> Débloquer l'utilisateur</a>
                        @else
                            <a href="{{ route('config.user.lock', $user->id) }}" class="btn btn-danger btn-round white"><i class="fa fa-lock"></i> Bloquer l'utilisateur</a>
                        @endif
                    </div>
                </header>
                <div class="panel-body">
                    {{ Form::model($user, ["route" => ["config.user.update", $user->id], "class" => "form-horizontal", "method" => "PUT", "files" => true]) }}
                    <div class="form-group row">
                        {{ Form::label('name *', 'Nom', ["class" => "form-control-label col-md-3"]) }}
                        <div class="col-md-6">
                            {{ Form::text('name', null, ["class" => "form-control round"]) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('email *', 'Email', ["class" => "form-control-label col-md-3"]) }}
                        <div class="col-md-6">
                            {{ Form::email('email', null, ["class" => "form-control round"]) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('poste', 'Poste', ["class" => "form-control-label col-md-3"]) }}
                        <div class="col-md-6">
                            {{ Form::text('poste', null, ["class" => "form-control round"]) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('avatar', 'Avatar', ["class" => "form-control-label col-md-3"]) }}
                        <div class="col-md-6">
                            {{ Form::file('avatar', ["class" => "form-control"]) }}
                        </div>
                    </div>
                    <div class="text-xs-right">
                        <button class="btn btn-success">Valider</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    
@stop