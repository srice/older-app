<?php

namespace App\Http\Controllers\GestionAsc\Prestation;

use App\Model\GestionAsc\Prestation\PrestationLocal;


use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class LocalisationController extends PrestationController
{
    public function storeLocalize(Request $request, $id){
        $localize = PrestationLocal::create([
            "prestations_id"    => $id,
            "type"              => $request->type,
            "adresse"           => $request->adresse,
            "codePostal"        => $request->codePostal,
            "ville"             => $request->ville
        ]);

        if($localize){
            Toastr::success("Localisation ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout de la localisation !");
            return redirect()->back();
        }
    }

    public function deleteLocalize($prestations_id, $localize_id){
        $localize = PrestationLocal::destroy($localize_id);

        if($localize){
            Toastr::success("Localisation supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de la localisation !");
            return redirect()->back();
        }
    }

    public function mapper($prestations_id, $locale_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];
        $localize = PrestationLocal::find($locale_id);

        $result = app('geocoder')->using('chain')->geocode($localize->adresse.','.$localize->codePostal)->all();
        $lng = $result[0]->getLongitude();
        $lat = $result[0]->getLatitude();

        Mapper::map($lat, $lng, ['zoom' => 15, 'center' => true, 'marker' => true, 'type' => 'HYBRID', 'overlay' => 'TRAFFIC']);

        return view('GestionAsc.Prestation.map', compact('config'));
    }
}
