(function ($) {
    $(".dateStart").datepicker({
        language: 'fr',
        format: "dd-mm-yyyy"
    });
    $(".hour").timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        minTime: '0',
        maxTime: '23:45',
        defaultTime: '00',
        startTime: '00:00am',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    })
})(jQuery);

