<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComptaAscPlanCompte extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 101,
            "numSector"     => 10,
            "nameCompte"    => "CAPITAL"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 110,
            "numSector"     => 11,
            "nameCompte"    => "REPORT A NOUVEAU (CREDIT)"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 119,
            "numSector"     => 11,
            "nameCompte"    => "REPORT A NOUVEAU (DEBIT)"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 120,
            "numSector"     => 12,
            "nameCompte"    => "RESULTAT DE L'EXERCICE (CREDIT)"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 129,
            "numSector"     => 12,
            "nameCompte"    => "RESULTAT DE L'EXERCICE (DEBIT)"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 168,
            "numSector"     => 16,
            "nameCompte"    => "AUTRES EMPRUNTS ET DETTES ASSIMILLEES"
        ]);

        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 201,
            "numSector"     => 20,
            "nameCompte"    => "FRAIS D'ETABLISSEMENT"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 205,
            "numSector"     => 20,
            "nameCompte"    => "Concessions et droits similaires, brevets, licences, marques, procédés, logiciels, droits et valeurs similaires"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 206,
            "numSector"     => 20,
            "nameCompte"    => "DROIT AU BAIL"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 211,
            "numSector"     => 21,
            "nameCompte"    => "TERRAINS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 215,
            "numSector"     => 21,
            "nameCompte"    => "Installations techniques, matériels et outillage industriels"
        ]);

        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 345,
            "numSector"     => 34,
            "nameCompte"    => "PRESTATION DE SERVICES EN COURS"
        ]);

        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 401,
            "numSector"     => 40,
            "nameCompte"    => "FOURNISSEURS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 410,
            "numSector"     => 41,
            "nameCompte"    => "SALARIES"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => "410DIV",
            "numSector"     => 41,
            "nameCompte"    => "CLIENT DIVERS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 411,
            "numSector"     => 41,
            "nameCompte"    => "AYANT DROITS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 471,
            "numSector"     => 47,
            "nameCompte"    => "COMPTE D'ATTENTE"
        ]);

        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 512,
            "numSector"     => 51,
            "nameCompte"    => "BANQUE"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 530,
            "numSector"     => 53,
            "nameCompte"    => "CAISSE"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 580,
            "numSector"     => 58,
            "nameCompte"    => "VIREMENT EMIS"
        ]);

        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 601,
            "numSector"     => 60,
            "nameCompte"    => "ACHATS STOCKES"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 607,
            "numSector"     => 60,
            "nameCompte"    => "REMBOURSEMENT SUR FACTURE"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 611,
            "numSector"     => 61,
            "nameCompte"    => "SOUS-TRAITANCE"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 613,
            "numSector"     => 61,
            "nameCompte"    => "LOCATIONS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 615,
            "numSector"     => 61,
            "nameCompte"    => "ENTRETIENS ET REPARATIONS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 616,
            "numSector"     => 61,
            "nameCompte"    => "ASSURANCES"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 618,
            "numSector"     => 61,
            "nameCompte"    => "DIVERS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 622,
            "numSector"     => 62,
            "nameCompte"    => "HONORAIRES"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 623,
            "numSector"     => 62,
            "nameCompte"    => "PUBLICITES"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 625,
            "numSector"     => 62,
            "nameCompte"    => "DEPLACEMENTS, MISSIONS ET RECEPTIONS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 626,
            "numSector"     => 62,
            "nameCompte"    => "FRAIS POSTAUX ET DE TELECOMMUNICATION"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 627,
            "numSector"     => 62,
            "nameCompte"    => "SERVICES BANCAIRES ET ASSIMILEES"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 628,
            "numSector"     => 62,
            "nameCompte"    => "DIVERS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 635,
            "numSector"     => 63,
            "nameCompte"    => "Autres impôts, taxes et versements assimilés (administrations des impôts)"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 638,
            "numSector"     => 63,
            "nameCompte"    => "Autres impôts, taxes et versements assimilés (autres organismes)"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 651,
            "numSector"     => 65,
            "nameCompte"    => "Redevances pour concessions, brevets, licences, marques, procédés, logiciels, droits et valeurs similaires "
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 661,
            "numSector"     => 66,
            "nameCompte"    => "CHARGES D'INTERETS"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 668,
            "numSector"     => 66,
            "nameCompte"    => "AUTRES CHARGES FINANCIERES"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 671,
            "numSector"     => 67,
            "nameCompte"    => "CHARGES EXCEPTIONNELLES SUR OPERATION DE GESTION"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 678,
            "numSector"     => 67,
            "nameCompte"    => "AUTRES CHARGES EXCEPTIONNELLES"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 706,
            "numSector"     => 70,
            "nameCompte"    => "PRESTATION DE SERVICE"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 740,
            "numSector"     => 74,
            "nameCompte"    => "SUBVENTION D'EXPLOITATION"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 771,
            "numSector"     => 77,
            "nameCompte"    => "PRODUITS EXCEPTIONNELLES SUR OPERATION DE GESTION"
        ]);
        DB::table('compta_plan_comptes')->insert([
            "numCompte"     => 778,
            "numSector"     => 77,
            "nameCompte"    => "AUTRES PRODUITS EXCEPTIONNELLES"
        ]);

    }
}
