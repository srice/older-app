<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class UserStatementMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param null $user
     * @return mixed
     * @internal param Guard $guard
     */
    public function handle($request, Closure $next, $user = null)
    {
        $user = Auth::user();
        if($user->etat == 0){
            return response(view('errors.lockUser'), 403);
        }else{
            return $next($request);
        }
    }
}
