@extends('template')
@section('title')
    Vos Prestations
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="panel">
        <header class="panel-header">
            <div class="row">
                <div class="col-md-10">
                    <div class="nav-tabs-horizontal" data-plugin="tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($familles as $famille)
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-toggle="tab" href="#{{ str_slug($famille->name) }}" aria-controls="exampleTabsFour" role="tab">{{ $famille->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('prestations.create') }}" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Ajouter une prestation</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="tab-content p-t-20">
                @foreach($familles as $famille)
                    <div class="tab-pane" id="{{ str_slug($famille->name) }}" role="tabpanel">
                        <div class="panel">
                            <header class="panel-header">
                                <div class="row">
                                    <div class="col-md-10"><h3 class="panel-title">Liste des Prestations: <strong>{{ $famille->name }}</strong></h3></div>
                                </div>
                            </header>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <?php
                                            $prestations = \App\Model\GestionAsc\Prestation\Prestation::where('familles_id', $famille->id)->get()->load('dates', 'locales', 'tarifs');
                                            foreach ($prestations as $prestation):
                                            ?>
                                            <tr>
                                                <td style="width: 10%; text-align: center;">
                                                    <img src="/assets/custom/images/prestation/{{ str_slug($famille->name) }}/{{ $prestation->id }}.png" class="img-responsive" width="80" alt="">
                                                </td>
                                                <td style="width: 35%">
                                                    <strong>{{ $prestation->name }}</strong><br>
                                                    <span class="text-muted">{{ $famille->name }}<br>
                                                        @if($config->configuration['presta']->validite == 1)
                                                        du {{ $prestation->dates[0]->dateStart }} au {{ $prestation->dates[0]->dateEnd }}<br>
                                                            @if(\App\Http\Controllers\GestionAsc\Prestation\DateController::isOutdate($prestation->dates[0]->dateEnd) == false)
                                                                <span class="tag tag-danger">La prestation est arrivé à terme</span>
                                                            @else
                                                                <span class="tag tag-info">Fin de la prestation {{ $prestation->dates[0]->dateEnd->diffForHumans() }}</span>
                                                            @endif
                                                        @endif
                                                    </span>
                                                </td>
                                                <td style="width: 30%;">
                                                    {{ $prestation->locales[0]->type }}<br>
                                                    {{ $prestation->locales[0]->adresse }}<br>
                                                    {{ $prestation->locales[0]->codePostal }} {{ $prestation->locales[0]->ville }}
                                                </td>
                                                <td style="width: 25%; text-align: right;">
                                                    {{ \App\Http\Controllers\OtherController::euro($prestation->tarifs[0]->price) }}<br><br>
                                                    <a href="{{ route('prestations.show', $prestation->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i> Voir la prestation</a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/custom/js/gestionasc/prestation/index.js"></script>
@stop