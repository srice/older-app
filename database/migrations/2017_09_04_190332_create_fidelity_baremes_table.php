<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFidelityBaremesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fidelity_baremes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('compteurs_id');
            $table->string('subStart')->default(0);
            $table->string('subEnd')->default(0);
            $table->integer('nbPoint')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fidelity_baremes');
    }
}
