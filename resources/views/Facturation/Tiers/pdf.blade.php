

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $name }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="/assets/custom/css/pdf.css">
    <style type="text/css">
        body{
            font-family: Arial, sans-serif;
            font-size: 15px;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
<table style="width: 100%;">
    <tr>
        <td style="width: 33%" class="comite">
            <img src="/assets/custom/images/logos/1.jpg" width="100" alt="...">
            <br>
            Comite de Test<br>
            No Adresse<br>
            010101 No Ville
        </td>
        <td style="width: 33%;">&nbsp;</td>
        <td style="width: 33%; opacity: 0.5; font-weight: 400; font-size: 24px;">
            Historique des documents de facturation
        </td>
    </tr>
</table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<table style="width: 100%;">
    <tr>
        <td style="width: 50%;">
            &nbsp;
        </td>
        <td style="width: 50%; border: solid 1px #006600; padding: 15px; border-radius: 5px;">
            <span style="font-weight: 900;">Tiers:</span><br>
            {{ $tier->nomTier }}<br>
            {{ $tier->adresseTier }}<br>
            {{ $tier->codePostalTier }} {{ $tier->villeTier }}
        </td>
    </tr>
</table>
<table style="margin-top: 25px; margin-bottom: 25px"></table>
<h2>Historique</h2>
<table style="width: 100%;" cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th style="width: 15%; text-align: center; background-color: #07d5ff; padding: 15px;">Numéro du document</th>
        <th style="width: 40%; text-align: center; background-color: #07d5ff; padding: 15px;">Libellé du document</th>
        <th style="width: 15%; text-align: center; background-color: #07d5ff; padding: 15px;">Type de document</th>
        <th style="width: 15%; text-align: center; background-color: #07d5ff; padding: 15px;">Montant total du document</th>
        <th style="width: 15%; text-align: center; background-color: #07d5ff; padding: 15px;">Solde</th>
    </tr>
    </thead>
    <tbody>
    @foreach($soldes as $solde)
        <tr>
            <td style="padding: 5px; border: solid 1px;text-align: center;">{{ $solde->numEcriture }}</td>
            <td style="padding: 5px; border: solid 1px">{{ $solde->designationEcriture }}</td>
            <td style="padding: 5px; border: solid 1px;text-align: center;">{{ \App\Http\Controllers\Facturation\Tiers\TiersSoldeController::typeDocument($solde->typeEcriture) }}</td>
            <td style="padding: 5px; border: solid 1px;text-align: right;">
                @if($solde->typeEcriture == 0)
                    -{{ \App\Http\Controllers\OtherController::euro($solde->debitEcriture) }}
                @else
                    {{ \App\Http\Controllers\OtherController::euro($solde->creditEcriture) }}
                @endif
            </td>
            <td style="padding: 5px; border: solid 1px;text-align: right;">{!! \App\Http\Controllers\Facturation\Tiers\TiersSoldeController::formSoldePdf($solde->soldeEcriture) !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>