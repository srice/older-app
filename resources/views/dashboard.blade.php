@extends('template')
@section('title')
    Bienvenue
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/template/examples/css/widgets/weather.css">
    <link rel="stylesheet" href="/assets/global/fonts/weather-icons/weather-icons.css">
@stop

@section("content")
    <div class="row">
        <div class="col-md-8">
            <!-- Card -->
            <div id="weather"></div>
            <!-- End Card -->
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/skycons/skycons.js"></script>
    <script src="/assets/template/examples/js/widgets/weather.js"></script>
    <script src="/assets/custom/js/dashboard.js"></script>
@stop