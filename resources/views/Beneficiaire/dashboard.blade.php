@extends('template')
@section('title')
    Tableau de Bord
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="row">
        <div class="col-md-6">
            <div class="card card-block p-35 clearfix">
                <div class="pull-xs-left white">
                    <i class="icon icon-circle icon-2x md-accounts bg-red-600" aria-hidden="true"></i>
                </div>
                <div class="counter counter-md counter text-xs-right pull-xs-right">
                    <div class="counter-number-group">
                        <span class="counter-number" id="countSalarie"></span>
                        <span class="counter-number-related text-capitalize">Salariés</span>
                    </div>
                    <div class="counter-label text-capitalize font-size-16">dans le comité</div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-block p-35 clearfix">
                <div class="pull-xs-left white">
                    <i class="icon icon-circle icon-2x md-accounts-alt bg-green-600" aria-hidden="true"></i>
                </div>
                <div class="counter counter-md counter text-xs-right pull-xs-right">
                    <div class="counter-number-group">
                        <span class="counter-number" id="countAd"></span>
                        <span class="counter-number-related text-capitalize">Ayants Droit</span>
                    </div>
                    <div class="counter-label text-capitalize font-size-16">dans le comité</div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/custom/js/beneficiaire/dashboard.js"></script>
@stop