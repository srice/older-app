<?php

namespace App\Model\GestionAsc\Prestation\Achat;

use Illuminate\Database\Eloquent\Model;

class AchatReglement extends Model
{
    protected $guarded = [];

    public function achat(){
        return $this->belongsTo(AchatReglement::class);
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateReglement"];
    }
}
