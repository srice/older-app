(function ($) {
    $(".datepicker").datepicker({
        autoclose: true,
        calendarweeks: true,
        clearBtn: true,
        format:'dd-mm-yyyy',
        language: 'fr'
    })
})(jQuery);