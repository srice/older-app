<?php

namespace App\Http\Controllers\GestionAsc\Billetterie\Salarie;

use App\Http\Controllers\ComptaAsc\Etat\CompteController;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalBanque;
use App\Model\ComptaAsc\Journal\AscJournalCaisse;
use App\Model\GestionAsc\Billetterie\BilletSalarie;
use App\Model\GestionAsc\Billetterie\ReglementBilletSalarie;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class BilletReglementController extends Controller
{
    public static function modeReglement($modes_id){
        switch ($modes_id){
            case 1: return "Chèque";
            case 2: return "Espèce";
            case 3: return "Carte Bancaire";
            case 4: return "Virement bancaire";
            case 5: return "Prémèvement Bancaire";
            default: return false;
        }
    }

    public static function countReglement(){
        $count = ReglementBilletSalarie::all()->count();
        return $count;
    }

    public static function lastReglement(){
        $last = ReglementBilletSalarie::all()->last();
        return $last->id;
    }

    public static function totalReglementByBillet($billets_id){
        $billet = ReglementBilletSalarie::where('billet_salarie_id', $billets_id)->get()->sum('totalReglement');
        return $billet;
    }

    public function addReglement(Request $request, $billets_id){
        $this->validate($request, [
            "modeReglement"         => "required",
            "depositaireReglement"  => "required",
            "dateReglement"         => "required|date",
            "totalReglement"        => "required"
        ]);

        $billet = BilletSalarie::find($billets_id)->load('salarie');
        $totalBillet = $billet->totalBillet;


        if($request->modeReglement == 1){
            $numReglementBilletSalarie = $request->numReglementBilletSalarie;
        }else{
            if(self::countReglement() == 0){
                $id = 1;
            }else{
                $id = self::lastReglement();
            }
            $numReglementBilletSalarie = Controller::genNumerator("TRS", $id);
        }

        $date = $request->dateReglement;
        $strt = strtotime($date);
        $dateReglement = Carbon::createFromTimestamp($strt);

        $add = ReglementBilletSalarie::create([
            "numReglementBilletSalarie"     => $numReglementBilletSalarie,
            "billet_salarie_id"             => $billets_id,
            "modeReglement"                 => $request->modeReglement,
            "dateReglement"                 => $dateReglement,
            "totalReglement"                => $request->totalReglement,
            "depositaireReglement"          => $request->depositaireReglement
        ]);

        //Inscription dans le journal
        if($request->modeReglement == 2){
            $newJournalCaisse = AscJournalCaisse::create([
                "numCaisse"     => $numReglementBilletSalarie,
                "dateCaisse"    => $dateReglement,
                "numCompte"     => 530,
                "libelleCaisse" => "Règlement en espèce pour la vente de billetterie N°<strong>".$billet->numBilletSalarie."</strong>",
                "debitCaisse"  => $request->totalReglement
            ]);

            // Mise à jour des Comptes
            $debit = CompteController::soldeDebit('530') + $request->totalReglement;
            $credit = CompteController::soldeCredit($billet->salarie->numCompte) + $request->totalReglement;

            $cptDebit = AscCompte::where('numCompte', '530')->update(["debit" => $debit]);
            $cptCredit = AscCompte::where('numCompte', $billet->salarie->numCompte)->update(["credit" => $credit]);
        }else{
            $newJournalbanque = AscJournalBanque::create([
                "numBanque"     => $numReglementBilletSalarie,
                "dateBanque"    => $dateReglement,
                "numCompte"     => 512,
                "libelleBanque" => "Règlement en <strong>".self::modeReglement($request->modeReglement)."</strong> pour la vente de billetterie N°<strong>".$billet->numBilletSalarie."</strong>",
                "debitBanque"  => $request->totalReglement
            ]);

            // Mise à jour des Comptes
            $debit = CompteController::soldeDebit('512') + $request->totalReglement;
            $credit = CompteController::soldeCredit($billet->salarie->numCompte) + $request->totalReglement;

            $cptDebit = AscCompte::where('numCompte', '512')->update(["debit" => $debit]);
            $cptCredit = AscCompte::where('numCompte', $billet->salarie->numCompte)->update(["credit" => $credit]);
        }

        $totalReg = self::totalReglementByBillet($billets_id);

        if($totalReg < $totalBillet){
            $billet->update(["etatBillet" => 2]);
        }else{
            $billet->update(["etatBillet" => 3]);
        }

        if($add && $billet && $cptCredit && $cptDebit){
            Toastr::success("Le règlement à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l'ajout du règlement !");
            return redirect()->back();
        }

    }

    public function delReglement($billets_id, $reglements_id){
        $billet = BilletSalarie::find($billets_id)->load('salarie');
        $totalBillet = $billet->totalBillet;
        $reglement = ReglementBilletSalarie::find($reglements_id);

        if($reglement->modeReglement == 2){
            $delJournalCaisse = AscJournalCaisse::where('numCaisse', $reglement->numReglement)->delete();

            // Mise à jour des Comptes
            $debit = CompteController::soldeDebit($billet->salarie->numCompte) - $reglement->totalReglement;
            $credit = CompteController::soldeCredit('530') - $reglement->totalReglement;

            $cptDebit = AscCompte::where('numCompte', '530')->update(["debit" => $debit]);
            $cptCredit = AscCompte::where('numCompte', $billet->salarie->numCompte)->update(["credit" => $credit]);
        }else{
            $delJournalBanque = AscJournalBanque::where('numBanque', $reglement->numReglement)->delete();

            // Mise à jour des Comptes
            $debit = CompteController::soldeDebit('512') - $reglement->totalReglement;
            $credit = CompteController::soldeCredit($billet->salarie->numCompte) - $reglement->totalReglement;

            $cptDebit = AscCompte::where('numCompte', $billet->salarie->numCompte)->update(["debit" => $debit]);
            $cptCredit = AscCompte::where('numCompte', '512')->update(["credit" => $credit]);
        }

        $del = $reglement->delete();

        $totalReg = self::totalReglementByBillet($billets_id);

        if($totalReg < $totalBillet && $totalReg > 0){
            $billet->update(["etatBillet" => 2]);
        }elseif($totalReg == 0){
            $billet->update(["etatBillet" => 1]);
        }else{
            $billet->update(["etatBillet" => 3]);
        }

        if($del && $billet && $cptDebit && $cptCredit){
            Toastr::success("Le règlement à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du règlement !");
            return redirect()->back();
        }

    }
}
