
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `asc_bilan_initials` WRITE;
/*!40000 ALTER TABLE `asc_bilan_initials` DISABLE KEYS */;
INSERT INTO `asc_bilan_initials` VALUES (1,'101','0','0',NULL,NULL),(2,'110','0','0',NULL,NULL),(3,'119','0','0',NULL,NULL),(4,'120','0','0',NULL,NULL),(5,'129','0','0',NULL,NULL),(6,'168','0','0',NULL,NULL),(7,'201','0','0',NULL,NULL),(8,'205','0','0',NULL,NULL),(9,'206','0','0',NULL,NULL),(10,'211','0','0',NULL,NULL),(11,'215','0','0',NULL,NULL),(12,'345','0','0',NULL,NULL),(13,'401','0','0',NULL,NULL),(14,'410','0','0',NULL,NULL),(15,'410DIV','0','0',NULL,NULL),(16,'411','0','0',NULL,NULL),(17,'471','0','0',NULL,NULL),(18,'512','0','0',NULL,NULL),(19,'530','0','0',NULL,NULL),(20,'580','0','0',NULL,NULL),(21,'601','0','0',NULL,NULL),(22,'607','0','0',NULL,NULL),(23,'611','0','0',NULL,NULL),(24,'613','0','0',NULL,NULL),(25,'615','0','0',NULL,NULL),(26,'616','0','0',NULL,NULL),(27,'618','0','0',NULL,NULL),(28,'622','0','0',NULL,NULL),(29,'623','0','0',NULL,NULL),(30,'625','0','0',NULL,NULL),(31,'626','0','0',NULL,NULL),(32,'627','0','0',NULL,NULL),(33,'628','0','0',NULL,NULL),(34,'635','0','0',NULL,NULL),(35,'638','0','0',NULL,NULL),(36,'651','0','0',NULL,NULL),(37,'661','0','0',NULL,NULL),(38,'668','0','0',NULL,NULL),(39,'671','0','0',NULL,NULL),(40,'678','0','0',NULL,NULL),(41,'706','0','0',NULL,NULL),(42,'740','0','0',NULL,NULL),(43,'771','0','0',NULL,NULL),(44,'778','0','0',NULL,NULL);
/*!40000 ALTER TABLE `asc_bilan_initials` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

