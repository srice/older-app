@extends('template')
@section('title')
    Votre Comité
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="row">
        <div class="col-md-3">
            <div class="card card-shadow text-xs-center">
                <div class="card-block">
                    <a class="avatar avatar-lg" href="javascript:void(0)">
                        @if($infoComite)
                            @if(env('APP_ENV') == 'dev')
                                <img src="{{ url('https://gestion.srice.dev/assets/custom/img/logo/'.$infoComite->id.'.jpg') }}" alt="...">
                            @elseif(env('APP_ENV') == 'testing')
                                <img src="{{ url('https://gestion.srice.ovh/assets/custom/img/logo/'.$infoComite->id.'.jpg') }}" alt="...">
                            @else
                                <img src="{{ url('https://gestion.srice.eu/assets/custom/img/logo/'.$infoComite->id.'.jpg') }}" alt="...">
                            @endif
                        @else
                            <img src="{{ url('/assets/template/images/logo-blue@2x.png') }}" alt="">
                        @endif
                    </a>
                    <h4 class="profile-user">{{ $infoComite->name }}</h4>
                    <p class="profile-job">{{ $infoComite->customersId }}</p>
                </div>
                <!--<div class="card-footer">
                    <div class="row no-space">
                        <div class="col-xs-4">
                            <strong class="profile-stat-count">260</strong>
                            <span>Follower</span>
                        </div>
                        <div class="col-xs-4">
                            <strong class="profile-stat-count">180</strong>
                            <span>Following</span>
                        </div>
                        <div class="col-xs-4">
                            <strong class="profile-stat-count">2000</strong>
                            <span>Tweets</span>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" data-toggle="tab" href="#info" aria-controls="profile" role="tab">
                                Information
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" data-toggle="tab" href="#licence" aria-controls="profile" role="tab">
                                Licence
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active animation-slide-left" id="info" role="tabpanel">
                            <div class="m-t-15"></div>
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="icon md-info text-info"></i> Informations</h3>
                                    <div class="panel-actions">
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td style="font-weight: bold; text-align: right;">Numéro du comité</td>
                                                <td>{{ $infoComite->customersId }}</td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; text-align: right;">Numéro de l'application</td>
                                                <td>{{ $espaceInfo->id }}</td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; text-align: right;">Adresse</td>
                                                <td>
                                                    {{ $infoComite->adresse }}<br>
                                                    {{ $infoComite->cp }} {{ $infoComite->ville }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; text-align: right;">Coordonnées</td>
                                                <td>
                                                    @if(!empty($infoComite->tel))
                                                        <strong><i class="icon md-phone"></i></strong>: {{ $infoComite->tel }}<br>
                                                    @endif
                                                    @if(!empty($infoComite->email))
                                                        <strong><i class="fa fa-envelope"></i></strong>: {{ $infoComite->email }}<br>
                                                    @endif
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="tab-pane animation-slide-left" id="licence" role="tabpanel">
                            <div class="m-t-15"></div>
                            <h3 class="animation-slide-left"><i class="icon md-key text-info"></i> Licence</h3>
                            <div class="animation-slide-left">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td style="font-weight: bold;">Numéro de licence</td>
                                            <td>{{ $licence->numLicence }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Date de fin de licence</td>
                                            <td id="licenceTimeout">

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/custom/js/configuration/comite/index.js"></script>
@stop