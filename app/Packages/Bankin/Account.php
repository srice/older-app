<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 15/08/2017
 * Time: 22:05
 */

namespace App\Packages\Bankin;


class Account extends Appel
{
    public function listAccounts($bearer){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "limit"         => 50
        ];

        $call = $this->get('accounts?', $params, true, $bearer);

        return $call;
    }

    public function account($accountId){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
        ];

        $call = $this->get('accounts/'.$accountId.'?', $params, true, session('access_token'));

        return $call;
    }
}