<?php


namespace App\HelperClass\Banker;


use App\Packages\Bankin\Categories;

class BankerCategory
{
    public static function getIconCategory($category_id)
    {
        switch ($category_id)
        {
            case 321: return '/assets/custom/images/banker/category/321.png'; break;
            case 317: return '/assets/custom/images/banker/category/321.png'; break;
            case 316: return '/assets/custom/images/banker/category/316.png'; break;
            case 248: return '/assets/custom/images/banker/category/248.png'; break;
            case 235: return '/assets/custom/images/banker/category/235.png'; break;
            case 267: return '/assets/custom/images/banker/category/267.png'; break;
            case 266: return '/assets/custom/images/banker/category/266.png'; break;
            case 259: return '/assets/custom/images/banker/category/259.png'; break;
            case 241: return '/assets/custom/images/banker/category/241.png'; break;
            case 240: return '/assets/custom/images/banker/category/240.png'; break;
            case 279: return '/assets/custom/images/banker/category/240.png'; break;
            case 239: return '/assets/custom/images/banker/category/239.png'; break;
            case 238: return '/assets/custom/images/banker/category/238.png'; break;
            case 237: return '/assets/custom/images/banker/category/237.png'; break;
            case 441900: return '/assets/custom/images/banker/category/441900.png'; break;
            case 441899: return '/assets/custom/images/banker/category/441899.png'; break;
            case 441898: return '/assets/custom/images/banker/category/441898.png'; break;
            case 441988: return '/assets/custom/images/banker/category/441898.png'; break;
            case 302: return '/assets/custom/images/banker/category/441898.png'; break;
            case 209: return '/assets/custom/images/banker/category/441898.png'; break;
            case 208: return '/assets/custom/images/banker/category/441898.png'; break;
            case 206: return '/assets/custom/images/banker/category/441898.png'; break;
            case 441897: return '/assets/custom/images/banker/category/441897.png'; break;
            case 441896: return '/assets/custom/images/banker/category/441896.png'; break;
            case 441895: return '/assets/custom/images/banker/category/441895.png'; break;
            case 441892: return '/assets/custom/images/banker/category/441892.png'; break;
            case 441891: return '/assets/custom/images/banker/category/441891.png'; break;
            case 441890: return '/assets/custom/images/banker/category/441891.png'; break;
            case 230: return '/assets/custom/images/banker/category/441891.png'; break;
            case 441889 || 90: return '/assets/custom/images/banker/category/441889.png'; break;
            case 90: return '/assets/custom/images/banker/category/441889.png'; break;
            case 441886: return '/assets/custom/images/banker/category/441886.png'; break;
            case 274: return '/assets/custom/images/banker/category/274.png'; break;
            case 270: return '/assets/custom/images/banker/category/270.png'; break;
            case 265: return '/assets/custom/images/banker/category/265.png'; break;
            case 205: return '/assets/custom/images/banker/category/265.png'; break;
            case 204: return '/assets/custom/images/banker/category/265.png'; break;
            case 276: return '/assets/custom/images/banker/category/265.png'; break;
            case 203: return '/assets/custom/images/banker/category/203.png'; break;
            case 222: return '/assets/custom/images/banker/category/203.png'; break;
            case 202: return '/assets/custom/images/banker/category/202.png'; break;
            case 207: return '/assets/custom/images/banker/category/207.png'; break;
            case 324: return '/assets/custom/images/banker/category/324.png'; break;
            case 308: return '/assets/custom/images/banker/category/308.png'; break;
            case 294: return '/assets/custom/images/banker/category/294.png'; break;
            case 278: return '/assets/custom/images/banker/category/278.png'; break;
            case 1: return '/assets/custom/images/banker/category/1.png'; break;
            case 289: return '/assets/custom/images/banker/category/1.png'; break;
            case 233: return '/assets/custom/images/banker/category/1.png'; break;
            case 3: return '/assets/custom/images/banker/category/1.png'; break;
            case 328: return '/assets/custom/images/banker/category/1.png'; break;
            case 320: return '/assets/custom/images/banker/category/320.png'; break;
            case 83: return '/assets/custom/images/banker/category/320.png'; break;
            case 310: return '/assets/custom/images/banker/category/310.png'; break;
            case 269: return '/assets/custom/images/banker/category/269.png'; break;
            case 263: return '/assets/custom/images/banker/category/263.png'; break;
            case 249: return '/assets/custom/images/banker/category/249.png'; break;
            case 244: return '/assets/custom/images/banker/category/244.png'; break;
            case 242: return '/assets/custom/images/banker/category/242.png'; break;
            case 227: return '/assets/custom/images/banker/category/227.png'; break;
            case 226: return '/assets/custom/images/banker/category/226.png'; break;
            case 224: return '/assets/custom/images/banker/category/224.png'; break;
            case 223: return '/assets/custom/images/banker/category/223.png'; break;
            case 309: return '/assets/custom/images/banker/category/309.png'; break;
            case 288: return '/assets/custom/images/banker/category/288.png'; break;
            case 264: return '/assets/custom/images/banker/category/264.png'; break;
            case 251: return '/assets/custom/images/banker/category/251.png'; break;
            case 247: return '/assets/custom/images/banker/category/247.png'; break;
            case 198: return '/assets/custom/images/banker/category/198.png'; break;
            case 197: return '/assets/custom/images/banker/category/197.png'; break;
            case 196: return '/assets/custom/images/banker/category/196.png'; break;
            case 87: return '/assets/custom/images/banker/category/87.png'; break;
            case 84: return '/assets/custom/images/banker/category/84.png'; break;
            case 306: return '/assets/custom/images/banker/category/306.png'; break;
            case 79: return '/assets/custom/images/banker/category/306.png'; break;
            case 195: return '/assets/custom/images/banker/category/195.png'; break;
            case 194: return '/assets/custom/images/banker/category/194.png'; break;
            case 192: return '/assets/custom/images/banker/category/192.png'; break;
            case 191: return '/assets/custom/images/banker/category/191.png'; break;
            case 89: return '/assets/custom/images/banker/category/89.png'; break;
            case 441894: return '/assets/custom/images/banker/category/89.png'; break;
            case 326: return '/assets/custom/images/banker/category/326.png'; break;
            case 282: return '/assets/custom/images/banker/category/326.png'; break;
            case 88: return '/assets/custom/images/banker/category/88.png'; break;
            case 85: return '/assets/custom/images/banker/category/85.png'; break;
            case 78: return '/assets/custom/images/banker/category/78.png'; break;
            case 280: return '/assets/custom/images/banker/category/280.png'; break;
            case 277: return '/assets/custom/images/banker/category/277.png'; break;
            case 258: return '/assets/custom/images/banker/category/258.png'; break;
            case 219: return '/assets/custom/images/banker/category/219.png'; break;
            case 180: return '/assets/custom/images/banker/category/180.png'; break;
            case 313: return '/assets/custom/images/banker/category/313.png'; break;
            case 273: return '/assets/custom/images/banker/category/273.png'; break;
            case 441888: return '/assets/custom/images/banker/category/441888.png'; break;
            case 319: return '/assets/custom/images/banker/category/319.png'; break;
            case 318: return '/assets/custom/images/banker/category/318.png'; break;
            case 272: return '/assets/custom/images/banker/category/272.png'; break;
            case 262: return '/assets/custom/images/banker/category/262.png'; break;
            case 243: return '/assets/custom/images/banker/category/243.png'; break;
            case 186: return '/assets/custom/images/banker/category/186.png'; break;
            case 184: return '/assets/custom/images/banker/category/186.png'; break;
            case 183: return '/assets/custom/images/banker/category/186.png'; break;
            case 325: return '/assets/custom/images/banker/category/325.png'; break;
            case 322: return '/assets/custom/images/banker/category/322.png'; break;
            case 268: return '/assets/custom/images/banker/category/268.png'; break;
            case 261: return '/assets/custom/images/banker/category/261.png'; break;
            case 245: return '/assets/custom/images/banker/category/245.png'; break;
            case 236: return '/assets/custom/images/banker/category/236.png'; break;
            case 441893: return '/assets/custom/images/banker/category/441893.png'; break;
            case 327: return '/assets/custom/images/banker/category/327.png'; break;
            case 314: return '/assets/custom/images/banker/category/314.png'; break;
            case 216: return '/assets/custom/images/banker/category/314.png'; break;
            case 283: return '/assets/custom/images/banker/category/283.png'; break;
            case 271: return '/assets/custom/images/banker/category/271.png'; break;
            case 232: return '/assets/custom/images/banker/category/232.png'; break;
            case 231: return '/assets/custom/images/banker/category/231.png'; break;
            case 80: return '/assets/custom/images/banker/category/80.png'; break;
            case 323: return '/assets/custom/images/banker/category/323.png'; break;
            case 293: return '/assets/custom/images/banker/category/293.png'; break;
            case 246: return '/assets/custom/images/banker/category/246.png'; break;
            case 221: return '/assets/custom/images/banker/category/221.png'; break;
            case 220: return '/assets/custom/images/banker/category/220.png'; break;
            case 218: return '/assets/custom/images/banker/category/218.png'; break;
            case 217: return '/assets/custom/images/banker/category/217.png'; break;
            //default: return '/assets/custom/images/banker/category/1.png';

        }
    }

    public static function getNameCategory($category_id)
    {
        $category = new Categories();

        $info = $category->category($category_id);

        return $info->name;
    }
}