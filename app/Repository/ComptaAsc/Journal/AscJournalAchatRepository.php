<?php
namespace App\Repository\ComptaAsc\Journal;

use App\Model\ComptaAsc\Journal\AscJournalAchat;

class AscJournalAchatRepository
{
    /**
     * @var AscJournalAchat
     */
    private $ascJournalAchat;

    /**
     * AscJournalVenteRepository constructor.
     * @param AscJournalAchat $ascJournalAchat
     */

    public function __construct(AscJournalAchat $ascJournalAchat)
    {

        $this->ascJournalAchat = $ascJournalAchat;
    }

    public function create($id, $created_at, int $numCompte, string $libelleVente, $montant)
    {
        return $this->ascJournalAchat->newQuery()
            ->create([
                "numAchat"  => "ANCVACH".$id,
                "dateAchat" => $created_at,
                "numCompte" => $numCompte,
                "libelleAchat" => $libelleVente,
                "debitAchat"=> $montant,
                "creditAchat" => 0
            ]);
    }


}

        