<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComptaAscCompte extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('asc_comptes')->insert(["numCompte" => "101"]);
        DB::table('asc_comptes')->insert(["numCompte" => "110"]);
        DB::table('asc_comptes')->insert(["numCompte" => "119"]);
        DB::table('asc_comptes')->insert(["numCompte" => "120"]);
        DB::table('asc_comptes')->insert(["numCompte" => "129"]);
        DB::table('asc_comptes')->insert(["numCompte" => "168"]);
        DB::table('asc_comptes')->insert(["numCompte" => "201"]);
        DB::table('asc_comptes')->insert(["numCompte" => "205"]);
        DB::table('asc_comptes')->insert(["numCompte" => "206"]);
        DB::table('asc_comptes')->insert(["numCompte" => "211"]);
        DB::table('asc_comptes')->insert(["numCompte" => "215"]);
        DB::table('asc_comptes')->insert(["numCompte" => "345"]);
        DB::table('asc_comptes')->insert(["numCompte" => "401"]);
        DB::table('asc_comptes')->insert(["numCompte" => "410"]);
        DB::table('asc_comptes')->insert(["numCompte" => "410DIV"]);
        DB::table('asc_comptes')->insert(["numCompte" => "411"]);
        DB::table('asc_comptes')->insert(["numCompte" => "471"]);
        DB::table('asc_comptes')->insert(["numCompte" => "512"]);
        DB::table('asc_comptes')->insert(["numCompte" => "530"]);
        DB::table('asc_comptes')->insert(["numCompte" => "580"]);
        DB::table('asc_comptes')->insert(["numCompte" => "601"]);
        DB::table('asc_comptes')->insert(["numCompte" => "607"]);
        DB::table('asc_comptes')->insert(["numCompte" => "611"]);
        DB::table('asc_comptes')->insert(["numCompte" => "613"]);
        DB::table('asc_comptes')->insert(["numCompte" => "615"]);
        DB::table('asc_comptes')->insert(["numCompte" => "616"]);
        DB::table('asc_comptes')->insert(["numCompte" => "618"]);
        DB::table('asc_comptes')->insert(["numCompte" => "622"]);
        DB::table('asc_comptes')->insert(["numCompte" => "623"]);
        DB::table('asc_comptes')->insert(["numCompte" => "625"]);
        DB::table('asc_comptes')->insert(["numCompte" => "626"]);
        DB::table('asc_comptes')->insert(["numCompte" => "627"]);
        DB::table('asc_comptes')->insert(["numCompte" => "628"]);
        DB::table('asc_comptes')->insert(["numCompte" => "635"]);
        DB::table('asc_comptes')->insert(["numCompte" => "638"]);
        DB::table('asc_comptes')->insert(["numCompte" => "651"]);
        DB::table('asc_comptes')->insert(["numCompte" => "661"]);
        DB::table('asc_comptes')->insert(["numCompte" => "668"]);
        DB::table('asc_comptes')->insert(["numCompte" => "671"]);
        DB::table('asc_comptes')->insert(["numCompte" => "678"]);
        DB::table('asc_comptes')->insert(["numCompte" => "706"]);
        DB::table('asc_comptes')->insert(["numCompte" => "740"]);
        DB::table('asc_comptes')->insert(["numCompte" => "771"]);
        DB::table('asc_comptes')->insert(["numCompte" => "778"]);
    }
}
