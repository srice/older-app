@extends('template')
@section("title")
    Liste des comptes du secteur: <strong>{{ $sector->nameSector }}</strong>
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="panel-title">@yield('title')</h3>
                </div>
                <div class="col-md-2">
                    <a href="#addCompte" data-toggle="modal" class="btn btn-lg btn-block btn-primary"><i class="fa fa-plus-circle"></i> Ajouter un compte</a>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Numéro</th>
                            <th>Désignation</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($comptes as $compte)
                            <tr>
                                <td>{{ $compte->numCompte }}</td>
                                <td>{{ $compte->nameCompte}}</td>
                                <td>
                                    {{ Form::model($compte, ["route" => ["comptaAsc.config.plan.compte.delete", $classe->numClasse, $sector->numSector, $compte->numCompte], "method" => "DELETE"]) }}
                                    <a href="{{ route('comptaAsc.config.plan.compte.edit', [$classe->numClasse, $sector->numSector, $compte->numCompte]) }}" class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" data-title="Editer le compte"><i class="fa fa-edit"></i> </a>
                                    <button class="btn btn-sm btn-icon btn-danger" data-toggle="tooltip" data-title="Supprimer le compte"><i class="fa fa-trash"></i> </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addCompte" aria-hidden="true" aria-labelledby="examplePositionSidebar"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-bottom modal-sidebar modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Ajout d'un compte</h4>
                </div>
                {{ Form::model($sector, ["route" => ["comptaAsc.config.plan.compte.store", $classe->numClasse, $sector->numSector]]) }}
                <div class="modal-body">
                    <div class="form-group form-material">
                        {{ Form::label('numCompte', "Numéro de compte", ["class" => "control-label"]) }}
                        {{ Form::text('numCompte', null, ["class" => "form-control"]) }}
                    </div>
                    <div class="form-group form-material">
                        {{ Form::label('nameCompte', "Nom du secteur", ["class" => "control-label"]) }}
                        {{ Form::text('nameCompte', null, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Valider</button>
                    <button type="button" class="btn btn-default btn-block btn-pure" data-dismiss="modal">Fermer</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    