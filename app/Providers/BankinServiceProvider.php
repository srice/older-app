<?php

namespace App\Providers;

use App\Packages\Bankin\Config;
use Illuminate\Support\ServiceProvider;

class BankinServiceProvider extends ServiceProvider
{

    protected $defer = false;
    protected $configName = 'bankin';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__.'/../config/'.$this->configName.'.php';
        $this->publishes([$configPath => config_path($this->configName.'.php')], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__.'/../../config/'.$this->configName.'.php';
        $this->mergeConfigFrom($configPath, $this->configName);
        $this->app->bind(Config::class, Config::class);
        $this->app->singleton('config', function($app){
            return new Config();
        });

        $this->app->alias('config', Config::class);
    }
}
