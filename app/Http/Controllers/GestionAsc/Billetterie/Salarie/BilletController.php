<?php

namespace App\Http\Controllers\GestionAsc\Billetterie\Salarie;

use App\Http\Controllers\ComptaAsc\Etat\CompteController;
use App\Http\Controllers\GestionAsc\Configuration\ModeReglementController;
use App\Model\Beneficiaire\Salarie\Salarie;
use App\Model\Beneficiaire\Salarie\SalarieFidelity;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\ComptaAsc\Journal\AscJournalVente;
use App\Model\Fidelity\FidelityCompteur;
use App\Model\GestionAsc\Billetterie\BilletSalarie;
use App\Model\GestionAsc\Billetterie\LigneBilletSalarie;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Model\GestionAsc\Prestation\Famille;
use App\Model\GestionAsc\Prestation\Prestation;
use App\Model\GestionAsc\Prestation\PrestationTarif;
use App\Packages\Srice\Comite;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;
use PDF;

class BilletController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'gestionasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function create(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $salaries = Salarie::where('active', 1)->get();

        return view('GestionAsc.Billetterie.Salarie.create', compact('config', 'salaries'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "salaries_id"   => "required",
            "dateBillet"    => "required"
        ]);

        if(BilletOtherController::countBillet() == 0){
            $id = 1;
        }else{
            $id = BilletOtherController::countBillet() + 1;
        }

        $date = $request->dateBillet;
        $strt = strtotime($date);
        $dateBillet = Carbon::createFromTimestamp($strt);

        $numBilletSalarie = Controller::genNumerator("BLTSAL", $id);

        $billet = BilletSalarie::create([
            "numBilletSalarie"     => $numBilletSalarie,
            "salaries_id"          => $request->salaries_id,
            "dateBillet"           => $dateBillet,
            "totalBillet"          => 0,
            "etatBillet"           => 0
        ]);

        if($billet){
            Toastr::success("La facture de vente à été créé");
            return redirect()->route('billetteries.index');
        }else{
            Toastr::error("Erreur lors de la création de la facture de vente!");
            return redirect()->back();
        }
    }

    public function show($id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $billet = BilletSalarie::find($id)->load('salarie', 'lignes', 'reglements');
        $familles = Famille::all();
        $modeReglements = ModeReglementController::listeMode();

        return view('GestionAsc.Billetterie.Salarie.show', compact('config', 'billet', 'familles', 'modeReglements'));
    }

    public function edit($id){

    }

    public function update(Request $request, $id){

    }

    public function delete($id){

    }

    public function check($billets_id){
        $billet = BilletSalarie::find($billets_id)->load('salarie');
        $lignes = LigneBilletSalarie::where('billet_salarie_id', $billets_id)->get();

        //dd($billet, $lignes);

        foreach ($lignes as $ligne){
            if($this->configuration['presta']->stock == 1){
                $tarif = PrestationTarif::where('prestations_id', $ligne->prestations_id)->first();

                $newStock = $tarif->stockActuel - $ligne->qte;
                $tarif->update(["stockActuel" => $newStock]);
            }
            if($this->modules[12]->state == 1){
                $prestation = Prestation::find($ligne->prestations_id);
                $compteur = FidelityCompteur::find($prestation->compteurs_id)->load('baremes');
                $salarie = Salarie::find($billet->salaries_id)->load('fidelities');
                foreach ($compteur->baremes as $bareme){
                    $fidelity = BilletOtherController::newFidelities($ligne->prestations_id, $ligne->id);
                    $newFidelity = $salarie->fidelityPoint + $fidelity;
                    $salarie->update(["fidelityPoint" => $newFidelity]);
                    SalarieFidelity::create([
                        "salaries_id"   => $billet->salaries_id,
                        "numTransaction"=> $billet->numBilletSalarie,
                        "operation"     => "Vente de Billetterie",
                        "point"         => $fidelity,
                        "solde"         => $newFidelity
                    ]);
                }
            }
        }

        $billet->update(["etatBillet" => 1]);

        $newJournalVente = AscJournalVente::create([
            "numVente"      => $billet->numBilletSalarie,
            "dateVente"     => $billet->dateBillet,
            "numCompte"     => 706,
            "libelleVente"  => "Vente de prestation <strong>".$billet->salarie->nom." ".$billet->salarie->prenom."</strong>",
            "creditVente"   => $billet->totalBillet
        ]);

        //Mise à jour des Compte

        $debit = CompteController::soldeDebit($billet->salarie->numCompte) + $billet->totalBillet;
        $credit = CompteController::soldeCredit('706') + $billet->totalBillet;

        $cptDebit = AscCompte::where('numCompte', $billet->salarie->numCompte)->update(["debit" => $debit]);
        $cptCredit = AscCompte::where('numCompte', '706')->update(["credit" => $credit]);

        if($billet && $lignes && $cptDebit && $cptCredit && $newJournalVente){
            Toastr::success("La facture à été validé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la validation de la facture !");
            return redirect()->back();
        }
    }

    public function showPdf($billets_id, Comite $comite){
        $billet = BilletSalarie::find($billets_id)->load('salarie', 'lignes', 'reglements');
        $infoCom = $comite->comite();
        $name = "Facture de Billetterie Salarie N°".$billet->numBilletSalarie;
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('GestionAsc.Billetterie.Salarie.pdf', compact('billet', 'infoCom', 'name'));
        return $pdf->stream($name);
    }

    public function savePdf($billets_id, Comite $comite){
        $billet = BilletSalarie::find($billets_id)->load('salarie', 'lignes', 'reglements');
        $infoCom = $comite->comite();
        $name = "Facture de Billetterie Salarie N°".$billet->numBilletSalarie.".pdf";
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('GestionAsc.Billetterie.Salarie.pdf', compact('billet', 'infoCom', 'name'));
        return $pdf->download($name);
    }
}
