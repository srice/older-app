(function ($) {
    getTimeOutLicence();
})(jQuery);

function getTimeOutLicence() {
    var div = $("#licenceTimeout");
    div.html('<div class="loading loading-circle"></div>');
    $.get('/config/comite/timeout')
        .done(function (data) {
            div.html(data)
        })
}