<?php

namespace App\Http\Controllers\ComptaAsc\Remise\Position;

use App\Model\ComptaAsc\Remise\RemiseBanque;
use App\Model\GestionAsc\Billetterie\ReglementBilletAd;
use App\Model\GestionAsc\Billetterie\ReglementBilletSalarie;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PositionController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptaasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $salaries = ReglementBilletSalarie::where('modeReglement', 1)
            ->orWhere('modeReglement', 2)
            ->get();
        $ads = ReglementBilletAd::where('modeReglement', 1)
            ->orWhere('modeReglement', 2)
            ->get();
        $remises = RemiseBanque::where('etatRemise', 1)->get();

        return view('ComptaAsc.Remise.Position.index', compact('config', 'salaries', 'ads', 'remises'));
    }
}
