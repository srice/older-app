<?php

namespace App\Http\Controllers\ComptaFct\Etat;

use App\Model\ComptaFct\Configuration\Plan\FctPlanCompte;
use App\Model\ComptaFct\Etat\FctCompte;
use App\Packages\Srice\Comite;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class EtatController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptafct';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function bilan(Comite $comite){
        $infoCom = $comite->comite();
        $name = "Bilan du ".date('01/01/Y')." au ".date('31/12/Y');
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('ComptaFct.Etat.bilan', compact('infoCom',  'name'))->setPaper('a4', 'landscape');
        return $pdf->stream($name);
    }

    public function balance(Comite $comite){
        $infoCom = $comite->comite();
        $name = "Balance du ".date('01/01/Y')." au ".date('31/12/Y');
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('ComptaFct.Etat.balance', compact('infoCom', 'name'))->setPaper('a4');
        return $pdf->stream($name);
    }

    public function resultat(Comite $comite){
        $infoCom = $comite->comite();
        $name = "Compte de résultat du ".date('01/01/Y')." au ".date('31/12/Y');
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('ComptaFct.Etat.resultat', compact('infoCom', 'name'))->setPaper('a4', 'landscape');
        return $pdf->stream($name);
    }
}
class BilanController extends EtatController
{
    public static function arrayCompteImmos()
    {
        $comptes = FctPlanCompte::where('numCompte', '>=', '200')->where('numCompte', '<', '300')->get();
        return $comptes;
    }

    public static function arrayCompteStocks()
    {
        $comptes = FctPlanCompte::where('numCompte', '>=', '300')->where('numCompte', '<', '400')->get();
        return $comptes;
    }

    public static function arrayCompteSalaries()
    {
        $comptes = FctPlanCompte::where('numCompte', '>=', '410')->where('numCompte', '<', '412')->get();
        return $comptes;
    }

    public static function arrayCompteFinance()
    {
        $comptes = FctPlanCompte::where('numCompte', '>=', '500')->where('numCompte', '<', '600')->get();
        return $comptes;
    }

    public static function arrayCompteCapitaux()
    {
        $compte = FctPlanCompte::where('numCompte', '>=', '100')->where('numCompte', '<', '120')->get();
        return $compte;
    }

    public static function arrayCompteProvision()
    {
        $compte = FctPlanCompte::where('numCompte', '>=', '168')->where('numCompte', '<', '200')->get();
        return $compte;
    }

    public static function arrayCompteDettes()
    {
        $compte = FctPlanCompte::where('numCompte', '>=', '401')->where('numCompte', '<', '402')->get();
        return $compte;
    }

    public static function getTotalN($numCompte)
    {
        $compte = FctCompte::where('numCompte', $numCompte)->first();
        return $compte->debit - $compte->credit;
    }

    public static function getTotalResultat()
    {
        $charge = FctCompte::where('numCompte', '>=', '600')->where('numCompte', '<', '700')->get()->sum('debit');
        $produit = FctCompte::where('numCompte', '>=', '700')->where('numCompte', '<', '800')->get()->sum('credit');

        return $produit - $charge;
    }

    public static function getTotalActif()
    {
        $immo = FctCompte::where('numCompte', '>=', '200')->where('numCompte', '<', '300')->get();
        $debitImmo = $immo->sum('debit');
        $creditImmo = $immo->sum('credit');
        $sumImmo = $debitImmo - $creditImmo;


        $stock = FctCompte::where('numCompte', '>=', '300')->where('numCompte', '<', '400')->get();
        $debitStock = $stock->sum('debit');
        $creditStock = $stock->sum('credit');
        $sumStock = $debitStock - $creditStock;

        $salarie = FctCompte::where('numCompte', '>=', '410')->where('numCompte', '<', '412')->get();
        $debitSal = $salarie->sum('debit');
        $creditSal = $salarie->sum('credit');
        $sumSal = $debitSal - $creditSal;

        $finance = FctCompte::where('numCompte', '>=', '500')->where('numCompte', '<', '600')->get();
        $debitFin = $finance->sum('debit');
        $creditFin = $finance->sum('credit');
        $sumFin = $debitFin - $creditFin;


        return $sumImmo + $sumStock + $sumSal + $sumFin;
    }

    public static function getTotalPassif()
    {
        $cap = FctCompte::where('numCompte', '>=', '100')->where('numCompte', '<', '120')->get();
        $debitCap = $cap->sum('debit');
        $creditCap = $cap->sum('credit');
        $sumCap = $debitCap - $creditCap;

        $prov = FctCompte::where('numCompte', '>=', '168')->where('numCompte', '<', '200')->get();
        $debitProv = $prov->sum('debit');
        $creditProv = $prov->sum('credit');
        $sumProv = $debitProv - $creditProv;

        $dette = FctCompte::where('numCompte', '>=', '401')->where('numCompte', '<', '402')->get();
        $debitDette = $dette->sum('debit');
        $creditDette = $dette->sum('credit');
        $sumDette = $debitDette - $creditDette;

        $resultat = self::getTotalResultat();

        return $sumCap + $sumProv + $sumDette + $resultat;
    }
}
class BalanceController extends EtatController{

    public static function arrayComptes(){
        $comptes = FctPlanCompte::all();
        return $comptes;
    }

    public static function getTotalDebit($numCompte){
        $compte = FctCompte::where('numCompte', $numCompte)->first();
        return $compte->debit;
    }

    public static function getTotalCredit($numCompte){
        $compte = FctCompte::where('numCompte', $numCompte)->first();
        return $compte->credit;
    }

    public static function getDebiteur(){
        $total = FctCompte::all()->sum('debit');
        return $total;
    }

    public static function getCrediteur(){
        $total = FctCompte::all()->sum('credit');
        return $total;
    }

}
class ResultatController extends EtatController{

    public static function arrayCompteCharges(){
        $comptes = FctPlanCompte::where('numCompte', '>=', 600)->where('numCompte', '<', 700)->get();
        return $comptes;
    }

    public static function arrayCompteproduits(){
        $comptes = FctPlanCompte::where('numCompte', '>=', 700)->where('numCompte', '<', 800)->get();
        return $comptes;
    }

    public static function getTotalNCharge($numCompte){
        $compte = FctCompte::where('numCompte', $numCompte)->first();
        return $compte->debit;
    }

    public static function getTotalNProduit($numCompte){
        $compte = FctCompte::where('numCompte', $numCompte)->first();
        return $compte->credit;
    }

    public static function getTotalCharge(){
        $charge = FctCompte::where('numCompte', '>=', '600')->where('numCompte', '<', '700')->sum('debit');
        return $charge;
    }

    public static function getTotalProduit(){
        $produit = FctCompte::where('numCompte', '>=', '700')->where('numCompte', '<', '800')->sum('credit');
        return $produit;
    }

    public static function getTotalResultat(){
        $charge = self::getTotalCharge();
        $produit = self::getTotalProduit();

        return $produit - $charge;
    }

    public static function getColorResultat(){
        $charge = self::getTotalCharge();
        $produit = self::getTotalProduit();
        $calc = $produit-$charge;

        if($calc < 0){
            return "#ce0000";
        }elseif($calc == 0){
            return "#0d47a1";
        }else{
            return "#3fbf00";
        }
    }

}
