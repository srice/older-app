<?php

namespace App\Model\GestionAsc\Billetterie;

use App\Model\Beneficiaire\Salarie\Salarie;
use Illuminate\Database\Eloquent\Model;

class BilletSalarie extends Model
{
    protected $guarded = [];

    public function salarie(){
        return $this->belongsTo(Salarie::class, 'salaries_id');
    }

    public function lignes(){
        return $this->hasMany(LigneBilletSalarie::class, 'billet_salarie_id');
    }

    public function reglements(){
        return $this->hasMany(ReglementBilletSalarie::class, 'billet_salarie_id');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateBillet"];
    }
}
