<?php
namespace App\Repository\ComptaAsc\Etat;

use App\Model\ComptaAsc\Etat\AscCompte;

class AscCompteRepository
{
    /**
     * @var AscCompte
     */
    private $ascCompte;

    /**
     * AscCompteRepository constructor.
     * @param AscCompte $ascCompte
     */

    public function __construct(AscCompte $ascCompte)
    {
        $this->ascCompte = $ascCompte;
    }

    public function getCompte($numCompte)
    {
        return $this->ascCompte->newQuery()
            ->where('numCompte', $numCompte)
            ->first();
    }

    public function updateCredit(int $numCompte, $credit)
    {
        return $this->ascCompte->newQuery()
            ->where('numCompte', $numCompte)
            ->first()
            ->update([
                "credit"    => $credit
            ]);
    }

    public function updateDebit(int $numCompte, $debit)
    {
        return $this->ascCompte->newQuery()
            ->where('numCompte', $numCompte)
            ->first()
            ->update([
                "debit"    => $debit
            ]);
    }

}

        