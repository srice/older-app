<?php

namespace App\Model\Beneficiaire\Ad;

use App\Model\Beneficiaire\Salarie\Salarie;
use App\Model\GestionAsc\Billetterie\BilletAd;
use Illuminate\Database\Eloquent\Model;

class AyantDroit extends Model
{
    protected $guarded = [];

    public function salarie(){
        return $this->belongsTo(Salarie::class, 'salaries_id');
    }

    public function billet(){
        return $this->belongsTo(BilletAd::class, 'ads_id');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateNaissance"];
    }
}
