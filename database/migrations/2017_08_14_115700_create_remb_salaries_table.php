<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRembSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remb_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numRembSalarie');
            $table->integer('salaries_id');
            $table->timestamp('dateRemb')->nullable();
            $table->string('totalRemb');
            $table->integer('etatRemb')->default(0); //0: Brouillon | 1: Valider | 2: Partiellement remboursé |3: Remboursé
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remb_salaries');
    }
}
