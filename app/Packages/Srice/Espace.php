<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 27/06/2017
 * Time: 00:55
 */

namespace App\Packages\Srice;



class Espace extends Api
{
    protected $espaces_id;

    public function espace(){
        $espace = $this->db->table('espaces')->where('comites_id', $this->auth['api_comite_id'])->first();
        $this->espaces_id = $espace->id;
        return $espace;
    }

    public function application(){
        $application = $this->db->table('espace_apps')->where('espaces_id', $this->espaces_id)->first();
        return $application;
    }

    public function installation(){
        $install = $this->db->table('espace_installs')->where('espaces_id', $this->espaces_id)->get();
        return $install;
    }

    public function licence(){
        $licence = $this->db->table('espace_licences')->where('espaces_id', $this->espaces_id)->first();
        return $licence;
    }

    public function modules(){
        $modules = $this->db->table('espace_modules')->where('espaces_id', $this->espaces_id)->get();
        return $modules;
    }

    public function listeModule(){
        $espace = $this->espace();
        $modules = $this->db->table('espace_modules')
            ->where('espaces_id', $espace->id)
            ->join('modules', 'espace_modules.modules_id', '=', 'modules.id')->get();
        return $modules;
    }

    public function module($idModule){
        $module = $this->db->table('espace_modules')
            ->where('modules_id', $idModule)
            ->join('modules', 'espace_modules.modules_id', '=', 'modules.id')->first();
        return $module;
    }

    public function activeModule($idModule){
        $module = $this->db->table('espace_modules')
            ->where('modules_id', $idModule)
            ->update(["active" => 1]);
        if($module){
            return true;
        }else{
            return false;
        }
    }

    public function desactiveModule($idModule){
        $module = $this->db->table('espace_modules')
            ->where('modules_id', $idModule)
            ->update(["active" => 0]);

        if($module){
            return true;
        }else{
            return false;
        }
    }
}