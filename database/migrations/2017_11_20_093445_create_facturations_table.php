<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tiers_id')->comment("Relation: facturation_tiers");
            $table->integer('typesCompta')->default(0)->comment("0: Asc |1: Fct");
            $table->integer('typesFacture')->comment("0: Client |1: Fournisseurs");
            $table->string('numInvoice');
            $table->timestamp('dateFacture')->nullable();
            $table->string('totalFacture');
            $table->integer('etatFacture')->default(0)->comment("0: Brouillon |1: Valider |2: Terminer");
            $table->integer('sousEtatFacture')->default(0)->comment("0: En attente de validation |1: En attente de Paiement |2: Payer |3: Impayer");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturations');
    }
}
