<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComptaFctCompte extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fct_comptes')->insert(["numCompte" => "101"]);
        DB::table('fct_comptes')->insert(["numCompte" => "110"]);
        DB::table('fct_comptes')->insert(["numCompte" => "119"]);
        DB::table('fct_comptes')->insert(["numCompte" => "120"]);
        DB::table('fct_comptes')->insert(["numCompte" => "129"]);
        DB::table('fct_comptes')->insert(["numCompte" => "168"]);
        DB::table('fct_comptes')->insert(["numCompte" => "201"]);
        DB::table('fct_comptes')->insert(["numCompte" => "205"]);
        DB::table('fct_comptes')->insert(["numCompte" => "206"]);
        DB::table('fct_comptes')->insert(["numCompte" => "211"]);
        DB::table('fct_comptes')->insert(["numCompte" => "215"]);
        DB::table('fct_comptes')->insert(["numCompte" => "345"]);
        DB::table('fct_comptes')->insert(["numCompte" => "401"]);
        DB::table('fct_comptes')->insert(["numCompte" => "410"]);
        DB::table('fct_comptes')->insert(["numCompte" => "410DIV"]);
        DB::table('fct_comptes')->insert(["numCompte" => "411"]);
        DB::table('fct_comptes')->insert(["numCompte" => "471"]);
        DB::table('fct_comptes')->insert(["numCompte" => "512"]);
        DB::table('fct_comptes')->insert(["numCompte" => "530"]);
        DB::table('fct_comptes')->insert(["numCompte" => "580"]);
        DB::table('fct_comptes')->insert(["numCompte" => "601"]);
        DB::table('fct_comptes')->insert(["numCompte" => "607"]);
        DB::table('fct_comptes')->insert(["numCompte" => "611"]);
        DB::table('fct_comptes')->insert(["numCompte" => "613"]);
        DB::table('fct_comptes')->insert(["numCompte" => "615"]);
        DB::table('fct_comptes')->insert(["numCompte" => "616"]);
        DB::table('fct_comptes')->insert(["numCompte" => "618"]);
        DB::table('fct_comptes')->insert(["numCompte" => "622"]);
        DB::table('fct_comptes')->insert(["numCompte" => "623"]);
        DB::table('fct_comptes')->insert(["numCompte" => "625"]);
        DB::table('fct_comptes')->insert(["numCompte" => "626"]);
        DB::table('fct_comptes')->insert(["numCompte" => "627"]);
        DB::table('fct_comptes')->insert(["numCompte" => "628"]);
        DB::table('fct_comptes')->insert(["numCompte" => "635"]);
        DB::table('fct_comptes')->insert(["numCompte" => "638"]);
        DB::table('fct_comptes')->insert(["numCompte" => "651"]);
        DB::table('fct_comptes')->insert(["numCompte" => "661"]);
        DB::table('fct_comptes')->insert(["numCompte" => "668"]);
        DB::table('fct_comptes')->insert(["numCompte" => "671"]);
        DB::table('fct_comptes')->insert(["numCompte" => "678"]);
        DB::table('fct_comptes')->insert(["numCompte" => "706"]);
        DB::table('fct_comptes')->insert(["numCompte" => "740"]);
        DB::table('fct_comptes')->insert(["numCompte" => "771"]);
        DB::table('fct_comptes')->insert(["numCompte" => "778"]);
    }
}
