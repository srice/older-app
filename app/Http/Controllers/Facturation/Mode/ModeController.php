<?php

namespace App\Http\Controllers\Facturation\Mode;

use App\Model\Facturation\FacturationModeReglement;
use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class ModeController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'facturation';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $modes = FacturationModeReglement::all();

        return view('Facturation.Mode.index', compact('config', 'modes'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "libelle"   => "required"
        ]);

        $mode = FacturationModeReglement::create(["libelle" => $request->libelle]);

        if($mode){
            Toastr::success("Le mode de règlement à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du mode de règlement");
            return redirect()->back();
        }
    }

    public function edit($modes_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $mode = FacturationModeReglement::find($modes_id);

        return view('Facturation.Mode.edit', compact('config', 'mode'));
    }

    public function update(Request $request, $modes_id){
        $this->validate($request, [
            "libelle"   => "required"
        ]);

        $mode = FacturationModeReglement::find($modes_id)->update(["libelle" => $request->libelle]);

        if($mode){
            Toastr::success("Le mode de règlement à été édité");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'édition du mode de règlement");
            return redirect()->back();
        }
    }

    public function delete($modes_id){
        $mode = FacturationModeReglement::find($modes_id);
        $mode->delete();

        if($mode){
            Toastr::success("Le mode de règlement <strong>".$mode->libelle."</strong> à été supprimé !");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du mode de règlement !");
            return redirect()->back();
        }
    }
}
