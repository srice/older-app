<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 27/06/2017
 * Time: 00:51
 */

namespace App\Packages\Srice;


class Contract extends Api
{
    public function contracts(){
        $contracts = $this->db->table('contrats')->where('comites_id', $this->auth['api_comite_id'])->get();
        return $contracts;
    }

    public function contract($contrat_id){
        $contrat = $this->db->table('contrats')->find($contrat_id);
        return $contrat;
    }
}