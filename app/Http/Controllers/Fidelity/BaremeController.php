<?php

namespace App\Http\Controllers\Fidelity;

use App\Model\Fidelity\FidelityBareme;
use App\Model\Fidelity\FidelityCompteur;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class BaremeController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'fidelity';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index($compteurs_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $compteur = FidelityCompteur::find($compteurs_id)->load('baremes');
        $baremes = FidelityBareme::where('compteurs_id', $compteurs_id)->get();


        return view('Fidelity.Bareme.index', compact('config', 'compteur', 'baremes'));
    }

    public function store(Request $request, $compteurs_id){
        $this->validate($request, [
            "subStart"  => "required",
            "subEnd"    => "required",
            "nbPoint"   => "required"
        ]);

        $bareme = FidelityBareme::create([
            "compteurs_id"=> $compteurs_id,
            "subStart"  => $request->subStart,
            "subEnd"    => $request->subEnd,
            "nbPoint"   => $request->nbPoint
        ]);

        if($bareme){
            Toastr::success("Le barême à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du barême !");
            return redirect()->back();
        }
    }

    public function delete($compteurs_id, $baremes_id){
        $bareme = FidelityBareme::find($baremes_id)->delete();

        if($bareme){
            Toastr::success("Le barême à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du barême !");
            return redirect()->back();
        }
    }
}
