<?php

namespace App\Model\ComptaFct\Configuration\Plan;

use Illuminate\Database\Eloquent\Model;

class FctPlanClasse extends Model
{
    protected $guarded = [];

    public function sectors(){
        return $this->hasMany(FctPlanSector::class, 'numClasse');
    }
}
