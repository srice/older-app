<?php

namespace App\Http\Controllers\GestionAsc;

use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Model\GestionAsc\Prestation\Prestation;
use App\Model\GestionAsc\Prestation\PrestationDate;
use App\Model\GestionAsc\Prestation\PrestationTarif;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GestionAscController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'gestionasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function dashboard(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $prestationValidites = PrestationDate::where('dateEnd', '<=', Carbon::now())->get()->load('prestation');
        $prestationStocks = PrestationTarif::where('stockActuel', '==', 0)->get()->load('prestation');


        return view('GestionAsc.dashboard', compact('config', 'prestationValidites', 'prestationStocks'));
    }

    public static function countPrestation(){
        $count = Prestation::all()->count();
        return $count;
    }

    public static function countPrestaOutdate(){
        $count = PrestationDate::where('dateEnd', '<=', Carbon::now())->get()->count();
        return $count;
    }

    public static function countPrestationUnpack(){
        $count = PrestationTarif::where('stockActuel', '==', 0)->get()->count();
        return $count;
    }
}
