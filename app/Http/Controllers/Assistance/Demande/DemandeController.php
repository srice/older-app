<?php

namespace App\Http\Controllers\Assistance\Demande;

use App\Model\GestionAsc\Configuration\ConfigAscBillet;
use App\Model\GestionAsc\Configuration\ConfigAscPresta;
use App\Model\GestionAsc\Configuration\ConfigAscRemb;
use App\Packages\Srice\Api;
use App\Packages\Srice\Comite;
use App\Packages\Srice\Demande;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class DemandeController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = '';
        $this->modules = $espace->listeModule();
        $this->configuration = [
            "presta" => ConfigAscPresta::find(1),
            "billet" => ConfigAscBillet::find(1),
            "remb"   => ConfigAscRemb::find(1)
        ];
    }

    public function index(Demande $demande){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $demandes = $demande->demandes();

        return view('Assistance.Demande.index', compact('config', 'demandes'));

    }

    public function create(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        return view('Assistance.Demande.create', compact('config'));
    }

    public function store(Request $request, Comite $comite, Demande $demande){
        $this->validate($request, [
            "titleDemande"  => "required",
            "descDemande"   => "required|min:20"
        ]);
        $com = $comite->comite();
        $st = $demande->postDemande($com->id, $request->titleDemande, $request->descDemande);
        if($st == true){
            Toastr::success("Votre demande à été transmis à notre équipe technique !");
            return redirect()->route('demandes.index');
        }else{
            Toastr::error("Erreur lors de la transmission de votre demande à notre équipe technique !");
            return redirect()->back();
        }
    }

    public function show($id, Demande $demande){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];
        $dem = $demande->demande($id);

        return view('Assistance.Demande.show', compact('config', 'dem'));
    }
}
