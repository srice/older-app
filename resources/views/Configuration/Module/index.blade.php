@extends('template')
@section('title')
    Modules
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="panel">
        <header class="panel-header">
            <h3 class="panel-title">Liste des modules</h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="listeModule" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Désignation</th>
                            <th class="text-xs-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($modules as $module)
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-1 text-xs-center">
                                        <img src="//old-gest.{{ env('APP_DOMAIN') }}/storage/media/img/module/{{ $module->image }}" class="img-responsive" width="45" alt="">
                                    </div>
                                    <div class="col-md-5">
                                        <strong>{{ $module->designation }}</strong><br>
                                        <i>{{ $module->description }}</i>
                                    </div>
                                    <div class="col-md-6">
                                        <strong>Version:</strong> {{ $module->version }}<br>
                                        <strong>Release:</strong> {!! \App\Http\Controllers\Configuration\Module\ModuleOtherController::releaseModuleTag($module->release) !!}
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a id="btnFicheProjet" data-id="{{ $module->modules_id }}" class="btn btn-icon btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Voir la fiche du projet"><i class="fa fa-search"></i></a>
                                @if($module->release >= 0 && $module->release <= 3)

                                @endif
                                @if($module->release == 4 || $module->release == 5)
                                    @if($module->checkout == 0)
                                        <a id="btnClefAccess" data-id="{{ $module->modules_id }}" class="btn btn-icon bg-yellow-500" data-toggle="tooltip" data-placement="bottom" data-title="Demander une clef d'accès"><i class="fa fa-key"></i> </a>
                                    @else
                                        @if($module->state == 0)
                                            <a href="{{ route('config.module.activeModule', $module->modules_id) }}" class="btn btn-icon btn-success" data-toggle="tooltip" data-placement="bottom" data-title="Activer le module"><i class="fa fa-power-off"></i> </a>
                                        @else
                                            <a href="{{ route('config.module.desactiveModule', $module->modules_id) }}" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" data-title="Désactiver le module"><i class="fa fa-power-off"></i> </a>
                                        @endif
                                    @endif
                                @endif
                                @if($module->release == 6)
                                    @if($module->checkout == 0)
                                        <a id="btnCheckout" data-id="{{ $module->modules_id }}" class="btn btn-icon bg-pink-500 white" data-toggle="tooltip" data-placement="bottom" data-title="Acheter le module"><i class="fa fa-shopping-basket"></i> </a>
                                    @else
                                        @if($module->state == 0)
                                            <a href="{{ route('config.module.activeModule', $module->modules_id) }}" class="btn btn-icon btn-success" data-toggle="tooltip" data-placement="bottom" data-title="Activer le module"><i class="fa fa-power-off"></i> </a>
                                        @else
                                            <a href="{{ route('config.module.desactiveModule', $module->modules_id) }}" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" data-title="Désactiver le module"><i class="fa fa-power-off"></i> </a>
                                        @endif
                                    @endif
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="ficheProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="modalProject">

            </div>
        </div>
    </div>
    <div class="modal fade" id="clefAccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="modalClefAccess">

            </div>
        </div>
    </div>
    <div class="modal fade" id="checkout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="modalCheckout">

            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/custom/js/configuration/module/index.js"></script>
@stop