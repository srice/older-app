<?php
class Chatbot{

    protected $auth = [
        "access_token"  => null
    ];

    protected $client;

    public function __construct(array $auth = []){
        $auth = collect($auth);

        if($auth->isEmpty() === false){
            $this->auth = collect($auth)->only([
                'access_token'
            ]);
        }else{
            $this->loadAuthFromConfig();
        }

        $this->client = new \ApiAi\Client($this->auth['access_token']);
    }

    protected function loadAuthFromConfig(){
        $this->auth['access_token'] = config('chatbot.access_token');

        return $this;
    }


}