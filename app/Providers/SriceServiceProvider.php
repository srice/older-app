<?php

namespace App\Providers;

use App\Packages\Srice\Api;
use Illuminate\Support\ServiceProvider;

class SriceServiceProvider extends ServiceProvider
{
    protected $defer = false;
    protected $configName = 'api';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__.'/../config/'.$this->configName.'.php';
        $this->publishes([$configPath => config_path($this->configName . '.php')], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__.'/../../config/'.$this->configName.'.php';
        $this->mergeConfigFrom($configPath, $this->configName);
        $this->app->bind(Api::class, Api::class);
        $this->app->singleton('api', function($app){
            return new Api();
        });

        $this->app->alias('api', Api::class);
    }
}
