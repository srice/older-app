<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 27/06/2017
 * Time: 00:29
 */

namespace App\Packages\Srice;


use Illuminate\Support\Facades\DB;

class Comite extends Api
{
    /**
     * Unique
     * @return mixed
     */
    public function comite(){
        $comite = $this->db->table('comites')->where('id', $this->auth['api_comite_id'])->first();
        return $comite;
    }

    /**
     * Liste des banques
     * @return mixed
     * @internal param $comites_id
     */
    public function banques(){
        $banques = $this->db->table('comite_banques')->where('comites_id', $this->auth['api_comite_id'])->get();
        return $banques;
    }

    /**
     * Liste des contacts
     * @return mixed
     * @internal param $comites_id
     */
    public function contacts(){
        $contacts = $this->db->table('comite_contacts')->where('comites_id', $this->auth['api_comite_id'])->get();
        return $contacts;
    }

    /**
     * Information d'un contact
     * @return mixed
     * @internal param $contact_id
     */
    public function contact($contacts_id){
        $contact = $this->db->table('comite_contacts')->find($contacts_id);
        return $contact;
    }

    public function firstContact(){
        $contact = $this->db->table('comite_contacts')->where('comites_id', $this->auth['api_comite_id'])->first();
        return $contact;
    }

    /**
     * Information social du contact
     * @param $contact_id
     * @return mixed
     */
    public function social($contact_id){
        $social = $this->db->table('comite_socials')->where('contacts_id', $contact_id)->first();
        return $social;
    }
}