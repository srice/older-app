<?php

namespace App\Http\Controllers\Beneficiaire\Configuration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigurationOtherController extends ConfigurationController
{
    // Route



    // Static
    public static function soldeIsActive($value){
        if($value == 1){
            return true;
        }else{
            return false;
        }
    }

    public static function activateurIsActive($activateur)
    {
        if($activateur == 1){
            return true;
        }else{
            return false;
        }
    }

    public static function ageLimitIsActive($adAgeLimit)
    {
        if($adAgeLimit == 1){
            return true;
        }else{
            return false;
        }
    }

}
