<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Admin Responsive web app kit</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
    <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
            <tbody>
            <tr>
                <td style="vertical-align: top; padding-bottom:30px;" align="center">
                    <a href="javascript:void(0)" target="_blank">
                        NOTIFICATION JOURNALIERE D'EXECUTION DE TACHE
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <div style="padding: 40px; background: #fff;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tbody>
                <tr>
                    <td>
                        <p>Bonjour,</p>
                        <p>Nous sommes le <strong>{{ \Carbon\Carbon::now()->format("d/m/Y") }}</strong> et voici la liste des exécution journalière pour l'espace <strong>{{ env('APP_URL') }}</strong>:</p>
                        <table width="100%">
                            <thead>
                                <tr>
                                    <th>Nom de la tache</th>
                                    <td>Description de la tache</td>
                                    <td>Etat de la tache</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($automate as $item)
                                    <tr>
                                        <td>{{ $item->commandAutomate }}</td>
                                        <td>{{ $item->descAutomate }}</td>
                                        <td>{{ $item->etatAutomate }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
            <p> Powered by Themedesigner.in <br>
                <a href="javascript: void(0);" style="color: #b2b2b5; text-decoration: underline;">Unsubscribe</a> </p>
        </div>
    </div>
</div>
</body>
</html>