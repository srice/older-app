<?php

namespace App\Http\Controllers\ComptaAsc\Configuration\Plan;

use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanClasse;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanSector;
use App\Packages\Srice\Espace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class SectorController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptaasc';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index($classes_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $classe = ComptaPlanClasse::where('numClasse', $classes_id)->first();
        $sectors = ComptaPlanSector::where('numClasse', $classes_id)->get();

        return view('ComptaAsc.Configuration.Plan.Sector.index', compact('config', 'sectors', 'classe'));
    }

    public function store(Request $request, $classes_id){
        $this->validate($request, [
            "numSector"     => "required",
            "nameSector"    => "required|min:2"
        ]);

        $add = ComptaPlanSector::create([
            "numClasse" => $classes_id,
            "numSector" => $request->numSector,
            "nameSector"=> $request->nameSector
        ]);

        if($add){
            Toastr::success("Le secteur à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout du secteur !");
            return redirect()->back();
        }
    }

    public function edit($classes_id, $sectors_id){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 1
        ];

        $classe = ComptaPlanClasse::where('numClasse', $classes_id)->first();
        $sector = ComptaPlanSector::where('numSector', $sectors_id)->first();

        return view('ComptaAsc.Configuration.Plan.Sector.edit', compact('config', 'classe', 'sector'));
    }

    public function update(Request $request, $classes_id, $sectors_id){
        $this->validate($request, [
            "numSector"     => "required",
            "nameSector"    => "required|min:2"
        ]);

        $edit = ComptaPlanSector::where('numSector', $sectors_id)->first()->update([
            "numSector"     => $request->numSector,
            "nameSector"    => $request->nameSector
        ]);

        if($edit){
            Toastr::success("Le secteur à été édité");
            return redirect()->route('comptaAsc.config.plan.sector.index', $classes_id);
        }else{
            Toastr::error("Erreur lors de l\'édition du secteur !");
            return redirect()->back();
        }
    }
}
