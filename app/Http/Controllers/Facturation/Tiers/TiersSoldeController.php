<?php

namespace App\Http\Controllers\Facturation\Tiers;

use App\Http\Controllers\OtherController;
use App\Model\Facturation\FacturationSolde;
use App\Model\Facturation\FacturationTier;
use App\Packages\Srice\Comite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class TiersSoldeController extends TiersController
{
    // Route

    public function soldePrint($tiers_id, Comite $comite){
        $soldes = FacturationSolde::where('tiers_id', $tiers_id)->orderBy('id', 'desc')->get();
        $tier = FacturationTier::find($tiers_id);

        $infoCom = $comite->comite();
        $name = "Historique du compte de Tier";
        PDF::setOptions(['defaultFont' => 'arial']);
        $pdf = PDF::loadView('Facturation.Tiers.pdf', compact('soldes', 'tier', 'infoCom', 'name'));
        return $pdf->stream($name);
    }

    // Static
    public static function typeDocument($value){
        switch ($value){
            case 0: return "Facture";
            case 1: return "Règlement";
        }
    }

    public static function formSolde($value){
        if($value < 0){
            return '<span class="text-danger">'.OtherController::euro($value).'</span>';
        }else{
            return '<span class="text-success">'.OtherController::euro($value).'</span>';
        }
    }

    public static function lastSolde($tiers_id){
        $solde = FacturationSolde::where('tiers_id', $tiers_id)->get()->last();

        if(!empty($solde)){
            if($solde->soldeEcriture < 0){
                return '<span class="text-danger">'.OtherController::euro($solde->soldeEcriture).'</span>';
            }else{
                return '<span class="text-success">'.OtherController::euro($solde->soldeEcriture).'</span>';
            }
        }else{
            return '<span class="text-success">0,00 €</span>';
        }
    }

    public static function formSoldePdf($soldeEcriture)
    {
        if($soldeEcriture < 0){
            return '<span style="color: #ff2d28;">'.OtherController::euro($soldeEcriture).'</span>';
        }else{
            return '<span style="color: #00cb26;">'.OtherController::euro($soldeEcriture).'</span>';
        }
    }

}
