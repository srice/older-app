<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 03/07/2017
 * Time: 20:12
 */

namespace App\Packages\Srice;


use Carbon\Carbon;
use Illuminate\Support\Facades\Request;

class Demande extends Api
{
    public function demandes(){
        $demandes = $this->db->table('demandes')->where('comites_id', $this->auth['api_comite_id'])->get();
        return $demandes;
    }

    public function demande($demandes_id){
        $demande = $this->db->table('demandes')->where('id', $demandes_id)->first();
        return $demande;
    }

    public function postDemande($comites_id, $titleDemande, $descDemande){

        $comite = $this->db->table('comites')->where('id', $comites_id)->first();

        $post = $this->db->table('demandes')->insert([
            "comites_id"    => $comites_id,
            "titleDemande"  => $titleDemande,
            "descDemande"   => $descDemande,
            "etatDemande"   => 0,
            "created_at"    => Carbon::now(),
            "updated_at"    => Carbon::now()
        ]);

        $last = $this->db->table('demandes')->get()->last();

        $comment = $this->db->table('demande_comments')->insert([
            "demandes_id"   => $last->id,
            "author"        => $comite->nameComite,
            "comment"       => $descDemande,
            "created_at"    => Carbon::now(),
            "updated_at"    => Carbon::now()
        ]);

        return $comment;

    }
}