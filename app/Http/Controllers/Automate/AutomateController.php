<?php

namespace App\Http\Controllers\Automate;

use App\Model\Automate\Automate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AutomateController extends Controller
{
    public static function now(){
        NowController::exec();
    }
    public static function hourly(){
        HourlyController::exec();
    }
    public static function daily(){
        DailyController::exec();
    }
    public static function fifty(){

    }

    public static function datastore($command, $desc, $statement){
        Automate::create([
            "commandAutomate"   => $command,
            "descAutomate"      => $desc,
            "etatAutomate"      => $statement
        ]);
    }
}
