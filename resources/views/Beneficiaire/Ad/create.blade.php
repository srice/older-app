@extends('template')
@section('title')
    Création d'un ayant droit
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
@stop

@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield('title')</h3>
        </header>
        <div class="panel-body">
            {{ Form::model($salarie, ["route" => ["ad.store", $salarie->id], "class" => "form-horizontal"]) }}
            <div class="form-group row">
                {{ Form::label('nom', 'Nom *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::text('nom', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('prenom', 'Prénom *', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::text('prenom', null, ["class" => "form-control round"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('dateNaissance', 'Date de naissance', ["class" => "form-control-label col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::text('dateNaissance', null, ["class" => "form-control round datepicker", "data-plugin" => "datepicker"]) }}
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/custom/js/beneficiaire/ad/create.js"></script>
@stop