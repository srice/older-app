<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 15/08/2017
 * Time: 21:57
 */

namespace App\Packages\Bankin;


class Bank extends Appel
{
    public function listBanks(){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
            "limit"         => 50
        ];

        $call = $this->get('banks?', $params);

        return $call;
    }

    public function bank($bankId){
        $params = [
            "client_id"     => $this->auth['clientId'],
            "client_secret" => $this->auth['clientSecret'],
        ];

        $call = $this->get('banks/'.$bankId.'?', $params);

        return $call;
    }
}