<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 15/08/2017
 * Time: 20:47
 */

namespace App\Packages\Bankin;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Response;
use Kamaln7\Toastr\Facades\Toastr;

class Appel extends Config
{
    public function get($uri, array $params = [], $authenticable = false, $bearer = "", $redirect = false){
        $ch = curl_init();
        $params = http_build_query($params);
        curl_setopt_array($ch, [
            CURLOPT_URL             => $this->auth['endpoint'].$uri.$params,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_CUSTOMREQUEST   => "GET"
        ]);

        if($authenticable == false){
            $headers = [];
            $headers[] = "Bankin-Version: 2019-02-18";
            $headers[] = "Accept-Language: fr";
        }else{
            $headers = [];
            $headers[] = "Authorization: Bearer ".$bearer;
            $headers[] = "Bankin-Version: 2019-02-18";
            $headers[] = "Accept-Language: fr";

        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if(curl_errno($ch)){
            return Toastr::warning(curl_error($ch));
        }else{
            if($redirect == true){
                return header('Location: '.$this->auth['endpoint'].$uri.$params);
            }else{
                curl_close($ch);
                return json_decode($result);
            }
        }
    }

    public function post($uri, array $params = [], $authenticable = false, $bearer = ""){
        $ch = curl_init();
        $params = http_build_query($params);
        curl_setopt_array($ch, [
            CURLOPT_URL             => $this->auth['endpoint'].$uri.$params,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POST            => true
        ]);

        if($authenticable == false){
            $headers = [];
            $headers[] = "Bankin-Version: 2019-02-18";
            $headers[] = "Accept-Language: fr";
        }else{
            $headers = [];
            $headers[] = "Authorization: Bearer ".$bearer;
            $headers[] = "Bankin-Version: 2019-02-18";
            $headers[] = "Accept-Language: fr";

        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if(curl_errno($ch)){
            return Toastr::warning(curl_error($ch));
        }else{
            curl_close($ch);
            return json_decode($result);
        }
    }

    public function put($uri, array $params = [], $authenticable = false, $bearer = ""){
        $ch = curl_init();
        $params = http_build_query($params);
        curl_setopt_array($ch, [
            CURLOPT_URL             => $this->auth['endpoint'].$uri.$params,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_CUSTOMREQUEST   => "PUT"
        ]);

        if($authenticable == false){
            $headers = [];
            $headers[] = "Bankin-Version: 2019-02-18";
            $headers[] = "Accept-Language: fr";
        }else{
            $headers = [];
            $headers[] = "Authorization: Bearer ".$bearer;
            $headers[] = "Bankin-Version: 2019-02-18";
            $headers[] = "Accept-Language: fr";

        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if(curl_errno($ch)){
            return Toastr::warning(curl_error($ch));
        }else{
            curl_close($ch);
            return json_decode($result);
        }
    }
    public function delete($uri, array $params = [], $authenticable = false, $bearer = ""){
        $ch = curl_init();
        $params = http_build_query($params);
        curl_setopt_array($ch, [
            CURLOPT_URL             => $this->auth['endpoint'].$uri.$params,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_CUSTOMREQUEST   => "DELETE"
        ]);

        if($authenticable == false){
            $headers = [];
            $headers[] = "Bankin-Version: 2019-02-18";
            $headers[] = "Accept-Language: fr";
        }else{
            $headers = [];
            $headers[] = "Authorization: Bearer ".$bearer;
            $headers[] = "Bankin-Version: 2019-02-18";
            $headers[] = "Accept-Language: fr";

        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if(curl_errno($ch)){
            return Toastr::warning(curl_error($ch));
        }else{
            curl_close($ch);
            return json_decode($result);
        }
    }
}