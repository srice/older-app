<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ConfigBeneficiaireSeeder::class);
        $this->call(SalarieSeeder::class);

        $this->call(ConfigAscPrestaSeeder::class);
        $this->call(ConfigAscBilletSeeder::class);
        $this->call(ConfigAscRembSeeder::class);
        $this->call(FamillePresta::class);

        $this->call(ComptaAscPlanClasse::class);
        $this->call(ComptaAscPlanSector::class);
        $this->call(ComptaAscPlanCompte::class);
        $this->call(ComptaAscCompte::class);
        $this->call(ComptaAscBilanInitial::class);

        $this->call(ComptaFctPlanClasse::class);
        $this->call(ComptaFctPlanSector::class);
        $this->call(ComptaFctPlanCompte::class);
        $this->call(ComptaFctCompte::class);
        $this->call(ComptaFctBilanInitial::class);
    }
}
