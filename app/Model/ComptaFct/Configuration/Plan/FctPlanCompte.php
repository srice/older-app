<?php

namespace App\Model\ComptaFct\Configuration\Plan;

use App\Model\ComptaFct\Configuration\Bilan\FctBilanInitial;
use App\Model\ComptaFct\Etat\FctCompte;
use App\Model\ComptaFct\Journal\FctJournalAchat;
use Illuminate\Database\Eloquent\Model;

class FctPlanCompte extends Model
{
    protected $guarded = [];

    public function sector(){
        return $this->belongsTo(FctPlanSector::class, 'numSector');
    }

    public function achats(){
        return $this->hasMany(FctJournalAchat::class, 'numSector');
    }

    public function fct_compte(){
        return $this->belongsTo(FctCompte::class, 'numCompte');
    }

    public function bilan_initial(){
        return $this->belongsTo(FctBilanInitial::class, 'numCompte');
    }
}
