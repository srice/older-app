@extends('template')
@section("title")
    Gestion des chèques vacances (ANCV)
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/select2/select2.css">
@stop
@section("content")
    <div class="row">
        <div class="col-md-8">
            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="panel-title">Création d'un Achat</h3>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" action="{{ route("Ancv.Achat.store") }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="" class="form-control-label col-md-3">Solution</label>
                            <div class="col-md-9">
                                <select id="field_solution" class="form-control round selectpicker" data-plugin="select2" data-live-search="true" name="solution_id" onchange="getSolution()" required>
                                    <option value=""></option>
                                    @foreach($solutions as $solution)
                                        <option value="{{ $solution['name'] }}">{{ $solution['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="form-control-label col-md-3">Montant Total</label>
                            <div class="col-md-3">
                                <input id="field_part_ce" type="text" class="form-control round" name="total" onchange="getTotal()" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                &nbsp;
                            </div>
                            <div class="col-md-1 text-right">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Valider</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="panel-title">Récapitulatif</h3>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <ul>
                        <li id="designation"></li>
                    </ul>
                    <hr>
                    <div class="text-right">
                        <span style="font-weight: bold; font-size: 13px;">Total</span>
                        <span id="total" style="font-weight: bold; font-size: 13px; text-align: right;">0,00 €</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/select2/select2.full.min.js"></script>
    <script src="/assets/global/js/Plugin/select2.js"></script>
    <script>
        function getSolution()
        {
            let solution = $("#field_solution").val()
            let div = $("#designation");

            div.html(solution)
        }
        function getTotal()
        {
            let part_ce = parseFloat($("#field_part_ce").val())
            let total = (part_ce)

            $("#total").html(new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'EUR'}).format(total))
        }

        (function ($) {
            $(".selectpicker").select2()
        })(jQuery)

    </script>
@stop    