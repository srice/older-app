<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComptaAscPlanClasse extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compta_plan_classes')->insert([
            "numClasse"     => 1,
            "nameClasse"    => "CAPITAUX"
        ]);
        DB::table('compta_plan_classes')->insert([
            "numClasse"     => 2,
            "nameClasse"    => "IMMOBILISATIONS"
        ]);
        DB::table('compta_plan_classes')->insert([
            "numClasse"     => 3,
            "nameClasse"    => "STOCKS"
        ]);
        DB::table('compta_plan_classes')->insert([
            "numClasse"     => 4,
            "nameClasse"    => "TIERS"
        ]);
        DB::table('compta_plan_classes')->insert([
            "numClasse"     => 5,
            "nameClasse"    => "FINANCES"
        ]);
        DB::table('compta_plan_classes')->insert([
            "numClasse"     => 6,
            "nameClasse"    => "CHARGES"
        ]);
        DB::table('compta_plan_classes')->insert([
            "numClasse"     => 7,
            "nameClasse"    => "PRODUITS"
        ]);
        DB::table('compta_plan_classes')->insert([
            "numClasse"     => 8,
            "nameClasse"    => "SPECIAUX"
        ]);
    }
}
