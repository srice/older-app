<?php

namespace App\Model\Facturation;

use Illuminate\Database\Eloquent\Model;

class FacturationTier extends Model
{
    protected $guarded = [];

    public function factures(){
        return $this->hasMany(Facturation::class, 'tiers_id');
    }

    public function soldes(){
        return $this->hasMany(FacturationSolde::class, 'tiers_id');
    }
}
