<?php

namespace App\Http\Controllers;

use Gmopx\LaravelOWM\LaravelOWM;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class WeatherController extends Controller
{
    public function get(){
        $lown = new LaravelOWM();
        $forecasts = $lown->getWeatherForecast('Les Sables d olonne', 'fr');
        $current = $forecasts->current();
        $city = $forecasts->city;
        ob_start();
        ?>
        <div class="card card-inverse card-shadow card-primary white text-xs-center bg-blue-600">
            <div class="card-block p-25">
                <h3 class="white">
                    <span class="font-size-30"><?= $city->name; ?>, </span><?= $city->country; ?>
                </h3>
                <p class="weather-day-date m-b-30">
                    <?= $current->time->day->format('d/m/Y'); ?>
                </p>
                <div class="m-b-30">
                    <i class="<?= WeatherController::icon($current->weather->description); ?> font-size-70 m-r-40 text-top"></i>
                    <div class="inline-block">
                              <span class="font-size-50"><?= $current->temperature->now->getValue(); ?>°
                                <span class="font-size-40">C</span>
                              </span>
                        <p class="text-xs-left"><?= Str::upper($current->weather->description); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public static function icon($value){
        switch ($value){
            case 'légères pluies': return 'wi-showers';
            case 'partiellement ensoleillé': return 'wi-day-cloudy';
            case 'nuageux': return 'wi-cloud';
            case 'couvert': return 'wi-cloudy';
            case 'ciel dégagé': return 'wi-day-cloudy';
            case 'pluies modérées': return 'wi-rain';
            default: return false;
        }
    }
}
