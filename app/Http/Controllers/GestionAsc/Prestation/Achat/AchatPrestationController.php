<?php

namespace App\Http\Controllers\GestionAsc\Prestation\Achat;

use App\Http\Controllers\OtherController;
use App\Model\GestionAsc\Prestation\Achat\Achat;
use App\Model\GestionAsc\Prestation\Achat\AchatPrestation;
use App\Model\GestionAsc\Prestation\Prestation;
use App\Model\GestionAsc\Prestation\PrestationTarif;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class AchatPrestationController extends AchatController
{
    public static function getName($id){
        $presta = Prestation::find($id);
        return $presta->name;
    }

    public static function getPrice($tarifs_id){
        $presta = PrestationTarif::find($tarifs_id);
        return OtherController::euro($presta->price);
    }

    public static function getNameTarif($tarifs_id){
        $presta = PrestationTarif::find($tarifs_id);
        return $presta->libelle;
    }

    public static function getNameFamilleSlug($prestations_id){
        $presta = Prestation::find($prestations_id)->load('famille');
        $famille = str_slug($presta->famille->name);
        return $famille;
    }

    public static function getTotalLigne($tarifs_id, $qte, $euro = false){
        $presta = PrestationTarif::find($tarifs_id);
        $calc = $presta->price * $qte;
        if($euro == true){
            return OtherController::euro($calc);
        }else{
            return $calc;
        }
    }

    public static function countQuantity($achats_id){
        $count = AchatPrestation::where('achats_id', $achats_id)->get()->sum('qte');
        return $count;
    }

    public function addPrestation(Request $request, $achats_id){
        $this->validate($request, [
            "tarifs_id" => "required",
            "qte"       => "required|sup:1"
        ]);

        $tarif = PrestationTarif::find($request->tarifs_id);
        $prestations_id = $tarif->prestations_id;

        $total = self::getTotalLigne($request->tarifs_id, $request->qte);

        $achat = Achat::find($achats_id);
        $totalAchat = $achat->totalAchat;
        $newTotal = $total+$totalAchat;
        $achat->update([
            "totalAchat"    => $newTotal
        ]);

        $presta = AchatPrestation::create([
            "achats_id"         => $achats_id,
            "prestations_id"    => $prestations_id,
            "qte"               => $request->qte,
            "total"             => $total,
            "tarifs_id"         => $request->tarifs_id
        ]);

        if($achat && $presta){
            Toastr::success("La prestation à été ajouté");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l\'ajout de la prestation !");
            return redirect()->back();
        }
    }

    public function deletePrestation($achats_id, $id){
        $ligne = AchatPrestation::findOrFail($id);
        $achat = Achat::findOrFail($achats_id);

        $newTotal = $achat->totalAchat - $ligne->total;

        $achat->update([
            "totalAchat"    => $newTotal
        ]);

        $ligne->delete();

        if($achat && $ligne){
            Toastr::success("La prestation à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression de la prestation !");
            return redirect()->back();
        }
    }
}
