<?php

namespace App\Http\Controllers\Assistance\Ticket;

use App\Packages\Srice\Ticket;
use App\presenters\DatePresenter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketOtherController extends TicketController
{
    public static function nameService($services_id){
        $ticket = new Ticket();
        $service = $ticket->service($services_id);
        return $service->nameService;
    }

    public static function nameDepartement($departements_id){
        $ticket = new Ticket();
        $departement = $ticket->departement($departements_id);
        return $departement->nameDepartement;
    }

    public static function namePriorities($priorities_id){
        switch ($priorities_id){
            case 0: return "Basse";
            case 1: return "Moyenne";
            case 2: return "Haute";
            case 3: return "Urgente";
            default: return false;
        }
    }

    public static function lastNameChat($tickets_id){
        $ticket = new Ticket();
        $chat = $ticket->lastChat($tickets_id);
        if($chat->group == 0){
            return "Support";
        }else{
            return "Client";
        }
    }

    public static function etatTicketTag($value){
        switch ($value){
            case 0: return '<span class="tag tag-info"><i class="fa fa-unlock"></i> Ouvert</span>';
            case 1: return '<span class="tag tag-default"><i class="fa fa-clock-o"></i> En attente de réponse client</span>';
            case 2: return '<span class="tag tag-warning"><i class="fa fa-clock-o"></i> En attente de réponse support</span>';
            case 3: return '<span class="tag tag-success"><i class="fa fa-check-circle"></i> Terminer</span>';
            default: return false;
        }
    }

    public static function etatTicket($value){
        switch ($value){
            case 0: return '<span class="blue-500"><i class="fa fa-unlock"></i> Ouvert</span>';
            case 1: return '<span class="grey-500"><i class="fa fa-clock-o"></i> En attente de réponse client</span>';
            case 2: return '<span class="orange-500"><i class="fa fa-clock-o"></i> En attente de réponse support</span>';
            case 3: return '<span class="green-500"><i class="fa fa-check-circle"></i> Terminer</span>';
            default: return false;
        }
    }


    public static function genNumTicket($id){
        return "TCK".date('Ymd').$id;
    }
}
