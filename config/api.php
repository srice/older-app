<?php
return [
    'api_key'       => env('API_KEY', 'api_key'),
    'api_comite_id' => env('API_COMITE_ID', ''),
    'api_endpoint'  => env('API_ENDPOINT', 'https://gestion.srice.dev/api')
];