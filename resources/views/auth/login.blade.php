<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <title>Connexion | SRICE</title>
    <link rel="apple-touch-icon" href="/assets/template/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="/assets/template/images/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="/assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="/assets/template/css/site.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="/assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="/assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="/assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="/assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="/assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="/assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="/assets/global/vendor/waves/waves.css">
    <link rel="stylesheet" href="/assets/template/examples/css/pages/login-v2.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="/assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="/assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <!--[if lt IE 9]>
    <script src="/assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="/assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="/assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="/assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition page-login-v2 layout-full page-dark">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Page -->
<div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content">
        <div class="page-brand-info">
            <div class="brand">
                <h2 class="brand-text font-size-40">SRICE</h2>
            </div>
            <p class="font-size-20">Suivez l'évolution de votre CE gràce à ce logiciel !</p>
        </div>
        <div class="page-login-main">
            <div class="brand hidden-md-up">
                <img class="brand-img" src="/assets/template/images/logo-blue@2x.png" alt="...">
                <h3 class="brand-text font-size-40">Remark</h3>
            </div>
            <div class="text-center">
                <img src="/assets/template/images/logo-blue@2x.png" class="img-responsive" width="130" alt="">
            </div>
            <h3 class="font-size-24">Se connecter</h3>
            <p>Veuillez saisir vos identifiants</p>
            {{ Form::open(["route" => "login"]) }}
            <div class="form-group form-material floating" data-plugin="formMaterial">
                {{ Form::email('email', 'test@srice.eu', ["class" => "form-control empty", "id" => "inputEmail"]) }}
                {{ Form::label('email', "Email", ["class" => "floating-label"]) }}
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="password" class="form-control empty" id="inputPassword" name="password" value="TJdw06o">
                <label class="floating-label" for="inputPassword">Mot de passe</label>
            </div>
            <div class="form-group clearfix">
                <div class="checkbox-custom checkbox-inline checkbox-primary pull-xs-left">
                    {{ Form::checkbox('remember', null, true) }}
                    {{ Form::label('remember', "Se souvenir de moi") }}
                </div>
                <a class="pull-xs-right" href="forgot-password.html">Mot de passe perdu ?</a>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Me connecter</button>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- End Page -->
<!-- Core  -->
<script src="/assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
<script src="/assets/global/vendor/jquery/jquery.js"></script>
<script src="/assets/global/vendor/tether/tether.js"></script>
<script src="/assets/global/vendor/bootstrap/bootstrap.js"></script>
<script src="/assets/global/vendor/animsition/animsition.js"></script>
<script src="/assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="/assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
<script src="/assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
<script src="/assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
<script src="/assets/global/vendor/waves/waves.js"></script>
<!-- Plugins -->
<script src="/assets/global/vendor/switchery/switchery.min.js"></script>
<script src="/assets/global/vendor/intro-js/intro.js"></script>
<script src="/assets/global/vendor/screenfull/screenfull.js"></script>
<script src="/assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="/assets/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<!-- Scripts -->
<script src="/assets/global/js/State.js"></script>
<script src="/assets/global/js/Component.js"></script>
<script src="/assets/global/js/Plugin.js"></script>
<script src="/assets/global/js/Base.js"></script>
<script src="/assets/global/js/Config.js"></script>
<script src="/assets/template/js/Section/Menubar.js"></script>
<script src="/assets/template/js/Section/GridMenu.js"></script>
<script src="/assets/template/js/Section/Sidebar.js"></script>
<script src="/assets/template/js/Section/PageAside.js"></script>
<script src="/assets/template/js/Plugin/menu.js"></script>
<script src="/assets/global/js/config/colors.js"></script>
<script src="/assets/template/js/config/tour.js"></script>
<script>
    Config.set('assets', '/assets/template/');
</script>
<!-- Page -->
<script src="/assets/template/js/Site.js"></script>
<script src="/assets/global/js/Plugin/asscrollable.js"></script>
<script src="/assets/global/js/Plugin/slidepanel.js"></script>
<script src="/assets/global/js/Plugin/switchery.js"></script>
<script src="/assets/global/js/Plugin/jquery-placeholder.js"></script>
<script src="/assets/global/js/Plugin/material.js"></script>
<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
</script>
</body>
</html>