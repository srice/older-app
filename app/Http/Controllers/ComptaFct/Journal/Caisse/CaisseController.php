<?php

namespace App\Http\Controllers\ComptaFct\Journal\Caisse;

use App\Model\ComptaFct\Etat\FctCompte;
use App\Model\ComptaFct\Journal\FctJournalCaisse;
use App\Packages\Srice\Espace;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class CaisseController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public function __construct(Espace $espace)
    {
        $this->sector = 'comptafct';
        $this->modules = $espace->listeModule();
        $this->configuration = [

        ];
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        $caisses = FctJournalCaisse::all()->load('compte');

        return view('ComptaFct.Journal.Caisse.index', compact('config', 'caisses'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "typeCaisse"    => "required",
            "dateCaisse"    => "required",
            "libelleCaisse" => "required|min:5",
            "montantCaisse" => "required"
        ]);

        //Mise en forme de la date
        $date = strtotime($request->dateCaisse);
        $dateCaisse = Carbon::createFromTimestamp($date);

        //Numéro de Mouvement Bancaire
        if(empty($request->numCaisse)){
            $numCaisse = CaisseOtherController::genNumMouvement();
        }else{
            $numCaisse = $request->numCaisse;
        }

        //Choix de recette ou de dépense
        if($request->typeCaisse == 0){
            $debitCaisse = $request->montantCaisse;
            $creditCaisse = 0;
        }else{
            $creditCaisse = $request->montantCaisse;
            $debitCaisse = 0;
        }

        $caisse = FctJournalCaisse::create([
            "numCaisse"     => $numCaisse,
            "dateCaisse"    => $dateCaisse,
            "numCompte"     => '530',
            "libelleCaisse" => $request->libelleCaisse,
            "debitCaisse"   => $debitCaisse,
            "creditCaisse"  => $creditCaisse
        ]);

        //Mise à jour des comptes
        if($request->typeCaisse == 0){
            $client = FctCompte::where('numCompte', '410DIV')->first();
            $cptCaisse = FctCompte::where('numCompte', '530')->first();

            $creditClient = $request->montantCaisse;
            $newCredit = $client->credit + $creditClient;
            $client->update(["credit" => $newCredit]);

            $debitCptCaisse = $request->montantCaisse;
            $newDebit = $cptCaisse->debit + $debitCptCaisse;
            $cptCaisse->update(["debit" => $newDebit]);
        }else{
            $four = FctCompte::where('numCompte', '401')->first();
            $cptCaisse = FctCompte::where('numCompte', '530')->first();

            $debitFour = $request->montantCaisse;
            $newDebit = $four->debit + $debitFour;
            $four->update(["debit" => $newDebit]);

            $creditCptCaisse = $request->montantCaisse;
            $newCredit = $cptCaisse->credit + $creditCptCaisse;
            $cptCaisse->update(["credit" => $newCredit]);
        }

        if($caisse && $cptCaisse){
            Toastr::success("Le mouvement de caisse à été ajouter");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de l'ajout du mouvement de caisse");
            return redirect()->back();
        }
    }

    public function delete($numCaisse){
        $caisse = FctJournalCaisse::where('numCaisse', $numCaisse)->first();

        if($caisse->debitCaisse > 0){
            $client = FctCompte::where('numCompte', '410DIV')->first();
            $cptCaisse = FctCompte::where('numCompte', $caisse->numCompte)->first();

            $creditClient = $client->credit;
            $newCredit = $creditClient - $caisse->debitCaisse;
            $client->update(["credit" => $newCredit]);

            $debitCptCaisse = $cptCaisse->debit;
            $newDebit = $debitCptCaisse - $caisse->debitCaisse;
            $cptCaisse->update(["debit" => $newDebit]);
        }else{
            $four = FctCompte::where('numCompte', '401')->first();
            $cptCaisse = FctCompte::where('numCompte', $caisse->numCompte)->first();

            $debitFour = $four->debit;
            $newDebit = $debitFour - $caisse->debitCaisse;
            $four->update(["debit" => $newDebit]);

            $creditCptCaisse = $cptCaisse->credit;
            $newCredit = $creditCptCaisse - $caisse->creditCaisse;
            $cptCaisse->update(["credit" => $newCredit]);
        }

        $caisse->delete();

        if($caisse && $cptCaisse){
            Toastr::success("Le mouvement de caisse à été supprimé");
            return redirect()->back();
        }else{
            Toastr::error("Erreur lors de la suppression du mouvement de caisse !");
            return redirect()->back();
        }
    }
}
class CaisseOtherController extends CaisseController{
    public static function genNumMouvement(){
        return "CAMOUV".date('Ym').rand(0, 999);
    }
}

