@extends('template')
@section("title")
    Edition d'une facture de remboursement (salarié)
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield('title')</h3>
        </header>
        <div class="panel-body">
            {{ Form::model($rembSalarie, ["route" => ["remb.salarie.update", $rembSalarie->id], "class" => "form-horizontal", "method" => "PUT"]) }}
            <div class="form-group row form-material">
                {{ Form::label('salaries_id', "Salarié", ["class" => "control-label col-md-3"]) }}
                <div class="col-md-6">
                    <select name="salaries_id" class="form-control" data-plugin="select2" data-placeholder="Selectionner un salarié dans la liste">
                        <option value="{{ $rembSalarie->salarie->id }}">{{ $rembSalarie->salarie->nom }} {{ $rembSalarie->salarie->prenom }}</option>
                        @foreach($salaries as $salary)
                            <option value="{{ $salary->id }}">{{ $salary->nom }} {{ $salary->prenom }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row form-material">
                {{ Form::label('dateRemb', 'Date du remboursement', ["class" => "control-label col-md-3"]) }}
                <div class="col-md-3">
                    {{ Form::text('dateRemb', $rembSalarie->dateRemb->format('d-m-Y'), ["class" => "form-control date"]) }}
                </div>
            </div>
            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="/assets/global/vendor/select2/select2.full.min.js"></script>
    <script src="/assets/global/js/Plugin/select2.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.fr.min.js"></script>
    <script src="/assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="/assets/custom/js/gestionasc/remboursement/salarie/create.js"></script>
@stop    