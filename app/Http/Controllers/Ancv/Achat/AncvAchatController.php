<?php

namespace App\Http\Controllers\Ancv\Achat;

use App\HelperClass\Ancv\Ancv;
use App\Packages\Srice\Espace;
use App\Repository\Ancv\AncvAchatRepository;
use App\Repository\Beneficiaire\Salarie\SalarieRepository;
use App\Repository\ComptaAsc\Etat\AscCompteRepository;
use App\Repository\ComptaAsc\Journal\AscJournalAchatRepository;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Kamaln7\Toastr\Facades\Toastr;

class AncvAchatController extends Controller
{
    public $sector;
    public $modules;
    public $configuration;
    public $auth;
    /**
     * @var Espace
     */
    private $espace;
    /**
     * @var AncvAchatRepository
     */
    private $ancvAchatRepository;
    /**
     * @var SalarieRepository
     */
    private $salarieRepository;
    /**
     * @var AscJournalAchatRepository
     */
    private $ascJournalAchatRepository;
    /**
     * @var AscCompteRepository
     */
    private $ascCompteRepository;

    /**
     * AncvAchatController constructor.
     * @param Espace $espace
     * @param Guard $auth
     * @param AncvAchatRepository $ancvAchatRepository
     * @param SalarieRepository $salarieRepository
     * @param AscJournalAchatRepository $ascJournalAchatRepository
     * @param AscCompteRepository $ascCompteRepository
     */
    public function __construct(
        Espace $espace,
        Guard $auth,
        AncvAchatRepository $ancvAchatRepository,
        SalarieRepository $salarieRepository,
        AscJournalAchatRepository $ascJournalAchatRepository,
        AscCompteRepository $ascCompteRepository
    )
    {
        $this->auth = $auth;
        $this->sector = 'ancv';
        $this->modules = $espace->listeModule();
        $this->espace = $espace;
        $this->ancvAchatRepository = $ancvAchatRepository;
        $this->salarieRepository = $salarieRepository;
        $this->ascJournalAchatRepository = $ascJournalAchatRepository;
        $this->ascCompteRepository = $ascCompteRepository;
    }

    public function index()
    {
        $config = (object) [
            "sector"    => $this->sector,
            "moduleMenu"=> $this->modules,
            "configuration" => $this->configuration,
            "parent"    => 0
        ];

        return view("Ancv.Achat.index", [
            "config" => $config,
            "achats" => $this->ancvAchatRepository->list()
        ]);
    }

    public function create()
    {
        $config = (object)[
            "sector" => $this->sector,
            "moduleMenu" => $this->modules,
            "configuration" => $this->configuration,
            "parent" => 1
        ];

        return view("Ancv.Achat.create", [
            "config" => $config,
            "salaries" => $this->salarieRepository->list(),
            "solutions" => Ancv::solutionArray()
        ]);
    }

    public function store(Request $request)
    {
        //dd($request->all());

        try {
            $ancv = $this->ancvAchatRepository->create(
                Ancv::getSolutionArray($request->solution_id),
                $request->total
            );


        } catch (\Exception $exception) {
            Log::error($exception->getMessage());

            Toastr::error("Erreur de la création de l'achat ! Contactez un administrateur", "Création de l'achat");
            return redirect()->back();
        }

        // Enregistrement dans le journal de vente
        try {
            $journal = $this->ascJournalAchatRepository->create(
                $ancv->id,
                $ancv->created_at,
                6011,
                "Achat de " . $request->solution_id,
                $request->total
            );

        } catch (\Exception $exception) {
            Log::error($exception->getMessage());

            Toastr::error("Erreur lors de l'inscription dans le journal de vente ! Contactez un administrateur", "Journal de vente");
            return redirect()->back();
        }

        // Import des informations de compte
        try {
            $compte = $this->ascCompteRepository->getCompte(6011);
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());

            Toastr::error("Erreur lors de la récupération du compte ! Contactez un administrateur", "GET Compte");
            return redirect()->back();
        }

        // Mise à jour du compte
        try {
            $debit = $compte->debit + $request->total;

            $this->ascCompteRepository->updateDebit(
                6011,
                $debit
            );

            Toastr::success("L'achat ANCV à été créer", "Succès");
            return redirect()->route('Ancv.Achat.index');
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());

            Toastr::error("Erreur lors de l'inscription en compte ! Contactez un administrateur", "Inscription en compte");
            return redirect()->back();
        }
    }


}
