@extends('template')
@section('title')
    Edition du fournisseur: <strong>{{ $fournisseur->name }}</strong>
    @parent
@stop
@section('header_styles')

@stop

@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield("title")</h3>
        </header>
        <div class="panel-body">
            {{ Form::model($fournisseur, ["route" => ["fournisseurs.update", $fournisseur->id], "method" => "PUT"]) }}
            <div class="form-group form-material">
                {{ Form::label('name', "Nom du fournisseur *", ["class" => "control-label"]) }}
                {{ Form::text('name', null, ["class" => "form-control"]) }}
            </div>

            <div class="text-xs-right">
                <button class="btn btn-success">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('footer_scripts')
    
@stop