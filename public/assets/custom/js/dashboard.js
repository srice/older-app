(function ($) {
    getWeather();
})(jQuery);

function getWeather(){
    var div = $("#weather");
    $.get('/weather')
        .done(function (data) {
            div.html(data);
        })
}