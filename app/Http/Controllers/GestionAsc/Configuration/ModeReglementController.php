<?php

namespace App\Http\Controllers\GestionAsc\Configuration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModeReglementController extends Controller
{
    public static function listeMode(){
        $modes = [
            [
                "id"    => 1,
                "name"  => "Chèque"
            ],
            [
                "id"    => 2,
                "name"  => "Espèce"
            ],
            [
                "id"    => 3,
                "name"  => "Carte Bancaire"
            ],
            [
                "id"    => 4,
                "name"  => "Virement bancaire"
            ],
            [
                "id"    => 5,
                "name"  => "Prélèvement Bancaire"
            ]
        ];
        return $modes;
    }

}
