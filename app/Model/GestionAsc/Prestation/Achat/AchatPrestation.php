<?php

namespace App\Model\GestionAsc\Prestation\Achat;

use Illuminate\Database\Eloquent\Model;

class AchatPrestation extends Model
{
    protected $guarded = [];

    public function achat(){
        return $this->belongsTo(AchatPrestation::class, 'achats_id');
    }
}
