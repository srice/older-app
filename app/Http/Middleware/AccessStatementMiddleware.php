<?php

namespace App\Http\Middleware;

use App\Packages\Srice\Espace;
use Closure;

class AccessStatementMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param null $espace
     * @return mixed
     * @internal param null $api
     */
    public function handle($request, Closure $next, $espace = null)
    {
        $espace = new Espace();
        $site = $espace->espace();

        if($site->state == 0){
            return response(view('errors.hl'), 403);
        }
        if($site->state == 1){
            return response(view('errors.maintenance'), 503);
        }
        if($site->state == 2){
            return $next($request);
        }
        if($site->state == 3){
            return $next($request);
        }
        if($site->state == 4){
            return response(view('errors.expire'), 403);
        }
    }
}
