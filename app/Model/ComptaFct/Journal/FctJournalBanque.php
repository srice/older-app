<?php

namespace App\Model\ComptaFct\Journal;

use App\Model\ComptaFct\Configuration\Plan\FctPlanCompte;
use Illuminate\Database\Eloquent\Model;

class FctJournalBanque extends Model
{
    protected $guarded = [];

    public function compte(){
        return $this->belongsTo(FctPlanCompte::class, 'numCompte', 'numCompte');
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateBanque"];
    }
}
