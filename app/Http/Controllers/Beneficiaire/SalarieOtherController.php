<?php

namespace App\Http\Controllers\Beneficiaire;

use App\Model\Beneficiaire\Ad\AyantDroit;
use App\Model\Beneficiaire\Salarie\Salarie;
use App\Model\ComptaAsc\Configuration\Plan\ComptaPlanCompte;
use App\Model\ComptaAsc\Etat\AscCompte;
use App\Model\GestionAsc\Billetterie\BilletSalarie;
use App\Model\GestionAsc\Remboursement\Salarie\RembSalarie;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Kamaln7\Toastr\Facades\Toastr;
use Maatwebsite\Excel\Facades\Excel;

class SalarieOtherController extends SalarieController
{

    // Route

    public function getVille($codePostal){
        $data = @json_decode(file_get_contents("https://vicopo.selfbuild.fr/cherche/".$codePostal));
        $coves = $data->cities;
        ob_start();
        foreach ($coves as $cove) {
            ?>
            <option value="<?= $cove->city; ?>"><?= $cove->city; ?></option>
            <?php
        }
        $content = ob_get_clean();
        return $content;
    }

    public function inactif($salaries_id){
        $salaries = Salarie::find($salaries_id);
        $ads = AyantDroit::where('salaries_id', $salaries_id)->get();
        $countAd = $ads->count();

        if($countAd != 0){
            foreach ($ads as $ad)
            {
                $ad->update(["active" => 0]);
            }
        }

        $update = $salaries->update(["active" => 0]);

        if($update){
            Toastr::success("Le salarié <strong>".$salaries->nom." ".$salaries->prenom."</strong> et ses ayant droits ont été désactivés !");
            return redirect()->back();
        }
    }

    public function actif($salaries_id){
        $salaries = Salarie::find($salaries_id);
        $ads = AyantDroit::where('salaries_id', $salaries_id)->get();
        $countAd = $ads->count();

        if($countAd != 0){
            foreach ($ads as $ad)
            {
                $ad->update(["active" => 1]);
            }
        }

        $update = $salaries->update(["active" => 1]);

        if($update){
            Toastr::success("Le salarié <strong>".$salaries->nom." ".$salaries->prenom."</strong> et ses ayant droits ont été activés !");
            return redirect()->back();
        }
    }

    public function massInactif(Request $request){

        foreach ($request->salaries as $salary){
            $up = Salarie::find($salary)->update(['active' => 0]);
            $count = AyantDroit::where('salaries_id', $salary)->get()->count();
            $ads = AyantDroit::where('salaries_id', $salary)->get();

            if($count != 0){
                foreach ($ads as $ad){
                    $upF = AyantDroit::find($ad->id)->update(["active" => 0]);
                }
            }
        }

        Toastr::success("La désactivation de masse à été exécuté !");
        return redirect()->back();
    }

    public function massActif(Request $request){

        foreach ($request->salaries as $salary){
            $up = Salarie::find($salary)->update(['active' => 1]);
            $count = AyantDroit::where('salaries_id', $salary)->get()->count();
            $ads = AyantDroit::where('salaries_id', $salary)->get();

            if($count != 0){
                foreach ($ads as $ad){
                    $upF = AyantDroit::find($ad->id)->update(["active" => 1]);
                }
            }
        }

        Toastr::success("L\'activation de masse à été exécuté !");
        return redirect()->back();
    }

    public function importCsv(Request $request){
        $file = Input::file("csv");
        Excel::load($file, function ($reader){
            $resultats = $reader->get()->all();
            $i = 0;
            foreach ($resultats as $resultat){
                $str = str_limit($resultat->nom, 3, '');
                $numCompte = "410".$str;

                $ben = Salarie::create([
                    "nom"           => $resultat->nom,
                    "prenom"        => $resultat->prenom,
                    "adresse"       => $resultat->adresse,
                    "codePostal"    => $resultat->code_postal,
                    "ville"         => $resultat->ville,
                    "tel"           => $resultat->telephone,
                    "email"         => $resultat->email,
                    "numCompte"     => $numCompte
                ]);

                $cpt = ComptaPlanCompte::create([
                    "numCompte"     => $numCompte,
                    "numSector"     => 41,
                    "nameCompte"    => $resultat->nom.' '.$resultat->prenom
                ]);

                $compte = AscCompte::create(["numCompte" => $numCompte]);

                if($ben && $cpt && $compte){
                    $i++;
                }
            }

            if($i > 0){
                Toastr::success("L\'import des salariés à été éxécuté");
            }else{
                Toastr::warning("Aucune ligne du fichier n'\a été importé");
            }
        });

        return redirect()->back();
    }

   // Static


    public static function getCaVenteChart($salaries_id){
        return [
            self::getValueByMonthBillet('01-01-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-02-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-03-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-04-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-05-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-06-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-07-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-08-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-09-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-10-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-11-'.date('Y'), $salaries_id),
            self::getValueByMonthBillet('01-12-'.date('Y'), $salaries_id),
        ];

    }

    public static function getCaRembChart($salaries_id){
        return [
            self::getValueByMonthRemb('01-01-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-02-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-03-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-04-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-05-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-06-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-07-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-08-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-09-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-10-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-11-'.date('Y'), $salaries_id),
            self::getValueByMonthRemb('01-12-'.date('Y'), $salaries_id),
        ];

    }

    private static function getValueByMonthBillet($dateStart, $salaries_id){
        $strt = strtotime($dateStart);
        $start = Carbon::createFromTimestamp($strt);
        $end = Carbon::createFromTimestamp($strt)->endOfMonth();

        $select = BilletSalarie::where('dateBillet', '>=', $start)
            ->where('dateBillet', '<=', $end)
            ->where('salaries_id', $salaries_id)
            ->get()->sum('totalBillet');

        return $select;
    }
    private static function getValueByMonthRemb($dateStart, $salaries_id){
        $strt = strtotime($dateStart);
        $start = Carbon::createFromTimestamp($strt);
        $end = Carbon::createFromTimestamp($strt)->endOfMonth();

        $select = RembSalarie::where('dateRemb', '>=', $start)
            ->where('dateRemb', '<=', $end)
            ->where('salaries_id', $salaries_id)
            ->get()->sum('totalRemb');

        return $select;
    }

    public static function countAdBySalarie($salaries_id){
        $count = AyantDroit::where('salaries_id', $salaries_id)->get()->count();
        return $count;
    }

    public static function getCaBilletBySalarie($salaries_id){
        $sum = BilletSalarie::where('salaries_id', $salaries_id)->get()->sum('totalBillet');
        return $sum;
    }

    public static function getCaRembBySalarie($salaries_id){
        $sum = RembSalarie::where('salaries_id', $salaries_id)->get()->sum('totalRemb');
        return $sum;
    }

    public static function AverageByFidelity(){
        $sum = Salarie::all()->average('fidelityPoint');
        return $sum;
    }

    public static function etatSalarie($value){
        switch ($value){
            case 0: return '<span class="tag bg-danger"><i class="fa fa-lock"></i> Inactif</span>';
            case 1: return '<span class="tag bg-green-500"><i class="fa fa-unlock"></i> Actif</span>';
        }
    }

    public static function SalarieIsActif($value){
        if($value == 1){
            return true;
        }else{
            return false;
        }
    }
}
