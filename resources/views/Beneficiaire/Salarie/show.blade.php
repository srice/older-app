@extends('template')
@section('title')
    Fiche du salarié <strong>{{$salarie->nom}} {{$salarie->prenom}}</strong>
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="/assets/template/examples/css/pages/profile.css">
@stop

@section("content")
    <div class="row">
        <div class="col-md-4">
            <div class="card card-shadow text-xs-center">
                <div class="card-block">
                    <a class="avatar avatar-lg img-bordered @if(\App\Http\Controllers\Beneficiaire\SalarieOtherController::SalarieIsActif($salarie->active) == true) bg-green-500 @else bg-red-500 @endif" href="javascript:void(0)">
                        @if($salarie->avatar == 0)
                            <img src="{{ \Creativeorange\Gravatar\Facades\Gravatar::get($salarie->email, 'large') }}" alt="...">
                        @else
                            <img src="/assets/custom/images/beneficiaire/salarie/{{ $salarie->id }}.png" alt="...">
                        @endif
                    </a>
                    <h4 class="profile-user">{{ $salarie->nom }} {{ $salarie->prenom }}</h4>
                    @if(!empty($salarie->matricule))
                        <p class="profile-job"><strong>Matricule: </strong>{{ $salarie->matricule }}</p>
                    @endif
                    <table class="table">
                        <tbody>
                            @if(!empty($salarie->adresse))
                                <tr>
                                    <td class="font-weight-bold">Adresse</td>
                                    <td>
                                        {{ $salarie->adresse }}<br>
                                        {{ $salarie->codePostal }} {{ $salarie->ville }}
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td class="font-weight-bold">Coordonnées</td>
                                <td>
                                    @if(!empty($salarie->tel))
                                        <strong><i class="md-phone"></i>: </strong>{{ $salarie->tel }}<br>
                                    @endif
                                    @if(!empty($salarie->port))
                                        <strong><i class="md-smartphone"></i>: </strong>{{ $salarie->port }}<br>
                                    @endif
                                    @if(!empty($salarie->email))
                                        <strong><i class="md-email"></i>: </strong>{{ $salarie->email }}<br>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="row no-space">
                        <div class="col-xs-4">
                            <strong class="profile-stat-count">{{ \App\Http\Controllers\Beneficiaire\SalarieOtherController::countAdBySalarie($salarie->id) }}</strong>
                            <span>Ayants Droit</span>
                        </div>
                        <div class="col-xs-4">
                            <strong class="profile-stat-count">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\Beneficiaire\SalarieOtherController::getCaBilletBySalarie($salarie->id)) }}</strong>
                            <span>d'achats</span>
                        </div>
                        <div class="col-xs-4">
                            <strong class="profile-stat-count">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\Beneficiaire\SalarieOtherController::getCaRembBySalarie($salarie->id)) }}</strong>
                            <span>de Remboursement</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel">
                <div class="panel-body">
                    {!! $chart->render() !!}
                </div>
            </div>
            <div class="panel">
                <header class="panel-heading">
                    <div class="nav-tabs-horizontal" data-plugin="tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" data-toggle="tab" href="#ad" aria-controls="exampleTabsFour" role="tab">Ayants Droit</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#billetterie" aria-controls="exampleTabsFour" role="tab">Ventes de billetterie</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" data-toggle="tab" href="#remboursement" aria-controls="exampleTabsFour" role="tab">Remboursement</a>
                            </li>
                            @if($config->moduleMenu[12]->state == 1)
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" data-toggle="tab" href="#fidelity" aria-controls="exampleTabsFour" role="tab">Fidélité</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="tab-content p-t-20">
                        <div class="tab-pane active" id="ad" role="tabpanel">
                            <div class="row p-b-20">
                                <div class="col-md-10">&nbsp;</div>
                                <div class="col-md-2">
                                    <a href="{{ route('ad.create', $salarie->id) }}" class="btn btn-round btn-primary"><i class="md-plus-circle"></i> Nouvel ayant droit</a>
                                </div>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Identité</th>
                                        <th>Information usuel</th>
                                        <th>Etat</th>
                                        <th class="text-center"><i class="fa fa-cog"></i> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($salarie->ad as $ad)
                                    <tr>
                                        <td>{{ $ad->nom }} {{ $ad->prenom }}</td>
                                        <td>{{ $ad->dateNaissance }} <span class="tag tag-info">{{ $ad->dateNaissance->diffForHumans() }}</span> </td>
                                        <td>
                                            {!! \App\Http\Controllers\Beneficiaire\AdOtherController::AdIsActif($ad->active) !!}<br>
                                            @if($ad->active == 1)
                                                <a href="{{ route('ad.inactif', [$salarie->id, $ad->id]) }}" class="btn btn-xs btn-danger"><i class="fa fa-lock"></i> Désactiver l'ayant droit</a>
                                            @else
                                                <a href="{{ route('ad.actif', [$salarie->id, $ad->id]) }}" class="btn btn-xs btn-success"><i class="fa fa-unlock"></i> Activer l'ayant droit</a>
                                            @endif
                                        </td>
                                        <td>
                                            {{ Form::model($ad, ["route" => ["ad.delete", $salarie->id, $ad->id], "method" => "DELETE"]) }}
                                            <a href="{{ route('ad.edit', [$salarie->id, $ad->id]) }}" class="btn btn-primary btn-icon"><i class="md-edit"></i> </a>
                                            <button class="btn btn-danger btn-icon"><i class="md-delete"></i> </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="billetterie" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Numéro de la facture</th>
                                            <th>Date de la facture</th>
                                            <th>Montant de la facture</th>
                                            <th>Etat de la facture</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($billets as $billet)
                                        <tr>
                                            <td>{{ $billet->numBilletSalarie }}</td>
                                            <td>{{ $billet->dateBillet->format('d/m/Y') }}</td>
                                            <td>{{ \App\Http\Controllers\OtherController::euro($billet->totalBillet) }}</td>
                                            <td>{!! \App\Http\Controllers\GestionAsc\Billetterie\BilletterieController::etatBilletLabel($billet->etatBillet) !!}</td>
                                            <td>
                                                <a href="{{ route('billet.salarie.show', $billet->id) }}" class="btn btn-sm btn-icon btn-default"><i class="fa fa-eye"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="remboursement" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Numéro de la facture</th>
                                            <th>Date de la facture</th>
                                            <th>Montant du remboursement</th>
                                            <th>Etat de la facture</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($rembs as $remb)
                                        <tr>
                                            <td>{{ $remb->numRembSalarie }}</td>
                                            <td>{{ $remb->dateRemb->format('d/m/Y') }}</td>
                                            <td>{{ \App\Http\Controllers\OtherController::euro($remb->totalRemb) }}</td>
                                            <td>{!! \App\Http\Controllers\GestionAsc\Remboursement\RemboursementController::etatRembLabel($remb->etatRemb) !!}</td>
                                            <td>
                                                <a href="{{ route('remb.salarie.show', $remb->id) }}" class="btn btn-sm btn-icon btn-default"><i class="fa fa-eye"></i> </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if($config->moduleMenu[12]->state == 1)
                        <div class="tab-pane" id="fidelity" role="tabpanel">
                            <div class="panel">
                                <header class="panel-heading bg-blue-500 white">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="panel-title white">Point de fidélité du salarié</h3>
                                        </div>
                                        <div class="col-md-4" style="font-size: 15px;">
                                            <div style="padding-top: 20px;">
                                                <strong>Point de fidélité actuel:</strong> {{ $salarie->fidelityPoint }}
                                            </div>
                                        </div>
                                    </div>
                                </header>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Numéro de Transaction</th>
                                                    <th>Opération</th>
                                                    <th>Nb point</th>
                                                    <th>Solde</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($salarie->fidelities as $fidelity)
                                                <tr>
                                                    <td>{{ $fidelity->created_at->format('d/m/Y') }}</td>
                                                    <td>{{ $fidelity->numTransaction }}</td>
                                                    <td>{{ $fidelity->operation }}</td>
                                                    <td>{{ $fidelity->point }}</td>
                                                    <td>{{ $fidelity->solde }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('footer_scripts')
    <script src="/assets/custom/js/beneficiaire/salarie/show.js"></script>
    <script src="/assets/global/js/Plugin/responsive-tabs.js"></script>
    <script src="/assets/global/js/Plugin/closeable-tabs.js"></script>
    <script src="/assets/global/js/Plugin/tabs.js"></script>
@stop