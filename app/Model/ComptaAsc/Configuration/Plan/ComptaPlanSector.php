<?php

namespace App\Model\ComptaAsc\Configuration\Plan;

use Illuminate\Database\Eloquent\Model;

class ComptaPlanSector extends Model
{
    protected $guarded = [];

    public function classe(){
        return $this->belongsTo(ComptaPlanClasse::class, 'numClasse');
    }

    public function comptes(){
        return $this->hasMany(ComptaPlanCompte::class, 'numSector');
    }
}
