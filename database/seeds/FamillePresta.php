<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FamillePresta extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("APP_ENV") == 'local'){
            DB::table('familles')->insert([
                "name" => "Concert"
            ]);
            DB::table('familles')->insert([
                "name" => "Sport"
            ]);
            DB::table('familles')->insert([
                "name" => "Théâtre"
            ]);
            DB::table('familles')->insert([
                "name" => "Exposition/Foire"
            ]);
            DB::table('familles')->insert([
                "name" => "Parcs"
            ]);
            DB::table('familles')->insert([
                "name" => "Salons"
            ]);
            DB::table('familles')->insert([
                "name" => "Festivals"
            ]);
            DB::table('familles')->insert([
                "name" => "Autres"
            ]);
        }

        if(env("APP_ENV") == 'test'){
            DB::table('familles')->insert([
                "name" => "Concert"
            ]);
            DB::table('familles')->insert([
                "name" => "Sport"
            ]);
            DB::table('familles')->insert([
                "name" => "Théâtre"
            ]);
            DB::table('familles')->insert([
                "name" => "Exposition/Foire"
            ]);
            DB::table('familles')->insert([
                "name" => "Parcs"
            ]);
            DB::table('familles')->insert([
                "name" => "Salons"
            ]);
            DB::table('familles')->insert([
                "name" => "Festivals"
            ]);
            DB::table('familles')->insert([
                "name" => "Autres"
            ]);
        }

        if(env("APP_ENV") == 'prod'){
            DB::table('familles')->insert([
                "name" => "Concert"
            ]);
            DB::table('familles')->insert([
                "name" => "Sport"
            ]);
            DB::table('familles')->insert([
                "name" => "Théâtre"
            ]);
            DB::table('familles')->insert([
                "name" => "Exposition/Foire"
            ]);
            DB::table('familles')->insert([
                "name" => "Parcs"
            ]);
            DB::table('familles')->insert([
                "name" => "Salons"
            ]);
            DB::table('familles')->insert([
                "name" => "Festivals"
            ]);
            DB::table('familles')->insert([
                "name" => "Autres"
            ]);
        }
    }
}
