@extends('template')
@section("title")
    Position de la caisse
    @parent
@stop
@section("header_styles")
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield('title')</h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="position" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Désignation</th>
                            <th>Type</th>
                            <th>Débit</th>
                            <th>Crédit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($salaries as $salary)
                            <tr>
                                <td>{{ $salary->dateReglement->format('d/m/Y') }}</td>
                                <td>Règlement N°<strong>{{ $salary->numReglementBilletSalarie }}</strong> par <strong>{{ \App\Http\Controllers\GestionAsc\Billetterie\Salarie\BilletReglementController::modeReglement($salary->modeReglement) }}</strong> par <strong>{{ $salary->depositaireReglement }}</strong></td>
                                <td>Règlement</td>
                                <td></td>
                                <td>{{ \App\Http\Controllers\OtherController::euro($salary->totalReglement) }}</td>
                            </tr>
                        @endforeach
                        @foreach($ads as $ad)
                            <tr>
                                <td>{{ $ad->dateReglement->format('d/m/Y') }}</td>
                                <td>Règlement N°<strong>{{ $ad->numReglementBilletAd }}</strong> par <strong>{{ \App\Http\Controllers\GestionAsc\Billetterie\Salarie\BilletReglementController::modeReglement($ad->modeReglement) }}</strong> par <strong>{{ $ad->depositaireReglement }}</strong></td>
                                <td>Règlement</td>
                                <td></td>
                                <td>{{ \App\Http\Controllers\OtherController::euro($ad->totalReglement) }}</td>
                            </tr>
                        @endforeach
                        @foreach($remises as $remise)
                        <tr>
                            <td>{{ $remise->dateRemise->format('d/m/Y') }}</td>
                            <td>Remise en banque <strong>({{ \App\Http\Controllers\ComptaAsc\Remise\Position\PositionOtherController::getNameType($remise->typeRemise) }})</strong> N° <strong>{{ $remise->numRemise }}</strong></td>
                            <td>Remise en Banque</td>
                            <td>{{ \App\Http\Controllers\OtherController::euro($remise->totalRemise) }}</td>
                            <td></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="text-xs-right" style="font-weight: bold;" colspan="3">Solde Disponible</td>
                            <td class="text-xs-right" style="font-weight: bold;" colspan="2">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaAsc\Remise\Position\PositionOtherController::sumReglement()) }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="/assets/custom/js/comptaasc/remise/position/index.js"></script>
@stop    