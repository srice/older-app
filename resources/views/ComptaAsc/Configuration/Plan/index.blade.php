@extends('template')
@section("title")
    Plan Comptable
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">CLASSES</h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Numéro</th>
                            <th>Désignation</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($classes as $class)
                        <tr>
                            <td>{{ $class->numClasse }}</td>
                            <td>{{ $class->nameClasse }}</td>
                            <td>
                                <a href="{{ route('comptaAsc.config.plan.sector.index', $class->numClasse) }}" class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" data-placement="top" data-title="Voir les secteurs"><i class="fa fa-eye"></i> </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    