<?php

namespace App\Model\GestionAsc\Remboursement\Salarie;

use App\Model\Beneficiaire\Salarie\Salarie;
use Illuminate\Database\Eloquent\Model;

class RembSalarie extends Model
{
    protected $guarded = [];

    public function salarie(){
        return $this->belongsTo(Salarie::class, "salaries_id");
    }

    public function prestations(){
        return $this->hasMany(RembPrestaSalarie::class, "remb_salarie_id");
    }

    public function reglements(){
        return $this->hasMany(RembRegSalarie::class, "remb_salarie_id");
    }

    public function getDates()
    {
        return ["created_at", "updated_at", "dateRemb"];
    }
}
