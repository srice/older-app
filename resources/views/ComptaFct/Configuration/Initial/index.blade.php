@extends('template')
@section("title")
    Bilan Initial
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">@yield('title')</h3>
            <div class="panel-actions">
                {{ Form::open(["route" => "comptaFct.config.initial.validation"]) }}
                <button class="btn btn-success"><i class="fa fa-check"></i> Valider le bilan Initial</button>
                {{ Form::close() }}
            </div>        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Compte</th>
                            <th>Débit</th>
                            <th>Crédit</th>
                            <th><i class="fa fa-bars"></i> </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($comptes as $compte)
                        <tr>
                            <td>{{ $compte->numCompte }} - {{ \App\Http\Controllers\ComptaFct\Configuration\Initial\InitialOtherController::getNameCompte($compte->numCompte) }}</td>
                            <td style="text-align: right;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Configuration\Initial\InitialOtherController::soldeDebit($compte->numCompte)) }}</td>
                            <td style="text-align: right;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Configuration\Initial\InitialOtherController::soldeCredit($compte->numCompte)) }}</td>
                            <td>
                                <a href="{{ route('comptaFct.config.initial.edit', $compte->numCompte) }}" class="btn btn-icon btn-sm btn-primary"><i class="fa fa-edit"></i> </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style="font-weight: bold; font-size: 20px; text-align: right;">Total Bilan Initial</td>
                            <td style="font-weight: bold; font-size: 20px; text-align: right;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Configuration\Initial\InitialOtherController::getTotalDebit()) }}</td>
                            <td style="font-weight: bold; font-size: 20px; text-align: right;">{{ \App\Http\Controllers\OtherController::euro(\App\Http\Controllers\ComptaFct\Configuration\Initial\InitialOtherController::getTotalCredit()) }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    