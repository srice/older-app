<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturationTiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturation_tiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('typeTier')->comment("0: Client |1: Fournisseur");
            $table->string('codeCompta');
            $table->string('numTier');
            $table->string("nomTier");
            $table->string('adresseTier');
            $table->string('codePostalTier', 5);
            $table->string('villeTier');
            $table->string('soldeTier')->default(0)->comment("Solde de facturation Tiers");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturation_tiers');
    }
}
